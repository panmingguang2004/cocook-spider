package cn.datawin;

/**
 * 
 * @author Administrator
 *
 */
public interface Builder {
	
	<T> T build(Class<T> _class);
	
	<T> void  destory(Class<T> _class);
	
	<T> T getCache(Class<T> _class);

}
