package cn.datawin;

public interface Client {
	
	void finish();
	
	boolean isDone();
	
	void setParam(String key, String value);
	
	Object getParam(String key);
	
	Object removeParams(String key);

	public String getClientid();
}
