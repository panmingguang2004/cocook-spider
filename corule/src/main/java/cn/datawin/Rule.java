package cn.datawin;

import java.io.Serializable;

public interface Rule {
	
	abstract void setClient(Client client);
	
	abstract void setService(Service service);
	
	abstract Service getService();
	
	abstract void setSerialize(Serializable obj);
	
	abstract Serializable getSerialize();
	
	abstract <T> void setObject(T t);
	
	abstract <T> T getObject(Class<T> _class);
	
	abstract void next(String ruleName);

	abstract void do_(String ruleName);
	
	abstract void first();


}
