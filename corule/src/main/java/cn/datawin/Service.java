package cn.datawin;

public interface Service {
	
	void finish();
	
	boolean isDone();
	
	void setParam(String key, String value);
	
	Object getParam(String key);
	
	Object removeParams(String key);

	Client getClient();
	
	void exec();
	
	

}
