package cn.datawin.crule;

import java.util.HashMap;
import java.util.Map;

import cn.datawin.Client;

public abstract class CoClient implements Client {

	private String clientid;

	private Map<String, Object> params = new HashMap<String, Object>();

	private int state;

	@Override
	public boolean isDone() {
		return state == 1;
	}

	@Override
	public void finish() {
		this.state = 1;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	@Override
	public Object getParam(String key) {
		return params.get(key);
	}

	@Override
	public Object removeParams(String key) {
		return params.remove(key);
	}

	@Override
	public void setParam(String key, String value) {
		params.put(key, value);
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	@Override
	public String getClientid() {
		return clientid;
	}
	
}
