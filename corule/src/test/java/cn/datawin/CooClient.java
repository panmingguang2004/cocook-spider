package cn.datawin;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;

import cn.datawin.crule.CoClient;
import cn.datawin.spider.httputil.HttpClientService;
import cn.datawin.spider.httputil.HttpClientService_2_1;
import cn.datawin.spider.httputil.HttpService;

public class CooClient extends CoClient{
	
	private HttpService httpService = new HttpClientService_2_1(new ThreadSafeClientConnManager());
	
	private Map<String, Service> services = new HashMap<String, Service>();

	
	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}


	public HttpService getHttpService() {
		return httpService;
	}
	
	public void setService(String key, Service service) {
		this.services.put(key, service);
	}

	public Service getService(String key) {
		return this.services.get(key);
	}

	public Map<String, Service> getServices() {
		return services;
	}

	public void start(Class<? extends Rule> _class){
		new CooService(this,_class).doo();
	}

}
