package cn.datawin;

import cn.datawin.crule.CoService;
import cn.datawin.crule.ServiceBuilder;

public class CooService extends CoService{
	
	private Rule rule ;
	
	Client client;
	
	@Override
	public Client getClient() {
		return client;
	}
	
	public CooService(Client client, Class<? extends Rule> _class) {
		this.client  = client;
		rule = ServiceBuilder.getInstance().build(_class);
		rule.setClient(client);
		rule.setService(this);
	}
	
	/**
	 * 以前通过 每个返回 的taskid 来保持service
	 */
	public void doo(){
		rule.first();
		String taskid = "123456";
		((CooClient)getClient()).setService(taskid, this);
	}
	
	
	public void dooResult(Result result){
		rule.setObject(result);
		rule.do_(result.getMap("sig") + "Result");
	}

	/**
	 * 做结束 判断 和其他的 统一的业务操作
	 */
	@Override
	public void exec() {
		if(isDone()) return;
//		Task task = rule.getObject(Task.class); //缓存的对象, 用于外部的 业务逻辑操作 
//		if(client != null){
//			String cid = (client).getClientid();
//			task.setClientid(cid);
//		}
//		insert(task);
//		(client).setService(task.get_id(), this);
	
		
	}
	
	

}
