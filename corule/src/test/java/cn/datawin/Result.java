package cn.datawin;

import java.util.HashMap;
import java.util.Map;

public class Result {

	private Map<String, Object> map = new HashMap<String, Object>();

	public void setMap(String key, Object value) {
		map.put(key, value);
	}

	public Object getMap(String key) {
		return map.get(key);
	}
	
	

}
