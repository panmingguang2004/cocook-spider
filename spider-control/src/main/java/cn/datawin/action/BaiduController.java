package cn.datawin.action;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.googlecode.jsonplugin.JSONException;
import com.mongodb.DBObject;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.BaiduService;
import cn.datawin.service.RuleService;
import cn.datawin.service.TaskService;

@Controller
public class BaiduController extends BaseController{
	@Resource
	BaiduService baiduService;
	@Resource
	TaskService taskService;
	@Resource
	RuleService ruleService;
	
	
	@RequestMapping
	public void baiduList(HttpServletResponse res, Form form,ReqData reqData,HttpServletRequest req) throws IOException, JSONException{
		printJSON(res, baiduService.getBaidu(form,reqData,req.getParameter("type")));
	}
	
	/**
	 *  URL:	http://115.231.92.107/spider/baidu/copytask
	 *  method
		参数:
			{type:"",    //全吧--quanba	,知道--zhidao,百科--baike,搜吧--souba,吧内--banei		
			key:"关键字",
			name:"吧名"}
	 * @param res
	 * @param req
	 * @throws IOException
	 * @throws JSONException
	 */
	@RequestMapping
	public void copytask(HttpServletResponse res, HttpServletRequest req) throws IOException, JSONException{
		List<DBObject> task = taskService.getTaskByWork(req.getParameter("type"));
		DBObject newTask = task.get(0);
		baiduService.copyBaidu(req, newTask);
		printJSON(res, 1);
		
	}
	
	
	
	@RequestMapping
	public void copytaskhy(HttpServletResponse res, HttpServletRequest req) throws IOException, JSONException{
		List<DBObject> task = taskService.getTaskByWork(req.getParameter("type"));
		DBObject newTask = task.get(0);
		baiduService.copyBaiduhy(req, newTask);
		printJSON(res, 1);
		
	}
	
	@RequestMapping
	public void copybaidusearch(HttpServletResponse res, HttpServletRequest req) throws IOException, JSONException{
		String type1=req.getParameter("type1");
		List<DBObject> task = taskService.getTaskByWork(type1);
		DBObject newTask = task.get(0);
		
		String type2=req.getParameter("type2");
		List<DBObject> task2 = taskService.getTaskByWork(type2);
		DBObject newTask2 = task2.get(0);
		
		if(type1.equals("imp_baiduSearch")){
			baiduService.copyBaiduSearch(req, newTask);
		}else{
			baiduService.copyWeixinSearch(req, newTask);
		}
		
		if(type2.equals("imp_baiduSearch")){
			baiduService.copyBaiduSearch(req, newTask2);
		}else{
			baiduService.copyWeixinSearch(req, newTask2);
		}
		
		printJSON(res, 1);
		
	}

}
