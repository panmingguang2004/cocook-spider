package cn.datawin.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.googlecode.jsonplugin.JSONException;
import com.mongodb.DBObject;

import cn.datawin.bean.Form;
import cn.datawin.bean.RuleBase;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.CityRuleBaseService;
import cn.datawin.util.HttpUtil;

@Controller
public class CityRuleController extends BaseController{
	
	@Resource
	CityRuleBaseService cityruleBaseService;
	
	
	@RequestMapping
	public void insertRuleBase(RuleBase ruleBase, HttpServletResponse res){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rulename", ruleBase.getRulename());
		map.put("type", ruleBase.getType());
		map.put("expression", ruleBase.getExpression());
		map.put("pageRule", ruleBase.getPageRule());
		map.put("name", ruleBase.getName());
		map.put("pageSize", ruleBase.getPageSize());
		map.put("url", ruleBase.getUrl());
		map.put("belone", ruleBase.getBelone());
		cityruleBaseService.insertRuleBase(map);
		HashMap<String, String> rmap = new HashMap<String, String>();
		rmap.put("status", "success");
		printJSON(res, rmap);
	}
	
	@RequestMapping
	public void updateRuleBase(RuleBase ruleBase, HttpServletResponse res,String rulebaseid){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rulename", ruleBase.getRulename());
		map.put("type", ruleBase.getType());
		map.put("expression", ruleBase.getExpression());
		map.put("pageRule", ruleBase.getPageRule());
		map.put("name", ruleBase.getName());
		map.put("pageSize", ruleBase.getPageSize());
		map.put("url", ruleBase.getUrl());
		map.put("belone", ruleBase.getBelone());
		cityruleBaseService.updateRuleBase(map,rulebaseid);
		HashMap<String, String> rmap = new HashMap<String, String>();
		rmap.put("status", "success");
		printJSON(res, rmap);
	}
	
	@RequestMapping
	public void deleteRuleBase(HttpServletResponse res,String rulebaseid){
		cityruleBaseService.deleteRuleBase(rulebaseid);
		HashMap<String, String> rmap = new HashMap<String, String>();
		rmap.put("status", "success");
		printJSON(res, rmap);
	}
	
	@RequestMapping
	public void ruleList(HttpServletResponse res,ReqData reqData, Form form,String rulename) throws IOException, JSONException{
		printJSON(res, cityruleBaseService.getRuleBase(reqData,form,rulename));
	}
	
	
	@RequestMapping
	public String ruleTestDB(String id, Model model) throws Exception{
		String ids[] = id.split(",");
		List<HashMap<String, Object>> paramss = new ArrayList<HashMap<String, Object>>();
		List<String> titles = new ArrayList<String>();
		for (int i = 0; i < ids.length; i++) {
			DBObject rule = cityruleBaseService.findRuleBase(ids[i]);
			if(rule.get("type").toString().equals("DataProcessor")){
				String reshtml = HttpUtil.getStr(rule.get("url").toString(), "utf-8");
				List<Map<String, String>> expressions = (List<Map<String, String>>) HttpUtil.deserialize(rule.get("expression").toString());
				HashMap<String, Object> params = cityruleBaseService.ruleTest(expressions,reshtml);
				String type=rule.get("rulename").toString();
				titles.add(type);
				if(type.contains("58")){
					cityruleBaseService.addcityDB(params,rule.get("belone").toString(),"58");
				}
				paramss.add(params);
			}
			
		}
		
		model.addAttribute("params", paramss);
		model.addAttribute("titles", titles);
		return "ruleBase/testcityrule";
	
	}
}
