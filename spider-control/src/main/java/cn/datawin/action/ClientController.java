package cn.datawin.action;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.ClientService;

import com.googlecode.jsonplugin.JSONException;

@Controller
public class ClientController extends BaseController{

	@Resource
	ClientService clientService;
	
	
	@RequestMapping
	public void list(HttpServletResponse res, ReqData reData ,String name) throws IOException, JSONException{
		printJSON(res, clientService.list(reData, name));
	}
	
	
	@RequestMapping
	public void add(HttpServletResponse res, ReqData reData ,Form form) throws IOException, JSONException{
		printHTML(res, clientService.add(form));
	}
	
	@RequestMapping
	public void edit(HttpServletResponse res, Form form) throws IOException, JSONException{
		printHTML(res, clientService.edit(form));
	}
	
	@RequestMapping
	public void update(HttpServletResponse res,String workbeach,String count) throws IOException, JSONException{
		printHTML(res, clientService.updatec(workbeach,count));
	}
	
	@RequestMapping
	public void del(HttpServletResponse res, String ids) throws IOException, JSONException{
		printHTML(res, clientService.del(ids));
	}
	
}
