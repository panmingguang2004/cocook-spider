package cn.datawin.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.CommService;

@Controller
public class CommController extends BaseController{
	
	@Resource
	CommService commService;
	
	/* 通用跳转 */
	@RequestMapping
	public String f(Model model,String t, String p) {
		model.addAttribute("t", t);
		return p;
	}
	
	@RequestMapping
	public String taskList(HttpServletResponse resp, Form form, ReqData reqData) {
		printJSON(resp, commService.taskList(form, reqData));
		return null;
	}
	
//	@RequestMapping
//	public String remove(HttpServletResponse resp, Form form) {
//		printHTML(resp, commService.remove(form));
//		return null;
//	}
	
}
