package cn.datawin.action;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.datawin.bean.page.ReqData;
import cn.datawin.service.DataService;

@Controller
public class DataController extends BaseController {
	@Resource
	DataService dataService;
	@RequestMapping
	public void getdata(String col,HttpServletResponse res,ReqData reqData){
		if(col==null||col.equals("")){
			col=dataService.getDataNames().get(0);
		}
		printJSON(res, dataService.getTestdata(col,reqData));
	}
	@RequestMapping
	public void getDataNames(HttpServletResponse res){
		HashMap<String, List<String>> map = new HashMap<String, List<String>>();
		map.put("names", dataService.getDataNames());
		printJSON(res, map);
		
	}
	
	@RequestMapping
	public void geturls(HttpServletResponse res,String url){
		printJSON(res,dataService.findurls(url));
	}
	
}
