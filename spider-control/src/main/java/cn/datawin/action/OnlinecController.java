package cn.datawin.action;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.ClientService;
import cn.datawin.service.OnlinecService;

import com.googlecode.jsonplugin.JSONException;

@Controller
public class OnlinecController extends BaseController{

	@Resource
	OnlinecService onclientService;
	
	
	@RequestMapping
	public void list(HttpServletResponse res, ReqData reData ,String acc) throws IOException, JSONException{
		printJSON(res, onclientService.list(reData, acc));
	}
	
	@RequestMapping
	public void liston(HttpServletResponse res, ReqData reData ,String type) throws IOException, JSONException{
		printJSON(res, onclientService.liston(reData, type));
	}
	
	@RequestMapping
	public void add(HttpServletResponse res, ReqData reData ,Form form) throws IOException, JSONException{
		printHTML(res, onclientService.add(form));
	}
	
	@RequestMapping
	public void edit(HttpServletResponse res, Form form) throws IOException, JSONException{
		printHTML(res, onclientService.edit(form));
	}

	@RequestMapping
	public void del(HttpServletResponse res, String ids) throws IOException, JSONException{
		printHTML(res, onclientService.del(ids));
	}
	
	@RequestMapping
	public void allotclient(HttpServletResponse res, String oncid,String tids) throws IOException, JSONException{
		printHTML(res, onclientService.allotclient(oncid,tids));
	}
	
}
