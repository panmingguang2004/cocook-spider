package cn.datawin.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.googlecode.jsonplugin.JSONException;
import com.googlecode.jsonplugin.JSONUtil;
import com.mongodb.DBObject;

import cn.datawin.bean.Form;
import cn.datawin.bean.RuleBase;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.RuleBaseService;
import cn.datawin.service.RuleService;
import cn.datawin.util.HttpUtil;

@Controller
public class RuleController extends BaseController{
	@Resource
	RuleService ruleService;
	@Resource
	RuleBaseService ruleBaseService;
	
	@RequestMapping
	public void getRule(String taskid, String pid, HttpServletResponse res){
		String rule = ruleService.findRule(taskid,pid);
		printJSON(res, rule);
	}
	
	@RequestMapping
	public void insertRuleBase(RuleBase ruleBase, HttpServletResponse res){
		if (ruleBase.getPageNum()==0) {
			ruleBase.setPageNum(1);
		}
		ruleBaseService.insertRuleBase(ruleBase);
		HashMap<String, String> rmap = new HashMap<String, String>();
		rmap.put("status", "success");
		printJSON(res, rmap);
	}
	
	@RequestMapping
	public void updateRuleBase(RuleBase ruleBase, String id, HttpServletResponse res){
		if (ruleBase.getPageNum()==0) {
			ruleBase.setPageNum(1);
		}
		ruleBase.set_id(id);
		ruleBaseService.updateRuleBase(ruleBase);
		HashMap<String, String> rmap = new HashMap<String, String>();
		rmap.put("status", "success");
		printJSON(res, rmap);
	}
	
	@RequestMapping
	public void deleteRuleBase(HttpServletResponse res,String rulebaseid){
		String ids[] = rulebaseid.split(",");
		for (int i = 0; i < ids.length; i++) {
			ruleBaseService.deleteRuleBase(ids[i]);
		}
		
		HashMap<String, String> rmap = new HashMap<String, String>();
		rmap.put("status", "success");
		printJSON(res, rmap);
	}
	
	@RequestMapping
	public void ruleList(HttpServletResponse res, Form form,ReqData reqData) throws IOException, JSONException{
		printJSON(res, ruleBaseService.getRuleBase(form,reqData));
	}
	
	@RequestMapping
	public void getruletree(HttpServletResponse res,String taskid){
		printJSON(res, ruleService.getRuleTree(taskid));
	}
	
	@RequestMapping
	public void remrules(HttpServletResponse res,String tid){
		ruleService.delRulesByTid(tid);
		printHTML(res, "1");
	}
	
	
	@RequestMapping
	public String ruleTest(String id, Model model) throws Exception{
		String ids[] = id.split(",");
		List<HashMap<String, List<String>>> paramss = new ArrayList<HashMap<String, List<String>>>();
		List<String> titles = new ArrayList<String>();
		for (int i = 0; i < ids.length; i++) {
			DBObject rule = ruleBaseService.findRuleBase(ids[i]);
			String reshtml = "";
			String charset = (String)rule.get("charset");
			String url = (String)rule.get("url");
			if ("post".equals(rule.get("method1"))) {
				reshtml = HttpUtil.postStr(url, charset, ((Map<String, String>)ruleBaseService.deserialize((String)rule.get("data"))));
			}else{
				reshtml = HttpUtil.getStr(url,charset);
			}
			List<LinkedHashMap> expressions = (List<LinkedHashMap>) JSON.parseArray(rule.get("expression").toString(), LinkedHashMap.class);
			HashMap<String, Object> params = ruleBaseService.ruleTest(expressions,reshtml,rule.get("url").toString());
			//将取到的数据按照逗号分割
			HashMap<String, List<String>> dataparams = ruleBaseService.paramsSplit(expressions, rule, params);
			//测试Rule的Rule名称
			titles.add(rule.get("rulename").toString());
			//如果类型是PageProcessor则返回nextUrl，否则返回数据
			if (rule.get("type").toString().equals("PageProcessor")) {
				HashMap<String, List<String>> pagelist = ruleBaseService.nexturl(rule, expressions, params);
				paramss.add(pagelist);
			}else if(rule.get("type").toString().equals("TypeProcessor") &&rule.get("urlRule")!=null) {
				HashMap<String, List<String>> nextlists = ruleBaseService.urlRule(rule,expressions, params);
				paramss.add(nextlists);
			}else{
				paramss.add(dataparams);
			}
			
		}
//		System.out.println("params===="+paramss);
		model.addAttribute("params", paramss);
		model.addAttribute("titles", titles);
		return "ruleBase/testrulebase";
	
	}
	
	@RequestMapping
	public void remove(HttpServletResponse res,String id){
		ruleService.deleteRule(id);
		printHTML(res,"1");
	}
	
}
