package cn.datawin.action;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.mongodb.DBObject;
import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.RuleService;
import cn.datawin.service.TaskService;

@Controller
public class TaskController extends BaseController{
	@Resource
	TaskService taskService;
	@Resource
	RuleService ruleService;
	
	@RequestMapping
	public String huicong(HttpServletRequest req,HttpServletResponse res) throws IOException{
		
		return "huicong";
	}
	
	@RequestMapping
	public void inserttask(String serializeList, String rules,HttpServletResponse res){
		taskService.insertTask(serializeList, rules);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("status", "success");
		printJSON(res, map);
	}
	
	
	/*@RequestMapping(method=RequestMethod.POST)*/
	public void inserttaskF(String serializeList,HttpServletRequest req ,HttpServletResponse res){
		MultipartHttpServletRequest muRequest = (MultipartHttpServletRequest)req;
		MultipartFile file =  muRequest.getFile("file");
		System.out.println("===size==="+file.getSize());
//		taskService.insertTask(serializeList);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("status", "success");
		printJSON(res, map);
	}
	
	@RequestMapping
	public void gettask(String workbeach, String state, HttpServletResponse res, int num) throws ServletException, IOException{
		String task = taskService.findTask(workbeach,state, num);
		if (task.length()<5000) {
			printJSON(res, task);
		}else {
			excute(res, task);
		}
	}
	
	@RequestMapping
	public void deletetask(HttpServletResponse res, String id){
		taskService.deleteTask(id);
		
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void getalltask(HttpServletResponse res, Form form,ReqData reqData){
		printJSON(res,taskService.getalltask(form,reqData));
	}
	
	@RequestMapping
	public void getcom(HttpServletResponse res,String uid) throws ParseException{
		String s=taskService.getkeywords(uid);
		printHTML(res, s);
	}
	
	@RequestMapping
	public void addtaskrule(HttpServletResponse res,String ids,String rid,String taskid){
		printHTML(res,taskService.addrules(ids,rid,taskid));
	}
	
	@RequestMapping
	public void addtask(HttpServletResponse res,Form form){
		taskService.addTask(form);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void updatetask(HttpServletResponse res,Form form){
		taskService.updataTask(form);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void comptask(HttpServletResponse res,String taskid){
		taskService.comptask(taskid);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void failtask(HttpServletResponse res,String taskid){
		taskService.failtask(taskid);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void runtask(HttpServletResponse res,String ids){
		taskService.runTask(ids);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public String savetask(Model model,String tid){
		model.addAttribute("task",taskService.getTaskById(tid));
		return "task/addtask";
	}
	
	@RequestMapping
	public String copytask(Model model,String tid){
		DBObject task = taskService.getTaskById(tid);
		String taskid = taskService.insertTask(task);
		List<DBObject> ruleList = ruleService.findRule(tid);
		for (int i = 0; i < ruleList.size(); i++) {
			ruleService.copyRule(ruleList.get(i),taskid);
		}
		model.addAttribute("task",taskService.getTaskById(taskid));
		return "task/addtask";
	}
	
	@RequestMapping
	public void addkwtask(Model model,HttpServletResponse res,String url,String uid,String charset,String type){
		DBObject task = taskService.getTaskByWorkbeach();
		String tid=task.get("_id").toString();
		String taskid =taskService.insertKWtask(url, uid,charset,task,type);
		List<DBObject> ruleList = ruleService.findRule(tid);
		for (int i = 0; i < ruleList.size(); i++) {
			ruleService.copyRule(ruleList.get(i),taskid);
		}
		printJSON(res,"1");
	}
	
	@RequestMapping
	public void selectcity(Model model,HttpServletResponse res,String type,String key){
		printJSON(res,taskService.getCityList(type, key));
	}
	@RequestMapping
	public void wbList(HttpServletResponse res){
		printJSON(res, taskService.getWBList());
	}
	@RequestMapping
	public void allottask(HttpServletResponse res,String wids,String tids){
		taskService.allottask(tids, wids);
		printJSON(res,"1");
	}
	
	@RequestMapping
	public void deletemidtask(HttpServletResponse res){
		taskService.deletemidTask();
		printHTML(res,"1");
	}
	@RequestMapping
	public void deletechtask(HttpServletResponse res, String id){
		taskService.deletechTask(id);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void insertdata(String serializeList,HttpServletResponse res){
		taskService.insertData(serializeList);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("status", "success");
		printJSON(res, map);
	}
	
	//状态为1的task重置为0
	@RequestMapping
	public void resettask(HttpServletResponse res){
		taskService.resetTaskState();
		printHTML(res,"1");
	}
	
	
	@RequestMapping
	public void clear_ram(HttpServletResponse res){
		taskService.clear_ram();
		printHTML(res, 1);
	}
	
	
	@PostConstruct
	void r(){
		executor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try{
					TaskController.this.recycle();
				}catch(Exception e){
				}
			}

		}, 10, 30, TimeUnit.MINUTES);
	}
	
	private void recycle() {
		taskService.deletemidTask();
		taskService.deletemidData();
		taskService.clear_ram();
		//状态为3的task处理       重置状态为0     删除子任务
		taskService.modifyTaskState();
		
	}
	
	static ScheduledExecutorService executor = Executors.newScheduledThreadPool(3) ;
	
}
