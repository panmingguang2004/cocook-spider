package cn.datawin.action;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.service.TimerService;

import com.googlecode.jsonplugin.JSONException;

@Controller
public class TimerController extends BaseController{

	@Resource
	TimerService timerService;
	
	
	@RequestMapping
	public void list(HttpServletResponse res, ReqData reData ,String tname) throws IOException, JSONException{
		printJSON(res, timerService.list(reData, tname));
	}
	
	@RequestMapping
	public void timetask(HttpServletResponse res,String tmid,String tids){
		timerService.addtimertask(tids, tmid);
		
		printJSON(res,"1");
	}
	
	@RequestMapping
	public void add(HttpServletResponse res, ReqData reData ,Form form) throws IOException, JSONException{
		printHTML(res, timerService.add(form));
	}
	
	@RequestMapping
	public void edit(HttpServletResponse res, Form form) throws IOException, JSONException{
		printHTML(res, timerService.edit(form));
	}
	
	@RequestMapping
	public void del(HttpServletResponse res, String ids) throws IOException, JSONException{
		printHTML(res, timerService.del(ids));
	}
	
	@RequestMapping
	public void runtask(HttpServletResponse res, String ids) throws IOException, JSONException{
		timerService.runtask(ids);
		printHTML(res,"1");
	}
	
	@RequestMapping
	public void stoptask(HttpServletResponse res, String ids) throws IOException, JSONException{
		timerService.stoptask(ids);
		printHTML(res,"1");
	}
}
