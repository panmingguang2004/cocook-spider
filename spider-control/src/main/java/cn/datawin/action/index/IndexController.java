package cn.datawin.action.index;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mongodb.DBObject;

import cn.datawin.action.BaseController;
import cn.datawin.bean.User;
import cn.datawin.filter.DpsmInterceptor;
import cn.datawin.service.UserService;

@Controller
public class IndexController extends BaseController{
	@Resource
	UserService userService;
	
	
	@RequestMapping
	public String logout(HttpServletRequest req,HttpServletResponse res) throws IOException{
		req.getSession().invalidate();
		res.sendRedirect(DpsmInterceptor.getBasePath(req)+"/index.html");
		return null;
	}
	
	@RequestMapping
	public String index(HttpServletRequest req,HttpServletResponse res) throws IOException{
		
		return "main";
	}
	
	@RequestMapping
	public void test(HttpServletRequest req,HttpServletResponse res) throws IOException{
		
		printJSON(res, "{}");
	}
	
	@RequestMapping
	public String login(HttpServletRequest req,HttpServletResponse res, User user, HttpSession session) throws IOException{
		user = userService.checkUser(user.getUsername(), user.getPassword());
		if (user != null) {
			session.setAttribute("user", user);
			return "main";
		}else {
			res.sendRedirect(DpsmInterceptor.getBasePath(req)+"/index.html");
			return null;
		}
	}
	
}

