package cn.datawin.bean;

import java.util.HashMap;
import java.util.Random;

import org.springframework.util.StringUtils;

public class Constants {
	public static HashMap<String, String> apps = new HashMap<String, String>();
	private static Random random = new Random();
	
	static{
		
	}

	public static String getTrainQQ(){
		String train_qq = apps.get("train_qq");
		if(!StringUtils.hasLength(train_qq)) return "";
		String qqs[] = train_qq.split(",");
		return qqs[random.nextInt(qqs.length)];
	}
}
