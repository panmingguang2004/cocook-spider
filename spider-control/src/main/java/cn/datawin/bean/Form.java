package cn.datawin.bean;

import java.util.Date;

public class Form {
	private String id;
	private String note;
	private String url;
	private String workbeach;
	private String meth;
	private String charset;
	private String header = "{}";
	private String cookie = "{}";
	private String data = "{}";
	private String processor;
	private String key;
	private Date crawlerdate;//抓取时间
	private String datelong;
	private String name;
	private String extedata = "{}";
	private String acc;
	private String pwd;
	private String type;
	
	
	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDatelong() {
		return datelong;
	}

	public void setDatelong(String datelong) {
		this.datelong = datelong;
	}

	public Date getCrawlerdate() {
		return crawlerdate;
	}
	
	public void setCrawlerdate(Date crawlerdate) {
		this.crawlerdate = crawlerdate;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getWorkbeach() {
		return workbeach;
	}
	public void setWorkbeach(String workbeach) {
		this.workbeach = workbeach;
	}
	public String getMeth() {
		return meth;
	}
	public void setMeth(String meth) {
		this.meth = meth;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getCookie() {
		return cookie;
	}
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public String toString() {
		return "Form [charset=" + charset + ", cookie=" + cookie + ", data="
				+ data + ", header=" + header + ", id=" + id + ", meth=" + meth
				+ ", note=" + note + ", url=" + url + ", workbeach="
				+ workbeach + "]";
	}
	public void setProcessor(String processor) {
		this.processor = processor;
	}
	public String getProcessor() {
		return processor;
	}

	public String getExtedata() {
		return extedata;
	}

	public void setExtedata(String extedata) {
		this.extedata = extedata;
	}
	
	
}
