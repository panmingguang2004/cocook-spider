package cn.datawin.bean;

/**
 * 规则库
 * @author wx
 */
public class RuleBase {
	private String _id;
	private String rulename;
	private String type;
	private String expression;
	private String pageRule;
	private String name;
	private int pageSize=0;
	private String url;
	private String belone;
	private String charset;
	private String method1;
	private String data;
	private String urlRule;
	private int pageNum;
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getRulename() {
		return rulename;
	}
	public void setRulename(String rulename) {
		this.rulename = rulename;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getPageRule() {
		return pageRule;
	}
	public void setPageRule(String pageRule) {
		this.pageRule = pageRule;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBelone() {
		return belone;
	}
	public void setBelone(String belone) {
		this.belone = belone;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public String getMethod1() {
		return method1;
	}
	public void setMethod1(String method) {
		this.method1 = method;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setUrlRule(String urlRule) {
		this.urlRule = urlRule;
	}
	public String getUrlRule() {
		return urlRule;
	}

}
