package cn.datawin.bean;

/**
 * 登录用户对象
 * @author panmg
 */
public class User {
	private String _id;
	
	private String username;

	private String password;
	
	
	private String role;
	
	private String email;
	
	private long creationtime;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public void setRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	@Override
	public String toString() {
		return "User [_id=" + _id + ", username=" + username + ", password=" + password + ", role=" + role + "]";
	}

	public void setCreationtime(long creationtime) {
		this.creationtime = creationtime;
	}

	public long getCreationtime() {
		return creationtime;
	}
	
	
	

}
