package cn.datawin.bean.page;

/**
 * easyUI page params
 */
public class ReqData{

	private int page = 1; // 页码
	private int rows = 20;// 每页行数
	private String sort = null; //排序字段
	private String order = null; // 排序方式

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

}
