package cn.datawin.bean.page;


/**
 * easy grid JSON
 */
public class RespData<E> {

	private E rows;
	private E footer;
	private long total;

	public E getRows() {
		return this.rows;
	}

	public void setRows(E  rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return this.total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public void setFooter(E footer) {
		this.footer = footer;
	}

	public E getFooter() {
		return footer;
	}

}
