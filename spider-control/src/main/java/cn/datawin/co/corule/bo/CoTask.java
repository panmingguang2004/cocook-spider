package cn.datawin.co.corule.bo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoTask {

	private String id;
	
	/* vps 标识符 doCaptcha 作为打码标识符  */
	private String workbeach;
	
	/* 抓取网站标识符 */
	private String type;
	
	private String clientid;
	
	/* 任务标识符 */
	private String sig;

	private String state = "0";

	private String url;
	
	private String method = "post";

	private String charset = "UTF-8";

	private Map<String, String> header = Collections.emptyMap();
	
	private Map<String, String> extedata = Collections.emptyMap();

	private Map<String, String> cookie = Collections.emptyMap();
	/**
	 * post 数据
	 * { params: {} }
	 */
	private Map<String, String> data = Collections.emptyMap();

	private List<Map<String, Object>> rules = new ArrayList<Map<String, Object>>();

	public String getWorkbeach() {
		return workbeach;
	}

	public void setWorkbeach(String workbeach) {
		this.workbeach = workbeach;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public Map<String, String> getHeader() {
		return header;
	}

	public void setHeader(Map<String, String> header) {
		this.header = header;
	}

	public Map<String, String> getCookie() {
		return cookie;
	}

	public void setCookie(Map<String, String> cookie) {
		this.cookie = cookie;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	public List<Map<String, Object>> getRules() {
		return rules;
	}

	public void setRules(List<Map<String, Object>> rules) {
		this.rules = rules;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public Map<String, Object>  toMap(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("workbeach", workbeach);
		map.put("type", type);
		map.put("sig", sig);
		map.put("state", state);
		map.put("url", url);
		map.put("method", method);
		map.put("charset", charset);
		map.put("header", header);
		map.put("cookie", cookie);
		map.put("rules", rules);
		map.put("data", data);
		map.put("extedata", extedata);
		map.put("clientid", clientid);
		return map;
	}

	public Map<String, String> getExtedata() {
		return extedata;
	}

	public void setExtedata(Map<String, String> extedata) {
		this.extedata = extedata;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setSig(String sig) {
		this.sig = sig;
	}

	public String getSig() {
		return sig;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getClientid() {
		return clientid;
	}






}
