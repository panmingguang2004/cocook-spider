package cn.datawin.co.corule.bo;

import java.util.HashMap;
import java.util.Map;

public class CoTaskResult {
	private String id;
	
	private String taskid;
	
	private String clientid;
	
	private String sig;
	
	private String workbeach;
	
	private String type;
	
	/*读取的状态*/
	private String state ="0";
	
	private Map<String, Object> result = new HashMap<String, Object>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}
	
	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getSig() {
		return sig;
	}

	public void setSig(String sig) {
		this.sig = sig;
	}

	public String getWorkbeach() {
		return workbeach;
	}

	public void setWorkbeach(String workbeach) {
		this.workbeach = workbeach;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}
	
	
	public Map<String, Object>  toMap(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("taskid", taskid);
		map.put("workbeach", workbeach);
		map.put("type", type);
		map.put("sig", sig);
		map.put("clientid", clientid);
		map.put("result", result);
		map.put("state", state);
		return map;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}

}
