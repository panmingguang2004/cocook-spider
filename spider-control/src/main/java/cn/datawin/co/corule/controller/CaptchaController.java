package cn.datawin.co.corule.controller;

import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.datawin.action.BaseController;
import cn.datawin.bean.Form;
import cn.datawin.co.corule.service.CaptchaService;

@Controller
public class CaptchaController extends BaseController{
	@Resource
	CaptchaService captchaService;
	
	/**
	 * 获取验证码
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping
	public String captcha(HttpServletResponse res, Form form) throws IOException{
		downImg(res, captchaService.getCaptcha(form));
		return null;
	}
	
	@RequestMapping
	public String hascaptcha(HttpServletResponse res){
		printHTML(res, captchaService.hasCaptcha());
		return null;
	}
	
	/*提交验证码*/
	@RequestMapping
	public String postcaptcha(HttpServletResponse res, Form form){
		printHTML(res, captchaService.postCaptcha(form));
		return null;
	}
	
	@RequestMapping
	public void getfailmsg(HttpServletResponse res){
		java.util.List<String> list = captchaService.getfailmsgs();
		printJSON(res, list);
	}

}
