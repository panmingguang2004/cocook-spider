package cn.datawin.co.corule.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


import cn.datawin.action.BaseController;
import cn.datawin.bean.Form;
import cn.datawin.co.corule.service.CoTaskService;
import cn.datawin.service.BaseService;
import cn.datawin.util.HttpUtil;

@Controller
public class CoTaskController extends BaseController{
	@Resource
	CoTaskService coTaskService;
	
	@RequestMapping
	public void getcotask(String workbeach,String state,HttpServletResponse res){
		printJSON(res,coTaskService.getCoTask(workbeach,state));
	}
	
	@RequestMapping
	public void getcotaskresult(String state,HttpServletResponse res) throws ServletException, IOException{
		String coTaskResult = HttpUtil.serialize(coTaskService.getCoTaskResult(state));
		if (coTaskResult.length()>5000) {
			excute(res, coTaskResult);
		}else {
			printJSON(res, coTaskResult);
		}
		
	}
	
	@RequestMapping
	public void insertcotask(String coTask,HttpServletResponse res){
		coTaskService.insertCoTask(coTask);
		printJSON(res, "1");
	}
	
	@RequestMapping
	public void insertcotaskresult(String coTaskResult,HttpServletResponse res){
		coTaskService.insertCoTaskResult(coTaskResult);
		printJSON(res, "1");
	}
	
	
	/* 修改 状态  flag = 0 删除 那个数据*/
	@RequestMapping
	public void updatecotaskresult(String id, String flag,HttpServletResponse res){
		coTaskService.updatecotaskResult(id,flag);
		printJSON(res, "1");
	}
	
	@RequestMapping
	public void updatecotask(String id, String flag,HttpServletResponse res){
		coTaskService.updatecotask(id,flag);
		printJSON(res, "1");
	}
	
	
	@RequestMapping
	public void baidulogin(HttpServletResponse res ,Form form){
		coTaskService.baidu_Login(form);
		printHTML(res, "1");
	}
	
	@RequestMapping
	public void acclogin(HttpServletResponse res,String aid,String workbeach){
		coTaskService.accLogin(aid, workbeach);
		printHTML(res, "1");
	}
	
	{
		
		//client.start(_class);  //抓取的rule
	}
}
