package cn.datawin.co.corule.rule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.types.ObjectId;
import cn.datawin.Client;
import cn.datawin.Service;
import cn.datawin.co.corule.bo.CoTask;
import cn.datawin.co.corule.bo.CoTaskResult;
import cn.datawin.crule.CoRule;

public class BaiduLogin extends CoRule{
	
	@Override
	public void first() {
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setCharset("UTF-8");
		task.setMethod("get");
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("cookie","BAIDUID");
		r1.put("name","BAIDUID");
		rul.add(r1);
		task.setRules(rul);
		task.setSig("first");
		task.setState("0");
		task.setType("baidu");
		task.setUrl("http://www.baidu.com");
		task.setWorkbeach(this.getService().getClient().getParam("workbeach").toString());
		this.setObject(task);
	}
	
	public void firstResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String baiduid = (String)ctr.getResult().get("BAIDUID");
		if(baiduid!= null){
			this.getService().setParam("BAIDUID", baiduid);
			second();
		}else{
			first();
		}
	}
	
	public void second(){
		
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setUrl("https://passport.baidu.com/v2/api/?getapi&class=login&tpl=mn&tangram=true");
		task.setMethod("get");
		task.setSig("second");
		task.setState("0");
		task.setType("baidu");
		task.setWorkbeach(this.getService().getClient().getParam("workbeach").toString());
		
		task.setCharset("UTF-8");
		Map<String,String> co = new HashMap<String, String>();
		co.put("BAIDUID", this.getService().getParam("BAIDUID").toString());
		task.setCookie(co);
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("regex","bdPass.api.params.login_token='(.+?)';");
		r1.put("name","login_token");
		rul.add(r1);
		task.setRules(rul);
		
		this.setObject(task);
	}
	
	public void secondResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String login_token = (String)ctr.getResult().get("login_token");
		if(login_token!= null){
			this.getService().setParam("login_token", login_token);
			getpublicKey();
		}
	}
	
	public void getpublicKey(){
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setUrl("https://passport.baidu.com/v2/getpublickey?token="+this.getService().getParam("login_token").toString()+"&tpl=mn&apiver=v3&tt="+new Date().getTime());
		task.setMethod("get");
		task.setSig("getpublicKey");
		task.setState("0");
		task.setType("baidu");
		task.setWorkbeach(this.getService().getClient().getParam("workbeach").toString());
		task.setCharset("UTF-8");
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		// "key":'hVAovc7QvGFBSQwfc2wyZxcwMs86c2Ic'}
		r1.put("regex","\"key\":'(.+?)'");
		r1.put("name","key");
		rul.add(r1);
		task.setRules(rul);
		
		this.setObject(task);
	}
	
	public void getpublicKeyResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String key = (String)ctr.getResult().get("key");
		this.getService().setParam("key",key);
		reqCaptcha();
	}
	
	public void reqCaptcha(){
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setUrl("https://passport.baidu.com/v2/api/?logincheck&token="+this.getService().getParam("login_token").toString()+"&tpl=mn&apiver=v3&tt="+new Date().getTime()+"&username="+getService().getClient().getParam("username")+"&isphone=false");
		task.setCharset("UTF-8");
		Map<String,String> co = new HashMap<String, String>();
		co.put("BAIDUID", this.getService().getParam("BAIDUID").toString());
		task.setCookie(co);
		task.setMethod("get");
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("regex","\"codeString\" : \"(.+?)\"");
		r1.put("name","genimage");
		
//		r1.put("captche","bdPass.api.params.login_token='(.+?)';");
//		r1.put("name","captche");
		
		rul.add(r1);
		task.setRules(rul);
		task.setSig("reqCaptcha");
		task.setState("0");
		task.setType("baidu");
		task.setWorkbeach(this.getService().getClient().getParam("workbeach").toString());
		this.setObject(task);
	}
	
	public void reqCaptchaResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String genimage = (String)ctr.getResult().get("genimage");
		if(null==genimage||genimage.length()<20){
			third();
		}else{
			this.getService().setParam("genimage",genimage);
			getCaptcha();
		}
	}
	
	public void getCaptcha(){
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setUrl("https://passport.baidu.com/cgi-bin/genimage?"+this.getService().getParam("genimage").toString());
		task.setCharset("UTF-8");
		Map<String,String> co = new HashMap<String, String>();
		co.put("BAIDUID", this.getService().getParam("BAIDUID").toString());
		task.setCookie(co);
		task.setMethod("get");
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("captcha","");
		r1.put("name","captcha");
		rul.add(r1);
		task.setRules(rul);
		
		task.setSig("getCaptcha");
		task.setState("0");
		task.setType("baidu");
		task.setWorkbeach(this.getService().getClient().getParam("workbeach").toString());
		this.setObject(task);
	}
	
	public void getCaptchaResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String captcha = (String)ctr.getResult().get("captcha");
		this.getService().setParam("captcha",captcha);
		sendCaptcha();
	}
	
	
	public void sendCaptcha(){
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setUrl("");
		task.setCharset("UTF-8");
		task.setMethod("get");
		task.setSig("sendCaptcha");
		task.setState("0");
		task.setType("baidu");
		task.setWorkbeach("doCaptcha");
		
		Map<String, String> params=new HashMap<String, String>();
		params.put("captcha", (String)getService().getParam("captcha"));
		task.setData(params);
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("name","captcha");
		rul.add(r1);
		task.setRules(rul);
		
		this.setObject(task);
	}
	
	
	public void sendCaptchaResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String captcha = (String)ctr.getResult().get("captcha");
		this.getService().setParam("captcha",captcha);
		third();
	}
	
	public void third(){
		String captche = (String) getService().getParam("captcha");
		CoTask task = new CoTask();
		task.setId(new ObjectId().toString());
		task.setUrl("https://passport.baidu.com/v2/api/?login");
		task.setMethod("post");
		task.setCharset("UTF-8");
		
		Map<String, String> params=new HashMap<String, String>();
		params.put("username", this.getService().getClient().getParam("username").toString());
		params.put("password",this.getService().getClient().getParam("password").toString());
		params.put("charset", "utf-8");
		params.put("token", this.getService().getParam("login_token").toString());
		params.put("isPhone", "false");
		params.put("quick_user", "0");
		params.put("tt", System.currentTimeMillis()+"");
		params.put("loginmerge", "true");
		params.put("logintype", "dialogLogin");
		params.put("index", "0");
		params.put("staticpage", "http://www.baidu.com/cache/user/html/jump.html");
		params.put("loginType", "1");
		params.put("mem_pass", "on");
		params.put("splogin", "rate");
		params.put("apiver", "v3");
		params.put("tpl", "mn");
		params.put("safeflg", "0");
		params.put("ppui_logintime", "43661");
		params.put("u", "http://www.baidu.com/?tn=92734721_hao_pg");
		params.put("callback","parent.bdPass.api.login._postCallback");
		
		if(captche != null){
			params.put("staticpage","http://www.baidu.com/cache/user/html/v3Jump.html");
			params.put("codestring",(String)getService().getParam("genimage"));
			params.put("verifycode",captche);
			params.put("callback","parent.bd__pcbs__fv6g3o");
			params.put("mem_pass","on");
		}
		
		task.setData(params);
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("cookie","BDUSS");
		r1.put("name","BDUSS");
		Map<String,Object> r2 = new HashMap<String, Object>();
		r2.put("cookie","PTOKEN");
		r2.put("name","PTOKEN");
		Map<String,Object> r3 = new HashMap<String, Object>();
		r3.put("cookie","STOKEN");
		r3.put("name","STOKEN");
		rul.add(r1);
		rul.add(r2);
		rul.add(r3);
		task.setRules(rul);
		task.setSig("third");
		task.setState("0");
		task.setType("baidu");
		
		task.setWorkbeach(this.getService().getClient().getParam("workbeach").toString());
		this.setObject(task);
	}
	
	public void thirdResult(){
		CoTaskResult ctr = this.getObject(CoTaskResult.class);
		String BDUSS = (String)ctr.getResult().get("BDUSS");
		String PTOKEN = (String)ctr.getResult().get("PTOKEN");
		String STOKEN = (String)ctr.getResult().get("STOKEN");
		if(BDUSS!= null&&PTOKEN!=null&&null!=STOKEN){
			this.getService().finish();
			this.getService().getClient().finish();
			
		}else{
			first() ;
		}
	}
	

	@Override
	public Serializable getSerialize() {
		return null;
	}

	@Override
	public void setClient(Client client) {
		
	}

	@Override
	public void setSerialize(Serializable obj) {
		
	}

	@Override
	public void setService(Service service) {
		super.setService(service);
	}

	@Override
	public Service getService() {
		return super.getService();
	}

}
