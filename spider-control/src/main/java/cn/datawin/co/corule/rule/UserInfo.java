package cn.datawin.co.corule.rule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import cn.datawin.Client;
import cn.datawin.co.corule.bo.CoTask;
import cn.datawin.co.corule.bo.CoTaskResult;
import cn.datawin.crule.CoRule;

public class UserInfo extends CoRule{

	@Override
	public void first() {
		CoTask task = new CoTask();
		Integer page = Integer.valueOf((String)this.getService().getClient().getParam("page"));
		String pn =  (null == page || page == 1) ? "" : ("&pn="+50*(page-1));
		task.setUrl("http://tieba.baidu.com/f?kw=%D0%DD%CF%D0%D0%AC"+pn);
		task.setMethod("get");
		task.setId(new ObjectId().toString());
		task.setCharset("utf-8");
		task.setSig("$data");
		task.setState("0");
		task.setType("baidu");
		task.setWorkbeach("192.168.1.1");
		
		List<Map<String,Object>> rul = new ArrayList<Map<String,Object>>();
		Map<String,Object> r1 = new HashMap<String, Object>();
		r1.put("selector","div.threadlist_title a");
		r1.put("html","");
		r1.put("name","title");
		rul.add(r1);
		task.setRules(rul);
		this.setObject(task);
	}
	
	public void $dataResult(){
		CoTaskResult cr = this.getObject(CoTaskResult.class);
		String r = (String)cr.getResult().get("title");
		Integer page = Integer.valueOf((String)this.getService().getClient().getParam("page"));
		Integer all = Integer.valueOf((String)this.getService().getClient().getParam("all"));
		page = null==page?1:page+1;
		this.getService().getClient().setParam("page",page+"");
		if(null != r && page < all+1){
			first();
		}else{
			this.getService().finish();
		}
	}

	@Override
	public Serializable getSerialize() {
		return null;
	}

	@Override
	public void setClient(Client client) {
		
	}

	@Override
	public void setSerialize(Serializable obj) {
		
	}

}
