package cn.datawin.co.corule.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import sun.misc.BASE64Decoder;

import com.mongodb.DBObject;

import cn.datawin.bean.Form;
import cn.datawin.co.corule.bo.CoTask;
import cn.datawin.co.corule.bo.CoTaskResult;
import cn.datawin.service.BaseService;
import cn.datawin.util.ThreadUtil;

@Service
public class CaptchaService extends BaseService{
	
	@Resource
	CoTaskService coTaskService;
	
	private static Map<String, CoTask> captchas  = new HashMap<String, CoTask>();;
	private static List<CoTask> tasks = new ArrayList<CoTask>();;
	public static List<String> failmsgs = new ArrayList<String>();
	
	
	/**
	 * 获取验证码
	 * @return
	 * @throws IOException 
	 */
	public InputStream getCaptcha(Form from) throws IOException {
		CoTask task = captchas.get(from.getId());
		
		String img = task.getData().get("captcha");
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] img_b=decoder.decodeBuffer(img);	
		
		return new ByteArrayInputStream(img_b);
		
	}
	
	/*是否有验证码*/
	public synchronized String hasCaptcha(){
		if(tasks==null||tasks.size()==0){
			return "0";
		}
		CoTask task = (CoTask) tasks.remove(0);
		captchas.put(task.getId(), task);
		
		return task.getId()+","+task.getClientid();
	}
	
	/*提交验证码*/
	public int postCaptcha(Form form){
		final CoTask task = captchas.remove(form.getId());
		CoTaskResult coTaskResult=new CoTaskResult();
		coTaskResult.setTaskid(task.getId());
		coTaskResult.setClientid(task.getClientid());
		coTaskResult.setSig(task.getSig());
		coTaskResult.setType(task.getType());
		coTaskResult.setWorkbeach("doCaptcha");
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("captcha", form.getKey());
		coTaskResult.setResult(data);
		coTaskService.insertCoTaskResult(serialize(coTaskResult));
		
		//结束打码task
		coTaskService.finishTask(task.getId());
		return 1;
	}
	
	//@PostConstruct
	public void captchas(){
		ThreadUtil.exec(new Runnable() {
			@Override
			public void run() {
				while(true){
					try{
						List<DBObject> coTasks = coTaskService.getCoTask("doCaptcha", "0");
						if(coTasks!=null&& coTasks.size()!=0){
							tasks.addAll(dboToTask(coTasks));
						}
						Thread.sleep(2000);
					}catch(Exception e){
						e.printStackTrace();
						continue;
					}
				}
				
			}
		});
	}
	
	
	public List<CoTask> dboToTask(List<DBObject> list){
		List<CoTask> tasklist = new ArrayList<CoTask>();
		if (list!=null &&list.size() >0) {
			for (int i = 0; i < list.size(); i++) {
				DBObject map = list.get(i);
				CoTask task = new CoTask();
				task.setId((String)map.get("_id"));
				task.setWorkbeach((String)map.get("workbeach"));
				task.setType((String)map.get("type"));
				task.setSig((String)map.get("sig"));
				
				task.setState((String)map.get("state"));
				task.setMethod((String)map.get("method"));
				task.setUrl((String)map.get("url"));
				task.setCharset((String)map.get("charset"));
				task.setClientid((String)map.get("clientid"));
				
				task.setHeader((Map<String, String>)map.get("header"));
				task.setCookie((Map<String, String>)map.get("cookie"));
				task.setRules((List<Map<String, Object>>)map.get("rules"));
				task.setData((Map<String, String>)map.get("data") );
				task.setExtedata((Map<String, String>)map.get("extedata"));
				tasklist.add(task);
			}
		}
		return tasklist;
	}
	
	public List<String> getfailmsgs(){
		List<String> msgs = new ArrayList<String>();
		msgs.addAll(failmsgs);
		for(int i = 0 ; i < failmsgs.size(); i++){
			failmsgs.removeAll(msgs);
		}
		return msgs;
	}

}
