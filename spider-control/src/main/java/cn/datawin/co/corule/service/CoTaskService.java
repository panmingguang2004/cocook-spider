package cn.datawin.co.corule.service;

import javax.annotation.Resource;

import cn.datawin.Rule;
import cn.datawin.bean.Form;
import cn.datawin.co.corule.rule.BaiduLogin;
import cn.datawin.co.corule.rule.UserInfo;
import cn.datawin.co.corule.service.client.CoClient;
import cn.datawin.crule.CoRule;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import cn.datawin.service.BaseService;


@Service
public class CoTaskService extends BaseService{
	@Resource
	private CoTaskResultPoll coTaskResultPoll;
	
	public void insertCoTask(String coTask){
		Map<String, Object> map =(Map<String, Object>)deserialize(coTask);
		if (map.get("id") !=null) {
			map.put("_id",new ObjectId(map.remove("id").toString()));
		}
		String col="cotask";
		this.insert(col, map);
	}
	
	public void insertCoTaskResult(String coTaskResult){
		Map<String, Object> map =(Map<String, Object>)deserialize(coTaskResult);
		String col="cotaskResult";
		this.insert(col, map);
	}
	
	public List<DBObject> getCoTask(String workbeach,String state){
		DBObject query = new BasicDBObject();
		if (workbeach != null && !workbeach.equals("")) {
			query.put("workbeach", workbeach);
		}
		query.put("state", state);
		List<DBObject> list =  this.findCoTask(1, query);
		for (int i = 0; i < list.size(); i++) {
			finishTask(list.get(i).get("_id").toString());
		}
		return list;
	}
	
	List<DBObject> findCoTask(int times, DBObject query){
		List<DBObject> list =  this.findList("cotask", query);
		if(list.size()==0){
			while(times<10){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				times++;
				return findCoTask(times, query);
			}
		}
		return list;
	}
	
	public List<DBObject> getCoTaskR(String workbeach,String state){
		DBObject query = new BasicDBObject();
		if (workbeach != null && !workbeach.equals("")) {
			query.put("workbeach", workbeach);
		}
		query.put("state", state);
		List<DBObject> list =  this.findList("cotaskResult", query);
		for (int i = 0; i < list.size(); i++) {
			finishTask(list.get(i).get("_id").toString());
		}
		return list;
	}
	
	public List<DBObject>  getCoTaskResult(String state){
		DBObject query = new BasicDBObject();
		query.put("state", state);
		List<DBObject> list =  this.findList("cotaskResult", query);
		for (int i = 0; i < list.size(); i++) {
			DBObject updateValue=new BasicDBObject();
			updateValue.put("state", "1");
			this.updateByid("cotaskResult", list.get(i).get("_id").toString(),updateValue);
		}
		return list;
	}
	
	public void finishTask(String taskid){
		DBObject updateValue=new BasicDBObject();
		updateValue.put("state", "1");
		this.updateByid("cotask", taskid,updateValue);
	}
	
	public void accLogin(String aid,String workbeach){
		DBObject db = this.findOne("onclient",new BasicDBObject("_id",new ObjectId(aid)));
		CoClient client = new CoClient();
		coTaskResultPoll.setClient(client);
		client.setParam("username", db.get("acc").toString());
		client.setParam("password", db.get("pwd").toString());
		client.setParam("workbeach", workbeach);
		Class<CoRule> c = null;
		try {
			c = (Class<CoRule>) Class.forName("cn.datawin.co.corule.rule."+db.get("type").toString()+"Login");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		if(null!=c){
			client.start(c);
		}
		while(true){
			if(client.isDone()){
				System.out.println("登陆成功");
				db.put("clientid",client.getClientid());
				db.put("workbeach",workbeach);
				db.put("state","1");
				this.updateByid("onclient",aid,db);
				break;
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void baidu_Login(Form form){
		CoClient client = new CoClient();
		coTaskResultPoll.setClient(client);
		client.setParam("username", "1067611312@qq.com");
		client.setParam("password", "WANGZENG5566");
		client.start(BaiduLogin.class);
		
		while(true){
			if(client.isDone()){
				System.out.println("登陆成功");
				break;
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		client.setParam("page", "1");
		client.setParam("all", "5");
		client.start(UserInfo.class);
	}
	
	
	public void updatecotaskResult(String id, String flag){
		if (flag.equals("0")) {
			this.deleteById("cotaskResult", id);
		}else {
			DBObject updateValue=new BasicDBObject();
			updateValue.put("state", "2");
			this.updateByid("cotaskResult", id,updateValue);
		}
		
	}
	
	public void updatecotask(String id, String flag){
		if (flag.equals("0")) {
			this.deleteById("cotask", id);
		}else {
			DBObject updateValue=new BasicDBObject();
			updateValue.put("state", "2");
			this.updateByid("cotask", id,updateValue);
		}
		
	}

}
