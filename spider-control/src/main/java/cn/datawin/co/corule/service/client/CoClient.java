package cn.datawin.co.corule.service.client;

import java.util.HashMap;
import java.util.Map;
import org.bson.types.ObjectId;
import cn.datawin.Rule;
import cn.datawin.Service;

public class CoClient extends  cn.datawin.crule.CoClient{
	
	private Map<String, Service> services = new HashMap<String, Service>();
	
	public CoClient(){
		String clientid = new ObjectId().toString();
		this.setClientid(clientid);
	}

	public void setService(String key, Service service) {
		this.services.put(key, service);
	}

	public Service getService(String key) {
		return this.services.get(key);
	}

	public Map<String, Service> getServices() {
		return services;
	}

	public void start(Class<? extends Rule> _class){
		new CoService(this,_class).doo();
		
	}



}
