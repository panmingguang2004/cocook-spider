package cn.datawin.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.bean.page.RespData;
import com.googlecode.jsonplugin.JSONException;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;


@Service
public class BaiduService extends BaseService{
	@Resource
	TaskService taskService;
	@Resource
	RuleService ruleService;
	
	public Object getBaidu(Form form,ReqData reqData,String type) throws JSONException {
		RespData<List<DBObject>> res=makeRes(form, reqData,type);
		return res;
	}

	public RespData<List<DBObject>> makeRes(Form form,ReqData reqData,String type){
		RespData<List<DBObject>> respData = new RespData<List<DBObject>>();
		//获取条件
		BasicDBObject query = new BasicDBObject("root", true);
		
		if (!StringUtils.hasLength(type)||type.equals("all")) {
			List<String> list = new ArrayList<String>();
			list.add("banei");
			list.add("quanba");
			list.add("zhidao");
			list.add("souba");
			list.add("baike");
			query.put("workbeach", new BasicDBObject("$in", list));
		}else {
			query.put("workbeach",type);
		}
		if(StringUtils.hasLength(form.getKey())){
			BasicDBList values = new BasicDBList();
			Pattern pattern = Pattern.compile("^.*" + form.getKey()+ ".*$", Pattern.CASE_INSENSITIVE);
			BasicDBObject q1 = new BasicDBObject();
			q1.append("note", pattern);
			values.add(q1);
			query.append("$or",values);
		}
		int start = (reqData.getPage() - 1) * reqData.getRows();
		BasicDBObject sort = null;
		//设置sort
		if(reqData.getSort()!=null){
			sort = new BasicDBObject(reqData.getSort(), sortmap.get(reqData.getOrder())); 
		}
		long count = super.count("task", query);
		List<DBObject> rows = super.findList("task", query, sort, start, reqData.getRows());
		respData.setRows(rows);
		respData.setTotal(count);
		return respData;
	}
	
	
	public void copyBaidu(HttpServletRequest req, DBObject newTask) throws JSONException, UnsupportedEncodingException {
		String id = newTask.get("_id").toString();
		String url = "";
		String note = "";
		String key = req.getParameter("key");
		String name = req.getParameter("name");
		String type = req.getParameter("type");
		if ("banei".equals(type)) {
			url = "http://tieba.baidu.com/f/search/res?isnew=1&kw="+URLEncoder.encode(name, "gb2312")+"&qw="+URLEncoder.encode(key, "gb2312")+"&rn=10&un=&only_thread=0&sm=1&sd=&ed=&ie=gbk&pn=1";
			note = "百度 吧内" + key;
		}else if ("quanba".equals(type)) {
			url = "http://tieba.baidu.com/f/search/res?isnew=1&kw=&qw="+URLEncoder.encode(key, "gb2312")+"&rn=10&un=&only_thread=0&sm=1&sd=&ed=&ie=gbk&pn=1";
			note = "百度 全吧" + key;
		}else if ("souba".equals(type)) {
			url = "http://tieba.baidu.com/f/search/fm?kw=&qw="+URLEncoder.encode(key, "gb2312")+"&rn=10&un=&sm=&sd=&ed=&pn=1";
			note = "百度 搜吧" + key;
		}else if ("zhidao".equals(type)) {
			url = "http://zhidao.baidu.com/search?word="+URLEncoder.encode(key, "gb2312")+"&ie=gbk&site=-1&sites=0&date=0&pn=0";
			note = "百度 知道" + key;
		}else if ("baike".equals(type)) {
			url = "http://baike.baidu.com/search?word="+key+"&pn=0";
			note = "百度 百科" + key;
		}
		newTask.put("url",url)	;
		newTask.put("note",note);
		newTask.put("state","0");
		Map<String, String> map = new HashMap<String, String>();
		
		if ("banei".equals(type)) {
			map.put("keyword", name);
		}else {
			map.put("keyword", key);
		}
		newTask.put("extedata",map);
		
		String taskid = taskService.insertTask(newTask);
		List<DBObject> ruleList = ruleService.findRule(id);
		for (int i = 0; i < ruleList.size(); i++) {
			ruleService.copyRule(ruleList.get(i),taskid);
		}
	}
	
	public void copyBaiduhy(HttpServletRequest req, DBObject newTask) throws JSONException, UnsupportedEncodingException {
		String id = newTask.get("_id").toString();
		String url = "";
		String note = "";
		String key = req.getParameter("key");
		String type = req.getParameter("type");
		String uid = req.getParameter("uid");
		if ("quanba".equals(type)) {
			url = "http://tieba.baidu.com/f/search/res?isnew=1&kw=&qw="+URLEncoder.encode(key, "gb2312")+"&rn=10&un=&only_thread=0&sm=1&sd=&ed=&ie=gbk&pn=1";
			note = "百度 全吧" + key;
		}else if ("zhidao".equals(type)) {
			url = "http://zhidao.baidu.com/search?word="+URLEncoder.encode(key, "gb2312")+"&ie=gbk&site=-1&sites=0&date=0&pn=0";
			note = "百度 知道" + key;
		}else if ("baike".equals(type)) {
			url = "http://baike.baidu.com/search?word="+key+"&pn=0";
			note = "百度 百科" + key;
		}
		newTask.put("url",url);
		newTask.put("uid",uid);
		newTask.put("note",note);
		newTask.put("state","0");
		Map<String, String> map = new HashMap<String, String>();
		map.put("uid", uid);
		map.put("type", "3");//行业
		newTask.put("extedata",map);
		newTask.put("workbeach","keyword");
		
		String taskid = taskService.insertTask(newTask);
		List<DBObject> ruleList = ruleService.findRule(id);
		for (int i = 0; i < ruleList.size(); i++) {
			ruleService.copyRule(ruleList.get(i),taskid);
		}
	}
	
	public void copyBaiduSearch(HttpServletRequest req, DBObject newTask) throws JSONException, UnsupportedEncodingException {
		String id = newTask.get("_id").toString();
		String url = "";
		String note = "";
		String key = req.getParameter("key");
		String times = req.getParameter("times");
		String uid = req.getParameter("uid");
		url ="http://www.baidu.com/s?wd="+key+"&pn=000&oq="+key+"&tn=baiduadv&rn=100&ie=utf-8&lm="+times+"&usm=4&rsv_pq=d4b323fc000082cb&rsv_t=e604xrbyqQZsK9y6JA3JKxhH9JBiKVjoVdyjDOVb2zmYhrjJI3pM&rsv_page=1&f=8&rsv_bp=1";
		note = "百度 搜索  " + key;
		newTask.put("url",url);
		newTask.put("uid",uid);
		newTask.put("note",note);
		newTask.put("state","0");
		Map<String, String> map = new HashMap<String, String>();
		map.put("uid", uid);
		map.put("key", key);
		map.put("type", "baiduSearch");
		newTask.put("extedata",map);
		newTask.put("workbeach","baiduSearch");
		
		String taskid = taskService.insertTask(newTask);
		List<DBObject> ruleList = ruleService.findRule(id);
		for (int i = 0; i < ruleList.size(); i++) {
			DBObject rule =  ruleList.get(i);
			ruleService.copyRule(rule,taskid);
		}
	}
	
	public void copyWeixinSearch(HttpServletRequest req, DBObject newTask) throws JSONException, UnsupportedEncodingException {
		String id = newTask.get("_id").toString();
		String url = "";
		String note = "";
		String key = req.getParameter("key");
		String times = req.getParameter("times");
		String uid = req.getParameter("uid");
		if("0".equals(times)){
			url ="http://weixin.sogou.com/weixin?query="+key+"&tsn=0&p=40040100&sourceid=inttime_all&interation=&type=2&interV=kKIOkrELjbkRmLkElbkTkKIMkrELjboImLkEk74TkKIRmLkEk78TkKILkbELjboN_105333196&ie=utf8&cid=null";
		}else{
			url ="http://weixin.sogou.com/weixin?query="+key+"&tsn=1&p=40040100&sourceid=inttime_day&interation=&type=2&interV=kKIOkrELjbkRmLkElbkTkKIMkrELjboImLkEk74TkKIRmLkEk78TkKILkbELjboN_105333196&ie=utf8&cid=null";
		}
		
		note = "搜狗微信  搜索  " + key;
		newTask.put("url",url);
		newTask.put("uid",uid);
		newTask.put("note",note);
		newTask.put("state","0");
		Map<String, String> map = new HashMap<String, String>();
		map.put("uid", uid);
		map.put("key", key);
		map.put("type", "weixinSearch");
		newTask.put("extedata",map);
		newTask.put("workbeach","baiduSearch");
		
		String taskid = taskService.insertTask(newTask);
		List<DBObject> ruleList = ruleService.findRule(id);
		for (int i = 0; i < ruleList.size(); i++) {
			DBObject rule =  ruleList.get(i);
			ruleService.copyRule(rule,taskid);
		}
	}
}
