package cn.datawin.service;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.bean.page.RespData;

@Service
public class ClientService extends BaseService{
	
	
	public void poll(String workbeach){
		BasicDBObject query = new BasicDBObject();
		query.put("workbeach", workbeach);
		DBObject client = this.findOne("client", query); 
		if (client == null) {
			BasicDBObject newclient = new BasicDBObject();
			newclient.append("workbeach", workbeach).append("note", "").append("time", System.currentTimeMillis());
			super.insert("client", newclient);
		}
	}
	
	public Object list(ReqData reqData, String name){
		BasicDBObject query = new BasicDBObject();
		if(StringUtils.isNotEmpty(name)){
			Pattern  pattern = Pattern.compile("^.*" + name + ".*$", Pattern.CASE_INSENSITIVE);
			query.append("workbeach", pattern);
		}
		RespData<List<DBObject>> rows = super.page("client", query, reqData);
		online(rows.getRows());
		return  rows;
	}
	
	public void online(List<DBObject> list){
		for(DBObject dbo: list){
			Long time =  (Long) dbo.get("time");
			long now = System.currentTimeMillis();
			dbo.put("online", "在线");
			if(now - time > ( 30 * 60 * 1000)){
				dbo.put("online", "掉线");
			}
		}
	}

	public Object add(Form form) {
		BasicDBObject client = new BasicDBObject();
		client.append("workbeach", form.getWorkbeach()).append("note", form.getNote())
			.append("time", System.currentTimeMillis());
		super.insert("client", client);
		return 1;
	}

	public Object edit(Form form) {
		BasicDBObject client = new BasicDBObject();
		client.append("workbeach", form.getWorkbeach()).append("note", form.getNote());
		super.updateByid("client", form.getId(), client);
		return 1;
	}
	
	public Object updatec(String workbeach,String count) {
		BasicDBObject query = new BasicDBObject();
		query.put("workbeach", workbeach);
		DBObject dbObject=super.findOne("client", query);
		if(dbObject.containsKey("catchurls")){
			int catchurls=(Integer)dbObject.get("catchurls");
			BasicDBObject client = new BasicDBObject();
			int c=Integer.parseInt(count);
			if(c==0){
				client.put("last", catchurls);
				client.put("orbusy", "空闲");
				client.put("catchurls", catchurls);
			}else{
				client.put("orbusy", (c+(Integer) dbObject.get("last"))==catchurls?"空闲":"忙碌");
				client.put("catchurls", c+(Integer) dbObject.get("last"));
			}
			client.put("time", System.currentTimeMillis());
			DBObject updateSetValue=new BasicDBObject("$set",client);  
			super.update("client", query, updateSetValue, false, true);
		}else{
			int c=Integer.parseInt(count);
			BasicDBObject client = new BasicDBObject();
			client.put("orbusy", c==0?"空闲":"忙碌");
			client.put("catchurls", c);
			client.put("last", 0);
			client.put("time", System.currentTimeMillis());
			DBObject updateSetValue=new BasicDBObject("$set",client);  
			super.update("client", query, updateSetValue, false, true);
		}
		return 1;
	}

	public Object del(String ids) {
		for(String id: ids.split(",")){
			super.deleteById("client", id);
		}
		return 1;
	}

}
