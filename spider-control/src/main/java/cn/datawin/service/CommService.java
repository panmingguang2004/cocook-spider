package cn.datawin.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBObject;

import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;

/**
 * 基础业务, 通用业务service
 * @author Administrator
 * 
 */
@Service
public class CommService extends BaseService {

	public Object taskList(Form form, ReqData reqData) {
		BasicDBObject query = new BasicDBObject();
		return super.parseObjectId(super.page("taskresult", query, reqData), "clientid");
	}

//	public int remove(Form form) {
//		if(StringUtils.hasLength(form.getIds())){
//			for(String id: form.getIds().split(",")){
//				super.deleteById("taskresult", id);
//			}
//			return 1;
//		}
//		super.remove("taskresult", new BasicDBObject());
//		return 1;
//	}
	
	
	
	
}
