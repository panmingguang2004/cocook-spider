package cn.datawin.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import cn.datawin.bean.Form;
import cn.datawin.bean.page.ReqData;
import cn.datawin.bean.page.RespData;

@Service
public class TimerService extends BaseService{
	
	
	public Object list(ReqData reqData, String name){
		BasicDBObject query = new BasicDBObject();
		if(StringUtils.isNotEmpty(name)){
			Pattern  pattern = Pattern.compile("^.*" + name + ".*$", Pattern.CASE_INSENSITIVE);
			query.append("name", pattern);
		}
		RespData<List<DBObject>> rows = super.page("timer", query, reqData);
		List<DBObject> list=rows.getRows();
		for (int i = 0; i < list.size(); i++) {
			DBObject dbObject=list.get(i);
			if(dbObject.containsKey("tasks")){
				dbObject.put("tasks", dbObject.get("tasks").toString());
			}
			if(dbObject.containsKey("state")){
				dbObject.put("state", dbObject.get("state").toString().equals("0")?"已启动":"已关闭");
			}
		}
		return  rows;
	}
	

	public Object add(Form form) {
		BasicDBObject client = new BasicDBObject();
		client.append("name", form.getName()).append("note", form.getNote()).append("datelong", Integer.parseInt(form.getDatelong()));
		super.insert("timer", client);
		return 1;
	}

	public Object edit(Form form) {
		BasicDBObject client = new BasicDBObject();
		client.append("name", form.getName()).append("note", form.getNote()).append("datelong", Integer.parseInt(form.getDatelong()));
		super.updateByid("timer", form.getId(), client);
		return 1;
	}
	
	public Object del(String ids) {
		for(String id: ids.split(",")){
			super.deleteById("timer", id);
		}
		return 1;
	}

	public DBObject getById(String id){
		return super.findOne("timer", new BasicDBObject("_id",new ObjectId(id)));
	}
	
	public void runtask(String ids) {
		if(!StringUtils.isNotEmpty(ids))return;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String[] id = ids.split(",");
		for(String _id : id){
			DBObject dbObject=getById(_id);
			this.updateMultiSingle("timer", dbObject, new BasicDBObject("state", "0").append("exctime", sdf.format(new Date())));
		}
	}
	
	public void stoptask(String ids) {
		if(!StringUtils.isNotEmpty(ids))return;
		String[] id = ids.split(",");
		for(String _id : id){
			DBObject dbObject=getById(_id);
			this.updateMultiSingle("timer", dbObject, new BasicDBObject("state", "1"));
		}
	}
	
	public void addtimertask(String tids,String tmid){
		if(!StringUtils.isNotEmpty(tids))return;
		if(!StringUtils.isNotEmpty(tmid))return;
		String[] tid = tids.split(",");
		Map<String, String> map=new HashMap<String, String>();
		for (int i = 0; i < tid.length; i++) {
			map.put(tid[i], this.findOne("task", new BasicDBObject("_id",new ObjectId(tid[i]))).get("note").toString());
		}
		DBObject o=this.findOne("timer",new BasicDBObject("_id",new ObjectId(tmid)));
		if(o.containsKey("tasks")){
			DBObject dbObject=(DBObject) o.get("tasks");
			for(String set:dbObject.keySet()){
				map.put(set, dbObject.get(set).toString());
		   }
		}
		this.updateMultiSingle("timer",new BasicDBObject("_id",new ObjectId(tmid)),new BasicDBObject("tasks",new BasicDBObject(map)));
	}
}
