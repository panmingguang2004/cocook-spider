package cn.datawin.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import cn.datawin.bean.User;
import cn.datawin.util.MD5;


@Service
public class UserService extends BaseService{
	
	public User checkUser(String username,String password) {
		String col="user";
		MD5 md5 = new MD5();
		DBObject query = new BasicDBObject();
		query.put("username", username);
		query.put("password", md5.MD5Encode(password));
		User user = this.getBean(col, query, User.class);
		return user;
	}
	
	
	
}
