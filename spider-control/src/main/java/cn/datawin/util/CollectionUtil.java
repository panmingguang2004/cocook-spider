package cn.datawin.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.mongodb.DBObject;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;

public class CollectionUtil {
	
	static Random random = new Random();
	
	/*map随机值*/
	public static <T> T  getRandomValue(Map<?,T> map){
		Object key =getRandomKey(map.keySet());
		return map.get(key);
	}
	
	/*随机key*/
	public static <T> T getRandomKey(Set<T> set){
		List<T> list = new ArrayList<T>(set);
		return list.get(random.nextInt(set.size()));
	}
	/*list随机值*/
	public static <T> T getRandomVal(List<T> list){
		return list.get(random.nextInt(list.size()));
	}
	
	/*[{"aa":"123"},{"bb": "234"}]  -->{"aa":"123","bb":"234" }*/
	public static Map<String, Object> toMap(List<DBObject> list, String key, String value){
		Map<String, Object> map = new HashMap<String, Object>();
		for(DBObject dbo: list){
			map.put(dbo.get(key).toString(), dbo.get(value));
		}
		return map;
	}
	
	/**
	 * 根据key 合并 两个 list
	 * @param list1
	 * @param list2
	 * @param key
	 * @return
	 */
	public static List<Map<String, Object>>  merger(List<Map<String, Object>> list1, List<Map<String, Object>> list2, String key){
		for(Map<String, Object> map1 : list1){
			Object obj = map1.get(key);
			for(Map<String, Object> map2: list2){
				Object obj2 = map2.get(key);
				if(obj.equals(obj2)){
					map1.putAll(map2);
					break;
				}
			}
		}
		return list1;
	}
	
	
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("a", "a");
		map.put("b", "b");
		map.put("c", "c");
		map.put("d", "d");
		
		System.out.println(getRandomValue(map));
		
		
	}

}
