package cn.datawin.util;

import org.apache.log4j.Logger;

public class CommUtil {
	
	static Logger log= Logger.getLogger("commLogger");
	
	static int i =0;
	
	public static void info(Object message){
		log.info(message);
	}
	
	public static void info(Object message, Throwable e){
		log.info(message, e);
	}
	
	public static boolean do_(){
		i++;
		/**
		 * 注意 数字的控制
		 */
		if(i%10 ==0){
			return true;
		}
		return false;
	}

	
	
	
}
