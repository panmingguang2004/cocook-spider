package cn.datawin.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static Date getDayAfter(Date edate) {  
        Calendar c = Calendar.getInstance();  
        c.setTime(edate);  
        int day = c.get(Calendar.DATE);  
        c.set(Calendar.DATE, day + 1);  
        return c.getTime();  
    }
}
