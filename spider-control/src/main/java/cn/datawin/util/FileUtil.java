package cn.datawin.util;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.util.Enumeration;
import javax.imageio.ImageIO;

//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFCellStyle;
//import org.apache.poi.hssf.usermodel.HSSFRichTextString;
//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class FileUtil {

	/**
	 * 删除文件
	 * 
	 * @param filePath
	 *            文件绝对路径
	 */
	public static void delFile(String filePath) {
		File myDelFile = new File(filePath);
		if (myDelFile.exists()) {
			myDelFile.delete();
		}
	}

	/**
	 * 获取文件大小
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static long getFileSize(File file) throws IOException {
		long s = 0;
		if (file.exists()) {
			return file.length();
		}
		return s;
	}

	/**
	 * 转换文件大小
	 * 
	 * @param files
	 * @return
	 */
	public static String formateFileSize(long files) {
		DecimalFormat df = new DecimalFormat("#.00");
		String fileSizeString = "";
		if (files < 1024) {
			fileSizeString = df.format((double) files) + "B";
		} else if (files < 1048576) {
			fileSizeString = df.format((double) files / 1024) + "K";
		} else if (files < 1073741824) {
			fileSizeString = df.format((double) files / 1048576) + "M";
		} else {
			fileSizeString = df.format((double) files / 1073741824) + "G";
		}
		return fileSizeString;
	}


	public static void copy(File src, File dest) throws IOException {
		if (!dest.exists())
			dest.createNewFile();
		transfer(new FileInputStream(src), new FileOutputStream(dest));
	}

	public static void transfer(FileInputStream src, FileOutputStream dest) throws IOException {
		FileChannel source = null;
		FileChannel target = null;
		try {
			source = src.getChannel();
			target = dest.getChannel();
			long position = 0, size = source.size();
			while ((position += source.transferTo(position, size - position, target)) < size)
				;
		} finally {
			if (src != null) {
				try {
					source.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (dest != null) {
				try {
					target.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	public static void copy(InputStream _in, OutputStream _out) {
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new BufferedInputStream(_in);
			out = new BufferedOutputStream(_out);
			byte[] b = new byte[1024];
			int len = 0;
			while ((len = in.read(b, 0, b.length)) != -1) {
				out.write(b, 0, len);
			}
			out.flush();
		} catch (Exception e) {
		} finally {
			close(in, out);
		}
	}

	/**
	 * add by panmg copy一个文件夹下面的文件到另一个文件夹
	 * 
	 * @param source
	 *            必须是文件夹
	 * @param target
	 *            必须是文件夹
	 * @throws IOException
	 */
	public static void copyFolder(String source, String target) throws IOException {
		new File(target).mkdirs(); // 目标文件夹
		File fsource = new File(source);
		String[] fNames = fsource.list();
		for (String name : fNames) {
			File temp = new File(source + "/" + name);
			if (temp.isFile()) {
				FileInputStream in = null;
				FileOutputStream out = null;
				try {
					in = new FileInputStream(temp);
					out = new FileOutputStream(target + "/" + name);
					byte[] b = new byte[10240];
					int len;
					while ((len = in.read(b)) != -1) {
						out.write(b, 0, len);
					}
				} finally {
					if (out != null)
						out.close();
					if (in != null)
						in.close();
				}
			} else {
				copyFolder(source + "/" + name, target + "/" + name);
			}

		}
	}

	/**
	 * add by panmg 删除某个文件夹中的文件
	 * 
	 * @param ObjectPath
	 */
	public static void deleteDirectory(String ObjectPath) {
		File file = new File(ObjectPath);
		if(!file.exists()) return;
		File[] files = file.listFiles();
		for (File f : files) {
			if (f.isFile()) {
				f.delete();
			} else {
				/* 先删除文件夹的内文件, 在删文件夹 */
				deleteDirectory(ObjectPath + "/" + f.getName());
				f.delete();
			}
		}
	}

	/**
	 * add by panmg
	 * 
	 * @param out
	 * @param f
	 * @param base
	 * @throws Exception
	 */
	@Deprecated
	public static void makZip(ZipOutputStream out, File f, String base) throws Exception {
		if (f.isDirectory()) {
			File[] fl = f.listFiles();
			base = base.length() == 0 ? "" : base + File.separator;
			for (int i = 0; i < fl.length; i++) {
				makZip(out, fl[i], base + fl[i].getName());
			}
		} else {
			if (f.isHidden() || f.lastModified() == 0)
				return;
			out.putNextEntry(new ZipEntry(base));

			FileInputStream in = new FileInputStream(f);
			int len = -1;
			byte[] b = new byte[1024];
			while ((len = in.read(b)) != -1) {
				out.write(b, 0, len);
			}
			in.close();
		}
	}
	
	/**
	 * 单层文件压缩
	 * @param zos
	 * @param file
	 */
	public static void makFileZip(ZipOutputStream zos, File file) {
		File[] files = file.listFiles();
		InputStream in = null;
		try {
			for (int i = 0; i < files.length; i++) {
				/* 将每个File作为压缩key,读取每个File流 */
				ZipEntry entryitem = new ZipEntry(files[i].getName());

				zos.putNextEntry(entryitem);
				in = new FileInputStream(files[i]);
				byte[] b = new byte[10240];
				int len = -1;
				while ((len = in.read(b)) != -1) {
					zos.write(b, 0, len);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(in, zos);
		}
	}
	
	
	/**
	 * 解压缩一个文件到指定文件夹 支持中文 add by panmg
	 * 
	 * @param zf
	 *            压缩文件
	 * @param folderPath
	 *            解压缩的目标目录
	 */
	public static void unZipFile(ZipFile zf, String folderPath) {
		File desDir = new File(folderPath);
		if (!desDir.exists()) {
			desDir.mkdirs();
		}
		for (Enumeration<?> entries = zf.getEntries(); entries.hasMoreElements();) {
			ZipEntry entry = ((ZipEntry) entries.nextElement());
			String str = folderPath + File.separator + entry.getName();
			File desFile = new File(str);
			InputStream in = null;
			OutputStream out = null;
			try {
				if (!desFile.exists()) {
					File fileParentDir = desFile.getParentFile();
					if (!fileParentDir.exists()) {
						fileParentDir.mkdirs();
					}
					desFile.createNewFile();
				}
				in = zf.getInputStream(entry);
				out = new FileOutputStream(desFile);
				byte buffer[] = new byte[1024];
				int len;
				while ((len = in.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}
			} catch (Exception e) {
			} finally {
				if (in != null)
					try {
						in.close();
					} catch (IOException e) {
					}

				if (out != null)
					try {
						out.close();
					} catch (IOException e) {
					}
			}
		}

	}


	public static void close(InputStream in, OutputStream out) {
		if (in != null) {
			try {
				in.close();
			} catch (IOException e) {
				in = null;
			}
		}
		if (out != null) {
			try {
				out.close();
			} catch (IOException e) {
				in = null;
			}
		}
	}
	


	/**
	 * add by panmg
	 * 
	 * @param src
	 * @param desc
	 * @param width
	 * @param height
	 * @param proportion
	 * @throws IOException
	 */
	public static void compressPic(InputStream in, OutputStream out, int width, int height, boolean proportion) throws IOException {
		// 获得源文件
		java.awt.Image img = ImageIO.read(in);
		if(img.getWidth(null)==width && img.getHeight(null)==height)  return;
		int newWidth, newHeight;
		// 判断是否是等比缩放
		if (proportion == true) {
			// 为等比缩放计算输出的图片宽度及高度
			double rate1 = ((double) img.getWidth(null)) / (double) width + 0.1;
			double rate2 = ((double) img.getHeight(null)) / (double) height + 0.1;
			// 根据缩放比率大的进行缩放控制
			double rate = rate1 > rate2 ? rate1 : rate2;
			newWidth = (int) ((img.getWidth(null)) / rate);
			newHeight = (int) ((img.getHeight(null)) / rate);
		} else {
			newWidth = width; // 输出的图片宽度
			newHeight = height; // 输出的图片高度
		}

		BufferedImage tag = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
		/*
		 * Image.SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
		 */
		tag.getGraphics().drawImage(img.getScaledInstance(newWidth, newHeight, java.awt.Image.SCALE_SMOOTH), 0, 0, null);
		// JPEGImageEncoder可适用于其他图片类型的转换
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		encoder.encode(tag);
		out.close();
	}
	
//	/**
//	 * 文件导出 add by panmg
//	 * @param list
//	 * @param sheetName
//	 * @param header
//	 * @return
//	 */
//	public static HSSFWorkbook createExcel(List<Map<String, Object>> list, String sheetName,String... header){
//		
//		HSSFWorkbook excel = new HSSFWorkbook();
//		HSSFSheet sheet = excel.createSheet(sheetName);
//		int rowIndex = 0; // 记录当前列
//		/* 解析标题  begin*/
//		if(header.length>0){ 
//			HSSFRow row = sheet.createRow(rowIndex);
//			for(int i=0;i<header.length;i++){
//				HSSFCell cell = row.createCell(i);
//				cell.setCellValue(new HSSFRichTextString(header[i]));
//			}
//			rowIndex++;
//		}
//		/* 解析标题  end*/
//		
//		for(int i=0; i<list.size(); i++){
//			HSSFRow row = sheet.createRow(rowIndex);
//			Map<String, Object> map = list.get(i);
//			/* 解析列名 begin */
//			if(i==0){  
//				HSSFCellStyle style = excel.createCellStyle();
//				setCellStyle(style);
//				int j=0;
//				for(String key : map.keySet()){
//					HSSFCell cell = row.createCell(j);
//					cell.setCellStyle(style);
//					cell.setCellValue(key);
//					j++;
//				}
//				rowIndex++; //跳过column 那列
//			}
//			/* 解析列名 end */
//			
//			
//			/* 解析列数据 begin*/
//			row = sheet.createRow(rowIndex);
//			int j = 0;
//			for(String key : map.keySet()){
//				sheet.setColumnWidth(j, 2500); //设置固定宽度
//				//sheet.autoSizeColumn(j); //自适应 单元格宽度
//				HSSFCell cell = row.createCell(j);
//				Object value = map.get(key);
//				cell.setCellValue(String.valueOf(value));
//				j++;
//			}
//			rowIndex++;
//			/* 解析列数据 end*/
//		}
//		
//		return excel;
//	}
//	
//	public static void setCellStyle(HSSFCellStyle cellStyle){
//		cellStyle.setBorderTop((short) 1);
//		cellStyle.setBorderLeft((short) 1);
//		cellStyle.setBorderRight((short) 1);
//		cellStyle.setBorderBottom((short) 1);
//		cellStyle.setAlignment((short) 2);
//		cellStyle.setVerticalAlignment((short) 1);
//		cellStyle.setFillForegroundColor((short) 22);
//		cellStyle.setFillPattern((short) 1);
//		cellStyle.setWrapText(true);
//	}
	
}
