package cn.datawin.util;

import java.io.InputStream;

import org.jsoup.Jsoup;

import cn.datawin.spider.seletor.Html;

public class HtmlImg extends Html{
	
	private String url;
	
	public HtmlImg() {
	}
	
	public HtmlImg(String text) {
		super(text);
	}
	
	@Override
	public void init(String text) {
		super.init(text);
	}
	
	public void execjs(){
//		init(WebDriver.get(url));
	}
	/*
	public byte[] cut(){
		return WebpageScreenshot.getInstance().cut(getUrl());
	}*/

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		addElement(Jsoup.parse("<input id='purl' value='"+url+"'/>"));
		this.url = url;
	}
	
	public InputStream img(){
		
		return null;
	}
	public String purl(){
		return getUrl();
	}
	
	public static void main(String[] args) {
		HtmlImg html = new HtmlImg("hello");
		html.setUrl("http://baidu.com");
		System.out.println(html.$("#purl"));
	}
	
}
