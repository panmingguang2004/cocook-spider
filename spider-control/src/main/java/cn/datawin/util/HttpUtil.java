package cn.datawin.util;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;

import cn.datawin.spider.httputil.HttpClientService;
import cn.datawin.spider.httputil.HttpRequest;
import cn.datawin.spider.httputil.HttpRequest.Method;
import cn.datawin.spider.httputil.HttpResponse;
import cn.datawin.spider.httputil.HttpService;

public class HttpUtil {
	private static ArrayList<HttpService> services =  new ArrayList<HttpService>();
	static Random random = new Random();
	static{
		services.add(new HttpClientService());
		services.add(new HttpClientService());
		services.add(new HttpClientService());
		services.add(new HttpClientService());
		services.add(new HttpClientService());
		services.add(new HttpClientService());
	}
	
	public static HttpService getHttpClient(){
		return services.get(random.nextInt(services.size()));
	}
	
	public static List<Map<String, Object>> get(String url, Map<String, String> params) throws Exception{
		return get(url + toString(params));
	}
	
	public static List<Map<String, Object>> get(String url) throws Exception{
		return get(url, "UTF-8");
	}
	
	public static List<Map<String, Object>> get(String url, String charset, Map<String, String> params) throws Exception{
		return get(url+ toString(params), charset);
	}
	
	public static List<Map<String, Object>> get(String url, String charset) throws Exception{
		return (List<Map<String, Object>>) deserialize(getStr(url, charset));
	}
	
	public static String getStr(String url, String charset) throws Exception{
		HttpRequest request = new HttpRequest(url, Method.get);
		request.setCharset(charset);
		HttpResponse res =  getHttpClient().execute(request);
		return res.getResponseString(charset);
	}
	
	public static String post(String url, Map<String, String> params) throws Exception{
		return postStr(url, "UTF-8", params);
	}
	
	public static String postStr(String url,String charset, Map<String, String> params) throws Exception{
		HttpRequest request = new HttpRequest(url, Method.post);
		request.setCharset(charset);
		request.setPostParams(params);
		HttpResponse res =  getHttpClient().execute(request);
		return res.getResponseString(charset);
	}
	
	public static Object deserialize(String str){
		return JSONArray.parseArray(str, LinkedHashMap.class);
	}
	
	public static String serialize(Object obj){
		try{
			return JSON.toJSONString(obj, SerializerFeature.DisableCircularReferenceDetect, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String encode(String url) {
		try {
			return URLEncoder.encode(url, "utf8");
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String toString(Map<String, String> params){
		StringBuffer buffer = new StringBuffer();
		for(String key: params.keySet()){
			if(buffer.length()>0){
				buffer.append("&");
			}
			buffer.append(key).append("=").append(encode(params.get(key)));
		}
		return buffer.toString();
	}
	
	
	public static void main(String[] args) throws Exception {
		System.out.println(""); 
	}
	

}
