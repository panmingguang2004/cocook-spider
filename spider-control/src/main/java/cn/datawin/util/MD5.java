package cn.datawin.util;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonplugin.JSONException;
import com.googlecode.jsonplugin.JSONUtil;

/**
 * @作者 yq
 * @创建日期  2010-11-5
 * @版本 V 1.0
 * @描述 MD5加密
 */
public class MD5 {
	private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

	/**
	 * 转换字节数组为16进制字串
	 * 
	 * @param b
	 *            字节数组
	 * @return 16进制字串
	 */

	public static String byteArrayToHexString(byte[] b) {
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			resultSb.append(byteToHexString(b[i]));
		}
		return resultSb.toString();
	}

	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n = 256 + n;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}

	public static String MD5Encode(String origin) {
		String resultString = null;

		try {
			resultString = new String(origin);
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byteArrayToHexString(md.digest(resultString
					.getBytes()));
		} catch (Exception ex) {

		}
		return resultString;
	}
	
	 public static byte[] encryptMD5(byte[] data) throws Exception {  
        MessageDigest md5 = MessageDigest.getInstance("MD5");  
        md5.update(data);  
        return md5.digest();  
    }  
	
	 public static long md5_long(String s){
		BigInteger md5 = null;
		try {
			md5 = new BigInteger(1,encryptMD5(s.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return md5.longValue();
	 }
	 
	public static void main(String[] args) throws Exception {
		BigInteger md5 = new BigInteger(1,encryptMD5("http://HS0606.bmlink.com/company".getBytes()));
		BigInteger md51 = new BigInteger(-1,encryptMD5("http://HS0606.bmlink.com/company".getBytes()));
		System.out.println(md5.longValue());
		System.out.println(md5.toString(16));
		
		//-7191231423097668688
		//18dd51b5dda6128c9c339d1f18a2fbb0
		// 7191231423097668688
		//-18dd51b5dda6128c9c339d1f18a2fbb0

	}

}