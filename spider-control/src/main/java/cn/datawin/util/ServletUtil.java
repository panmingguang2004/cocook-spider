package cn.datawin.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.jsonplugin.JSONException;
import com.googlecode.jsonplugin.JSONUtil;

public class ServletUtil {
	
	private static Log log = LogFactory.getLog("ServletUtil");
	
	public static void printJSON(HttpServletResponse res,String obj) throws IOException {
		res.setContentType("application/json;charset=UTF-8"); // "text/html; charset=utf-8"
		res.setHeader("Pragma", "no-cache");
		res.setHeader("Cache-Control", "no-cache");
		res.setDateHeader("Expires", 0L);
//		res.setHeader("Content-Encoding", "gzip"); 
//		PrintWriter pw = new PrintWriter(new GZIPOutputStream(res.getOutputStream()));
//		pw.write(obj);  // 编码指定
//		pw.flush();
//		pw.close();
		res.getWriter().write(obj);
	}
	
	public static String toJsonString(Object obj){
		try {
			return (obj instanceof String) ? (String)obj : com.googlecode.jsonplugin.JSONUtil.serialize(obj);
		} catch (JSONException e) {
			e.printStackTrace();
			log.info("toJsonString:", e);
			return null;
		}
	}
	
	public static Object deserialize(String json){
		try {
			return JSONUtil.deserialize(json);
		} catch (JSONException e) {
			e.printStackTrace();
			log.info("deserialize:", e);
			return null;
		}
	}
	
	public static String getIpAddr(HttpServletRequest request){
		String ip = request.getHeader("X-REAL-IP");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	
	public static Object invoke(Method m, Object obj, Object... params) {
		try {
			return m.invoke(obj, params);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("invoke:", e);
		}
		return new Object();
	}
	
	
   public static PrintWriter createGzipPw(HttpServletResponse resp) throws IOException {  
        PrintWriter pw =  new PrintWriter(new GZIPOutputStream(resp.getOutputStream()));  
        return pw;  
	}  
   
   public static String gziptoString(ServletRequest req){
	   try{
		   GZIPInputStream in = new GZIPInputStream(req.getInputStream());
			byte [] b= new byte[1024];
			int temp =0;
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			while((temp = in.read(b,0,b.length))!=-1){
				out.write(b, 0, temp);
			}
			out.flush();
			in.close();
			out.close();
			return out.toString("UTF-8");
	   }catch(IOException e){
		   log.error(e);
		   return null;
	   }
   }
	
	public static void main(String[] args) throws JSONException {
		String str = "[{\"jsonPath\":null,\"name\":\"captcha\",\"rule\":null,\"type\":\"string\"}]";
		
		System.out.println(JSONUtil.deserialize(str));
	}
}
