package cn.datawin.util;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class SpiderUtil {
	static Map<String, Method> htmlMethod = new HashMap<String, Method>();
	static{
		//selector xpath regex
		try{
			Class<?> _class = String.class;
			htmlMethod.put("selector", HtmlImg.class.getMethod("$", _class));
			htmlMethod.put("$", HtmlImg.class.getMethod("$", _class));
			htmlMethod.put("find", HtmlImg.class.getMethod("$", _class));
			htmlMethod.put("xpath", HtmlImg.class.getMethod("xpath", _class));
			htmlMethod.put("regex", HtmlImg.class.getMethod("regex", _class));
			htmlMethod.put("regexAll", HtmlImg.class.getMethod("regexAll", _class));
			htmlMethod.put("attr", HtmlImg.class.getMethod("attr", _class));
			htmlMethod.put("jsonpath", HtmlImg.class.getMethod("jsonPath", _class));  // jsonpath 表达式
			htmlMethod.put("html", HtmlImg.class.getMethod("html"));
			htmlMethod.put("first", HtmlImg.class.getMethod("first"));
			htmlMethod.put("last", HtmlImg.class.getMethod("last"));
			htmlMethod.put("text", HtmlImg.class.getMethod("text"));
			htmlMethod.put("img", HtmlImg.class.getMethod("img"));  
			htmlMethod.put("purl", HtmlImg.class.getMethod("purl"));  // 直接取当前URL 作为分页URL
//			htmlMethod.put("cutImg", HtmlImg.class.getMethod("cut"));  // 注释掉, 使用了 selenium 包超级多
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static Method getHtmlMethod(String type){
		return htmlMethod.get(type);
	}
	
	public static Object invokeMethod(Method method, Object obj, Object... params){
		try {
			return method.invoke(obj, params);
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	
	

}
