//package cn.datawin.util;
//
//import java.util.ArrayList;
//import java.util.Random;
//
//import org.openqa.selenium.WebDriverException;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
//
//public class WebDriver {
//
//	private static ArrayList<HtmlUnitDriver> drivers = new ArrayList<HtmlUnitDriver>();
//	static Random random = new Random();
//
//	static {
//		drivers.add(new HtmlUnitDriver(true));
//		drivers.add(new HtmlUnitDriver(true));
//		drivers.add(new HtmlUnitDriver(true));
//	}
//
//	public static HtmlUnitDriver getDriver() {
//		return drivers.get(random.nextInt(drivers.size()));
//	}
//
//	public static String get(String url) {
//		HtmlUnitDriver driver = getDriver();
//		synchronized (driver) {
//			try {
//				driver.get(url);
//			} catch (WebDriverException e) {
//				System.out.println(e);
//			}
//			return driver.getPageSource();
//		}
//	}
//
//}
