-- url 去重表 - 单表 32T
create table url(
 id bigserial primary key,
 href text 
);

-- 插入 500数据测试
insert into url select r, 
reverse('http://cz.58.com/bijibendiannao/21344265890186x.shtml?PGTID=14267543077580.5493437256664038&ClickID='|| r )
from  generate_series(1,5000000) as r;


-- URL 去重方式
/**
pgsql + url 压缩
1. url 压缩通过md5 加密, 加密后转 long 值 保存到数据库
2. 做id 与 url 的映射 , id 加索引就好了, longValue, 超过一定的数量级别 可能会重复, 可以忽略
**/


----mysql 4T

