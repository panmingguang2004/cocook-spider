<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<table  id="testgrid" >

</table>

<script>
	$("#testgrid").datagrid({
    		iconCls:'icon-back',
			title : '测试',
			url: path + '/index/test',
			toolbar:['-',{ text:'添加',  iconCls:'icon-add',  handler: open },
					'-', {text:'修改',   iconCls:'icon-edit',  handler: null},  
		       		 '-',{text:'删除',  iconCls:'icon-remove',  handler: null},'-'],
		    columns:[[
			   	{field:'ck',checkbox:true},
			    {field:'_id',title:'id',align:'left',width:150,sortable:true},
			    {field:'name',title:'菜单名称',align:'left',width:150,sortable:true},
			    {field:'href',title:'链接',align:'left',width:150,sortable:true}																		
			]]
			//sortName:'name',
			//sortOrder:'desc'
    	})
    	
    	function open(){
    		$.JTUI.dialog({
    			title:"弹出框",
    			modal: true,
		        url: null,
		        content: "这是一个弹出框的测试....",
		        onCancel: function(dialog) {return true},
		        onSure: function(dialog) {},
		        initData: function(dialog) {}
    		})
    	}

</script>

