<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="oclientF" onsubmit="searcholList();return false;">
  <input type="text" name="acc"  placeholder="类型1|2|3" id="search">
  <button onclick="searcholList();" type="button" class="btn btn-primary">Search</button>
<!--  <div class="alert" style="display: inline-block; margin:0 0 0 20px;;padding:3px;"> 
  	QQ 登录 qun.qq.com 使用上面的 加群操作</div>-->
</form>

<form class="hide" action="" id="editAccount" method="post">
	<input  type="hidden" name="id">
	<table>
		<tr>
			<td>账户</td>
			<td><input name="acc" type="text"/></td> 
		</tr>
		<tr>
			<td>密码</td>
			<td><input name="pwd" type="text"/></td> 
		</tr>
		
		<tr>
			<td>类别</td>
			<td><input name="type" type="text"/></td> 
		</tr>
		
	</table>
</form>

<div style="height: 93.5%;">
	<table id="oclientList" style="padding:10px" ></table>
</div>

<script>
	var oclientList = $("#oclientList").datagrid({
			//title : '测试',
			url: path+'/onlinec/list',
			fitColumns: true,
			toolbar:['-',
			    { text:'添加账号',  iconCls:'icon-remove',  handler: addaccount},'-',
			    { text:'修改账号',  iconCls:'icon-add',  handler: updaccount},'-',
				{ text:'删除账号',  iconCls:'icon-remove',  handler: delaccount},'-',
				{ text:'分配vps登录',  iconCls:'icon-remove',  handler: allotaccount},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'acc',title:'账号',align:'center',width:150},
			    {field:'pwd',title:'密码',align:'center',width:150},
			    {field:'type',title:'类别',align:'center',width:150},
			    {field:'state',title:'状态',align:'center',width:150}
	            ]],
			pagination: true
    	})
    	
   	function searcholList(){
		oclientList.datagrid("load",$("#oclientF").jsonForm());
	}
    	
	function addaccount(){
		$.JTUI.dialog({
			title: '添加客户端',
			modal: true,
			content:"#editAccount",
			onSure:function(d){
			
			    var acc=d.find("[name=acc]").val();
				var pwd=d.find("[name=pwd]").val();
				var t=d.find("[name=type]").val();
				if(acc=="" && pwd=="" && t==""){
				  alert("账号密码类别都不能为空！");
				  return false;
				}
				
			    d.find("form").ajaxSubmit({
				action: path+"/onlinec/add",
				success: function(){alert("success");
				greload(oclientList);},
			});
			return true;	
		 }			
		  });
		}

	function updaccount(){
		var rows = gcheckedRow(oclientList);
		if(rows==-1){
			alert("至少且只能选择一条数据进行操作");
			return ;
		}
		$.JTUI.dialog({
			title:'修改客户端信息',
			content: "#editAccount",
			initData: function(d){ 
				d.find("[name=id]").val(rows._id);
				d.find("[name=acc]").val(rows.acc);
				d.find("[name=pwd]").val(rows.pwd);
				d.find("[name=type]").val(rows.type);
				},
			modal: true,		
			onSure:function(d){	
				d.find("form").ajaxSubmit({
					action: path+"/onlinec/edit",
					success: function(){alert("success");
					greload(oclientList);},
				});
				return true;	
			}	
		});
		
	}
	
  	function delaccount(){
  		var rows = gchecked(oclientList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		if(confirm("确认删除")){
			$.get(path+"/onlinec/del?ids="+rows.join(","),function(data){
				if(data==1){
					greload(oclientList);
				}else{
					alert("操作失败");
				}
			},"text")
		}
	}
	
	function allotaccount(){
		var row = gcheckedRow(oclientList);
		if(row==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.JTUI.dialog({
			title:'在线client列表',
			url: path+"/comm/f?p=account/kalist",
			modal:true,
			onSure:function(){
				var id = getonclist();
				if(null==id){
					return false;
				}
				$.get(path+"/cotask/acclogin?workbeach="+id+"&aid="+row._id,function(d){
					if(d==1){
						greload(oclientList);
					}else{
						alert("操作失败");
					}
				},"text")
	    		return true;
	    		
    		}
		})
	}
	
</script>
