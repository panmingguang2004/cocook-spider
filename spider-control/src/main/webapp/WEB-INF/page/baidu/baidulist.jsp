<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="baiduF" onsubmit="searchbaiduList();return false;">
	<select id="type" name="type">
		<option value="all">全部</option>
		<option value="zhidao">百度知道</option>
		<option value="souba">贴吧搜吧</option>
		<option value="quanba">贴吧全吧搜索</option>
		<option value="banei">贴吧指定吧搜索关键字</option>
		<option value="baike">百度百科</option>
	</select>
	<input type="text" name="key" id="key">
  <button onclick="searchbaiduList();" type="button" class="btn btn-primary">Search</button>
<!--  <div class="alert" style="display: inline-block; margin:0 0 0 20px;;padding:3px;"> 
  	QQ 登录 qun.qq.com 使用上面的 加群操作</div>-->
</form>

<form class="form-inline" id="catchF" onsubmit="copybaiduList();return false;">
	<select id="catchtype" name="catchtype" onchange="change()">
		<option value="zhidao">百度知道</option>
		<option value="souba">贴吧搜吧</option>
		<option value="quanba">贴吧全吧搜索</option>
		<option value="banei">贴吧指定吧搜索关键字</option>
		<option value="baike">百度百科</option>
	</select>
	关键字<input type="text" name="catchkey" id="catchkey">
	吧名<input type="text" name="catchname" id="catchname" readonly="readonly">
  <button onclick="copybaiduList();" type="button" class="btn btn-primary">开始抓取</button>
<!--  <div class="alert" style="display: inline-block; margin:0 0 0 20px;;padding:3px;"> 
  	QQ 登录 qun.qq.com 使用上面的 加群操作</div>-->
</form>

<div style="height:80%;">
	<table id="baiduList" ></table>
</div>w
<script>
	function change(){
		if ($("#catchtype").val() == "banei") {
			document.getElementById("catchname").readOnly = false;
		}else{
			$("#catchname").val("");
			document.getElementById("catchname").readOnly = true;
		}
	}
	
	
	
	var baiduList = $("#baiduList").datagrid({
		//title : '测试',
		url:path+'/baidu/baiduList',
		fitColumns: true,
		toolbar:['-',
			{ text:'启动任务',  iconCls:'icon-add', handler: runtask},'-',
			{ text:'分配任务',  iconCls:'icon-remove',  handler: allottask},'-',
			{ text:'修改任务',  iconCls:'icon-add',  handler: updtask},'-',
			{ text:'删除任务',  iconCls:'icon-remove',  handler: delbaidu},'-'

			],
	    columns:[[
            {field:'note',title:'任务说明',align:'center',width:30},
		    {field:'url',title:'抓取地址',align:'center',width:30},
		    {field:'workbeach',title:'网站标识符',align:'center',width:30},
		    {field:'method',title:'提交方式',align:'center',width:30},
		    {field:'client',title:'VPS',align:'center',width:30},
		    {field:'header',title:'请求头信息',align:'center',width:30},
		    {field:'crawlerdate',title:'抓取日期',align:'center',width:30},
		    {field:'state',title:'状态',align:'center',width:30}
            ]],
		pagination: true
	});
	
	function searchbaiduList(){
		baiduList.datagrid("load",$("#baiduF").jsonForm());
	}
	
	function runtask(){
		var rows = gchecked(baiduList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.get(path+"/task/runtask?ids="+rows.join(","),function(d){
			if(d==1){
				greload(baiduList);
			}else{
				alert("操作失败");
			}
		},"text");
	}
	
	
	function copybaiduList(){
		var type = $("#catchtype").val();
		var key = $("#catchkey").val();
		if (key == null || key == "") {
			alert("请输入要搜索的字");
		}else if (type == "banei" && $("#name").val() == "") {
			alert("请输入要搜索的字");
		}else {
			$.post(path+"/baidu/copytask",{type:$("#catchtype").val() ,key: $("#catchkey").val(),name:$("#catchname").val()},function(d){
				if(d==1){
					greload(baiduList);
				}else{
					alert("操作失败");
				}
			},"text");
		}
	}
	
	function delbaidu(){
  		var rows = gchecked(baiduList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		if(confirm("确认删除")){
			$.get(path+"/task/deletetask?id="+rows.join(","),function(data){
				if(data==1){
					greload(baiduList);
				}else{
					alert("操作失败");
				}
			},"text");
		}
	}
	
	function updtask(){
		var row = gcheckedRow(baiduList);
		if(row==-1){
			alert("必须且只能选择一条数据");
			return;
		}
		showWindow(row._id,path+"/task/updatetask");
	}
	function showWindow(tid,act){
		$.JTUI.dialog({
			title:'添加/修改任务',
			url: path+"/task/savetask?tid="+tid,
			modal:true,
			initData:function(d){
				d.find("form")[0].action=act;
			},
			onSure:function(){
				submitaddtform();
				greload(baiduList);
	    		return true;
    		}
		});
	}
	
	function allottask(){
		var rows = gchecked(baiduList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.JTUI.dialog({
			title:'WorkBeach列表',
			url: path+"/comm/f?p=task/wblist",
			modal:true,
			onSure:function(){
				var ids = getWBlist();
				if(null==ids){
					return false;
				}
				$.get(path+"/task/allottask?wids="+ids+"&tids="+rows.join(","),function(d){
					if(d==1){
						greload(baiduList);
					}else{
						alert("操作失败");
					}
				},"text");
	    		return true;
	    		
    		}
		});
	}
	
</script>


