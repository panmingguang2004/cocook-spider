<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<div id="alertInfo" class="alert alert-info">
  <h4>打码 大神, 机会来了</h4>
  打开这个标签后会自动接受 验证码图片, 输入结果后 回车 或 按确定 提交
</div>

<div style="width:100%;height:90%; overflow:auto;">
<form  id="dama" class="form-inline hide" >
  <input name="id" value="" type="hidden"/>
  <span>登录</span>	
  <img src="" />
  <input type="text" name="key" class="input-small" placeholder="验证码">
  <button type="submit" class="btn">提交</button>
</form>
<div id="msgbox" class="alert fade in hide">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Holy guacamole!</strong> <span>Best check yo self, you're not looking too good.</span>
</div>
</div>


<script type="text/javascript">
	var Interval;
	if(Interval){
		clearInterval(Interval);
	}
	Interval = setInterval(getCaptcha, 7000);
	
	var alertInfo = $("#alertInfo");
	var dama =  $("#dama");
	function getCaptcha(){
		if(dama.length==0){
			clearInterval(Interval);
		}
		$.get(path+"/captcha/hascaptcha",function(data){
			var d = data.split(",");
			if(d[0]!=="0"){
				var dama_clone = dama.clone().show();
				dama_clone.find("span").html("clientId:"+d[1]);
				dama_clone[0].action=path+"/captcha/postcaptcha";
				dama_clone.find("[name=id]").val(d[0]);
				dama_clone.find("img")[0].src=path+"/captcha/captcha?id="+d[0];
				dama_clone.ajaxForm({
					success: function(){
						dama_clone.remove();
					}
				});
				alertInfo.after(dama_clone);
			}
			
		})
	}
	
	var msgs;
	if(msgs){
		clearInterval(msgs);
	}
	msgs = setInterval(getfailmsgs, 7000);
	
	/*取得打码失败信息*/
	function getfailmsgs(){
		var msgbox =  $("#msgbox");
		if(msgbox.length==0){
			clearInterval(msgs);
		}
		$.get(path+"/captcha/getfailmsg",function(data){
			if(data.length>0){
				for(var i = 0 ; i < data.length ;i++){
					var msgbox_c = msgbox.clone().show();
					msgbox_c.find("strong").html("失败原因");
					msgbox_c.find("span").html(data[i]);
					alertInfo.after(msgbox_c);
				}
			}
		},"json")
	}
</script>


