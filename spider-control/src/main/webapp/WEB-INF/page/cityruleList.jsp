<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="stateCity">
  
</form>

<div style="height: 93.5%;">
	<table id="cityruleList" ></table>
</div>

<script>
	var cityruleList = $("#cityruleList").datagrid({
			//title : '测试',
			url:path+'/cityrule/ruleList',
			fitColumns: true,
			queryParams:{"type":"2"},
			toolbar:['-',
				{ text:'添加城市Rule',  iconCls:'icon-add',  handler: addrulebase},'-',
				{ text:'修改Rule信息',  iconCls:'icon-edit',  handler: editRule},'-',
				{ text:'删除Rule信息',  iconCls:'icon-edit',  handler: deleterulebase},'-',
				{ text:'Rule测试入库',  iconCls:'icon-edit',  handler: testrulebase},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'rulename',title:'rule名称',align:'left',width:150},
			    {field:'type',title:'类别',align:'left',width:150},
			    {field:'expression',title:'表达式',align:'left',width:150},
			    {field:'url',title:'测试url',align:'left',width:150},
			    {field:'belone',title:'分类归属',align:'left',width:150},
			]],
			pagination: true
    	});	
	
	function addrulebase(){
		//console.info(row._id);
		$.JTUI.dialog({
			title: '添加Rule',
			modal: true,
			url: path + '/comm/f?p=ruleBase/addrulebase',
			onSure:function(d){	
			  d.find("form").ajaxSubmit({
				action: path+"/cityrule/insertRuleBase",
				success: function(){alert("success");
				greload(cityruleList);},
			   // error:function(){alert('error')}
			});
			return true;	
		 }			
		  });
		}

	function editRule(){
		var rows = gcheckedRow(cityruleList);
		if(rows==-1){
			alert("至少且只能选择一条数据进行操作");
			return ;
		}
		$.JTUI.dialog({
			title:'修改Rule信息',
			url: path+'/comm/f?p=ruleBase/editrulebase&t='+rows._id, 
			initData: function(d){ 
				d.find("#rulename").val(rows.rulename);
				d.find("#type").val(rows.type);
				d.find("#expression").val(rows.expression);
				d.find("#pageRule").val(rows.pageRule);
				d.find("#name").val(rows.name);
				d.find("#pageSize").val(rows.pageSize);
				d.find("#url").val(rows.url);
				d.find("#belone").val(rows.belone);
				},
			modal: true,		
			onSure:function(d){	
				d.find("form").ajaxSubmit({
					action: path+"/cityrule/updateRuleBase",
					success: function(){alert("success");
					greload(cityruleList);},
				   // error:function(){alert('error')}
				});
				return true;	
			}	
		});
		
	}
	
	function deleterulebase(){
		var row = gcheckedRow(cityruleList);
		
		if(row==-1){
			alert("必须选择一个Rule");
			return;
		}
		
		if(window.confirm("你确定删除这条数据吗？")){
			$.get(path+"/cityrule/deleteRuleBase?rulebaseid="+row._id,function(){
				greload(cityruleList);
			});
			
		}
	}
	
	
	function testrulebase(){
		var row = gchecked(cityruleList);
		if(row==-1){
			alert("至少选择一个Rule");
			return;
		}
		//console.info(row._id);
		$.JTUI.dialog({
			title: '单条Rule测试',
			modal: true,
			url: path+'/cityrule/ruleTestDB?id='+row,
			onSure:function(d){	
			  d.find("form").ajaxSubmit({
				action: path+"/comm/f?p=cityruleList",
				success: function(){alert("success");
				greload(cityruleList);},
			   // error:function(){alert('error')}
			});
			return true;	
		 }			
		  });
		}
		
		
	function delcities(){
		$.JTUI.dialog({
			title:'删除对应城市列表',
			url: path+'/comm/f?p=ruleBase/editrulebase&t='+rows._id, 
			initData: function(d){ 
				d.find("#rulename").val(rows.rulename);
				d.find("#type").val(rows.type);
				d.find("#expression").val(rows.expression);
				d.find("#pageRule").val(rows.pageRule);
				d.find("#name").val(rows.name);
				d.find("#pageSize").val(rows.pageSize);
				d.find("#url").val(rows.url);
				d.find("#belone").val(rows.belone);
				},
			modal: true,		
			onSure:function(d){	
				d.find("form").ajaxSubmit({
					action: path+"/cityrule/updateRuleBase",
					success: function(){alert("success");
					greload(cityruleList);},
				   // error:function(){alert('error')}
				});
				return true;	
			}	
		});
	
	}
	
</script>


