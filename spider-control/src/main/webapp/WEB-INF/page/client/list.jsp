<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="clientlistF" onsubmit="queryClientList();return false;">
	<input name="name"type="text">
	<input class="btn" type="button" value="查询" onclick="queryClientList()">
</form>

<form class="hide" action="" id="editClient" method="post">
	<input  type="hidden" name="id">
	<table>
		<tr>
			<td>workbeach(标识符)</td>
			<td><input name="workbeach" type="text"/></td> 
		</tr>
		<tr>
			<td>备注</td>
			<td><input name="note" type="text"/></td> 
		</tr>
	</table>
</form>

<div style="height: 93.5%;">
	<table id="clientlist" ></table>
</div>
<script>
	var clientlist = $("#clientlist").datagrid({
			url:path+'/client/list',
			fitColumns: true,
			toolbar:['-',
				{ text:'添加client',  iconCls:'icon-add',  handler: addClient},'-',
				{ text:'修改client',  iconCls:'icon-edit',  handler: editClient},'-',
				{ text:'删除client',  iconCls:'icon-remove',  handler: delClient},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'workbeach',title:'client唯一表示',align:'left',width:150},
			    {field:'online',title:'是否在线',align:'left',width:150},
			    {field:'orbusy',title:'是否空闲',align:'left',width:150},
			    {field:'catchurls',title:'已抓取url',align:'left',width:150},
			    {field:'note',title:'备注',align:'left',width:150}
			]],
			pagination: true
    	});	
	
	function addClient(){
		$.JTUI.dialog({
			title: '添加客户端',
			modal: true,
			content:"#editClient",
			onSure:function(d){	
			  d.find("form").ajaxSubmit({
				action: path+"/client/add",
				success: function(){alert("success");
				greload(clientlist);},
			});
			return true;	
		 }			
		  });
		}

	function editClient(){
		var rows = gcheckedRow(clientlist);
		if(rows==-1){
			alert("至少且只能选择一条数据进行操作");
			return ;
		}
		$.JTUI.dialog({
			title:'修改客户端信息',
			content: "#editClient",
			initData: function(d){ 
				d.find("[name=id]").val(rows._id);
				d.find("[name=workbeach]").val(rows.workbeach);
				d.find("[name=note]").val(rows.note);
				},
			modal: true,		
			onSure:function(d){	
				d.find("form").ajaxSubmit({
					action: path+"/client/edit",
					success: function(){alert("success");
					greload(clientlist);},
				});
				return true;	
			}	
		});
		
	}
	
	function delClient(){
		var row = gchecked(clientlist);
		
		if(row==-1){
			alert("必须选择一条数据");
			return;
		}
		
		if(window.confirm("你确定删除这条数据吗？")){
			$.get(path+"/client/del?ids="+row.join(","),function(){
				greload(clientlist);
			});
			
		}
	}
	
	function queryClientList(){
		clientlist.datagrid("load",$("#clientlistF").jsonForm());
		
	}
	
</script>


