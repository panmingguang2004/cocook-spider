<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="dataF" onsubmit="searchdataList();return false;">
<select id="cnames"  name="col">
</select>  
  <button onclick="searchdataList();" type="button" class="btn btn-primary">Search</button>
</form>
<div id="detailbox"  class="hide" style="height:500px; width: 600px;overflow:auto">
<table id="detail" border="1"></table>
</div>
<div style="height: 93.5%;">
	<table id="dataList" ></table>
</div>
<script>
		/**
		 * 查看spiderdata中的数据集合名 
		 **/
		var url=path+"/data/getDataNames";
		$.ajax({
			type:"get",
			url:url,
			async:false,
			success:function(data){
				var html="";
				 $.each(data.names,function(n,v){
					html+="<option value='"+v+"'>"+v+"</option>";
				}); 
				 $("#cnames").html(html);
			}
			
		});
		/**
		 * 显示数据表格
		 **/
		
		var dataList = $("#dataList").datagrid({
			//title : '测试',
			url:path+'/data/getdata',
			fitColumns: true, 
			queryParams:{"type":"2"},
			toolbar:['-',
				{ text:'查看数据',  iconCls:'icon-add',  handler: checkdata},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'url',title:'url',align:'left',width:150},
			    {field:'date',title:'date',align:'left',width:150},
			    {field:'state',title:'state',align:'left',width:150},
			]],
			pagination: true
		}); 
		function searchdataList(){
			dataList.datagrid("load",$("#dataF").jsonForm());
			
		}
		function checkdata(){
			var rows = gchecked(dataList);
			if(rows.length!=1){
				alert("选择一条数据进行操作");
				return ;
			};
			var row=gcheckedRow(dataList);
			$.JTUI.dialog({
				title: 	"细节数据",
				content:"#detailbox", 
				modal: true,
				initData:function(d){
					var detail =  d.find("#detail")
					var html="";
					$.each(row,function(n,v){
						detail.append("<tr><td><div>"+n+"</div></td><td><div style='width:500px;height:40px;overflow:auto;'>"+v+"</div></td></tr>");
					});
				}
			});
		};
	
	
</script>