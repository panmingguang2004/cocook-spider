<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="urlF" onsubmit="searchurlList();return false;">
<input id="urlid"  name="url"/>
  <button onclick="searchurlList();" type="button" class="btn btn-primary">Search</button>
</form>
<div style="height: 93.5%;">
	<table id="urlList" ></table>
</div>
<script>
		var urlList = $("#urlList").datagrid({
			//title : '测试',
			url:path+'/data/geturls',
			fitColumns: true, 
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'id',title:'id',align:'left',width:150},
			    {field:'url',title:'url',align:'left',width:150}
			]]
		}); 
		
		function searchurlList(){
			urlList.datagrid("load",$("#urlF").jsonForm());
		}
		
	
	
</script>