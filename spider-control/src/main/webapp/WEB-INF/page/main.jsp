<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML>
<html>
<head>
<%@include file="/inc/meta.jsp"  %>
<title></title>
</head>
<body>
<%@include file="/inc/head.jsp"  %>
<div id="content" style="width:94%;margin:auto;height:800px;border:1px solid #d7e1f4;">
	<div id="tab">
	</div>
</div>

<%@include file="/inc/footer.jsp"  %>


<script>
$("#topmenu a:not([data-toggle]) ").click(function(event){
	event.preventDefault();
	var name = $(this).html();
	openMenu(name,this.href);
})

$("#content").height(document.documentElement.clientHeight-106);

var tabs = $('#tab');
    tabs.tabs({
        fit: true,
        cache: false,
        border: false,
        tools:[
            {   iconCls:'icon-add', handler: function() {$.get(path+"/index/ceui"); } },
            {   iconCls:'icon-edit', handler: function() {$.get(path+"/tag/addCate"); } }
        ]
    });
    
    /*菜单事件*/
    function openMenu(name,href) {
        if (tabs.tabs('exists', name)) {   //如果tab标签已存在则选中
            tabs.tabs('select', name);
        } else {
            var url = href;
            var option = {
                title:  name,
                //iconCls : 'icon-save',
                href: url,
                closable: true,
                /*tools:[{   
			        iconCls:'icon-mini-refresh',   
			        handler:function(){   
			            tabs.tabs("getSelected").panel('refresh');  
			        }   
			    }] ,*/
            };
            if (url.lastIndexOf('ifr') != -1) { //判断是否以iframe的方式弹出
                delete option.href;  //删除content, 增加url
                option.content = "<iframe scrolling='auto' frameborder='0'  src='" + url + "' style='width:100%;height:100%;'></iframe>";
            }
            var tabcnt = tabs.tabs("tabs").length;    // 使用tabs插件方法
            if (tabcnt < 8) {    //标签个数小于5则新增
                tabs.tabs('add', option);
            } else {     //标签个数大于5替换选择的标签
                var tab = tabs.tabs('getSelected');
                tabs.tabs('update', {
                    tab: tab,
                    options: option
                });
            }
        }
    }
    
    $(window).resize(function(){
    	$("#content").height(document.documentElement.clientHeight-106);
		tabs.tabs("resize");
	});
</script>

</body>
</html>
