<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/loclist.js"></script>
<style>
.menu-first {
	background-color: #E9E9E9;
	border-top: 1px solid #EFEFEF;
	border-bottom: 1px solid #E1E1E1;
	font-size: 14px;
}
</style>
<script>
//js init() 初始化城市联动

</script>
<div style="">
	<form id="searchForm" method="post">
	<input id="id" name="id" value="${t}"  type="hidden" ></input>
		<table>
			<tr>
				<td>规则名称</td>
				<td><input id="rulename" name="rulename" type="text"></input></td>
				<td >类型</td>
				<td>
					<select class="input-small" name="type" id="type">
						<option value="TypeProcessor">TypeProcessor</option>
						<option value="PageProcessor">PageProcessor</option>
						<option value="DataProcessor">DataProcessor</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>分页规则</td>
				<td><input id="pageRule" name="pageRule" type="text"></input></td>
				<td>每页个数</td>
				<td><input id="pageNum" name="pageNum" type="text" class="input-small"></input></td>
			</tr>
			<tr>
				<td>url</td>
				<td><input id="url" name="url" type="text"></input></td>
				<td>分页数</td>
				<td><input id="pageSize" class="input-small" name="pageSize" type="text" value="0"></input></td>
			</tr>
			<tr >
				<td >归属</td>
				<td><input type="text" id="belone" name="belone"></input></td>
				<td>编码方式</td>
				<td><input type="text" id="charset" name="charset" class="input-small" ></input></td>
			</tr>
			<tr >
				<td>提交方式</td>
				<td>
					<input type="radio" name="method1"  value="get" checked="checked">get | 
					<input type="radio" name="method1"  value="post">post
				</td>
			</tr>
			
			<tr >
				<td>提交数据</td>
				<td>
					<textarea name="data">{}</textarea>
				</td>
				<td>typeUrl 规则</td>
				<td>
					<textarea name="urlRule" placeholder="%s 替换,支持多个"></textarea>
				</td>
			</tr>
			<tr>
			<td colspan="4" width=250 style="color:red">
				表达式支持规则: {selector[css选择器],attr,html,text}, xpath, regex <br> 
				例子: [{"name":"url","selector":"div.check a ","attr":"href"},{"name":"address","selector":"div.check ul li ","text":""}]
				<br><a target="_blank" href="http://www.open-open.com/jsoup/selector-syntax.htm">参考css选择器API</a>
			</td>
			</tr>
			<tr>
				<td>表达式</td>
				<td colspan="3"><textarea id="expression" name="expression" style="width:90%; height: 120px;"></textarea></td>
			</tr>
		</table>
	</form>

</div>
