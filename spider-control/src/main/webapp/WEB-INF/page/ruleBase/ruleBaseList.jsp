<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="qqstateF" onsubmit="searchqqstateList();return false;">
  <input type="text" name="key"  placeholder="类型1|2|3" id="search">
  <button onclick="searchqqstateList();" type="button" class="btn btn-primary">Search</button>
<!--  <div class="alert" style="display: inline-block; margin:0 0 0 20px;;padding:3px;"> 
  	QQ 登录 qun.qq.com 使用上面的 加群操作</div>-->
</form>

<div style="height: 93.5%;">
	<table id="qqstateList" ></table>
</div>
<script>
	var qqstateList = $("#qqstateList").datagrid({
			//title : '测试',
			url:path+'/rule/ruleList',
			fitColumns: true,
			queryParams:{"type":"2"},
			toolbar:['-',
				{ text:'添加Rule',  iconCls:'icon-add',  handler: addrulebase},'-',
				{ text:'修改Rule信息',  iconCls:'icon-edit',  handler: editRule},'-',
				{ text:'删除Rule信息',  iconCls:'icon-edit',  handler: deleterulebase},'-',
				{ text:'Rule测试',  iconCls:'icon-edit',  handler: testrulebase},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'rulename',title:'rule名称',align:'left',width:150},
			    {field:'type',title:'类别',align:'left',width:150},
			    {field:'expression',title:'表达式',align:'left',width:150},
			    {field:'pageRule',title:'分页规则',align:'left',width:150},
			    {field:'pageSize',title:'分页数',align:'left',width:150},
			    {field:'pageNum',title:'每页个数',align:'left',width:150},
			    {field:'url',title:'测试url',align:'left',width:150},
			    {field:'data',title:'参数',align:'left',width:150},
			    {field:'method1',title:'提交方式',align:'left',width:150},
			    {field:'belone',title:'分类归属',align:'left',width:150},
			    {field:'charset',title:'编码方式',align:'left',width:150},
			    //{field:'type',title:'类型',align:'left',width:150,formatter: parseqqstateType}
			]],
			pagination: true
    	});

	function searchqqstateList(){
		qqstateList.datagrid("load",$("#qqstateF").jsonForm());
	}
	
	
	function addrulebase(){
		//console.info(row._id);
		$.JTUI.dialog({
			title: '添加Rule',
			modal: true,
			url: path + '/comm/f?p=ruleBase/addrulebase',
			onSure:function(d){	
			  d.find("form").ajaxSubmit({
				action: path+"/rule/insertRuleBase",
				success: function(){alert("success");
				greload(qqstateList);},
			   // error:function(){alert('error')}
			});
			return true;	
		 }			
		  });
		}

	function editRule(){
		var rows = gcheckedRow(qqstateList);
		if(rows==-1){
			alert("至少且只能选择一条数据进行操作");
			return ;
		}
		$.JTUI.dialog({
			title:'修改Rule信息',
			url: path+'/comm/f?p=ruleBase/editrulebase&t='+rows._id, 
			initData: function(d){ 
			console.info(rows);
				d.find("#rulename").val(rows.rulename);
				d.find("#type").val(rows.type);
				d.find("#expression").val(rows.expression);
				d.find("#pageRule").val(rows.pageRule);
				d.find("#name").val(rows.name);
				d.find("#pageSize").val(rows.pageSize);
				d.find("#url").val(rows.url);
				d.find("#belone").val(rows.belone);
				d.find("#charset").val(rows.charset);
				d.find("#pageNum").val(rows.pageNum);
				
				if(rows.method1!=""&&rows.method1!=null)
					d.find("[value="+rows.method1+"]")[0].checked = true;
				else
					d.find("[value=get]")[0].checked = true;
				d.find("[name=data]").val(rows.data);
				d.find("[name=urlRule]").val(rows.urlRule);
				},
			modal: true,		
			onSure:function(d){	
				d.find("form").ajaxSubmit({
					action: path+"/rule/updateRuleBase",
					success: function(){alert("success");
					greload(qqstateList);},
				   // error:function(){alert('error')}
				});
				return true;	
			}	
		});
		
	}
	
	function deleterulebase(){
		var row = gchecked(qqstateList);
		
		if(row==-1){
			alert("必须选择一个Rule");
			return;
		}
		
		if(window.confirm("你确定删除这条数据吗？")){
			$.get(path+"/rule/deleteRuleBase?rulebaseid="+row,function(){
				greload(qqstateList);
			});
			
		}
	}
	
	
	function testrulebase(){
		var row = gchecked(qqstateList);
		if(row==-1){
			alert("必须选择一个Rule");
			return;
		}
		//console.info(row._id);
		$.JTUI.dialog({
			title: '单条Rule测试',
			modal: true,
			url: path+'/rule/ruleTest?id='+row,
			onSure:function(d){	
			  d.find("form").ajaxSubmit({
				action: path+"/comm/f?p=ruleBase/ruleBaseList",
				success: function(){alert("success");
				greload(qqstateList);},
			   // error:function(){alert('error')}
			});
			return true;	
		 }			
		  });
		}
	
</script>


