<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
	String path = request.getContextPath();
%>
<div style="height: 540px;width:1010px;">
	<div style="height: 100%;width:300px;float: left;padding:10px;">
		<form id="addtform" method="post" action="">
		<input type="hidden" name="id" id="taskid" value="${task._id}" />
		<table>
		<tr>
		<td>任务说明:</td><td><input type="text" name="note" id="note" value="${task.note}" /></td>
		</tr>
		<tr>
			<td>抓取地址:<input type="button" value="编辑" onclick="selectURL();"></td>
			<td><input type="text" name="url" id="url" value="${task.url}" /></td>
		</tr>
		<tr>
		<td>网址标识符:</td><td><input type="text" name="workbeach" id="workbeach" value="${task.workbeach}"/></td>
		</tr>
		<tr>
			<td>Header:<input type="button" value="编辑" onclick="addmap('header');" />
			</td>
			<td><textarea rows="3" cols="" name="header" id="header">${task.header==null?"{ }":task.header}</textarea></td>
		</tr>
		<tr>
			<td>Cookie:<input type="button" value="编辑" onclick="addmap('cookie');" /></td>
			<td><textarea rows="3" cols="" name="cookie" id="cookie">${task.cookie==null?"{ }":task.cookie}</textarea> </td>
		</tr>
		<tr>
			<td>预留字段</td>
			<td><textarea rows="3" cols="" name="extedata" id="extedata">${task.extedata==null?"{ }":task.extedata}</textarea> </td>
		</tr>
		</table>
		</form>
	</div>
	<div style="height: 100%;width:680px;float: left;">
		
		<div style="height:170px;;width:100%;position:relative;border: 1px solid #ccc;overflow: auto">
			<div style="position:absolute;right:5px;top:5px;"><input type="button" value="重置规则" onclick="rem_rules();" /></div>
			<ul id="ruletree" class="ztree">
			</ul>
		</div>
		<div style="height:370px;width:100%;">
			<div style="height:20px;width:100%;">
			<form id="sform" action="<%=path %>/rule/ruleList" style="float:right;margin: 0;" onsubmit="search();return false;">
			<input id="keybox" type="text" name="key" style="margin: 0;"/>
			<input onclick="search();" type="button" value="搜索" />
			</form>
			</div>
			<div style="height:350px;width:100%;">
			<table id="ruleList" style="padding:10px" ></table>
			</div>
		</div>
	</div>
</div>
	

<script>
	var tid = '${task._id}';
	initTree(tid);
	var ruleList = $("#ruleList").datagrid({
			title : '规则列表',
			url: path+'/rule/ruleList',
			fitColumns: true,
			toolbar:['-',
				{ text:'使用选中规则',  iconCls:'icon-add',  handler: addrules},'-'
				],
		    columns:[[
			    {field:'rulename',title:'规则名称',align:'center',width:30},
			    {field:'expression',title:'expression',align:'center',width:30},
			    {field:'pageRule',title:'pageRule',align:'center',width:30},
			    {field:'pageSize',title:'pageSize',align:'center',width:30},
			    {field:'url',title:'url',align:'center',width:30},
			    {field:'belone',title:'belone',align:'center',width:30}
	            ]],
			pagination: true
    	})
    	
    	function addrules(){
    		var rows = gchecked(ruleList);
			if(rows==-1){
				alert("至少选择一条数据");
				return ;
			}
			var taskid = $("#taskid").val();
			var rid = '';
			var treeObj = $.fn.zTree.getZTreeObj("ruletree");
			if(null!=treeObj){
				var nodes = treeObj.getSelectedNodes();
				if(null!=nodes&&nodes.length>0){
					rid = nodes[0]._id;
				}
			}
			$.get(path+"/task/addtaskrule?ids="+rows.join(",")+"&rid="+rid+"&taskid="+taskid,function(data){
				if(null!=data&&''!=data){
					initTree(data);
				}else{
					alert("操作失败");
				}
			},"text")
    	}
    	
    	function initTree(data){
    		$("#taskid").val(data); 
			var setting = {
		        async: 	{ enable: true, url: path + "/rule/getruletree?taskid="+data },
		        data:{
		        	simpleData:{
		        		enable:true,
		        		idKey:"cid",
		        		pIdKey:"pid",
		        		rootPId:null,
		        	}
		        },
		        callback:{
		        	onAsyncSuccess:expandAll,
		        	beforeRemove:function(treeId, treeNode){
		        					if(treeNode.isParent){
						    			alert("父节点无法删除");
						    			return false;
					    			}
		        					return confirm("确认删除");
		        				},
		        	onRemove:removerule
		        },
		        edit:{
		        	enable:true,
		       		showRemoveBtn:true,
		       		showRenameBtn:false,
		        	removeTitle:"remove"
		        }
		    };
			$.fn.zTree.init($("#ruletree"), setting);
    	}
    	
    	function submitaddtform(){
    		$("#addtform").ajaxSubmit({
    			success:function(data){
    				if(data==1){
    					alert("操作成功");
    				}
    			}
    		});
    	}
    	
    	function rem_rules(){
    		var taskid = $("#taskid").val();
    		$.get(path+"/rule/remrules?tid="+taskid,function(d){
    			if(d==1){
    				initTree(taskid);
    			}
    		},"text");
    	}
    	
    	function search(){
   			ruleList.datagrid("load",$("#sform").jsonForm());
   		}
    	
    	function addmap(id){
    		var str = $("#"+id).val();
    		$.JTUI.dialog({
				title:id+'编辑',
				url: path+"/comm/f?p=task/maptojson&t="+str,
				modal:true,
				onSure:function(){
					var jsonStr = maptojson();
					$("#"+id).val(jsonStr);
		    		return true;
	    		}
			})
    	}
    	function expandAll(){
    		var zTree   = $.fn.zTree.getZTreeObj("ruletree");
			zTree.expandAll(false);
    	}
    	function removerule(event, treeId, treeNode){
    		$.get(path+"/rule/remove?id="+treeNode._id,function(d){},"text");
    	}
    	function selectURL(){
    		$.JTUI.dialog({
				title:'URL选择',
				url: path+"/comm/f?p=task/selectURL",
				modal:true,
				onSure:function(){
					var url = getURL();
					if(null==url){
						return false;
					}
					$("#url").val(url);
		    		return true;
	    		}
			})
    	}
    	
</script>
