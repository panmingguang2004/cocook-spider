<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<div style="width:600px;height:500px">
<div style="height:40px;">
	<form id="searchform" >
	Type:<input type="text" name="type" id="type" />
	省市:<input type="text" name="key" id="key"/>
	<input type="button" value="搜索" onclick="searchiii();" style="margin-bottom:10px;"/>
	</form>
</div>
<div style="height:460px;">
	<table id="cityList" style="padding:10px" ></table>
</div>
</div>
<script>
	var cityList = $("#cityList").datagrid({
			title : '城市选择',
			url: path+'/task/selectcity',
			fitColumns: true,
			toolbar:[
				],
		    columns:[[
			    {field:'type',title:'TYPE',align:'center',width:30},
			    {field:'province',title:'省份',align:'center',width:30},
			    {field:'city',title:'城市',align:'center',width:30},
			    {field:'url',title:'抓取地址',align:'center',width:30},
			    {field:'note',title:'备注',align:'center',width:30}
	            ]],
			pagination: true
    	})
    	
    function searchiii(){
    	cityList.datagrid("load", $("#searchform").jsonForm());
    }
    	
	function getURL(){
		var row = gcheckedRow(cityList);
		if(row==-1){
			alert("必须且只能选择一条数据");
			return;
		}
		return row.url;
	}
</script>
