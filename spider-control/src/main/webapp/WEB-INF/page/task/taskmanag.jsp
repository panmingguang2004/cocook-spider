<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="taskF" onsubmit="searchtaskList();return false;">
  <input type="text" name="key"  placeholder="任务名称" id="search">
  <input type="checkbox" name="type" value="1"> 失败task 
  <button onclick="searchtaskList();" type="button" class="btn btn-primary">Search</button>
<!--  <div class="alert" style="display: inline-block; margin:0 0 0 20px;;padding:3px;"> 
  	QQ 登录 qun.qq.com 使用上面的 加群操作</div>-->
</form>
<div style="height: 93.5%;">
	<table id="taskList" style="padding:10px" ></table>
</div>

<script>
	var taskList = $("#taskList").datagrid({
			//title : '测试',
			url: path+'/task/getalltask',
			fitColumns: true,
			toolbar:['-',
				{ text:'新建任务',  iconCls:'icon-add',  handler: addtask},'-',
				{ text:'修改任务',  iconCls:'icon-add',  handler: updtask},'-',
				{ text:'删除任务',  iconCls:'icon-remove',  handler: deltask},'-',
				{ text:'启动任务',  iconCls:'icon-remove',  handler: runtask},'-',
				{ text:'分配任务',  iconCls:'icon-remove',  handler: allottask},'-',
				{ text:'复制任务',  iconCls:'icon-remove',  handler: copytask},'-',
				{ text:'添加定时任务',  iconCls:'icon-remove',  handler: timetask},'-',
				{ text:'重跑失败task',  iconCls:'icon-remove',  handler: resettask},'-'
				],
		    columns:[[
			    {field:'note',title:'任务说明',align:'center',width:30},
			    {field:'url',title:'抓取地址',align:'center',width:30},
			    {field:'workbeach',title:'网站标识符',align:'center',width:30},
			    {field:'client',title:'VPS',align:'center',width:30},
			    {field:'header',title:'请求头信息',align:'center',width:30},
			    {field:'crawlerdate',title:'抓取日期',align:'center',width:30},
			    {field:'state',title:'状态',align:'center',width:30},
			    {field:'processor',title:'processor',align:'center',width:30},
	            ]],
			pagination: true
    	})
    	
   	function searchtaskList(){
		taskList.datagrid("load",$("#taskF").jsonForm());
	}
    	
	function addtask(){
		showWindow("",path+"/task/addtask");
	}
	function updtask(){
		var row = gcheckedRow(taskList);
		if(row==-1){
			alert("必须且只能选择一条数据");
			return;
		}
		showWindow(row._id,path+"/task/updatetask");
	}
	function showWindow(tid,act){
		$.JTUI.dialog({
			title:'添加/修改任务',
			url: path+"/task/savetask?tid="+tid,
			modal:true,
			initData:function(d){
				d.find("form")[0].action=act;
			},
			onSure:function(){
				submitaddtform();
				greload(taskList);
	    		return true;
    		}
		})
	}
	
  	function deltask(){
  		var rows = gchecked(taskList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		if(confirm("确认删除")){
			$.get(path+"/task/deletetask?id="+rows.join(","),function(data){
				if(data==1){
					greload(taskList);
				}else{
					alert("操作失败");
				}
			},"text")
		}
	}
	
	function runtask(){
		var rows = gchecked(taskList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.get(path+"/task/runtask?ids="+rows.join(","),function(d){
			if(d==1){
				greload(taskList);
			}else{
				alert("操作失败");
			}
		},"text")
	}
	
	function allottask(){
		var rows = gchecked(taskList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.JTUI.dialog({
			title:'WorkBeach列表',
			url: path+"/comm/f?p=task/wblist",
			modal:true,
			onSure:function(){
				var ids = getWBlist();
				if(null==ids){
					return false;
				}
				$.get(path+"/task/allottask?wids="+ids+"&tids="+rows.join(","),function(d){
					if(d==1){
						greload(taskList);
					}else{
						alert("操作失败");
					}
				},"text")
	    		return true;
	    		
    		}
		})
	}
	
	
	function gaclient(){
		var rows = gchecked(taskList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.JTUI.dialog({
			title:'在线client列表',
			url: path+"/comm/f?p=account/onclist",
			modal:true,
			onSure:function(){
				var ids = getonclist1();
				if(null==ids){
					return false;
				}
				$.get(path+"/onlinec/allotclient?oncid="+ids+"&tids="+rows.join(","),function(d){
					if(d==1){
						greload(taskList);
					}else{
						alert("操作失败");
					}
				},"text")
	    		return true;
	    		
    		}
		})
		
	   }
	
	
	function gatask(){
		var rows = gchecked(taskList);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.JTUI.dialog({
			title:'WorkBeach列表',
			url: path+"/comm/f?p=account/kalist",
			modal:true,
			onSure:function(){
				var ids = getWBlist();
				if(null==ids){
					return false;
				}
				$.get(path+"/task/allottask?wids="+ids+"&tids="+rows.join(","),function(d){
					if(d==1){
						greload(taskList);
					}else{
						alert("操作失败");
					}
				},"text")
	    		return true;
	    		
    		}
		})
	}
	
	function copytask(){
		var row = gcheckedRow(taskList);
		if(row==-1){
			alert("必须且只能选择一条数据");
			return;
		}
		copyWindow(row._id);
	}
	
	function timetask(){
		var rows = gchecked(taskList);
		if(rows==-1){
			alert("至少选择一条数据");
			return;
		}
			$.JTUI.dialog({
			title:'定时器列表',
			url: path+"/comm/f?p=task/timerlist",
			modal:true,
			onSure:function(){
				var tmid = gettimerlist();
				if(null==tmid){
					return false;
				}
				$.get(path+"/timer/timetask?tmid="+tmid+"&tids="+rows.join(","),function(d){
					if(d==1){
						greload(timerList);
					}else{
						alert("操作失败");
					}
				},"text")
	    		return true;
    			}
			})
	   }
	
	function copyWindow(tid){
		$.JTUI.dialog({
			title:'添加/修改任务',
			url: path+"/task/copytask?tid="+tid,
			modal:true,
			initData:function(d){
				d.find("form")[0].action=path+"/task/updatetask";
			},
			onSure:function(){
				submitaddtform();
				greload(taskList);
	    		return true;
    		}
		})
	}
	
	function delmidtask(){
		if(confirm("确认删除")){
			$.get(path+"/task/deletemidtask",function(data){
				if(data==1){
					alert("操作成功");
					greload(taskList);
				}else{
					alert("操作失败");
				}
			});
		}
	}
	
	function resettask(){
		if(confirm("确认恢复")){
			$.get(path+"/task/resettask",function(data){
				if(data==1){
					alert("操作成功");
					greload(taskList);
				}else{
					alert("操作失败");
				}
			});
		}
	}
	
	function clearRAM(){
		if(confirm("确认删除")){
			$.get(path+"/task/clear_ram",function(data){
				alert("操作成功");
			});
		}
	}
</script>
