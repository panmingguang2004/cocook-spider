<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<div style="height:440px;width:500px;">
<div style="height:40px;width:100%;">
<form class="form-inline" id="timerform" onsubmit="querytimerList();return false;">
	<input name="tname"type="text" />
	<input class="btn" type="button" value="查询" onclick="querytimerList()" />
</form>
</div>
<div style="height:400px;width:100%;">
<table id="timerList" style="padding:10px;" ></table>
</div>
</div>
<script>
	var timerList = $("#timerList").datagrid({
			url: path+'/timer/list',
			fitColumns: true,
		    columns:[[
			    {field:'name',title:'Name',align:'center',width:30},
			    {field:'note',title:'Note',align:'center',width:30},
			    {field:'exctime',title:'ExcTime',align:'center',width:30},
			    {field:'datelong',title:'DateLong',align:'center',width:30},
			    {field:'tasks',title:'Tasks',align:'center',width:30},
			    {field:'state',title:'State',align:'center',width:30}
	            ]],
			pagination: true
    	})
    	
    	function gettimerlist(){
    		var row = gcheckedRow(timerList);
			if(row==-1){
				alert("只能选择一条数据");
				return ;
			}
			return row._id;
    	}
		
	function querytimerList(){
		timerList.datagrid("load",$("#timerform").jsonForm());
	}
</script>
