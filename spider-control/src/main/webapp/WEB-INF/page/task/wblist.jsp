<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<div style="height:440px;width:500px;">
<div style="height:40px;width:100%;">
<form class="form-inline" id="wbform" onsubmit="querywbList();return false;">
	<input name="name"type="text" />
	<input class="btn" type="button" value="查询" onclick="querywbList()" />
</form>
</div>
<div style="height:400px;width:100%;">
<table id="wbList" style="padding:10px;" ></table>
</div>
</div>
<script>
	var wbList = $("#wbList").datagrid({
			url: path+'/client/list',
			fitColumns: true,
		    columns:[[
			    {field:'workbeach',title:'WorkBeach',align:'center',width:30},
			    {field:'online',title:'Time',align:'center',width:30},
			    {field:'note',title:'Note',align:'center',width:30},
	            ]],
			pagination: true
    	})
    	
    	function getWBlist(){
    		var rows = getRowsId(wbList,"workbeach");
			if(rows==-1){
				alert("至少选择一条数据");
				return ;
			}
			return rows.join(",");
    	}
		
	function querywbList(){
		wbList.datagrid("load",$("#wbform").jsonForm());
	}
</script>
