<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="qqvpsF">
  <input type="hidden" name="tid"  placeholder="" >
  
</form>


<div style="height: 93.5%;">
	<table id="qqvpsList" ></table>
</div>

<script>
	$.get(path+"/tag/proTag", function(data){
			var select = $("#qqvpsF").find("select");
			for(var i=0;i<data.length; i++){
				var row = data[i];
				select.append("<option value="+row._id+">"+row.name+"</option>")
			}
		})

	var qqvpsList = $("#qqvpsList").datagrid({
			//title : '测试',
			url: path + '/task/taskqqlist',
			method:'get',
			fitColumns: true,
			toolbar:['-',
				{ text:'分配到项目',  iconCls:'icon-remove',  handler: addtag},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'_id',title:'taskqq',align:'left',width:150},
			    {field:'taskname',title:'任务名',align:'left',width:150},
			    {field:'classname',title:'分类名',align:'left',width:150,sortable:true},
			    {field:'tname',title:'所在项目',align:'left',width:150}
			]]
			
    	})		
	
	function addtag(){
		var rows = gchecked(qqvpsList);
		if(rows==-1){
			alert("至少选择一条数据进行操作");
			return ;
		}
		$.JTUI.dialog({
			title:'选择项目(一个项目下可以有多个task任务)',
			url: path + '/f?p=qq/tagsForm', 
			initData:function (d){	$(":hidden", d).val(rows.join(",")); initProTag();},
			onSure:function(d){
				d.find("form").ajaxSubmit({
					action: path+"/task/addtid",
					success: function(){greload(qqvpsList);},
				})
				return true;
			}
		});
	}
	
</script>


