<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<form class="form-inline" id="timerlistF" onsubmit="queryTimerList();return false;">
	<input name="tname"type="text">
	<input class="btn" type="button" value="查询" onclick="queryTimerList()">
</form>

<form class="hide" action="" id="editTimer" method="post">
	<input  type="hidden" name="id">
	<table>
		<tr>
			<td>timer名称</td>
			<td><input name="name" type="text"/></td> 
		</tr>
		<tr>
			<td>备注</td>
			<td><input name="note" type="text"/></td> 
		</tr>
		
		<tr>
			<td>周期</td>
			<td><input id="datelong" name="datelong" type="text"/>天</td> 
		</tr>
	</table>
</form>

<div style="height: 93.5%;">
	<table id="timerlist" ></table>
</div>
<script>
	var timerlist = $("#timerlist").datagrid({
			url:path+'/timer/list',
			fitColumns: true,
			toolbar:['-',
				{ text:'添加timer',  iconCls:'icon-add',  handler: addTimer},'-',
				{ text:'修改timer',  iconCls:'icon-edit',  handler: editTimer},'-',
				{ text:'删除timer',  iconCls:'icon-remove',  handler: delTimer},'-',
				{ text:'启动timer',  iconCls:'icon-remove',  handler: runTimer},'-',
				{ text:'关闭timer',  iconCls:'icon-remove',  handler: stopTimer},'-'
				],
		    columns:[[
			   	{field:'ck', checkbox:true},
			    {field:'name',title:'Timer名称',align:'center',width:150},
			    {field:'note',title:'Note',align:'center',width:150},
			    {field:'exctime',title:'ExcTime',align:'center',width:150},
			    {field:'datelong',title:'DateLong',align:'center',width:150},
			    {field:'tasks',title:'Tasks',align:'center',width:150},
			    {field:'state',title:'State',align:'center',width:150},
			]],
			pagination: true
    	});	
	
	function addTimer(){
		$.JTUI.dialog({
			title: '添加客户端',
			modal: true,
			content:"#editTimer",
			onSure:function(d){	
			
			var r = /^[0-9]*[1-9][0-9]*$/ 
		if(!r.test($("#datelong", d).val())){
		   alert("周期值只能为正整数！"); 
		   return false;
		}
			  d.find("form").ajaxSubmit({
				action: path+"/timer/add",
				success: function(){alert("success");
				greload(timerlist);},
			});
			return true;	
		 }			
		  });
		}

	function editTimer(){
		var rows = gcheckedRow(timerlist);
		if(rows==-1){
			alert("至少且只能选择一条数据进行操作");
			return ;
		}
		$.JTUI.dialog({
			title:'修改客户端信息',
			content: "#editTimer",
			initData: function(d){ 
				d.find("[name=id]").val(rows._id);
				d.find("[name=name]").val(rows.name);
				d.find("[name=note]").val(rows.note);
				d.find("[name=datelong]").val(rows.datelong);
				},
			modal: true,		
			onSure:function(d){	
				d.find("form").ajaxSubmit({
					action: path+"/timer/edit",
					success: function(){alert("success");
					greload(timerlist);},
				});
				return true;	
			}	
		});
		
	}
	
	function delTimer(){
		var row = gchecked(timerlist);
		
		if(row==-1){
			alert("至少选择一条数据");
			return;
		}
		
		if(window.confirm("你确定删除这些数据吗？")){
			$.get(path+"/timer/del?ids="+row.join(","),function(){
				greload(timerlist);
			});
			
		}
	}
	
	function queryTimerList(){
		timerlist.datagrid("load",$("#timerlistF").jsonForm());
		
	}
	
	function runTimer(){
		var rows = gchecked(timerlist);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.get(path+"/timer/runtask?ids="+rows.join(","),function(d){
			if(d==1){
				greload(timerlist);
			}else{
				alert("操作失败");
			}
		},"text")
	}
	
	function stopTimer(){
		var rows = gchecked(timerlist);
		if(rows==-1){
			alert("至少选择一条数据");
			return ;
		}
		$.get(path+"/timer/stoptask?ids="+rows.join(","),function(d){
			if(d==1){
				greload(timerlist);
			}else{
				alert("操作失败");
			}
		},"text")
	}
	
</script>


