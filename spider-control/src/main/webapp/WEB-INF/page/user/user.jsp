<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<div style="height: 93.5%;">
	<table id="userList" style="padding:10px" ></table>
</div>

<script>
	var userList = $("#userList").datagrid({
			//title : '测试',
			url: path + '/index/test',
			fitColumns: true,
			toolbar:['-',
				{ text:'审核同步',  iconCls:'icon-edit',  handler: synchroResult},'-',
				{ text:'结算同步',  iconCls:'icon-edit',  handler: cashResult},'-'
				],
		    columns:[[
			    {field:'uname',title:'用户名',align:'center',width:30},
			    {field:'shallow',title:'浅交互量',align:'center',width:30},
			    {field:'deep',title:'深交互量',align:'center',width:30},
			    {field:'deal',title:'成单量',align:'center',width:30}
	            ]],
			pagination: true
    	})
    	
    	
	function get_ttt(){
	    IDataList.datagrid("load",$("#userList").jsonForm())
	}
	
	function getSDD(uname,val,state,b,e){
		if('0'==val){
			alert("没有数据");
			return true;
		}
		$.JTUI.dialog({
			title:'明细查询',
			url: path + path+"/comm/f?p=data/showDetailList&t="+uname+","+state+","+b+","+e,
			modal:true,
			onSure:function(){return true;}
		})
	}
	
	function synchroResult(){
		$.get(path+ "/data/synchro_examine_result",function(data){
			if(data==1){
				alert("操作成功");
			}else{
				alert("操作失败");
			}
		},"text")
	}
	
  function cashResult(){
		$.get(path+ "/data/cash_result",function(data){
			if(data==1){
				alert("操作成功");
			}else{
				alert("操作失败");
			}
		},"text")
	}
	
	
</script>
