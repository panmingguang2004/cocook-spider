<!-- start header -->
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<style type="text/css">

#userInfo{
	height:30px;
	line-height:30px;
	float:right;	
	margin: 0 20px;
}

#userInfo a{
	margin-left: 15px;
	color: white;
}

#userInfo a:hover{
	color: #FF7700;
}

#topmenu{
	margin: 20px  0 0 300px;
	float: left;
}
#topmenu .menu1{
	background-color: #fff;
	padding: 3px 10px;
}

</style>
<div id="head" style="">
    <div id="userInfo">
	    <a id="about" href="javascript:void(0)"> 关于</a>
	    <a id="help" href="javascript:void(0)" target="_black">帮助</a>
	    <a id="editpass" href="javascript:void(0)">您好, ${user.username}</a>
	    <a id="out" href="logout" href="javascript:void(0)"> 退出</a> 
    </div>
	
	<div id="topmenu">
		<ul class="inline menu1">
	      <li class="dropdown">
	      	 <a href="#" data-toggle="dropdown">任务管理<i class="caret"></i></a>
	      	 <ul class="dropdown-menu">
	          <li><a href="<%=path%>/comm/f?p=task/taskmanag" >基本任务管理</a></li>
    		  <li><a href="<%=path%>/comm/f?p=client/list">客户端管理</a></li>
    		  <li><a href="<%=path%>/comm/f?p=account/alist">账号管理</a></li>
    		  <li><a href="<%=path%>/comm/f?p=ruleBase/ruleBaseList" >规则库</a></li>
    		   <li><a href="<%=path%>/comm/f?p=data/showdata" >数据查看</a></li>
    		   <li><a href="<%=path%>/comm/f?p=data/urldata" >urls查看</a></li>
	          <li class="divider"></li>
    		  <li><a href="<%=path%>/comm/f?p=cityruleList" >城市管理</a></li>
    		  <li><a href="<%=path%>/comm/f?p=timer/tlist">定时器管理</a></li>
	        </ul>
	      </li>
	      <li><a href="<%=path%>/comm/f?p=baidu/baidulist">百度任务管理</a></li>
	      <li>
	        <a href="<%=path%>/comm/f?p=captcha"">在线打码</a>
	      </li>
	    </ul>
	</div>
	
    
  <!--  <div id="topmenu" class="btn-group">
		<a id="1" class='btn' href="<%=path%>/comm/f?p=task/taskmanag" >任务管理</a>
    	<a id="4" class='btn' href="<%=path%>/comm/f?p=client/list">客户端管理</a>
    	<a id="6" class='btn' href="<%=path%>/comm/f?p=ruleBase/ruleBaseList" >规则库</a>
    	<a id="5" class='btn' href="<%=path%>/comm/f?p=cityruleList" >城市管理</a>
    	<a id="3" class='btn' href="<%=path%>/comm/f?p=timer/tlist">定时器管理</a>
    	<a id="7" class='btn' href="<%=path%>/comm/f?p=baidu/baidulist">百度任务管理</a>
    	<a id="8" class='btn' href="<%=path%>/comm/f?p=captcha">在线打码</a>
    </div>-->
</div>
<!-- end header -->
<script>
var path = "<%=path%>";
</script>
