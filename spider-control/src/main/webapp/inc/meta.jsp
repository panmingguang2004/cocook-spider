<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<%String path =request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();%>
<%-- <link rel="shortcut icon" type="image/x-icon" href="$ctx/js_css/favicon.ico">--%>
<link type="text/css" rel="stylesheet" href="<%=path%>/css/bootstrap-theme.min.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/main.css"  />

<link rel="stylesheet" type="text/css" href="<%=path%>/js/easyui-1.3.2/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/js/easyui-1.3.2/themes/easyui.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/js/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/js/jqueryUI/themes/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="<%=path%>/js/ztree/themes/zTreeStyle.css" />

<script type="text/javascript" src="<%=path%>/js/jquery-1.8.3.min.js "></script>
<script type="text/javascript" src="<%=path%>/js/jquery-extend.js "></script>

<script type="text/javascript" src="<%=path%>/js/jqueryUI/jquery.ui.datepicker.js"></script>

<script type="text/javascript" src="<%=path%>/js/easyui-1.3.2/base.js "></script>
<script type="text/javascript" src="<%=path%>/js/easyui-1.3.2/layout.js "></script>
<script type="text/javascript" src="<%=path%>/js/easyui-1.3.2/grid.js "></script>


<script type="text/javascript" src="<%=path%>/js/easyui-1.3.2/easyui-extend.js "></script> 

<script type="text/javascript" src="<%=path%>/js/ztree/jquery.ztree.core-3.5.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/ztree/jquery.ztree.excheck-3.5.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/ztree/jquery.ztree.exedit-3.5.min.js"></script>

<script type="text/javascript" src="<%=path %>/js/dialog/dialog.js"></script>

<script type="text/javascript" src="<%=path %>/js/selectdown.js "></script>
<script type="text/javascript" src="<%=path %>/js/dropdown.js"></script>
<script type="text/javascript" src="<%=path %>/js/bootstrap.min.js"></script>
<style type="text/css">
	.panel-body.panel-body-noheader.panel-body-noborder{
		margin:0;
	    padding:5px
	}

	/* 防止 fitcolumns: true 后 出现的横向滚动*/
	#tab .datagrid-body{
		overflow-x: hidden;
	}
	
	#tab .panel-body {
		overflow: hidden;
	}
	.tabs li {
		line-height: 0px;
	}
</style>