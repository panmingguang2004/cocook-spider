/***
 * create by panmg
 * @param {Object} $
 * @memberOf {TypeName} 
 * @return {TypeName} 
 */
(function($) {

	if(!$.JTUI){
		$.JTUI = {};
	};

    $.fn.center = function() {
        return this.css({
            position:'absolute',
            top:($(window).height() - this.height()) / 2 + $(window).scrollTop() + 'px',
            left: ( $(window).width() - this.width() ) / 2 + $(window).scrollLeft() + 'px'
        })
    };
    var createId = (function () {
        var i = 0;
        return function() {
            return ++i;
        }
    })();

    $.JTUI.dialog = function (option) {
    	var defaults = {
    		modal: false,
	        title: '',
	        url: null,
	        content: null,
	        message: null,
	        onCancel: function(dialog) {return true},
	        onSure: function(dialog) {},
	        initData: function(dialog) {},
	        button: '<a class="toolbar_button" id="button_cancel" href="javascript:void(0);"><span>取消</span></a><a class="toolbar_button" id="button_ok" href="javascript:void(0);"><span>确定</span></a>'
    	}
        option = $.extend({}, defaults, option);
        option.id = createId();

        var thead = ['<thead id="thead_hand"><tr style="cursor: move;">',
			            '<td width="13" height="33" class="thread_border_lt">' ,
			            	'<div style="width: 13px;"></div>' ,
			            '</td>',
			            '<td height="33" class="thread_border_ct">' ,
			            	'<div class="thread_border_ct_div">' ,
			            		'<a class="thread_border_title">' + option.title + '</a>' ,
			            	'</div>' ,
			            	'<a class="thread_border_close" id="thread_close" ></a>' ,
			            '</td>' ,
			            '<td width="13" height="33" class="thread_border_rt">' ,
			            	'<div style="width: 13px;"></div>' ,
			            '</td>' +
			         '</tr></thead>'].join('');
       


        var tfoot = ['<tfoot><tr><td width="13" height="13" class="tfoot_border_lb"></td>',
            '<td class="tfoot_border_cb"></td>',
            '<td width="13" height="13" class="tfoot_border_rb"></td></tr></tfoot>'
        ].join('');


        /*tbody拼接 message main button*/
        var tbody = ['<table id="JTUI_dg' + option.id + '" border="0"  cellpadding="0" cellspacing="0" width="">' +
        				'<tr>',
	        				'<td width="13" class="tbody_boder_mlm"></td>',
	            			'<td>',
            				'<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:#FFFFFF;"'];

        /*判断是否加入message*/
        if (option.message) { //style ="width:' + option.width + 'px;">
            var message = ['<tr style="display: block"><td height="50" >' +
                    		'<div class="message_div">',
                				'<div class="message_png"/>' ,
                				'<div style="height:100%;float: left; overflow: hidden;">' + option.message + '</div>' ,
                			'</div>' ,
                			'</td></tr>'].join('');
            tbody.push(message);
        }

        /*判断使用loadHTML*/ //style="padding:4px;width: ' + option.width + 'px;">
        var main = ['<tr><td><div style="padding:4px;">'];
        if (option.url) {
        	$.ajax({url:option.url,cache:false,	async: false,dataType:"html",
    				success:function(d){main.push(d);}
    		});
        } else if (option.content) {
            if (option.content.substr(0, 1) === '#') {
                var content = $("<div id='JTUI_toString'>").append($(option.content).clone().show()).html();
                main.push(content);
            } else { 
                main.push(option.content);
            }
        }
        main.push('</div></td></tr>');
        tbody.push(main.join(''));


        /*判断是否加入button*/
        if (option.button) {
            var button = '<tr><td height="36" id="toolbar">';
            button += option.button;
            button += '</td></tr>';
        }

        tbody.push(button);
        tbody.push('</table></td><td width="13" class="tbody_boder_mrm"></td></tr>');
        
        tbody.push(thead);
        tbody.push(tfoot);
        tbody.push("</table>");



        var mask = null;
        if (option.modal) {
            mask = $('<div style="position:absolute;left:0;top:0;background:#ccc;opacity:0.4;filter:alpha(opacity=30);width:100%;height:100%"></div>').appendTo('body');
            option.mask = mask;
        }
        
        var dialog = $(tbody.join("")).appendTo('body').center();
        
        try{
        	  dialog.draggable({handle:'#thead_hand'});
        }catch(e){}
      

        // 初始化数据
        try{
        	option.initData(dialog);
		}catch(e){
			alert(e);
			throw e;
		}
		
        function cancel() {
            if(option.onCancel(dialog)){
            	if (mask) mask.remove();
            	dialog.remove();
            }
        };

        function ok() {
            if (option.onSure(dialog)) {
                if (mask) mask.remove();
                dialog.remove();
            }
        };
        
        $('#button_ok', dialog).bind('click', ok);
        $('#button_cancel', dialog).bind('click', cancel);
        $('#thread_close', dialog).bind('click', cancel);

        dialog.data('JTUI_dg', option); //将数据保存到自身对象上
        return dialog;

    };

})(jQuery);