/*中文包扩展*/
if ($.fn.pagination) {
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}
if ($.fn.datagrid) {
    $.fn.datagrid.defaults.loadMsg = '正在处理，请稍待。。。';
    $.fn.datagrid.defaults.remoteSort = true; // 客户端排序(默认是服务端排序
   	$.fn.datagrid.defaults.striped = true;  //条纹间隔
    $.fn.datagrid.defaults.nowrap = false;   //是否换行显示数据  默认true 不换行
    $.fn.datagrid.defaults.pagination = true;  //显示分页
    $.fn.datagrid.defaults.fit = true;  //自适应外层窗口最
    $.fn.datagrid.defaults.sortOrder = "DESC";  //自适应外层窗口最
    $.fn.datagrid.defaults.selectOnCheck = true;  //选中checkbox选中行
    $.fn.datagrid.defaults.checkOnSelect = true;  //选择行不选中checkbox
    
    /*$.fn.datagrid.defaults.fitColumns = true;  //自适应列宽度和外层相同*/
    $.fn.datagrid.defaults.pageSize = 20;   //初始页面行数
    $.fn.datagrid.defaults.pageList = [20,50,100,150,200];  //分页下拉显示行数
    /*option.rownumbers = true;   //是否显示rownum*/
    /* singleSelect:false, 是否单选*/
}


/*默认plugins option扩展*/
if ($.parser) {
    $.parser.auto = false; //关闭自动解析easyUI 标签
}




/***********************************以下为easyui 扩展方法*****************************/
/**
 * 扩展panel
 * tab关闭时回收内存,效果不大
 * 2012-07-24
 */
$.fn.panel.defaults.onBeforeDestroy = function() {
    var frame = $(this).find('iframe'); // 取this对象向下寻找iframe对象
    try {
        frame.get(0).contentWindow.close();
        frame.get(0).src = "";
        frame.remove();
        if ($.browser.msie) {
            CollectGarbage();
        }
    } catch (e) {
    }
};

if ($.fn.panel){ // 修改拦截器 ,不过滤head
    $.fn.panel.defaults.extractor = function(data){return data}
}


//$("div.datagrid-cell-check input[type=checkbox]",body).attr("checked",true);

$.extend($.fn.tabs.methods, {
    /*添加双击关闭事件,在每次新增tab后调用此方法, tab个数固定只需调用一次*/
    dbclickclose: function(jq) {
      var tabs = jq.tabs("tabs");
         for(var i=0; i<tabs.length; i++){
            tabs[i].panel('options').tab.unbind("dblclick").bind('dblclick',function(e){
            		var tab = $(this).find(".tabs-closable")[0].innerHTML;
                	jq.tabs('close', tab);  //传入{index:i} 以供调用 关闭索引                
                });
            }
         
        /*jq.find("ul.tabs").find("li").bind("dblclick", function() {
            var tab = $(this).find(".tabs-closable")[0].innerHTML;
            jq.tabs("close", tab);
        })*/
    }
});

/*刷新grid*/
function greload(cgrid){
	cgrid.datagrid("clearSelections").datagrid("reload");
}


/*grid 多选 获取选中的clientid*/
function getClients(grid){
	var rows =  grid.datagrid("getSelections");
	if(rows.length===0){
		return -1;
	}
	var ids = [];
	for(var i=0;i<rows.length;i++){
		var row = rows[i];
		ids[i]= row.clientId;
	}
	return ids;
}
/* 指定 _id 的多选集合*/
function getRowsId(grid, _id){
	var rows =  grid.datagrid("getSelections");
	if(rows.length===0){
		return -1;
	}
	var ids = [];
	for(var i=0;i<rows.length;i++){
		var row = rows[i];
		ids[i]= row[_id];
	}
	return ids;
}

/*grid 多选 获取选中的_id*/
function gchecked(grid){
	var rows =  grid.datagrid("getSelections");
	if(rows.length===0){
		return -1;
	}
	var ids = [];
	for(var i=0;i<rows.length;i++){
		var row = rows[i];
		ids[i]= row._id
	}
	return ids;
}

/*grid 选择单条 行数据*/
function gcheckedRow(grid){
	var rows =  grid.datagrid("getSelections");
	if(rows.length!=1){
		return -1;   //有且只能选择一条数据进行操作
	}
	return rows[0];
}