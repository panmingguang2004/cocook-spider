(function($) {
    var fn = $.fn;
    
    /*使用方法 $.toString(jq)*/
    fn.toString = function() {
        return  $("<div id='jquery_toString'>").append(this.clone()).html();
    };

    var tab = function(tabId,activeClass){
           $("#" + tabId + " li").click(function() {
               // 此思路为所有隐藏,选择开发,
                $(this).addClass(activeClass).siblings().removeClass(activeClass); //siblings 找到同辈元素
                $("#contents div").hide().eq($(this).index()).show(); //隐藏所有 eq[找到匹配的]显示

                //还有一种是, 先选中开发的隐藏,后选择我开发
                $(this).siblings().find("."+activeClass).removeClass(activeClass);
                $(this).addClass(activeClass);

            });
    }
    
    
      /*元素居中*/
    fn.center = function() {
        return this.css({
            position:'absolute',
            top:($(window).height() - $(this).height()) / 2 + $(window).scrollTop() + 'px',
            left: ($(window).width() - $(this).width()) / 2 + $(window).scrollLeft() + 'px'
        })
    };

    /* 判断浏览器是否是IE并且版本小于8,非IE返回浏览器类型和版本 */
    fn.isLessThanIe8 = function() {
        if ($.browser.msie) {
            return ($.browser.version > 7);
        }
        return navigator.userAgent;
    };

    /*ajax提交*/
    fn.ajaxSubmit = function(option) {
        var defaults = {
            action: this[0].action,
            type: this[0].method,
            async: true,
            data:{},
            success: function(data) {},
            validate: function(){return true;}
        };
        
        defaults.data = this.jsonForm();
        
        /*option = $.extend(true, {}, defaults, option); true 表示深度拷贝, 
         * 有多级数据,  也可以手动分级拷贝
         * console.info(defaults)
        */ 
        option = $.extend({}, defaults, option);  //先拷贝外层option,再拷贝data
        option.data = $.extend({}, defaults.data, option.data);
        if(!option.validate(option.data)){//数据 监测
        	return;
        }
        return $.ajax({
            url: option.action,
            type: option.type,
            async: option.async,
            data: option.data,
            success: function(data) {
                option.success(data);
                /*option.success.call(option, data);
                	如果回调的只是一个function,用以下方式
                * option.call(this, data)*/
            }
        })
        
    }
    
    //重写submit提交方法
    fn.ajaxForm = function(option){
    	var self = this;
    	return this.submit(function(){
    		self.ajaxSubmit(option);
    		return false;
    	});
    }
    
    fn.jsonForm = function(){
	  	var json = {};	
	  	var fields = this.serializeArray();
	  	//alert(this.serialize());
		$.each(fields, function(i, field) {
			//if(field.value.length ===0) return true;
			if(json[field.name]){  //原则不出现此种 状况, alert 做提醒而已, 有的时候有必要出现
				alert(json[field.name]);
				json[field.name] +=","+field.value;
			}else{
				json[field.name] = field.value;
			}
		});
	  	return json;
    }
    
    fn.deserialize= function(data){
    	var self = this;
    	var realKey;
	  	for(key in data){
	  		if(key=="f_id") {
	  			realKey = "id";
	  		}else{
	  			realKey = key;
	  		}
	  		name = "[name="+realKey+"]";
	  		self.find(name).val(data[key]);
	  	}
	  	return self;
    }
    
    fn.loadPage = function(url){
    	var self = this;
    	$.ajax({
    		url:url,
    		cache:false,	
    		dataType:"html",
			success:function(d){
    			self.html(d);
			}
    	})
    	return self;
    }
    
   
})(jQuery);


//全局的ajax访问，处理ajax清求时sesion超时
$.ajaxSetup({
    cache:false ,
    contentType:"application/x-www-form-urlencoded;charset=utf-8", 
    complete:function(XMLHttpRequest, textStatus) {
        var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus"); //通过XMLHttpRequest取得响应头，sessionstatus，
        if (sessionstatus == "timeout") {
            //如果超时就处理 ，指定要跳转的页面
            window.location.replace(path);
        }
    }
});

