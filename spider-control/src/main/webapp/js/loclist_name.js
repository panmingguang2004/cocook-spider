//Jet().$package("alloy.util.loclist",function(){this.LOCLIST={Location:
var Location=	
{
    "CountryRegion": [
        {
            "_Code": "1", 
            "_Name": "中国", 
            "State": [
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "东城"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "房山"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "通州"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "顺义"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "西城"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "昌平"
                        }, 
                        {
                            "_Code": "24", 
                            "_Name": "大兴"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "平谷"
                        }, 
                        {
                            "_Code": "27", 
                            "_Name": "怀柔"
                        }, 
                        {
                            "_Code": "28", 
                            "_Name": "密云"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "延庆"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "崇文"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "宣武"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "朝阳"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "丰台"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "石景山"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "海淀"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "门头沟"
                        }
                    ], 
                    "_Code": "11", 
                    "_Name": "北京"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "和平"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "东丽"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "西青"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "津南"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "北辰"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "河东"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "宁河"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "武清"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "静海"
                        }, 
                        {
                            "_Code": "24", 
                            "_Name": "宝坻"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "蓟县"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "河西"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "南开"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "河北"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "红桥"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "塘沽"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "汉沽"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "大港"
                        }
                    ], 
                    "_Code": "12", 
                    "_Name": "天津"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "石家庄"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "廊坊"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "衡水"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "唐山"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "秦皇岛"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "邯郸"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "邢台"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "保定"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "张家口"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "承德"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "沧州"
                        }
                    ], 
                    "_Code": "13", 
                    "_Name": "河北"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "太原"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "临汾"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "吕梁"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "大同"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "阳泉"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "长治"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "晋城"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "朔州"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "晋中"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "运城"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "忻州"
                        }
                    ], 
                    "_Code": "14", 
                    "_Name": "山西"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "呼和浩特"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "包头"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "兴安"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "锡林郭勒"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "阿拉善"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "乌海"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "赤峰"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "通辽"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "鄂尔多斯"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "呼伦贝尔"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "巴彦淖尔"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "乌兰察布"
                        }
                    ], 
                    "_Code": "15", 
                    "_Name": "内蒙古"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "沈阳"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "辽阳"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "盘锦"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "铁岭"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "朝阳"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "葫芦岛"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "大连"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "鞍山"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "抚顺"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "本溪"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "丹东"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "锦州"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "营口"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "阜新"
                        }
                    ], 
                    "_Code": "21", 
                    "_Name": "辽宁"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "长春"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "吉林"
                        }, 
                        {
                            "_Code": "24", 
                            "_Name": "延边"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "四平"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "辽源"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "通化"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "白山"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "松原"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "白城"
                        }
                    ], 
                    "_Code": "22", 
                    "_Name": "吉林"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "哈尔滨"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "牡丹江"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "黑河"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "绥化"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "齐齐哈尔"
                        }, 
                        {
                            "_Code": "27", 
                            "_Name": "大兴安岭"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "鸡西"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "鹤岗"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "双鸭山"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "大庆"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "伊春"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "佳木斯"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "七台河"
                        }
                    ], 
                    "_Code": "23", 
                    "_Name": "黑龙江"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "黄浦"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "杨浦"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "闵行"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "宝山"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "嘉定"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "浦东新"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "金山"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "松江"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "南汇"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "奉贤"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "青浦"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "卢湾"
                        }, 
                        {
                            "_Code": "30", 
                            "_Name": "崇明"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "徐汇"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "长宁"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "静安"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "普陀"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "闸北"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "虹口"
                        }
                    ], 
                    "_Code": "31", 
                    "_Name": "上海"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "南京"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "扬州"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "镇江"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "泰州"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "宿迁"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "无锡"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "徐州"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "常州"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "苏州"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "南通"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "连云港"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "淮安"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "盐城"
                        }
                    ], 
                    "_Code": "32", 
                    "_Name": "江苏"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "杭州"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "台州"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "丽水"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "宁波"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "温州"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "嘉兴"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "湖州"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "绍兴"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "金华"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "衢州"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "舟山"
                        }
                    ], 
                    "_Code": "33", 
                    "_Name": "浙江"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "合肥"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "黄山"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "滁州"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "阜阳"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "宿州"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "巢湖"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "六安"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "亳州"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "池州"
                        }, 
                        {
                            "_Code": "18", 
                            "_Name": "宣城"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "芜湖"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "蚌埠"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "淮南"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "马鞍山"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "淮北"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "铜陵"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "安庆"
                        }
                    ], 
                    "_Code": "34", 
                    "_Name": "安徽"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "福州"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "厦门"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "莆田"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "三明"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "泉州"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "漳州"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "南平"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "龙岩"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "宁德"
                        }
                    ], 
                    "_Code": "35", 
                    "_Name": "福建"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "南昌"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "抚州"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "上饶"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "景德镇"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "萍乡"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "九江"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "新余"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "鹰潭"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "赣州"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "吉安"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "宜春"
                        }
                    ], 
                    "_Code": "36", 
                    "_Name": "江西"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "济南"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "威海"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "日照"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "莱芜"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "临沂"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "德州"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "聊城"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "滨州"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "菏泽"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "青岛"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "淄博"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "枣庄"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "东营"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "烟台"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "潍坊"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "济宁"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "泰安"
                        }
                    ], 
                    "_Code": "37", 
                    "_Name": "山东"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "郑州"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "许昌"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "漯河"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "三门峡"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "南阳"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "商丘"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "信阳"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "周口"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "驻马店"
                        }, 
                        {
                            "_Code": "18", 
                            "_Name": "济源"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "开封"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "洛阳"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "平顶山"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "安阳"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "鹤壁"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "新乡"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "焦作"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "濮阳"
                        }
                    ], 
                    "_Code": "41", 
                    "_Name": "河南"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "武汉"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "荆州"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "黄冈"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "咸宁"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "随州"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "黄石"
                        }, 
                        {
                            "_Code": "28", 
                            "_Name": "恩施"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "十堰"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "宜昌"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "襄樊"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "鄂州"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "荆门"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "孝感"
                        }, 
                        {
                            "_Code": "94", 
                            "_Name": "仙桃"
                        }, 
                        {
                            "_Code": "95", 
                            "_Name": "潜江"
                        }, 
                        {
                            "_Code": "96", 
                            "_Name": "天门"
                        }, 
                        {
                            "_Code": "A21", 
                            "_Name": "神农架"
                        }
                    ], 
                    "_Code": "42", 
                    "_Name": "湖北"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "长沙"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "郴州"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "永州"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "怀化"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "娄底"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "株洲"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "湘潭"
                        }, 
                        {
                            "_Code": "31", 
                            "_Name": "湘西"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "衡阳"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "邵阳"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "岳阳"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "常德"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "张家界"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "益阳"
                        }
                    ], 
                    "_Code": "43", 
                    "_Name": "湖南"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "广州"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "肇庆"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "惠州"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "梅州"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "汕尾"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "河源"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "阳江"
                        }, 
                        {
                            "_Code": "18", 
                            "_Name": "清远"
                        }, 
                        {
                            "_Code": "19", 
                            "_Name": "东莞"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "韶关"
                        }, 
                        {
                            "_Code": "20", 
                            "_Name": "中山"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "深圳"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "珠海"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "汕头"
                        }, 
                        {
                            "_Code": "51", 
                            "_Name": "潮州"
                        }, 
                        {
                            "_Code": "52", 
                            "_Name": "揭阳"
                        }, 
                        {
                            "_Code": "53", 
                            "_Name": "云浮"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "佛山"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "江门"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "湛江"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "茂名"
                        }
                    ], 
                    "_Code": "44", 
                    "_Name": "广东"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "南宁"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "百色"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "贺州"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "河池"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "来宾"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "崇左"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "柳州"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "桂林"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "梧州"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "北海"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "防城港"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "钦州"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "贵港"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "玉林"
                        }
                    ], 
                    "_Code": "45", 
                    "_Name": "广西"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "海口"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "三亚"
                        }, 
                        {
                            "_Code": "91", 
                            "_Name": "五指山"
                        }, 
                        {
                            "_Code": "92", 
                            "_Name": "琼海"
                        }, 
                        {
                            "_Code": "93", 
                            "_Name": "儋州"
                        }, 
                        {
                            "_Code": "95", 
                            "_Name": "文昌"
                        }, 
                        {
                            "_Code": "96", 
                            "_Name": "万宁"
                        }, 
                        {
                            "_Code": "97", 
                            "_Name": "东方"
                        }, 
                        {
                            "_Code": "A25", 
                            "_Name": "定安"
                        }, 
                        {
                            "_Code": "A26", 
                            "_Name": "屯昌"
                        }, 
                        {
                            "_Code": "A27", 
                            "_Name": "澄迈"
                        }, 
                        {
                            "_Code": "A28", 
                            "_Name": "临高"
                        }, 
                        {
                            "_Code": "A30", 
                            "_Name": "白沙"
                        }, 
                        {
                            "_Code": "A31", 
                            "_Name": "昌江"
                        }, 
                        {
                            "_Code": "A33", 
                            "_Name": "乐东"
                        }, 
                        {
                            "_Code": "A34", 
                            "_Name": "陵水"
                        }, 
                        {
                            "_Code": "A35", 
                            "_Name": "保亭"
                        }, 
                        {
                            "_Code": "A36", 
                            "_Name": "琼中"
                        }, 
                        {
                            "_Code": "A37", 
                            "_Name": "西沙"
                        }, 
                        {
                            "_Code": "A38", 
                            "_Name": "南沙"
                        }, 
                        {
                            "_Code": "A39", 
                            "_Name": "中沙"
                        }
                    ], 
                    "_Code": "46", 
                    "_Name": "海南"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "万州"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "万盛"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "双桥"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "渝北"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "巴南"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "涪陵"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "长寿"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "綦江"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "潼南"
                        }, 
                        {
                            "_Code": "24", 
                            "_Name": "铜梁"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "大足"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "荣昌"
                        }, 
                        {
                            "_Code": "27", 
                            "_Name": "璧山"
                        }, 
                        {
                            "_Code": "28", 
                            "_Name": "梁平"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "城口"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "渝中"
                        }, 
                        {
                            "_Code": "30", 
                            "_Name": "丰都"
                        }, 
                        {
                            "_Code": "31", 
                            "_Name": "垫江"
                        }, 
                        {
                            "_Code": "32", 
                            "_Name": "武隆"
                        }, 
                        {
                            "_Code": "33", 
                            "_Name": "忠县"
                        }, 
                        {
                            "_Code": "34", 
                            "_Name": "开县"
                        }, 
                        {
                            "_Code": "35", 
                            "_Name": "云阳"
                        }, 
                        {
                            "_Code": "36", 
                            "_Name": "奉节"
                        }, 
                        {
                            "_Code": "37", 
                            "_Name": "巫山"
                        }, 
                        {
                            "_Code": "38", 
                            "_Name": "巫溪"
                        }, 
                        {
                            "_Code": "39", 
                            "_Name": "黔江"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "大渡口"
                        }, 
                        {
                            "_Code": "40", 
                            "_Name": "石柱"
                        }, 
                        {
                            "_Code": "41", 
                            "_Name": "秀山"
                        }, 
                        {
                            "_Code": "42", 
                            "_Name": "酉阳"
                        }, 
                        {
                            "_Code": "43", 
                            "_Name": "彭水"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "江北"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "沙坪坝"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "九龙坡"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "南岸"
                        }, 
                        {
                            "_Code": "81", 
                            "_Name": "江津"
                        }, 
                        {
                            "_Code": "82", 
                            "_Name": "合川"
                        }, 
                        {
                            "_Code": "83", 
                            "_Name": "永川"
                        }, 
                        {
                            "_Code": "84", 
                            "_Name": "南川"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "北碚"
                        }
                    ], 
                    "_Code": "50", 
                    "_Name": "重庆"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "成都"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "内江"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "乐山"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "南充"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "眉山"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "宜宾"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "广安"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "达州"
                        }, 
                        {
                            "_Code": "18", 
                            "_Name": "雅安"
                        }, 
                        {
                            "_Code": "19", 
                            "_Name": "巴中"
                        }, 
                        {
                            "_Code": "20", 
                            "_Name": "资阳"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "自贡"
                        }, 
                        {
                            "_Code": "32", 
                            "_Name": "阿坝"
                        }, 
                        {
                            "_Code": "33", 
                            "_Name": "甘孜"
                        }, 
                        {
                            "_Code": "34", 
                            "_Name": "凉山"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "攀枝花"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "泸州"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "德阳"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "绵阳"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "广元"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "遂宁"
                        }
                    ], 
                    "_Code": "51", 
                    "_Name": "四川"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "贵阳"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "六盘水"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "铜仁"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "黔西南"
                        }, 
                        {
                            "_Code": "24", 
                            "_Name": "毕节"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "黔东南"
                        }, 
                        {
                            "_Code": "27", 
                            "_Name": "黔南"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "遵义"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "安顺"
                        }
                    ], 
                    "_Code": "52", 
                    "_Name": "贵州"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "昆明"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "楚雄"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "红河"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "文山"
                        }, 
                        {
                            "_Code": "28", 
                            "_Name": "西双版纳"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "大理"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "曲靖"
                        }, 
                        {
                            "_Code": "31", 
                            "_Name": "德宏"
                        }, 
                        {
                            "_Code": "33", 
                            "_Name": "怒江"
                        }, 
                        {
                            "_Code": "34", 
                            "_Name": "迪庆"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "玉溪"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "保山"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "昭通"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "丽江"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "普洱"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "临沧"
                        }
                    ], 
                    "_Code": "53", 
                    "_Name": "云南"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "拉萨"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "昌都"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "山南"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "日喀则"
                        }, 
                        {
                            "_Code": "24", 
                            "_Name": "那曲"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "阿里"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "林芝"
                        }
                    ], 
                    "_Code": "54", 
                    "_Name": "西藏"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "西安"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "商洛"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "铜川"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "宝鸡"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "咸阳"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "渭南"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "延安"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "汉中"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "榆林"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "安康"
                        }
                    ], 
                    "_Code": "61", 
                    "_Name": "陕西"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "兰州市"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "庆阳"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "定西"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "陇南"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "嘉峪关"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "临夏"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "金昌"
                        }, 
                        {
                            "_Code": "30", 
                            "_Name": "甘南"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "白银"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "天水"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "武威"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "张掖"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "平凉"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "酒泉"
                        }
                    ], 
                    "_Code": "62", 
                    "_Name": "甘肃"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "西宁"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "海东"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "海北"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "黄南"
                        }, 
                        {
                            "_Code": "25", 
                            "_Name": "海南"
                        }, 
                        {
                            "_Code": "26", 
                            "_Name": "果洛"
                        }, 
                        {
                            "_Code": "27", 
                            "_Name": "玉树"
                        }, 
                        {
                            "_Code": "28", 
                            "_Name": "海西"
                        }
                    ], 
                    "_Code": "63", 
                    "_Name": "青海"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "银川"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "石嘴山"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "吴忠"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "固原"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "中卫"
                        }
                    ], 
                    "_Code": "64", 
                    "_Name": "宁夏"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "乌鲁木齐"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "克拉玛依"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "吐鲁番"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "哈密"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "昌吉"
                        }, 
                        {
                            "_Code": "27", 
                            "_Name": "博尔塔拉"
                        }, 
                        {
                            "_Code": "28", 
                            "_Name": "巴音郭楞"
                        }, 
                        {
                            "_Code": "29", 
                            "_Name": "阿克苏"
                        }, 
                        {
                            "_Code": "30", 
                            "_Name": "克孜勒苏"
                        }, 
                        {
                            "_Code": "31", 
                            "_Name": "喀什"
                        }, 
                        {
                            "_Code": "32", 
                            "_Name": "和田"
                        }, 
                        {
                            "_Code": "40", 
                            "_Name": "伊犁"
                        }, 
                        {
                            "_Code": "42", 
                            "_Name": "塔城"
                        }, 
                        {
                            "_Code": "43", 
                            "_Name": "阿勒泰"
                        }, 
                        {
                            "_Code": "91", 
                            "_Name": "石河子"
                        }, 
                        {
                            "_Code": "92", 
                            "_Name": "阿拉尔"
                        }, 
                        {
                            "_Code": "93", 
                            "_Name": "图木舒克"
                        }, 
                        {
                            "_Code": "94", 
                            "_Name": "五家渠"
                        }
                    ], 
                    "_Code": "65", 
                    "_Name": "新疆"
                }, 
                {
                    "City": [
                        {
                            "_Code": "1", 
                            "_Name": "台北市"
                        }, 
                        {
                            "_Code": "10", 
                            "_Name": "桃园县"
                        }, 
                        {
                            "_Code": "11", 
                            "_Name": "新竹县"
                        }, 
                        {
                            "_Code": "12", 
                            "_Name": "苗栗县"
                        }, 
                        {
                            "_Code": "13", 
                            "_Name": "台中县"
                        }, 
                        {
                            "_Code": "14", 
                            "_Name": "彰化县"
                        }, 
                        {
                            "_Code": "15", 
                            "_Name": "南投县"
                        }, 
                        {
                            "_Code": "16", 
                            "_Name": "云林县"
                        }, 
                        {
                            "_Code": "17", 
                            "_Name": "嘉义县"
                        }, 
                        {
                            "_Code": "18", 
                            "_Name": "台南县"
                        }, 
                        {
                            "_Code": "19", 
                            "_Name": "高雄县"
                        }, 
                        {
                            "_Code": "2", 
                            "_Name": "高雄市"
                        }, 
                        {
                            "_Code": "20", 
                            "_Name": "屏东县"
                        }, 
                        {
                            "_Code": "21", 
                            "_Name": "澎湖县"
                        }, 
                        {
                            "_Code": "22", 
                            "_Name": "台东县"
                        }, 
                        {
                            "_Code": "23", 
                            "_Name": "花莲县"
                        }, 
                        {
                            "_Code": "3", 
                            "_Name": "基隆市"
                        }, 
                        {
                            "_Code": "4", 
                            "_Name": "台中市"
                        }, 
                        {
                            "_Code": "5", 
                            "_Name": "台南市"
                        }, 
                        {
                            "_Code": "6", 
                            "_Name": "新竹市"
                        }, 
                        {
                            "_Code": "7", 
                            "_Name": "嘉义市"
                        }, 
                        {
                            "_Code": "8", 
                            "_Name": "台北县"
                        }, 
                        {
                            "_Code": "9", 
                            "_Name": "宜兰县"
                        }
                    ], 
                    "_Code": "71", 
                    "_Name": "台湾"
                }, 
                {
                    "City": [
                        {
                            "_Code": "HCW", 
                            "_Name": "中西区"
                        }, 
                        {
                            "_Code": "HEA", 
                            "_Name": "东区"
                        }, 
                        {
                            "_Code": "HSO", 
                            "_Name": "南区"
                        }, 
                        {
                            "_Code": "HWC", 
                            "_Name": "湾仔区"
                        }, 
                        {
                            "_Code": "KKC", 
                            "_Name": "九龙城区"
                        }, 
                        {
                            "_Code": "KKT", 
                            "_Name": "观塘区"
                        }, 
                        {
                            "_Code": "KSS", 
                            "_Name": "深水埗区"
                        }, 
                        {
                            "_Code": "KWT", 
                            "_Name": "黄大仙区"
                        }, 
                        {
                            "_Code": "KYT", 
                            "_Name": "油尖旺区"
                        }, 
                        {
                            "_Code": "NIS", 
                            "_Name": "离岛区"
                        }, 
                        {
                            "_Code": "NKT", 
                            "_Name": "葵青区"
                        }, 
                        {
                            "_Code": "NNO", 
                            "_Name": "北区"
                        }, 
                        {
                            "_Code": "NSK", 
                            "_Name": "西贡区"
                        }, 
                        {
                            "_Code": "NST", 
                            "_Name": "沙田区"
                        }, 
                        {
                            "_Code": "NTM", 
                            "_Name": "屯门区"
                        }, 
                        {
                            "_Code": "NTP", 
                            "_Name": "大埔区"
                        }, 
                        {
                            "_Code": "NTW", 
                            "_Name": "荃湾区"
                        }, 
                        {
                            "_Code": "NYL", 
                            "_Name": "元朗区"
                        }
                    ], 
                    "_Code": "81", 
                    "_Name": "香港"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ANT", 
                            "_Name": "圣安多尼堂区"
                        }, 
                        {
                            "_Code": "CAT", 
                            "_Name": "大堂区"
                        }, 
                        {
                            "_Code": "CLN", 
                            "_Name": "路环"
                        }, 
                        {
                            "_Code": "LAW", 
                            "_Name": "望德堂区"
                        }, 
                        {
                            "_Code": "LAZ", 
                            "_Name": "风顺堂区"
                        }, 
                        {
                            "_Code": "OLF", 
                            "_Name": "花地玛堂区"
                        }, 
                        {
                            "_Code": "TPA", 
                            "_Name": "氹仔"
                        }
                    ], 
                    "_Code": "82", 
                    "_Name": "澳门"
                }
            ]
        }, 
        {
            "_Code": "ABW", 
            "_Name": "阿鲁巴"
        }, 
        {
            "_Code": "AFG", 
            "_Name": "阿富汗", 
            "State": {
                "City": [
                    {
                        "_Code": "HEA", 
                        "_Name": "赫拉特"
                    }, 
                    {
                        "_Code": "KBL", 
                        "_Name": "喀布尔"
                    }, 
                    {
                        "_Code": "KDH", 
                        "_Name": "坎大哈"
                    }, 
                    {
                        "_Code": "MZR", 
                        "_Name": "马扎里沙里夫"
                    }
                ]
            }
        }, 
        {
            "_Code": "AGO", 
            "_Name": "安哥拉", 
            "State": {
                "City": [
                    {
                        "_Code": "BGO", 
                        "_Name": "本戈"
                    }, 
                    {
                        "_Code": "BGU", 
                        "_Name": "本格拉"
                    }, 
                    {
                        "_Code": "BIE", 
                        "_Name": "比耶"
                    }, 
                    {
                        "_Code": "CAB", 
                        "_Name": "卡宾达"
                    }, 
                    {
                        "_Code": "CCU", 
                        "_Name": "宽多库邦戈"
                    }, 
                    {
                        "_Code": "CNN", 
                        "_Name": "库内内"
                    }, 
                    {
                        "_Code": "CNO", 
                        "_Name": "北宽扎"
                    }, 
                    {
                        "_Code": "CUS", 
                        "_Name": "南宽扎"
                    }, 
                    {
                        "_Code": "HUA", 
                        "_Name": "万博"
                    }, 
                    {
                        "_Code": "HUI", 
                        "_Name": "威拉"
                    }, 
                    {
                        "_Code": "LNO", 
                        "_Name": "北隆达"
                    }, 
                    {
                        "_Code": "LSU", 
                        "_Name": "南隆达"
                    }, 
                    {
                        "_Code": "LUA", 
                        "_Name": "罗安达"
                    }, 
                    {
                        "_Code": "MAL", 
                        "_Name": "马兰热"
                    }, 
                    {
                        "_Code": "MOX", 
                        "_Name": "莫希科"
                    }, 
                    {
                        "_Code": "NAM", 
                        "_Name": "纳米贝"
                    }, 
                    {
                        "_Code": "UIG", 
                        "_Name": "威热"
                    }, 
                    {
                        "_Code": "ZAI", 
                        "_Name": "扎伊尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "AIA", 
            "_Name": "安圭拉"
        }, 
        {
            "_Code": "ALA", 
            "_Name": "奥兰群岛"
        }, 
        {
            "_Code": "ALB", 
            "_Name": "阿尔巴尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BR", 
                        "_Name": "培拉特"
                    }, 
                    {
                        "_Code": "DI", 
                        "_Name": "迪勃拉"
                    }, 
                    {
                        "_Code": "DR", 
                        "_Name": "都拉斯"
                    }, 
                    {
                        "_Code": "EL", 
                        "_Name": "爱尔巴桑"
                    }, 
                    {
                        "_Code": "FR", 
                        "_Name": "费里"
                    }, 
                    {
                        "_Code": "GJ", 
                        "_Name": "吉诺卡斯特"
                    }, 
                    {
                        "_Code": "KO", 
                        "_Name": "科尔察"
                    }, 
                    {
                        "_Code": "KU", 
                        "_Name": "库克斯"
                    }, 
                    {
                        "_Code": "LE", 
                        "_Name": "莱什"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "斯库台"
                    }, 
                    {
                        "_Code": "TR", 
                        "_Name": "地拉那"
                    }, 
                    {
                        "_Code": "VL", 
                        "_Name": "发罗拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "AND", 
            "_Name": "安道尔", 
            "State": {
                "City": [
                    {
                        "_Code": "2", 
                        "_Name": "卡尼略"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "恩坎普"
                    }, 
                    {
                        "_Code": "4", 
                        "_Name": "马萨纳"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "奥尔迪诺"
                    }, 
                    {
                        "_Code": "6", 
                        "_Name": "圣胡利娅－德洛里亚"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "安道尔城"
                    }, 
                    {
                        "_Code": "8", 
                        "_Name": "莱塞斯卡尔德－恩戈尔达"
                    }
                ]
            }
        }, 
        {
            "_Code": "ANT", 
            "_Name": "荷属安地列斯"
        }, 
        {
            "_Code": "ARE", 
            "_Name": "阿拉伯联合酋长国", 
            "State": {
                "City": [
                    {
                        "_Code": "AL", 
                        "_Name": "艾因"
                    }, 
                    {
                        "_Code": "AZ", 
                        "_Name": "阿布扎比"
                    }, 
                    {
                        "_Code": "DU", 
                        "_Name": "迪拜"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "沙迦"
                    }
                ]
            }
        }, 
        {
            "_Code": "ARG", 
            "_Name": "阿根廷", 
            "State": {
                "City": [
                    {
                        "_Code": "AFA", 
                        "_Name": "圣拉斐尔"
                    }, 
                    {
                        "_Code": "BHI", 
                        "_Name": "布兰卡港"
                    }, 
                    {
                        "_Code": "BUE", 
                        "_Name": "布宜诺斯艾利斯"
                    }, 
                    {
                        "_Code": "CNQ", 
                        "_Name": "科连特斯"
                    }, 
                    {
                        "_Code": "COC", 
                        "_Name": "肯考迪娅"
                    }, 
                    {
                        "_Code": "COR", 
                        "_Name": "科尔多瓦"
                    }, 
                    {
                        "_Code": "CRD", 
                        "_Name": "里瓦达维亚海军准将城"
                    }, 
                    {
                        "_Code": "CTC", 
                        "_Name": "卡塔马卡"
                    }, 
                    {
                        "_Code": "FMA", 
                        "_Name": "福莫萨"
                    }, 
                    {
                        "_Code": "IRJ", 
                        "_Name": "拉里奥哈"
                    }, 
                    {
                        "_Code": "JUJ", 
                        "_Name": "胡胡伊"
                    }, 
                    {
                        "_Code": "LPG", 
                        "_Name": "拉普拉塔"
                    }, 
                    {
                        "_Code": "LUQ", 
                        "_Name": "圣路易斯"
                    }, 
                    {
                        "_Code": "MDQ", 
                        "_Name": "马德普拉塔"
                    }, 
                    {
                        "_Code": "MDZ", 
                        "_Name": "门多萨"
                    }, 
                    {
                        "_Code": "NQN", 
                        "_Name": "内乌肯"
                    }, 
                    {
                        "_Code": "PRA", 
                        "_Name": "巴拉那"
                    }, 
                    {
                        "_Code": "PSS", 
                        "_Name": "波萨达斯"
                    }, 
                    {
                        "_Code": "RCU", 
                        "_Name": "里奥夸尔托"
                    }, 
                    {
                        "_Code": "REL", 
                        "_Name": "特雷利乌"
                    }, 
                    {
                        "_Code": "RES", 
                        "_Name": "雷西斯滕匹亚"
                    }, 
                    {
                        "_Code": "RGL", 
                        "_Name": "里奥加耶戈斯"
                    }, 
                    {
                        "_Code": "ROS", 
                        "_Name": "罗萨里奥"
                    }, 
                    {
                        "_Code": "RSA", 
                        "_Name": "圣罗莎"
                    }, 
                    {
                        "_Code": "RWO", 
                        "_Name": "罗森"
                    }, 
                    {
                        "_Code": "SDE", 
                        "_Name": "圣地亚哥-德尔埃斯特罗"
                    }, 
                    {
                        "_Code": "SFN", 
                        "_Name": "圣菲"
                    }, 
                    {
                        "_Code": "SLA", 
                        "_Name": "萨尔塔"
                    }, 
                    {
                        "_Code": "SMC", 
                        "_Name": "圣米格尔-德图库曼"
                    }, 
                    {
                        "_Code": "SNS", 
                        "_Name": "圣尼古拉斯"
                    }, 
                    {
                        "_Code": "UAQ", 
                        "_Name": "圣胡安"
                    }, 
                    {
                        "_Code": "USH", 
                        "_Name": "乌斯怀亚"
                    }, 
                    {
                        "_Code": "VDM", 
                        "_Name": "别德马"
                    }, 
                    {
                        "_Code": "VLK", 
                        "_Name": "克劳斯城"
                    }
                ]
            }
        }, 
        {
            "_Code": "ARM", 
            "_Name": "亚美尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AGT", 
                        "_Name": "阿拉加措特恩"
                    }, 
                    {
                        "_Code": "ARA", 
                        "_Name": "阿拉拉特"
                    }, 
                    {
                        "_Code": "ARM", 
                        "_Name": "阿尔马维尔"
                    }, 
                    {
                        "_Code": "EVN", 
                        "_Name": "埃里温市"
                    }, 
                    {
                        "_Code": "GEG", 
                        "_Name": "格加尔库尼克"
                    }, 
                    {
                        "_Code": "KOT", 
                        "_Name": "科泰克"
                    }, 
                    {
                        "_Code": "LOR", 
                        "_Name": "洛里"
                    }, 
                    {
                        "_Code": "SHI", 
                        "_Name": "希拉克"
                    }, 
                    {
                        "_Code": "SYU", 
                        "_Name": "休尼克"
                    }, 
                    {
                        "_Code": "TAV", 
                        "_Name": "塔武什"
                    }, 
                    {
                        "_Code": "VAY", 
                        "_Name": "瓦约茨·佐尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "ASC", 
            "_Name": "阿森松岛"
        }, 
        {
            "_Code": "ASM", 
            "_Name": "美属萨摩亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AAN", 
                        "_Name": "阿纳"
                    }, 
                    {
                        "_Code": "AIT", 
                        "_Name": "艾加伊勒泰"
                    }, 
                    {
                        "_Code": "ATU", 
                        "_Name": "阿图阿"
                    }, 
                    {
                        "_Code": "FAA", 
                        "_Name": "法塞莱莱阿加"
                    }, 
                    {
                        "_Code": "GFG", 
                        "_Name": "加盖福毛加"
                    }, 
                    {
                        "_Code": "GMG", 
                        "_Name": "加加埃毛加"
                    }, 
                    {
                        "_Code": "PAL", 
                        "_Name": "帕劳利"
                    }, 
                    {
                        "_Code": "SAT", 
                        "_Name": "萨图帕伊泰阿"
                    }, 
                    {
                        "_Code": "SAV", 
                        "_Name": "萨瓦伊岛"
                    }, 
                    {
                        "_Code": "TUA", 
                        "_Name": "图阿马萨加"
                    }, 
                    {
                        "_Code": "UPO", 
                        "_Name": "乌波卢岛"
                    }, 
                    {
                        "_Code": "VAF", 
                        "_Name": "瓦奥福诺蒂"
                    }, 
                    {
                        "_Code": "VAI", 
                        "_Name": "韦西加诺"
                    }
                ]
            }
        }, 
        {
            "_Code": "ATA", 
            "_Name": "南极洲"
        }, 
        {
            "_Code": "ATF", 
            "_Name": "法属南部领地"
        }, 
        {
            "_Code": "ATG", 
            "_Name": "安提瓜岛和巴布达"
        }, 
        {
            "_Code": "AUS", 
            "_Name": "澳大利亚", 
            "State": [
                {
                    "City": {
                        "_Code": "CBR", 
                        "_Name": "堪培拉"
                    }, 
                    "_Code": "ACT", 
                    "_Name": "堪培拉"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BNE", 
                            "_Name": "布里斯班"
                        }, 
                        {
                            "_Code": "CNS", 
                            "_Name": "凯恩斯"
                        }, 
                        {
                            "_Code": "CUD", 
                            "_Name": "日光海岸"
                        }, 
                        {
                            "_Code": "OOL", 
                            "_Name": "黄金海岸"
                        }, 
                        {
                            "_Code": "TSV", 
                            "_Name": "汤斯维尔"
                        }, 
                        {
                            "_Code": "TWB", 
                            "_Name": "图文巴"
                        }
                    ], 
                    "_Code": "QLD", 
                    "_Name": "昆士兰"
                }, 
                {
                    "City": [
                        {
                            "_Code": "HBS", 
                            "_Name": "悉尼"
                        }, 
                        {
                            "_Code": "NTL", 
                            "_Name": "纽卡斯尔"
                        }, 
                        {
                            "_Code": "WOL", 
                            "_Name": "伍伦贡"
                        }
                    ], 
                    "_Code": "NSW", 
                    "_Name": "新南威尔士"
                }, 
                {
                    "City": [
                        {
                            "_Code": "DRW", 
                            "_Name": "达尔文"
                        }, 
                        {
                            "_Code": "PAL", 
                            "_Name": "北帕默斯顿"
                        }
                    ], 
                    "_Code": "NT", 
                    "_Name": "北部地区"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ADL", 
                            "_Name": "阿德莱德"
                        }, 
                        {
                            "_Code": "MGB", 
                            "_Name": "甘比亚山"
                        }, 
                        {
                            "_Code": "MYB", 
                            "_Name": "默里布里奇"
                        }, 
                        {
                            "_Code": "PLO", 
                            "_Name": "林肯港"
                        }, 
                        {
                            "_Code": "PPI", 
                            "_Name": "皮里港"
                        }, 
                        {
                            "_Code": "PUG", 
                            "_Name": "奥古斯塔港"
                        }, 
                        {
                            "_Code": "VHA", 
                            "_Name": "维克托港"
                        }, 
                        {
                            "_Code": "WAY", 
                            "_Name": "怀阿拉"
                        }
                    ], 
                    "_Code": "SA", 
                    "_Name": "南澳大利亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BWT", 
                            "_Name": "伯尼港"
                        }, 
                        {
                            "_Code": "DPO", 
                            "_Name": "德文波特"
                        }, 
                        {
                            "_Code": "HBA", 
                            "_Name": "霍巴特"
                        }, 
                        {
                            "_Code": "LST", 
                            "_Name": "朗塞斯顿"
                        }
                    ], 
                    "_Code": "TAS", 
                    "_Name": "塔斯马尼亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "GEX", 
                            "_Name": "吉朗"
                        }, 
                        {
                            "_Code": "MEL", 
                            "_Name": "墨尔本"
                        }
                    ], 
                    "_Code": "VIC", 
                    "_Name": "维多利亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ALH", 
                            "_Name": "奥尔巴尼"
                        }, 
                        {
                            "_Code": "BUY", 
                            "_Name": "班伯里"
                        }, 
                        {
                            "_Code": "FRE", 
                            "_Name": "弗里曼特尔港"
                        }, 
                        {
                            "_Code": "GET", 
                            "_Name": "杰拉尔顿"
                        }, 
                        {
                            "_Code": "KGI", 
                            "_Name": "卡尔古利"
                        }, 
                        {
                            "_Code": "MDU", 
                            "_Name": "曼哲拉"
                        }, 
                        {
                            "_Code": "PER", 
                            "_Name": "珀斯"
                        }
                    ], 
                    "_Code": "WA", 
                    "_Name": "西澳大利亚"
                }
            ]
        }, 
        {
            "_Code": "AUT", 
            "_Name": "奥地利", 
            "State": {
                "City": [
                    {
                        "_Code": "BUR", 
                        "_Name": "布尔根兰"
                    }, 
                    {
                        "_Code": "CAT", 
                        "_Name": "克恩顿"
                    }, 
                    {
                        "_Code": "LAU", 
                        "_Name": "下奥地利"
                    }, 
                    {
                        "_Code": "STY", 
                        "_Name": "施蒂利亚"
                    }, 
                    {
                        "_Code": "SZG", 
                        "_Name": "萨尔茨堡"
                    }, 
                    {
                        "_Code": "TYR", 
                        "_Name": "蒂罗尔"
                    }, 
                    {
                        "_Code": "UAU", 
                        "_Name": "上奥地利"
                    }, 
                    {
                        "_Code": "VDD", 
                        "_Name": "维也纳"
                    }, 
                    {
                        "_Code": "VOR", 
                        "_Name": "福拉尔贝格"
                    }
                ]
            }
        }, 
        {
            "_Code": "AZE", 
            "_Name": "阿塞拜疆", 
            "State": {
                "City": [
                    {
                        "_Code": "ABS", 
                        "_Name": "阿布歇隆"
                    }, 
                    {
                        "_Code": "GA", 
                        "_Name": "占贾"
                    }, 
                    {
                        "_Code": "XAC", 
                        "_Name": "哈奇马斯"
                    }, 
                    {
                        "_Code": "KAL", 
                        "_Name": "卡尔巴卡尔"
                    }, 
                    {
                        "_Code": "QAZ", 
                        "_Name": "卡扎赫"
                    }, 
                    {
                        "_Code": "LAN", 
                        "_Name": "连科兰"
                    }, 
                    {
                        "_Code": "MQA", 
                        "_Name": "密尔-卡拉巴赫"
                    }, 
                    {
                        "_Code": "MSA", 
                        "_Name": "穆甘-萨连"
                    }, 
                    {
                        "_Code": "NX", 
                        "_Name": "纳希切万"
                    }, 
                    {
                        "_Code": "NQA", 
                        "_Name": "纳戈尔诺－卡拉巴赫"
                    }, 
                    {
                        "_Code": "PRI", 
                        "_Name": "普利亚拉克斯"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "舍基"
                    }, 
                    {
                        "_Code": "SIR", 
                        "_Name": "锡尔万"
                    }, 
                    {
                        "_Code": "SMC", 
                        "_Name": "苏姆盖特"
                    }
                ]
            }
        }, 
        {
            "_Code": "BDI", 
            "_Name": "布隆迪", 
            "State": {
                "City": [
                    {
                        "_Code": "BB", 
                        "_Name": "布班扎"
                    }, 
                    {
                        "_Code": "BM", 
                        "_Name": "布琼布拉城市"
                    }, 
                    {
                        "_Code": "BR", 
                        "_Name": "布鲁里"
                    }, 
                    {
                        "_Code": "BU", 
                        "_Name": "布琼布拉乡村"
                    }, 
                    {
                        "_Code": "CA", 
                        "_Name": "坎库佐"
                    }, 
                    {
                        "_Code": "CI", 
                        "_Name": "锡比托凯"
                    }, 
                    {
                        "_Code": "GI", 
                        "_Name": "基特加"
                    }, 
                    {
                        "_Code": "KI", 
                        "_Name": "基龙多"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "卡鲁济"
                    }, 
                    {
                        "_Code": "KY", 
                        "_Name": "卡扬扎"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马坎巴"
                    }, 
                    {
                        "_Code": "MU", 
                        "_Name": "穆拉姆维亚"
                    }, 
                    {
                        "_Code": "MW", 
                        "_Name": "穆瓦洛"
                    }, 
                    {
                        "_Code": "MY", 
                        "_Name": "穆因加"
                    }, 
                    {
                        "_Code": "NG", 
                        "_Name": "恩戈齐"
                    }, 
                    {
                        "_Code": "RT", 
                        "_Name": "鲁塔纳"
                    }, 
                    {
                        "_Code": "RY", 
                        "_Name": "鲁伊吉"
                    }
                ]
            }
        }, 
        {
            "_Code": "BEL", 
            "_Name": "比利时", 
            "State": {
                "City": [
                    {
                        "_Code": "BRU", 
                        "_Name": "布鲁塞尔"
                    }, 
                    {
                        "_Code": "VAN", 
                        "_Name": "安特卫普"
                    }, 
                    {
                        "_Code": "VBR", 
                        "_Name": "佛兰芒-布拉班特"
                    }, 
                    {
                        "_Code": "VLI", 
                        "_Name": "林堡"
                    }, 
                    {
                        "_Code": "VOV", 
                        "_Name": "东佛兰德"
                    }, 
                    {
                        "_Code": "VWV", 
                        "_Name": "西佛兰德"
                    }, 
                    {
                        "_Code": "WBR", 
                        "_Name": "布拉班特-瓦隆"
                    }, 
                    {
                        "_Code": "WHT", 
                        "_Name": "埃诺"
                    }, 
                    {
                        "_Code": "WLG", 
                        "_Name": "列日"
                    }, 
                    {
                        "_Code": "WLX", 
                        "_Name": "卢森堡"
                    }, 
                    {
                        "_Code": "WNA", 
                        "_Name": "那慕尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "BEN", 
            "_Name": "贝宁", 
            "State": {
                "City": [
                    {
                        "_Code": "AK", 
                        "_Name": "阿塔科拉"
                    }, 
                    {
                        "_Code": "AQ", 
                        "_Name": "大西洋"
                    }, 
                    {
                        "_Code": "AL", 
                        "_Name": "阿黎博里"
                    }, 
                    {
                        "_Code": "BO", 
                        "_Name": "博尔古"
                    }, 
                    {
                        "_Code": "BOH", 
                        "_Name": "波希康市"
                    }, 
                    {
                        "_Code": "CO", 
                        "_Name": "丘陵"
                    }, 
                    {
                        "_Code": "DO", 
                        "_Name": "峡谷"
                    }, 
                    {
                        "_Code": "KO", 
                        "_Name": "库福"
                    }, 
                    {
                        "_Code": "LI", 
                        "_Name": "滨海"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "莫诺"
                    }, 
                    {
                        "_Code": "OU", 
                        "_Name": "韦梅"
                    }, 
                    {
                        "_Code": "PL", 
                        "_Name": "高原"
                    }, 
                    {
                        "_Code": "ZO", 
                        "_Name": "祖"
                    }
                ]
            }
        }, 
        {
            "_Code": "BFA", 
            "_Name": "布基纳法索", 
            "State": {
                "City": [
                    {
                        "_Code": "BAL", 
                        "_Name": "巴雷"
                    }, 
                    {
                        "_Code": "BAM", 
                        "_Name": "巴姆"
                    }, 
                    {
                        "_Code": "BAN", 
                        "_Name": "巴瓦"
                    }, 
                    {
                        "_Code": "BAZ", 
                        "_Name": "巴泽加"
                    }, 
                    {
                        "_Code": "BLG", 
                        "_Name": "布尔古"
                    }, 
                    {
                        "_Code": "BOK", 
                        "_Name": "布尔基恩德"
                    }, 
                    {
                        "_Code": "BOR", 
                        "_Name": "布古里巴"
                    }, 
                    {
                        "_Code": "COM", 
                        "_Name": "科莫埃"
                    }, 
                    {
                        "_Code": "GAN", 
                        "_Name": "冈祖尔古"
                    }, 
                    {
                        "_Code": "GNA", 
                        "_Name": "尼亚尼亚"
                    }, 
                    {
                        "_Code": "GOU", 
                        "_Name": "古尔马"
                    }, 
                    {
                        "_Code": "HOU", 
                        "_Name": "乌埃"
                    }, 
                    {
                        "_Code": "IOA", 
                        "_Name": "伊奥巴"
                    }, 
                    {
                        "_Code": "KAD", 
                        "_Name": "卡焦戈"
                    }, 
                    {
                        "_Code": "KEN", 
                        "_Name": "凯内杜古"
                    }, 
                    {
                        "_Code": "KOL", 
                        "_Name": "库尔佩罗戈"
                    }, 
                    {
                        "_Code": "KOO", 
                        "_Name": "科蒙加里"
                    }, 
                    {
                        "_Code": "KOP", 
                        "_Name": "孔皮恩加"
                    }, 
                    {
                        "_Code": "KOS", 
                        "_Name": "孔西"
                    }, 
                    {
                        "_Code": "KOT", 
                        "_Name": "库里滕加"
                    }, 
                    {
                        "_Code": "KOW", 
                        "_Name": "库尔维奥戈"
                    }, 
                    {
                        "_Code": "LER", 
                        "_Name": "雷拉巴"
                    }, 
                    {
                        "_Code": "LOR", 
                        "_Name": "罗卢姆"
                    }, 
                    {
                        "_Code": "MOU", 
                        "_Name": "穆翁"
                    }, 
                    {
                        "_Code": "NAH", 
                        "_Name": "纳乌里"
                    }, 
                    {
                        "_Code": "NAM", 
                        "_Name": "纳门滕加"
                    }, 
                    {
                        "_Code": "NAY", 
                        "_Name": "纳亚拉"
                    }, 
                    {
                        "_Code": "NOU", 
                        "_Name": "努姆比埃尔"
                    }, 
                    {
                        "_Code": "OUB", 
                        "_Name": "乌布里滕加"
                    }, 
                    {
                        "_Code": "OUD", 
                        "_Name": "乌达兰"
                    }, 
                    {
                        "_Code": "PAS", 
                        "_Name": "帕索雷"
                    }, 
                    {
                        "_Code": "PON", 
                        "_Name": "波尼"
                    }, 
                    {
                        "_Code": "SAG", 
                        "_Name": "桑吉"
                    }, 
                    {
                        "_Code": "SAM", 
                        "_Name": "桑马滕加"
                    }, 
                    {
                        "_Code": "SEN", 
                        "_Name": "塞诺"
                    }, 
                    {
                        "_Code": "SIS", 
                        "_Name": "锡西里"
                    }, 
                    {
                        "_Code": "SOM", 
                        "_Name": "苏姆"
                    }, 
                    {
                        "_Code": "SOR", 
                        "_Name": "苏鲁"
                    }, 
                    {
                        "_Code": "TAP", 
                        "_Name": "塔波阿"
                    }, 
                    {
                        "_Code": "TUY", 
                        "_Name": "图伊"
                    }, 
                    {
                        "_Code": "YAG", 
                        "_Name": "亚加"
                    }, 
                    {
                        "_Code": "YAT", 
                        "_Name": "亚滕加"
                    }, 
                    {
                        "_Code": "ZIR", 
                        "_Name": "济罗"
                    }, 
                    {
                        "_Code": "ZOD", 
                        "_Name": "宗多马"
                    }, 
                    {
                        "_Code": "ZOW", 
                        "_Name": "宗德韦奥戈"
                    }
                ]
            }
        }, 
        {
            "_Code": "BGD", 
            "_Name": "孟加拉", 
            "State": {
                "City": [
                    {
                        "_Code": "CGP", 
                        "_Name": "吉大港"
                    }, 
                    {
                        "_Code": "DAC", 
                        "_Name": "达卡"
                    }, 
                    {
                        "_Code": "KHL", 
                        "_Name": "库尔纳"
                    }
                ]
            }
        }, 
        {
            "_Code": "BGR", 
            "_Name": "保加利亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BOJ", 
                        "_Name": "布尔加斯"
                    }, 
                    {
                        "_Code": "GSO", 
                        "_Name": "索非亚市"
                    }, 
                    {
                        "_Code": "KHO", 
                        "_Name": "卡斯科伏"
                    }, 
                    {
                        "_Code": "LVP", 
                        "_Name": "洛维奇"
                    }, 
                    {
                        "_Code": "OZA", 
                        "_Name": "蒙塔纳"
                    }, 
                    {
                        "_Code": "PDV", 
                        "_Name": "普罗夫迪夫"
                    }, 
                    {
                        "_Code": "ROU", 
                        "_Name": "鲁塞"
                    }, 
                    {
                        "_Code": "SOF", 
                        "_Name": "索非亚"
                    }, 
                    {
                        "_Code": "VAR", 
                        "_Name": "瓦尔纳"
                    }
                ]
            }
        }, 
        {
            "_Code": "BHR", 
            "_Name": "巴林", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "哈德"
                    }, 
                    {
                        "_Code": "10", 
                        "_Name": "西部"
                    }, 
                    {
                        "_Code": "12", 
                        "_Name": "哈马德"
                    }, 
                    {
                        "_Code": "2", 
                        "_Name": "穆哈拉格"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "麦纳麦"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "中部"
                    }, 
                    {
                        "_Code": "8", 
                        "_Name": "伊萨城"
                    }, 
                    {
                        "_Code": "9", 
                        "_Name": "里法"
                    }
                ]
            }
        }, 
        {
            "_Code": "BHS", 
            "_Name": "巴哈马"
        }, 
        {
            "_Code": "BIH", 
            "_Name": "波斯尼亚和黑塞哥维那", 
            "State": {
                "City": [
                    {
                        "_Code": "FBP", 
                        "_Name": "波斯尼亚－波德里涅"
                    }, 
                    {
                        "_Code": "FHB", 
                        "_Name": "西波斯尼亚"
                    }, 
                    {
                        "_Code": "FHN", 
                        "_Name": "黑塞哥维那－涅雷特瓦"
                    }, 
                    {
                        "_Code": "FPO", 
                        "_Name": "波萨维纳"
                    }, 
                    {
                        "_Code": "FSA", 
                        "_Name": "萨拉热窝"
                    }, 
                    {
                        "_Code": "FSB", 
                        "_Name": "中波斯尼亚"
                    }, 
                    {
                        "_Code": "FTO", 
                        "_Name": "多米斯拉夫格勒"
                    }, 
                    {
                        "_Code": "FTU", 
                        "_Name": "图兹拉－波德里涅"
                    }, 
                    {
                        "_Code": "FUS", 
                        "_Name": "乌纳－萨纳"
                    }, 
                    {
                        "_Code": "FZE", 
                        "_Name": "泽尼察－多博伊"
                    }, 
                    {
                        "_Code": "FZH", 
                        "_Name": "西黑塞哥维那"
                    }
                ]
            }
        }, 
        {
            "_Code": "BLR", 
            "_Name": "白俄罗斯", 
            "State": {
                "City": [
                    {
                        "_Code": "BR", 
                        "_Name": "布列斯特"
                    }, 
                    {
                        "_Code": "HO", 
                        "_Name": "戈梅利"
                    }, 
                    {
                        "_Code": "HR", 
                        "_Name": "格罗德诺"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "莫吉廖夫"
                    }, 
                    {
                        "_Code": "MI", 
                        "_Name": "明斯克市"
                    }, 
                    {
                        "_Code": "VI", 
                        "_Name": "维捷布斯克"
                    }
                ]
            }
        }, 
        {
            "_Code": "BLZ", 
            "_Name": "伯利兹", 
            "State": {
                "City": [
                    {
                        "_Code": "BZ", 
                        "_Name": "伯利兹"
                    }, 
                    {
                        "_Code": "CR", 
                        "_Name": "科罗萨尔"
                    }, 
                    {
                        "_Code": "CY", 
                        "_Name": "卡约"
                    }, 
                    {
                        "_Code": "OW", 
                        "_Name": "橘园"
                    }, 
                    {
                        "_Code": "SC", 
                        "_Name": "斯坦港"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "托莱多"
                    }
                ]
            }
        }, 
        {
            "_Code": "BMU", 
            "_Name": "百慕大"
        }, 
        {
            "_Code": "BOL", 
            "_Name": "玻利维亚", 
            "State": {
                "City": [
                    {
                        "_Code": "ALT", 
                        "_Name": "奥尔托"
                    }, 
                    {
                        "_Code": "BEN", 
                        "_Name": "贝尼"
                    }, 
                    {
                        "_Code": "CBB", 
                        "_Name": "科恰班巴"
                    }, 
                    {
                        "_Code": "CHU", 
                        "_Name": "丘基萨卡"
                    }, 
                    {
                        "_Code": "QUI", 
                        "_Name": "基拉科洛"
                    }, 
                    {
                        "_Code": "LPB", 
                        "_Name": "拉巴斯"
                    }, 
                    {
                        "_Code": "ORU", 
                        "_Name": "奥鲁罗"
                    }, 
                    {
                        "_Code": "PAN", 
                        "_Name": "潘多"
                    }, 
                    {
                        "_Code": "POI", 
                        "_Name": "波多西"
                    }, 
                    {
                        "_Code": "SAC", 
                        "_Name": "萨卡巴"
                    }, 
                    {
                        "_Code": "SRZ", 
                        "_Name": "圣克鲁斯"
                    }, 
                    {
                        "_Code": "TJA", 
                        "_Name": "塔里哈"
                    }
                ]
            }
        }, 
        {
            "_Code": "BRA", 
            "_Name": "巴西", 
            "State": {
                "City": [
                    {
                        "_Code": "AC", 
                        "_Name": "阿克里"
                    }, 
                    {
                        "_Code": "AL", 
                        "_Name": "阿拉戈斯"
                    }, 
                    {
                        "_Code": "AM", 
                        "_Name": "亚马孙"
                    }, 
                    {
                        "_Code": "AP", 
                        "_Name": "阿马帕"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "巴伊亚"
                    }, 
                    {
                        "_Code": "BSB", 
                        "_Name": "巴西利亚"
                    }, 
                    {
                        "_Code": "CE", 
                        "_Name": "塞阿拉"
                    }, 
                    {
                        "_Code": "ES", 
                        "_Name": "圣埃斯皮里图"
                    }, 
                    {
                        "_Code": "GO", 
                        "_Name": "戈亚斯"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马拉尼昂"
                    }, 
                    {
                        "_Code": "MG", 
                        "_Name": "米纳斯吉拉斯"
                    }, 
                    {
                        "_Code": "MS", 
                        "_Name": "南马托格罗索"
                    }, 
                    {
                        "_Code": "MT", 
                        "_Name": "马托格罗索"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "帕拉"
                    }, 
                    {
                        "_Code": "PB", 
                        "_Name": "帕拉伊巴"
                    }, 
                    {
                        "_Code": "PE", 
                        "_Name": "伯南布哥"
                    }, 
                    {
                        "_Code": "PI", 
                        "_Name": "皮奥伊"
                    }, 
                    {
                        "_Code": "PR", 
                        "_Name": "巴拉那"
                    }, 
                    {
                        "_Code": "RJ", 
                        "_Name": "里约热内卢"
                    }, 
                    {
                        "_Code": "RN", 
                        "_Name": "北里奥格兰德"
                    }, 
                    {
                        "_Code": "RO", 
                        "_Name": "朗多尼亚"
                    }, 
                    {
                        "_Code": "RR", 
                        "_Name": "罗赖马"
                    }, 
                    {
                        "_Code": "RS", 
                        "_Name": "南里奥格兰德"
                    }, 
                    {
                        "_Code": "SC", 
                        "_Name": "圣卡塔琳娜"
                    }, 
                    {
                        "_Code": "SE", 
                        "_Name": "塞尔希培"
                    }, 
                    {
                        "_Code": "SP", 
                        "_Name": "圣保罗"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "托坎廷斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "BRB", 
            "_Name": "巴巴多斯岛"
        }, 
        {
            "_Code": "BRN", 
            "_Name": "文莱"
        }, 
        {
            "_Code": "BTN", 
            "_Name": "不丹"
        }, 
        {
            "_Code": "BVT", 
            "_Name": "布韦岛"
        }, 
        {
            "_Code": "BWA", 
            "_Name": "博茨瓦纳"
        }, 
        {
            "_Code": "CAF", 
            "_Name": "中非共和国", 
            "State": {
                "City": [
                    {
                        "_Code": "AC", 
                        "_Name": "瓦姆"
                    }, 
                    {
                        "_Code": "BB", 
                        "_Name": "巴明吉-班戈兰"
                    }, 
                    {
                        "_Code": "BGF", 
                        "_Name": "班吉直辖市"
                    }, 
                    {
                        "_Code": "BI", 
                        "_Name": "宾博"
                    }, 
                    {
                        "_Code": "BK", 
                        "_Name": "下科托"
                    }, 
                    {
                        "_Code": "HK", 
                        "_Name": "上科托"
                    }, 
                    {
                        "_Code": "HM", 
                        "_Name": "上姆博穆"
                    }, 
                    {
                        "_Code": "HS", 
                        "_Name": "曼贝雷-卡代"
                    }, 
                    {
                        "_Code": "KB", 
                        "_Name": "纳纳-格里比齐"
                    }, 
                    {
                        "_Code": "KG", 
                        "_Name": "凯莫"
                    }, 
                    {
                        "_Code": "LB", 
                        "_Name": "洛巴伊"
                    }, 
                    {
                        "_Code": "MB", 
                        "_Name": "姆博穆"
                    }, 
                    {
                        "_Code": "MP", 
                        "_Name": "翁贝拉-姆波科"
                    }, 
                    {
                        "_Code": "NM", 
                        "_Name": "纳纳-曼贝雷"
                    }, 
                    {
                        "_Code": "OP", 
                        "_Name": "瓦姆-彭代"
                    }, 
                    {
                        "_Code": "SE", 
                        "_Name": "桑加-姆巴埃雷"
                    }, 
                    {
                        "_Code": "UK", 
                        "_Name": "瓦卡"
                    }, 
                    {
                        "_Code": "VK", 
                        "_Name": "瓦卡加"
                    }
                ]
            }
        }, 
        {
            "_Code": "CAN", 
            "_Name": "加拿大", 
            "State": {
                "City": [
                    {
                        "_Code": "ABB", 
                        "_Name": "阿伯茨福"
                    }, 
                    {
                        "_Code": "BAR", 
                        "_Name": "巴里"
                    }, 
                    {
                        "_Code": "BRP", 
                        "_Name": "基奇纳"
                    }, 
                    {
                        "_Code": "CAL", 
                        "_Name": "卡里加里"
                    }, 
                    {
                        "_Code": "CBR", 
                        "_Name": "布列塔尼角"
                    }, 
                    {
                        "_Code": "CHA", 
                        "_Name": "夏洛特敦"
                    }, 
                    {
                        "_Code": "EDM", 
                        "_Name": "埃德蒙顿"
                    }, 
                    {
                        "_Code": "FRE", 
                        "_Name": "弗雷德里顿"
                    }, 
                    {
                        "_Code": "GLP", 
                        "_Name": "圭尔夫"
                    }, 
                    {
                        "_Code": "HAL", 
                        "_Name": "哈利法克斯"
                    }, 
                    {
                        "_Code": "HAM", 
                        "_Name": "哈密尔顿"
                    }, 
                    {
                        "_Code": "IQL", 
                        "_Name": "伊魁特"
                    }, 
                    {
                        "_Code": "KGN", 
                        "_Name": "金斯敦"
                    }, 
                    {
                        "_Code": "KWL", 
                        "_Name": "基劳纳"
                    }, 
                    {
                        "_Code": "QUE", 
                        "_Name": "魁北克"
                    }, 
                    {
                        "_Code": "LOD", 
                        "_Name": "伦敦"
                    }, 
                    {
                        "_Code": "MTR", 
                        "_Name": "蒙特利尔"
                    }, 
                    {
                        "_Code": "OSH", 
                        "_Name": "奥沙瓦"
                    }, 
                    {
                        "_Code": "OTT", 
                        "_Name": "渥太华"
                    }, 
                    {
                        "_Code": "REG", 
                        "_Name": "里贾纳"
                    }, 
                    {
                        "_Code": "SAK", 
                        "_Name": "萨斯卡通"
                    }, 
                    {
                        "_Code": "SBE", 
                        "_Name": "舍布鲁克"
                    }, 
                    {
                        "_Code": "SCA", 
                        "_Name": "圣卡塔琳娜"
                    }, 
                    {
                        "_Code": "SJB", 
                        "_Name": "圣约翰斯"
                    }, 
                    {
                        "_Code": "SUD", 
                        "_Name": "萨德伯里"
                    }, 
                    {
                        "_Code": "THU", 
                        "_Name": "桑德贝"
                    }, 
                    {
                        "_Code": "TOR", 
                        "_Name": "多伦多"
                    }, 
                    {
                        "_Code": "TRR", 
                        "_Name": "三河城"
                    }, 
                    {
                        "_Code": "VAN", 
                        "_Name": "温哥华"
                    }, 
                    {
                        "_Code": "VIC", 
                        "_Name": "维多利亚"
                    }, 
                    {
                        "_Code": "WDR", 
                        "_Name": "温莎"
                    }, 
                    {
                        "_Code": "WNP", 
                        "_Name": "温尼伯"
                    }, 
                    {
                        "_Code": "YXY", 
                        "_Name": "怀特霍斯"
                    }, 
                    {
                        "_Code": "YZF", 
                        "_Name": "耶洛奈夫"
                    }
                ]
            }
        }, 
        {
            "_Code": "CCK", 
            "_Name": "科科斯群岛"
        }, 
        {
            "_Code": "CHE", 
            "_Name": "瑞士", 
            "State": {
                "City": [
                    {
                        "_Code": "AG", 
                        "_Name": "阿尔高"
                    }, 
                    {
                        "_Code": "AI", 
                        "_Name": "内阿彭策尔"
                    }, 
                    {
                        "_Code": "AR", 
                        "_Name": "外阿彭策尔"
                    }, 
                    {
                        "_Code": "BE", 
                        "_Name": "伯尔尼"
                    }, 
                    {
                        "_Code": "BL", 
                        "_Name": "巴塞尔乡村"
                    }, 
                    {
                        "_Code": "BS", 
                        "_Name": "巴塞尔城市"
                    }, 
                    {
                        "_Code": "FR", 
                        "_Name": "弗里堡"
                    }, 
                    {
                        "_Code": "GE", 
                        "_Name": "日内瓦"
                    }, 
                    {
                        "_Code": "GL", 
                        "_Name": "格拉鲁斯"
                    }, 
                    {
                        "_Code": "GR", 
                        "_Name": "格劳宾登"
                    }, 
                    {
                        "_Code": "JU", 
                        "_Name": "汝拉"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "洛桑"
                    }, 
                    {
                        "_Code": "LU", 
                        "_Name": "卢塞恩"
                    }, 
                    {
                        "_Code": "NE", 
                        "_Name": "纳沙泰尔"
                    }, 
                    {
                        "_Code": "NW", 
                        "_Name": "下瓦尔登"
                    }, 
                    {
                        "_Code": "OW", 
                        "_Name": "上瓦尔登"
                    }, 
                    {
                        "_Code": "SG", 
                        "_Name": "圣加仑"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "沙夫豪森"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "索洛图恩"
                    }, 
                    {
                        "_Code": "SZ", 
                        "_Name": "施维茨"
                    }, 
                    {
                        "_Code": "TG", 
                        "_Name": "图尔高"
                    }, 
                    {
                        "_Code": "TI", 
                        "_Name": "提契诺"
                    }, 
                    {
                        "_Code": "UR", 
                        "_Name": "乌里"
                    }, 
                    {
                        "_Code": "VD", 
                        "_Name": "沃"
                    }, 
                    {
                        "_Code": "VS", 
                        "_Name": "瓦莱"
                    }, 
                    {
                        "_Code": "ZG", 
                        "_Name": "楚格"
                    }, 
                    {
                        "_Code": "ZH", 
                        "_Name": "苏黎世"
                    }
                ]
            }
        }, 
        {
            "_Code": "CHL", 
            "_Name": "智利", 
            "State": {
                "City": [
                    {
                        "_Code": "AI", 
                        "_Name": "伊瓦涅斯将军的艾森大区"
                    }, 
                    {
                        "_Code": "AN", 
                        "_Name": "安托法加斯塔大区"
                    }, 
                    {
                        "_Code": "AR", 
                        "_Name": "阿劳卡尼亚大区"
                    }, 
                    {
                        "_Code": "AT", 
                        "_Name": "阿塔卡马大区"
                    }, 
                    {
                        "_Code": "BI", 
                        "_Name": "比奥比奥大区"
                    }, 
                    {
                        "_Code": "CO", 
                        "_Name": "科金博大区"
                    }, 
                    {
                        "_Code": "LI", 
                        "_Name": "复活节岛"
                    }, 
                    {
                        "_Code": "LL", 
                        "_Name": "湖大区"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "麦哲伦-智利南极大区"
                    }, 
                    {
                        "_Code": "ML", 
                        "_Name": "马乌莱大区"
                    }, 
                    {
                        "_Code": "RM", 
                        "_Name": "圣地亚哥"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔拉帕卡大区"
                    }, 
                    {
                        "_Code": "VS", 
                        "_Name": "瓦尔帕莱索大区"
                    }
                ]
            }
        }, 
        {
            "_Code": "CXR", 
            "_Name": "圣诞岛"
        }, 
        {
            "_Code": "CIV", 
            "_Name": "科特迪瓦", 
            "State": {
                "City": [
                    {
                        "_Code": "AG", 
                        "_Name": "阿涅比"
                    }, 
                    {
                        "_Code": "BF", 
                        "_Name": "巴芬"
                    }, 
                    {
                        "_Code": "BS", 
                        "_Name": "下萨桑德拉"
                    }, 
                    {
                        "_Code": "DE", 
                        "_Name": "登盖莱"
                    }, 
                    {
                        "_Code": "DH", 
                        "_Name": "山地"
                    }, 
                    {
                        "_Code": "FR", 
                        "_Name": "弗罗马格尔"
                    }, 
                    {
                        "_Code": "HT", 
                        "_Name": "萨桑德拉"
                    }, 
                    {
                        "_Code": "LC", 
                        "_Name": "湖泊"
                    }, 
                    {
                        "_Code": "LG", 
                        "_Name": "泻湖"
                    }, 
                    {
                        "_Code": "MC", 
                        "_Name": "中科莫埃"
                    }, 
                    {
                        "_Code": "MR", 
                        "_Name": "马拉韦"
                    }, 
                    {
                        "_Code": "MV", 
                        "_Name": "中卡瓦利"
                    }, 
                    {
                        "_Code": "NC", 
                        "_Name": "恩济－科莫埃"
                    }, 
                    {
                        "_Code": "SB", 
                        "_Name": "南邦达马"
                    }, 
                    {
                        "_Code": "SC", 
                        "_Name": "南科莫埃"
                    }, 
                    {
                        "_Code": "SV", 
                        "_Name": "萨瓦纳"
                    }, 
                    {
                        "_Code": "VB", 
                        "_Name": "邦达马河谷"
                    }, 
                    {
                        "_Code": "WR", 
                        "_Name": "沃罗杜古"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "赞赞"
                    }
                ]
            }
        }, 
        {
            "_Code": "CMR", 
            "_Name": "喀麦隆", 
            "State": {
                "City": [
                    {
                        "_Code": "ADA", 
                        "_Name": "阿达马瓦"
                    }, 
                    {
                        "_Code": "CEN", 
                        "_Name": "中央"
                    }, 
                    {
                        "_Code": "EXN", 
                        "_Name": "北端"
                    }, 
                    {
                        "_Code": "EST", 
                        "_Name": "东部"
                    }, 
                    {
                        "_Code": "LIT", 
                        "_Name": "滨海"
                    }, 
                    {
                        "_Code": "NOR", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "NOT", 
                        "_Name": "西北"
                    }, 
                    {
                        "_Code": "OUE", 
                        "_Name": "西部"
                    }, 
                    {
                        "_Code": "SOU", 
                        "_Name": "西南"
                    }, 
                    {
                        "_Code": "SUD", 
                        "_Name": "南部"
                    }
                ]
            }
        }, 
        {
            "_Code": "COD", 
            "_Name": "刚果民主共和国"
        }, 
        {
            "_Code": "COG", 
            "_Name": "刚果"
        }, 
        {
            "_Code": "COK", 
            "_Name": "库克群岛"
        }, 
        {
            "_Code": "COL", 
            "_Name": "哥伦比亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AMZ", 
                        "_Name": "亚马孙"
                    }, 
                    {
                        "_Code": "ANT", 
                        "_Name": "安提奥基亚"
                    }, 
                    {
                        "_Code": "ARA", 
                        "_Name": "阿劳卡"
                    }, 
                    {
                        "_Code": "ATL", 
                        "_Name": "大西洋"
                    }, 
                    {
                        "_Code": "BDC", 
                        "_Name": "波哥大首都区"
                    }, 
                    {
                        "_Code": "BOL", 
                        "_Name": "博利瓦尔"
                    }, 
                    {
                        "_Code": "BOY", 
                        "_Name": "博亚卡"
                    }, 
                    {
                        "_Code": "CAQ", 
                        "_Name": "卡克塔"
                    }, 
                    {
                        "_Code": "CAL", 
                        "_Name": "卡尔达斯"
                    }, 
                    {
                        "_Code": "CAM", 
                        "_Name": "昆迪纳马卡"
                    }, 
                    {
                        "_Code": "CAS", 
                        "_Name": "卡萨纳雷"
                    }, 
                    {
                        "_Code": "CAU", 
                        "_Name": "考卡"
                    }, 
                    {
                        "_Code": "CES", 
                        "_Name": "塞萨尔"
                    }, 
                    {
                        "_Code": "CHO", 
                        "_Name": "乔科"
                    }, 
                    {
                        "_Code": "COR", 
                        "_Name": "科尔多巴"
                    }, 
                    {
                        "_Code": "GJR", 
                        "_Name": "瓜希拉"
                    }, 
                    {
                        "_Code": "GNA", 
                        "_Name": "瓜伊尼亚"
                    }, 
                    {
                        "_Code": "GVR", 
                        "_Name": "瓜维亚雷"
                    }, 
                    {
                        "_Code": "HUI", 
                        "_Name": "乌伊拉"
                    }, 
                    {
                        "_Code": "QUI", 
                        "_Name": "金迪奥"
                    }, 
                    {
                        "_Code": "MAG", 
                        "_Name": "马格达雷那"
                    }, 
                    {
                        "_Code": "MET", 
                        "_Name": "梅塔"
                    }, 
                    {
                        "_Code": "NAR", 
                        "_Name": "纳里尼奥"
                    }, 
                    {
                        "_Code": "NDS", 
                        "_Name": "北桑坦德"
                    }, 
                    {
                        "_Code": "PUT", 
                        "_Name": "普图马约"
                    }, 
                    {
                        "_Code": "RIS", 
                        "_Name": "利萨拉尔达"
                    }, 
                    {
                        "_Code": "SAN", 
                        "_Name": "桑坦德"
                    }, 
                    {
                        "_Code": "SAP", 
                        "_Name": "圣安德烈斯-普罗维登西亚"
                    }, 
                    {
                        "_Code": "SUC", 
                        "_Name": "苏克雷"
                    }, 
                    {
                        "_Code": "TOL", 
                        "_Name": "托利马"
                    }, 
                    {
                        "_Code": "VAU", 
                        "_Name": "沃佩斯"
                    }, 
                    {
                        "_Code": "VDC", 
                        "_Name": "考卡山谷"
                    }, 
                    {
                        "_Code": "VIC", 
                        "_Name": "维查达"
                    }
                ]
            }
        }, 
        {
            "_Code": "COM", 
            "_Name": "科摩罗"
        }, 
        {
            "_Code": "CPV", 
            "_Name": "佛得角", 
            "State": {
                "City": [
                    {
                        "_Code": "BR", 
                        "_Name": "布拉瓦岛"
                    }, 
                    {
                        "_Code": "BV", 
                        "_Name": "博阿维斯塔岛"
                    }, 
                    {
                        "_Code": "CA", 
                        "_Name": "圣卡塔琳娜"
                    }, 
                    {
                        "_Code": "CR", 
                        "_Name": "圣克鲁斯"
                    }, 
                    {
                        "_Code": "FO", 
                        "_Name": "福古岛"
                    }, 
                    {
                        "_Code": "IA", 
                        "_Name": "圣地亚哥岛"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马尤岛"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "莫斯特罗"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "保尔"
                    }, 
                    {
                        "_Code": "PN", 
                        "_Name": "波多诺伏"
                    }, 
                    {
                        "_Code": "PR", 
                        "_Name": "普拉亚"
                    }, 
                    {
                        "_Code": "RG", 
                        "_Name": "大里贝拉"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "圣安唐岛"
                    }, 
                    {
                        "_Code": "SD", 
                        "_Name": "圣多明戈"
                    }, 
                    {
                        "_Code": "SF", 
                        "_Name": "圣菲利普"
                    }, 
                    {
                        "_Code": "SL", 
                        "_Name": "萨尔岛"
                    }, 
                    {
                        "_Code": "SM", 
                        "_Name": "圣米戈尔"
                    }, 
                    {
                        "_Code": "SN", 
                        "_Name": "圣尼古拉岛"
                    }, 
                    {
                        "_Code": "SV", 
                        "_Name": "圣维森特岛"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔拉法尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "CRI", 
            "_Name": "哥斯达黎加", 
            "State": {
                "City": [
                    {
                        "_Code": "A", 
                        "_Name": "阿拉胡埃拉"
                    }, 
                    {
                        "_Code": "C", 
                        "_Name": "卡塔戈"
                    }, 
                    {
                        "_Code": "G", 
                        "_Name": "瓜纳卡斯特"
                    }, 
                    {
                        "_Code": "H", 
                        "_Name": "埃雷迪亚"
                    }, 
                    {
                        "_Code": "L", 
                        "_Name": "利蒙"
                    }, 
                    {
                        "_Code": "P", 
                        "_Name": "蓬塔雷纳斯"
                    }, 
                    {
                        "_Code": "SJ", 
                        "_Name": "圣何塞"
                    }
                ]
            }
        }, 
        {
            "_Code": "CUB", 
            "_Name": "古巴", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "比那尔德里奥"
                    }, 
                    {
                        "_Code": "10", 
                        "_Name": "拉斯图纳斯"
                    }, 
                    {
                        "_Code": "11", 
                        "_Name": "奥尔金"
                    }, 
                    {
                        "_Code": "12", 
                        "_Name": "格拉玛"
                    }, 
                    {
                        "_Code": "13", 
                        "_Name": "圣地亚哥"
                    }, 
                    {
                        "_Code": "14", 
                        "_Name": "关塔那摩"
                    }, 
                    {
                        "_Code": "2", 
                        "_Name": "哈瓦那"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "哈瓦那城"
                    }, 
                    {
                        "_Code": "4", 
                        "_Name": "马坦萨斯"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "比亚克拉拉"
                    }, 
                    {
                        "_Code": "6", 
                        "_Name": "西恩富戈斯"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "圣斯皮里图斯"
                    }, 
                    {
                        "_Code": "8", 
                        "_Name": "谢戈德阿维拉"
                    }, 
                    {
                        "_Code": "9", 
                        "_Name": "卡马圭"
                    }, 
                    {
                        "_Code": "99", 
                        "_Name": "青年岛特区"
                    }, 
                    {
                        "_Code": "MAY", 
                        "_Name": "马亚里"
                    }, 
                    {
                        "_Code": "MZO", 
                        "_Name": "曼萨尼罗"
                    }
                ]
            }
        }, 
        {
            "_Code": "CYM", 
            "_Name": "开曼群岛"
        }, 
        {
            "_Code": "CYP", 
            "_Name": "塞浦路斯", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "尼科西亚"
                    }, 
                    {
                        "_Code": "2", 
                        "_Name": "利马索尔"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "拉纳卡"
                    }, 
                    {
                        "_Code": "4", 
                        "_Name": "法马古斯塔"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "帕福斯"
                    }, 
                    {
                        "_Code": "6", 
                        "_Name": "凯里尼亚"
                    }
                ]
            }
        }, 
        {
            "_Code": "CZE", 
            "_Name": "捷克共和国", 
            "State": {
                "City": [
                    {
                        "_Code": "JC", 
                        "_Name": "南摩拉维亚"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "卡罗维发利"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "赫拉德茨-克拉洛韦"
                    }, 
                    {
                        "_Code": "LI", 
                        "_Name": "利贝雷克"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "摩拉维亚-西里西亚"
                    }, 
                    {
                        "_Code": "OL", 
                        "_Name": "奥洛穆茨"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "帕尔杜比采"
                    }, 
                    {
                        "_Code": "PL", 
                        "_Name": "比尔森"
                    }, 
                    {
                        "_Code": "PR", 
                        "_Name": "布拉格直辖市"
                    }, 
                    {
                        "_Code": "ST", 
                        "_Name": "中捷克"
                    }, 
                    {
                        "_Code": "US", 
                        "_Name": "乌斯季"
                    }, 
                    {
                        "_Code": "VY", 
                        "_Name": "维索基纳"
                    }, 
                    {
                        "_Code": "ZL", 
                        "_Name": "兹林"
                    }
                ]
            }
        }, 
        {
            "_Code": "DEU", 
            "_Name": "德国", 
            "State": {
                "City": [
                    {
                        "_Code": "AGB", 
                        "_Name": "奥格斯堡"
                    }, 
                    {
                        "_Code": "ANS", 
                        "_Name": "安斯巴格"
                    }, 
                    {
                        "_Code": "ARN", 
                        "_Name": "阿恩斯贝格"
                    }, 
                    {
                        "_Code": "BE", 
                        "_Name": "柏林"
                    }, 
                    {
                        "_Code": "BFE", 
                        "_Name": "比勒费尔德"
                    }, 
                    {
                        "_Code": "BOM", 
                        "_Name": "波鸿"
                    }, 
                    {
                        "_Code": "BRW", 
                        "_Name": "不伦瑞克"
                    }, 
                    {
                        "_Code": "BYU", 
                        "_Name": "拜伊罗特"
                    }, 
                    {
                        "_Code": "CGN", 
                        "_Name": "科隆"
                    }, 
                    {
                        "_Code": "CHE", 
                        "_Name": "开姆尼斯"
                    }, 
                    {
                        "_Code": "DAR", 
                        "_Name": "达姆施塔特"
                    }, 
                    {
                        "_Code": "DES", 
                        "_Name": "德绍"
                    }, 
                    {
                        "_Code": "DET", 
                        "_Name": "代特莫尔特"
                    }, 
                    {
                        "_Code": "DRS", 
                        "_Name": "德累斯顿"
                    }, 
                    {
                        "_Code": "DUS", 
                        "_Name": "杜塞尔多夫"
                    }, 
                    {
                        "_Code": "ERF", 
                        "_Name": "爱尔福特"
                    }, 
                    {
                        "_Code": "FBG", 
                        "_Name": "弗赖堡"
                    }, 
                    {
                        "_Code": "FFO", 
                        "_Name": "法兰克福"
                    }, 
                    {
                        "_Code": "GBN", 
                        "_Name": "吉森"
                    }, 
                    {
                        "_Code": "HAE", 
                        "_Name": "哈雷"
                    }, 
                    {
                        "_Code": "HAJ", 
                        "_Name": "汉诺威"
                    }, 
                    {
                        "_Code": "HB", 
                        "_Name": "不来梅"
                    }, 
                    {
                        "_Code": "HH", 
                        "_Name": "汉堡"
                    }, 
                    {
                        "_Code": "KAE", 
                        "_Name": "卡尔斯鲁厄"
                    }, 
                    {
                        "_Code": "KAS", 
                        "_Name": "卡塞尔"
                    }, 
                    {
                        "_Code": "KEL", 
                        "_Name": "基尔"
                    }, 
                    {
                        "_Code": "KOB", 
                        "_Name": "科布伦次"
                    }, 
                    {
                        "_Code": "LBG", 
                        "_Name": "吕讷堡"
                    }, 
                    {
                        "_Code": "LDH", 
                        "_Name": "兰茨胡特"
                    }, 
                    {
                        "_Code": "LEJ", 
                        "_Name": "莱比锡"
                    }, 
                    {
                        "_Code": "MAG", 
                        "_Name": "马格德堡"
                    }, 
                    {
                        "_Code": "MAI", 
                        "_Name": "美因兹"
                    }, 
                    {
                        "_Code": "MHG", 
                        "_Name": "曼海姆"
                    }, 
                    {
                        "_Code": "MUC", 
                        "_Name": "慕尼黑"
                    }, 
                    {
                        "_Code": "MUN", 
                        "_Name": "明斯特"
                    }, 
                    {
                        "_Code": "NUE", 
                        "_Name": "纽伦堡"
                    }, 
                    {
                        "_Code": "POT", 
                        "_Name": "波茨坦"
                    }, 
                    {
                        "_Code": "STR", 
                        "_Name": "斯图加特"
                    }, 
                    {
                        "_Code": "SWH", 
                        "_Name": "什未林"
                    }, 
                    {
                        "_Code": "TRI", 
                        "_Name": "特里尔"
                    }, 
                    {
                        "_Code": "WIB", 
                        "_Name": "威斯巴登"
                    }, 
                    {
                        "_Code": "WUG", 
                        "_Name": "维尔茨堡"
                    }
                ]
            }
        }, 
        {
            "_Code": "DJI", 
            "_Name": "吉布提", 
            "State": {
                "City": [
                    {
                        "_Code": "K", 
                        "_Name": "迪基勒区"
                    }, 
                    {
                        "_Code": "O", 
                        "_Name": "奥博克区"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "阿里萨比赫区"
                    }, 
                    {
                        "_Code": "T", 
                        "_Name": "塔朱拉区"
                    }
                ]
            }
        }, 
        {
            "_Code": "DMA", 
            "_Name": "多米尼加"
        }, 
        {
            "_Code": "DNK", 
            "_Name": "丹麦", 
            "State": {
                "City": [
                    {
                        "_Code": "AR", 
                        "_Name": "奥胡斯"
                    }, 
                    {
                        "_Code": "BO", 
                        "_Name": "博恩霍尔姆"
                    }, 
                    {
                        "_Code": "CPH", 
                        "_Name": "哥本哈根"
                    }, 
                    {
                        "_Code": "FRE", 
                        "_Name": "菲特烈堡"
                    }, 
                    {
                        "_Code": "FY", 
                        "_Name": "菲茵"
                    }, 
                    {
                        "_Code": "RIB", 
                        "_Name": "里伯"
                    }, 
                    {
                        "_Code": "RKE", 
                        "_Name": "罗斯基勒"
                    }, 
                    {
                        "_Code": "RKG", 
                        "_Name": "灵克宾"
                    }, 
                    {
                        "_Code": "ST", 
                        "_Name": "斯多斯特姆"
                    }, 
                    {
                        "_Code": "VBI", 
                        "_Name": "南日德兰"
                    }, 
                    {
                        "_Code": "VEJ", 
                        "_Name": "维厄勒"
                    }, 
                    {
                        "_Code": "VIB", 
                        "_Name": "维堡"
                    }, 
                    {
                        "_Code": "VS", 
                        "_Name": "西希兰"
                    }, 
                    {
                        "_Code": "VSV", 
                        "_Name": "北日德兰"
                    }
                ]
            }
        }, 
        {
            "_Code": "DOM", 
            "_Name": "多米尼加共和国"
        }, 
        {
            "_Code": "DZA", 
            "_Name": "阿尔及利亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AAE", 
                        "_Name": "安纳巴"
                    }, 
                    {
                        "_Code": "ADE", 
                        "_Name": "艾因·德夫拉"
                    }, 
                    {
                        "_Code": "ADR", 
                        "_Name": "阿德拉尔"
                    }, 
                    {
                        "_Code": "ALG", 
                        "_Name": "阿尔及尔"
                    }, 
                    {
                        "_Code": "ATE", 
                        "_Name": "艾因·蒂姆尚特"
                    }, 
                    {
                        "_Code": "BAT", 
                        "_Name": "巴特纳"
                    }, 
                    {
                        "_Code": "BEC", 
                        "_Name": "贝沙尔"
                    }, 
                    {
                        "_Code": "BIS", 
                        "_Name": "比斯克拉"
                    }, 
                    {
                        "_Code": "BJA", 
                        "_Name": "贝贾亚"
                    }, 
                    {
                        "_Code": "BLI", 
                        "_Name": "布利达"
                    }, 
                    {
                        "_Code": "BOA", 
                        "_Name": "布依拉"
                    }, 
                    {
                        "_Code": "BOR", 
                        "_Name": "布尔吉·布阿雷里吉"
                    }, 
                    {
                        "_Code": "BOU", 
                        "_Name": "布迈德斯"
                    }, 
                    {
                        "_Code": "CHL", 
                        "_Name": "谢里夫"
                    }, 
                    {
                        "_Code": "CZL", 
                        "_Name": "君士坦丁"
                    }, 
                    {
                        "_Code": "DJE", 
                        "_Name": "杰勒法"
                    }, 
                    {
                        "_Code": "EBA", 
                        "_Name": "贝伊德"
                    }, 
                    {
                        "_Code": "EOU", 
                        "_Name": "瓦德"
                    }, 
                    {
                        "_Code": "ETA", 
                        "_Name": "塔里夫"
                    }, 
                    {
                        "_Code": "GHA", 
                        "_Name": "盖尔达耶"
                    }, 
                    {
                        "_Code": "GUE", 
                        "_Name": "盖尔马"
                    }, 
                    {
                        "_Code": "ILL", 
                        "_Name": "伊利齐"
                    }, 
                    {
                        "_Code": "IOU", 
                        "_Name": "提济乌祖"
                    }, 
                    {
                        "_Code": "JIJ", 
                        "_Name": "吉杰尔"
                    }, 
                    {
                        "_Code": "KHE", 
                        "_Name": "罕西拉"
                    }, 
                    {
                        "_Code": "LAG", 
                        "_Name": "拉格瓦特"
                    }, 
                    {
                        "_Code": "MED", 
                        "_Name": "麦迪亚"
                    }, 
                    {
                        "_Code": "MIL", 
                        "_Name": "密拉"
                    }, 
                    {
                        "_Code": "MOS", 
                        "_Name": "莫斯塔加纳姆"
                    }, 
                    {
                        "_Code": "MSI", 
                        "_Name": "姆西拉"
                    }, 
                    {
                        "_Code": "MUA", 
                        "_Name": "马斯卡拉"
                    }, 
                    {
                        "_Code": "NAA", 
                        "_Name": "纳阿马"
                    }, 
                    {
                        "_Code": "OEB", 
                        "_Name": "乌姆布阿基"
                    }, 
                    {
                        "_Code": "ORA", 
                        "_Name": "奥兰"
                    }, 
                    {
                        "_Code": "OUA", 
                        "_Name": "瓦尔格拉"
                    }, 
                    {
                        "_Code": "REL", 
                        "_Name": "赫利赞"
                    }, 
                    {
                        "_Code": "SAH", 
                        "_Name": "苏克·阿赫拉斯"
                    }, 
                    {
                        "_Code": "SAI", 
                        "_Name": "赛伊达"
                    }, 
                    {
                        "_Code": "SBA", 
                        "_Name": "西迪贝勒阿贝斯"
                    }, 
                    {
                        "_Code": "SET", 
                        "_Name": "塞蒂夫"
                    }, 
                    {
                        "_Code": "SKI", 
                        "_Name": "斯基克达"
                    }, 
                    {
                        "_Code": "TAM", 
                        "_Name": "塔曼拉塞特"
                    }, 
                    {
                        "_Code": "TEB", 
                        "_Name": "特贝萨"
                    }, 
                    {
                        "_Code": "TIA", 
                        "_Name": "提亚雷特"
                    }, 
                    {
                        "_Code": "TIN", 
                        "_Name": "廷杜夫"
                    }, 
                    {
                        "_Code": "TIP", 
                        "_Name": "蒂巴扎"
                    }, 
                    {
                        "_Code": "TIS", 
                        "_Name": "蒂斯姆西勒特"
                    }, 
                    {
                        "_Code": "TLE", 
                        "_Name": "特莱姆森"
                    }
                ]
            }
        }, 
        {
            "_Code": "ECU", 
            "_Name": "厄瓜多尔", 
            "State": {
                "City": [
                    {
                        "_Code": "A", 
                        "_Name": "阿苏艾"
                    }, 
                    {
                        "_Code": "B", 
                        "_Name": "玻利瓦尔"
                    }, 
                    {
                        "_Code": "C", 
                        "_Name": "卡尔奇"
                    }, 
                    {
                        "_Code": "D", 
                        "_Name": "纳波，奥雷利亚纳"
                    }, 
                    {
                        "_Code": "E", 
                        "_Name": "埃斯梅拉尔达斯"
                    }, 
                    {
                        "_Code": "F", 
                        "_Name": "卡尼亚尔"
                    }, 
                    {
                        "_Code": "G", 
                        "_Name": "瓜亚斯"
                    }, 
                    {
                        "_Code": "H", 
                        "_Name": "钦博拉索"
                    }, 
                    {
                        "_Code": "X", 
                        "_Name": "科托帕希"
                    }, 
                    {
                        "_Code": "I", 
                        "_Name": "因巴布拉"
                    }, 
                    {
                        "_Code": "L", 
                        "_Name": "洛哈"
                    }, 
                    {
                        "_Code": "M", 
                        "_Name": "马纳比"
                    }, 
                    {
                        "_Code": "O", 
                        "_Name": "埃尔奥罗"
                    }, 
                    {
                        "_Code": "P", 
                        "_Name": "皮钦查"
                    }, 
                    {
                        "_Code": "R", 
                        "_Name": "洛斯里奥斯"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "莫罗纳－圣地亚哥"
                    }, 
                    {
                        "_Code": "T", 
                        "_Name": "通古拉瓦"
                    }, 
                    {
                        "_Code": "U", 
                        "_Name": "苏昆毕奥斯"
                    }, 
                    {
                        "_Code": "W", 
                        "_Name": "加拉帕戈斯"
                    }, 
                    {
                        "_Code": "Y", 
                        "_Name": "帕斯塔萨"
                    }, 
                    {
                        "_Code": "Z", 
                        "_Name": "萨莫拉－钦奇佩"
                    }
                ]
            }
        }, 
        {
            "_Code": "EGY", 
            "_Name": "埃及", 
            "State": {
                "City": [
                    {
                        "_Code": "ALY", 
                        "_Name": "亚历山大"
                    }, 
                    {
                        "_Code": "ASW", 
                        "_Name": "阿斯旺"
                    }, 
                    {
                        "_Code": "CAI", 
                        "_Name": "开罗"
                    }, 
                    {
                        "_Code": "GBY", 
                        "_Name": "古尔代盖"
                    }, 
                    {
                        "_Code": "SKH", 
                        "_Name": "苏布拉开马"
                    }
                ]
            }
        }, 
        {
            "_Code": "ERI", 
            "_Name": "厄立特里亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BR", 
                        "_Name": "加什·巴尔卡"
                    }, 
                    {
                        "_Code": "DE", 
                        "_Name": "南部"
                    }, 
                    {
                        "_Code": "DK", 
                        "_Name": "南红海"
                    }, 
                    {
                        "_Code": "KE", 
                        "_Name": "安塞巴"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "中部"
                    }, 
                    {
                        "_Code": "SK", 
                        "_Name": "北红海"
                    }
                ]
            }
        }, 
        {
            "_Code": "ESP", 
            "_Name": "西班牙", 
            "State": {
                "City": [
                    {
                        "_Code": "AGP", 
                        "_Name": "马拉加"
                    }, 
                    {
                        "_Code": "ALA", 
                        "_Name": "阿拉瓦"
                    }, 
                    {
                        "_Code": "ALB", 
                        "_Name": "阿尔瓦塞特"
                    }, 
                    {
                        "_Code": "ALC", 
                        "_Name": "阿利坎特"
                    }, 
                    {
                        "_Code": "ARL", 
                        "_Name": "拉里奥哈"
                    }, 
                    {
                        "_Code": "AST", 
                        "_Name": "阿斯图利亚斯"
                    }, 
                    {
                        "_Code": "AVI", 
                        "_Name": "阿维拉"
                    }, 
                    {
                        "_Code": "BCN", 
                        "_Name": "巴塞罗那"
                    }, 
                    {
                        "_Code": "BJZ", 
                        "_Name": "巴达霍斯"
                    }, 
                    {
                        "_Code": "BLR", 
                        "_Name": "巴利阿里"
                    }, 
                    {
                        "_Code": "BUR", 
                        "_Name": "布尔戈斯"
                    }, 
                    {
                        "_Code": "CAD", 
                        "_Name": "加的斯"
                    }, 
                    {
                        "_Code": "CAS", 
                        "_Name": "卡斯特利翁"
                    }, 
                    {
                        "_Code": "CCS", 
                        "_Name": "卡塞雷斯"
                    }, 
                    {
                        "_Code": "CIR", 
                        "_Name": "卡斯蒂利亚"
                    }, 
                    {
                        "_Code": "CUE", 
                        "_Name": "昆卡"
                    }, 
                    {
                        "_Code": "GRX", 
                        "_Name": "格拉纳达"
                    }, 
                    {
                        "_Code": "GRO", 
                        "_Name": "赫罗纳"
                    }, 
                    {
                        "_Code": "GUA", 
                        "_Name": "瓜达拉哈拉"
                    }, 
                    {
                        "_Code": "GUI", 
                        "_Name": "吉普斯夸"
                    }, 
                    {
                        "_Code": "HUC", 
                        "_Name": "韦斯卡"
                    }, 
                    {
                        "_Code": "HUV", 
                        "_Name": "韦尔瓦"
                    }, 
                    {
                        "_Code": "JAE", 
                        "_Name": "哈恩"
                    }, 
                    {
                        "_Code": "LCG", 
                        "_Name": "拉科鲁尼亚"
                    }, 
                    {
                        "_Code": "LEI", 
                        "_Name": "阿尔梅里亚"
                    }, 
                    {
                        "_Code": "LEN", 
                        "_Name": "莱昂"
                    }, 
                    {
                        "_Code": "LGO", 
                        "_Name": "卢戈"
                    }, 
                    {
                        "_Code": "LLE", 
                        "_Name": "莱里达"
                    }, 
                    {
                        "_Code": "LPA", 
                        "_Name": "拉斯帕尔马斯"
                    }, 
                    {
                        "_Code": "MAD", 
                        "_Name": "马德里"
                    }, 
                    {
                        "_Code": "MJV", 
                        "_Name": "穆尔西亚"
                    }, 
                    {
                        "_Code": "NVV", 
                        "_Name": "纳瓦拉"
                    }, 
                    {
                        "_Code": "ODB", 
                        "_Name": "科尔多瓦"
                    }, 
                    {
                        "_Code": "ORE", 
                        "_Name": "奥伦塞"
                    }, 
                    {
                        "_Code": "PAC", 
                        "_Name": "帕伦西亚"
                    }, 
                    {
                        "_Code": "PEV", 
                        "_Name": "蓬特韦德拉"
                    }, 
                    {
                        "_Code": "SCT", 
                        "_Name": "圣克鲁斯-德特内里费"
                    }, 
                    {
                        "_Code": "SDR", 
                        "_Name": "桑坦德"
                    }, 
                    {
                        "_Code": "SEG", 
                        "_Name": "塞哥维亚"
                    }, 
                    {
                        "_Code": "SLM", 
                        "_Name": "萨拉曼卡"
                    }, 
                    {
                        "_Code": "SOR", 
                        "_Name": "索里亚"
                    }, 
                    {
                        "_Code": "SVQ", 
                        "_Name": "塞维利亚"
                    }, 
                    {
                        "_Code": "TAR", 
                        "_Name": "塔拉戈纳"
                    }, 
                    {
                        "_Code": "TER", 
                        "_Name": "特鲁埃尔"
                    }, 
                    {
                        "_Code": "TOL", 
                        "_Name": "托莱多"
                    }, 
                    {
                        "_Code": "VLC", 
                        "_Name": "巴伦西亚"
                    }, 
                    {
                        "_Code": "VLL", 
                        "_Name": "巴利亚多利德"
                    }, 
                    {
                        "_Code": "VSE", 
                        "_Name": "比斯开"
                    }, 
                    {
                        "_Code": "ZAZ", 
                        "_Name": "萨拉戈萨"
                    }, 
                    {
                        "_Code": "ZMR", 
                        "_Name": "萨莫拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "EST", 
            "_Name": "爱沙尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "37", 
                        "_Name": "哈留"
                    }, 
                    {
                        "_Code": "39", 
                        "_Name": "希尤"
                    }, 
                    {
                        "_Code": "44", 
                        "_Name": "依达－维鲁"
                    }, 
                    {
                        "_Code": "49", 
                        "_Name": "耶盖瓦"
                    }, 
                    {
                        "_Code": "51", 
                        "_Name": "耶尔韦"
                    }, 
                    {
                        "_Code": "57", 
                        "_Name": "里亚内"
                    }, 
                    {
                        "_Code": "59", 
                        "_Name": "维鲁"
                    }, 
                    {
                        "_Code": "65", 
                        "_Name": "贝尔瓦"
                    }, 
                    {
                        "_Code": "67", 
                        "_Name": "帕尔努"
                    }, 
                    {
                        "_Code": "70", 
                        "_Name": "拉普拉"
                    }, 
                    {
                        "_Code": "74", 
                        "_Name": "萨雷"
                    }, 
                    {
                        "_Code": "78", 
                        "_Name": "塔尔图"
                    }, 
                    {
                        "_Code": "82", 
                        "_Name": "瓦尔加"
                    }, 
                    {
                        "_Code": "84", 
                        "_Name": "维良地"
                    }, 
                    {
                        "_Code": "86", 
                        "_Name": "沃鲁"
                    }
                ]
            }
        }, 
        {
            "_Code": "ETH", 
            "_Name": "埃塞俄比亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AA", 
                        "_Name": "亚的斯亚贝巴"
                    }, 
                    {
                        "_Code": "AF", 
                        "_Name": "阿法尔"
                    }, 
                    {
                        "_Code": "AH", 
                        "_Name": "阿姆哈拉"
                    }, 
                    {
                        "_Code": "BG", 
                        "_Name": "宾香古尔"
                    }, 
                    {
                        "_Code": "DD", 
                        "_Name": "德雷达瓦"
                    }, 
                    {
                        "_Code": "GB", 
                        "_Name": "甘贝拉各族"
                    }, 
                    {
                        "_Code": "HR", 
                        "_Name": "哈勒里民族"
                    }, 
                    {
                        "_Code": "OR", 
                        "_Name": "奥罗米亚"
                    }, 
                    {
                        "_Code": "SM", 
                        "_Name": "索马里"
                    }, 
                    {
                        "_Code": "SN", 
                        "_Name": "南方各族"
                    }, 
                    {
                        "_Code": "TG", 
                        "_Name": "提格雷"
                    }
                ]
            }
        }, 
        {
            "_Code": "FIN", 
            "_Name": "芬兰", 
            "State": {
                "City": [
                    {
                        "_Code": "ESP", 
                        "_Name": "埃斯波"
                    }, 
                    {
                        "_Code": "HEL", 
                        "_Name": "赫尔辛基"
                    }, 
                    {
                        "_Code": "HMY", 
                        "_Name": "海门林纳"
                    }, 
                    {
                        "_Code": "JOE", 
                        "_Name": "约恩苏"
                    }, 
                    {
                        "_Code": "KAJ", 
                        "_Name": "卡亚尼"
                    }, 
                    {
                        "_Code": "KOK", 
                        "_Name": "科科拉"
                    }, 
                    {
                        "_Code": "KTK", 
                        "_Name": "科特卡"
                    }, 
                    {
                        "_Code": "KUO", 
                        "_Name": "库奥皮奥"
                    }, 
                    {
                        "_Code": "LHI", 
                        "_Name": "拉赫蒂"
                    }, 
                    {
                        "_Code": "LPP", 
                        "_Name": "拉彭兰塔"
                    }, 
                    {
                        "_Code": "MHQ", 
                        "_Name": "玛丽港"
                    }, 
                    {
                        "_Code": "MIK", 
                        "_Name": "米凯利"
                    }, 
                    {
                        "_Code": "OLU", 
                        "_Name": "奥卢"
                    }, 
                    {
                        "_Code": "POR", 
                        "_Name": "波里"
                    }, 
                    {
                        "_Code": "PRV", 
                        "_Name": "博尔沃"
                    }, 
                    {
                        "_Code": "RVN", 
                        "_Name": "罗瓦涅米"
                    }, 
                    {
                        "_Code": "TKU", 
                        "_Name": "图尔库"
                    }, 
                    {
                        "_Code": "TMP", 
                        "_Name": "坦佩雷"
                    }, 
                    {
                        "_Code": "VAA", 
                        "_Name": "瓦萨"
                    }, 
                    {
                        "_Code": "VAT", 
                        "_Name": "万塔"
                    }
                ]
            }
        }, 
        {
            "_Code": "FJI", 
            "_Name": "斐济"
        }, 
        {
            "_Code": "FLK", 
            "_Name": "弗兰克群岛"
        }, 
        {
            "_Code": "FRA", 
            "_Name": "法国", 
            "State": {
                "City": [
                    {
                        "_Code": "AJA", 
                        "_Name": "阿雅克修"
                    }, 
                    {
                        "_Code": "AMI", 
                        "_Name": "亚眠"
                    }, 
                    {
                        "_Code": "ARL", 
                        "_Name": "阿尔勒"
                    }, 
                    {
                        "_Code": "BSN", 
                        "_Name": "贝桑松"
                    }, 
                    {
                        "_Code": "CFR", 
                        "_Name": "卡昂"
                    }, 
                    {
                        "_Code": "CSM", 
                        "_Name": "沙隆"
                    }, 
                    {
                        "_Code": "DIJ", 
                        "_Name": "第戎"
                    }, 
                    {
                        "_Code": "FRJ", 
                        "_Name": "弗雷瑞斯"
                    }, 
                    {
                        "_Code": "QXB", 
                        "_Name": "艾克斯"
                    }, 
                    {
                        "_Code": "LIG", 
                        "_Name": "利摩日"
                    }, 
                    {
                        "_Code": "LIO", 
                        "_Name": "里昂"
                    }, 
                    {
                        "_Code": "LLE", 
                        "_Name": "里尔"
                    }, 
                    {
                        "_Code": "MPL", 
                        "_Name": "蒙彼利埃"
                    }, 
                    {
                        "_Code": "MRS", 
                        "_Name": "马赛"
                    }, 
                    {
                        "_Code": "MZM", 
                        "_Name": "梅斯"
                    }, 
                    {
                        "_Code": "NCE", 
                        "_Name": "尼斯"
                    }, 
                    {
                        "_Code": "NTE", 
                        "_Name": "南特"
                    }, 
                    {
                        "_Code": "ORR", 
                        "_Name": "奥尔良"
                    }, 
                    {
                        "_Code": "PAR", 
                        "_Name": "巴黎"
                    }, 
                    {
                        "_Code": "RNS", 
                        "_Name": "雷恩"
                    }, 
                    {
                        "_Code": "TLS", 
                        "_Name": "图卢兹"
                    }, 
                    {
                        "_Code": "URO", 
                        "_Name": "鲁昂"
                    }, 
                    {
                        "_Code": "VAA", 
                        "_Name": "瓦朗斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "FRO", 
            "_Name": "法罗群岛"
        }, 
        {
            "_Code": "FSM", 
            "_Name": "密克罗尼西亚"
        }, 
        {
            "_Code": "GAB", 
            "_Name": "加蓬", 
            "State": {
                "City": [
                    {
                        "_Code": "ES", 
                        "_Name": "河口"
                    }, 
                    {
                        "_Code": "HO", 
                        "_Name": "上奥果韦"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "中奥果韦"
                    }, 
                    {
                        "_Code": "NG", 
                        "_Name": "恩古涅"
                    }, 
                    {
                        "_Code": "NY", 
                        "_Name": "尼扬加"
                    }, 
                    {
                        "_Code": "OI", 
                        "_Name": "奥果韦-伊温多"
                    }, 
                    {
                        "_Code": "OL", 
                        "_Name": "奥果韦-洛洛"
                    }, 
                    {
                        "_Code": "OM", 
                        "_Name": "滨海奥果韦"
                    }, 
                    {
                        "_Code": "WN", 
                        "_Name": "沃勒-恩特姆"
                    }
                ]
            }
        }, 
        {
            "_Code": "GBR", 
            "_Name": "英国", 
            "State": [
                {
                    "City": [
                        {
                            "_Code": "BAS", 
                            "_Name": "巴斯"
                        }, 
                        {
                            "_Code": "BIR", 
                            "_Name": "伯明翰"
                        }, 
                        {
                            "_Code": "BNH", 
                            "_Name": "布莱顿与赫福"
                        }, 
                        {
                            "_Code": "BRD", 
                            "_Name": "布拉德福德"
                        }, 
                        {
                            "_Code": "BST", 
                            "_Name": "布里斯托尔"
                        }, 
                        {
                            "_Code": "CAX", 
                            "_Name": "卡莱尔"
                        }, 
                        {
                            "_Code": "CAM", 
                            "_Name": "剑桥"
                        }, 
                        {
                            "_Code": "CEG", 
                            "_Name": "切斯特"
                        }, 
                        {
                            "_Code": "CNG", 
                            "_Name": "坎特伯雷"
                        }, 
                        {
                            "_Code": "COV", 
                            "_Name": "考文垂"
                        }, 
                        {
                            "_Code": "CST", 
                            "_Name": "奇切斯特"
                        }, 
                        {
                            "_Code": "DER", 
                            "_Name": "德比"
                        }, 
                        {
                            "_Code": "DUR", 
                            "_Name": "德罕"
                        }, 
                        {
                            "_Code": "EXE", 
                            "_Name": "爱塞特"
                        }, 
                        {
                            "_Code": "ELY", 
                            "_Name": "伊利"
                        }, 
                        {
                            "_Code": "GLO", 
                            "_Name": "格洛斯特"
                        }, 
                        {
                            "_Code": "HAF", 
                            "_Name": "赫里福德"
                        }, 
                        {
                            "_Code": "KUH", 
                            "_Name": "赫尔河畔京斯敦"
                        }, 
                        {
                            "_Code": "LAN", 
                            "_Name": "兰开斯特"
                        }, 
                        {
                            "_Code": "LCE", 
                            "_Name": "列斯特"
                        }, 
                        {
                            "_Code": "LCN", 
                            "_Name": "林肯"
                        }, 
                        {
                            "_Code": "LDS", 
                            "_Name": "利茲"
                        }, 
                        {
                            "_Code": "LHF", 
                            "_Name": "利奇菲尔德"
                        }, 
                        {
                            "_Code": "LIV", 
                            "_Name": "利物浦"
                        }, 
                        {
                            "_Code": "LND", 
                            "_Name": "伦敦"
                        }, 
                        {
                            "_Code": "MAN", 
                            "_Name": "曼彻斯特"
                        }, 
                        {
                            "_Code": "NCL", 
                            "_Name": "纽卡斯尔"
                        }, 
                        {
                            "_Code": "NGM", 
                            "_Name": "诺丁汉"
                        }, 
                        {
                            "_Code": "NRW", 
                            "_Name": "诺里奇"
                        }, 
                        {
                            "_Code": "OXF", 
                            "_Name": "牛津"
                        }, 
                        {
                            "_Code": "PLY", 
                            "_Name": "普利茅斯"
                        }, 
                        {
                            "_Code": "POR", 
                            "_Name": "朴茨茅斯"
                        }, 
                        {
                            "_Code": "PRE", 
                            "_Name": "普雷斯顿"
                        }, 
                        {
                            "_Code": "PTE", 
                            "_Name": "彼得伯勒"
                        }, 
                        {
                            "_Code": "RIP", 
                            "_Name": "里彭"
                        }, 
                        {
                            "_Code": "SHE", 
                            "_Name": "谢菲尔德"
                        }, 
                        {
                            "_Code": "SLF", 
                            "_Name": "索福特"
                        }, 
                        {
                            "_Code": "SLS", 
                            "_Name": "索尔斯堡"
                        }, 
                        {
                            "_Code": "SOT", 
                            "_Name": "特伦特河畔斯多克"
                        }, 
                        {
                            "_Code": "STH", 
                            "_Name": "南安普敦"
                        }, 
                        {
                            "_Code": "SUN", 
                            "_Name": "桑德兰"
                        }, 
                        {
                            "_Code": "TBL", 
                            "_Name": "圣阿本斯"
                        }, 
                        {
                            "_Code": "TRU", 
                            "_Name": "特鲁罗"
                        }, 
                        {
                            "_Code": "WKF", 
                            "_Name": "韦克菲尔德"
                        }, 
                        {
                            "_Code": "WLS", 
                            "_Name": "威尔斯"
                        }, 
                        {
                            "_Code": "WNE", 
                            "_Name": "温彻斯特"
                        }, 
                        {
                            "_Code": "WOR", 
                            "_Name": "伍斯特"
                        }, 
                        {
                            "_Code": "WOV", 
                            "_Name": "伍尔弗汉普顿"
                        }, 
                        {
                            "_Code": "YOR", 
                            "_Name": "约克"
                        }
                    ], 
                    "_Code": "ENG", 
                    "_Name": "英格兰"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BFS", 
                            "_Name": "贝尔法斯特"
                        }, 
                        {
                            "_Code": "DRY", 
                            "_Name": "德里"
                        }, 
                        {
                            "_Code": "LSB", 
                            "_Name": "利斯本"
                        }, 
                        {
                            "_Code": "NYM", 
                            "_Name": "纽里"
                        }
                    ], 
                    "_Code": "NIR", 
                    "_Name": "北爱尔兰"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ABD", 
                            "_Name": "阿伯丁"
                        }, 
                        {
                            "_Code": "DND", 
                            "_Name": "丹迪"
                        }, 
                        {
                            "_Code": "EDH", 
                            "_Name": "爱丁堡"
                        }, 
                        {
                            "_Code": "GLG", 
                            "_Name": "格拉斯哥"
                        }, 
                        {
                            "_Code": "INV", 
                            "_Name": "因弗内斯"
                        }, 
                        {
                            "_Code": "STG", 
                            "_Name": "斯特灵"
                        }
                    ], 
                    "_Code": "SCT", 
                    "_Name": "苏格兰"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BAN", 
                            "_Name": "班戈"
                        }, 
                        {
                            "_Code": "CDF", 
                            "_Name": "卡迪夫"
                        }, 
                        {
                            "_Code": "NWP", 
                            "_Name": "纽波特"
                        }, 
                        {
                            "_Code": "SWA", 
                            "_Name": "斯旺西"
                        }
                    ], 
                    "_Code": "WLS", 
                    "_Name": "威尔士"
                }
            ]
        }, 
        {
            "_Code": "GEO", 
            "_Name": "乔治亚"
        }, 
        {
            "_Code": "GGY", 
            "_Name": "格恩西岛"
        }, 
        {
            "_Code": "GHA", 
            "_Name": "加纳", 
            "State": {
                "City": [
                    {
                        "_Code": "AS", 
                        "_Name": "阿散蒂"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "布朗阿哈福"
                    }, 
                    {
                        "_Code": "CE", 
                        "_Name": "中部"
                    }, 
                    {
                        "_Code": "EA", 
                        "_Name": "东部"
                    }, 
                    {
                        "_Code": "GA", 
                        "_Name": "大阿克拉"
                    }, 
                    {
                        "_Code": "NO", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "OBU", 
                        "_Name": "奥布阿西"
                    }, 
                    {
                        "_Code": "UE", 
                        "_Name": "上东部"
                    }, 
                    {
                        "_Code": "UW", 
                        "_Name": "上西部"
                    }, 
                    {
                        "_Code": "VO", 
                        "_Name": "沃尔特"
                    }, 
                    {
                        "_Code": "WE", 
                        "_Name": "西部"
                    }
                ]
            }
        }, 
        {
            "_Code": "GIB", 
            "_Name": "直布罗陀"
        }, 
        {
            "_Code": "GIN", 
            "_Name": "几内亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BOK", 
                        "_Name": "博凯"
                    }, 
                    {
                        "_Code": "CNK", 
                        "_Name": "科纳克里"
                    }, 
                    {
                        "_Code": "FRN", 
                        "_Name": "法拉纳"
                    }, 
                    {
                        "_Code": "KND", 
                        "_Name": "金迪亚"
                    }, 
                    {
                        "_Code": "KNK", 
                        "_Name": "康康"
                    }, 
                    {
                        "_Code": "LAB", 
                        "_Name": "拉贝"
                    }, 
                    {
                        "_Code": "MAM", 
                        "_Name": "玛木"
                    }, 
                    {
                        "_Code": "NZR", 
                        "_Name": "恩泽雷科雷"
                    }
                ]
            }
        }, 
        {
            "_Code": "GLP", 
            "_Name": "瓜德罗普"
        }, 
        {
            "_Code": "GMB", 
            "_Name": "冈比亚"
        }, 
        {
            "_Code": "GNB", 
            "_Name": "几内亚比绍"
        }, 
        {
            "_Code": "GRC", 
            "_Name": "希腊", 
            "State": {
                "City": [
                    {
                        "_Code": "ATH", 
                        "_Name": "雅典"
                    }, 
                    {
                        "_Code": "CHQ", 
                        "_Name": "干尼亚"
                    }, 
                    {
                        "_Code": "CY", 
                        "_Name": "基克拉迪"
                    }, 
                    {
                        "_Code": "DO", 
                        "_Name": "多德卡尼斯"
                    }, 
                    {
                        "_Code": "HER", 
                        "_Name": "伊拉克里翁"
                    }, 
                    {
                        "_Code": "LES", 
                        "_Name": "莱斯博斯"
                    }, 
                    {
                        "_Code": "LST", 
                        "_Name": "拉西锡"
                    }, 
                    {
                        "_Code": "PRI", 
                        "_Name": "比雷埃夫斯"
                    }, 
                    {
                        "_Code": "RET", 
                        "_Name": "雷西姆农"
                    }, 
                    {
                        "_Code": "SMI", 
                        "_Name": "萨摩斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "GRD", 
            "_Name": "格林纳达"
        }, 
        {
            "_Code": "GRL", 
            "_Name": "格陵兰"
        }, 
        {
            "_Code": "GTM", 
            "_Name": "危地马拉", 
            "State": {
                "City": [
                    {
                        "_Code": "AV", 
                        "_Name": "上韦拉帕斯"
                    }, 
                    {
                        "_Code": "BV", 
                        "_Name": "下韦拉帕斯"
                    }, 
                    {
                        "_Code": "CQ", 
                        "_Name": "奇基穆拉"
                    }, 
                    {
                        "_Code": "CM", 
                        "_Name": "奇马尔特南戈"
                    }, 
                    {
                        "_Code": "ES", 
                        "_Name": "埃斯昆特拉"
                    }, 
                    {
                        "_Code": "GU", 
                        "_Name": "危地马拉"
                    }, 
                    {
                        "_Code": "HU", 
                        "_Name": "韦韦特南戈"
                    }, 
                    {
                        "_Code": "IZ", 
                        "_Name": "伊萨瓦尔"
                    }, 
                    {
                        "_Code": "JA", 
                        "_Name": "哈拉帕"
                    }, 
                    {
                        "_Code": "JU", 
                        "_Name": "胡蒂亚帕"
                    }, 
                    {
                        "_Code": "QC", 
                        "_Name": "基切"
                    }, 
                    {
                        "_Code": "QZ", 
                        "_Name": "克萨尔特南戈"
                    }, 
                    {
                        "_Code": "MIX", 
                        "_Name": "米克斯科"
                    }, 
                    {
                        "_Code": "PE", 
                        "_Name": "佩滕"
                    }, 
                    {
                        "_Code": "PR", 
                        "_Name": "埃尔普罗格雷索"
                    }, 
                    {
                        "_Code": "RE", 
                        "_Name": "雷塔卢莱乌"
                    }, 
                    {
                        "_Code": "SM", 
                        "_Name": "圣马科斯"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "索洛拉"
                    }, 
                    {
                        "_Code": "SR", 
                        "_Name": "圣罗莎"
                    }, 
                    {
                        "_Code": "ST", 
                        "_Name": "萨卡特佩克斯"
                    }, 
                    {
                        "_Code": "SU", 
                        "_Name": "苏奇特佩克斯"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "托托尼卡潘"
                    }, 
                    {
                        "_Code": "VIN", 
                        "_Name": "新城"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "萨卡帕"
                    }
                ]
            }
        }, 
        {
            "_Code": "GUF", 
            "_Name": "法属圭亚那"
        }, 
        {
            "_Code": "GUM", 
            "_Name": "关岛"
        }, 
        {
            "_Code": "GUY", 
            "_Name": "圭亚那", 
            "State": {
                "City": [
                    {
                        "_Code": "BW", 
                        "_Name": "巴里马-瓦伊尼"
                    }, 
                    {
                        "_Code": "CM", 
                        "_Name": "库尤尼-马扎鲁尼"
                    }, 
                    {
                        "_Code": "DM", 
                        "_Name": "德梅拉拉-马海卡"
                    }, 
                    {
                        "_Code": "EC", 
                        "_Name": "东伯比斯-科兰太因"
                    }, 
                    {
                        "_Code": "EW", 
                        "_Name": "埃塞奎博群岛-西德梅拉拉"
                    }, 
                    {
                        "_Code": "MB", 
                        "_Name": "马海卡-伯比斯"
                    }, 
                    {
                        "_Code": "PI", 
                        "_Name": "波塔罗-锡帕鲁尼"
                    }, 
                    {
                        "_Code": "PM", 
                        "_Name": "波默伦-苏佩纳姆"
                    }, 
                    {
                        "_Code": "UD", 
                        "_Name": "上德梅拉拉-伯比斯"
                    }, 
                    {
                        "_Code": "UT", 
                        "_Name": "上塔库图-上埃塞奎博"
                    }
                ]
            }
        }, 
        {
            "_Code": "HMD", 
            "_Name": "赫德和麦克唐纳群岛"
        }, 
        {
            "_Code": "HND", 
            "_Name": "洪都拉斯", 
            "State": {
                "City": [
                    {
                        "_Code": "AT", 
                        "_Name": "阿特兰蒂达"
                    }, 
                    {
                        "_Code": "CH", 
                        "_Name": "乔卢特卡"
                    }, 
                    {
                        "_Code": "CHO", 
                        "_Name": "乔罗马"
                    }, 
                    {
                        "_Code": "CL", 
                        "_Name": "科隆"
                    }, 
                    {
                        "_Code": "CM", 
                        "_Name": "科马亚瓜"
                    }, 
                    {
                        "_Code": "CP", 
                        "_Name": "科潘"
                    }, 
                    {
                        "_Code": "CR", 
                        "_Name": "科尔特斯"
                    }, 
                    {
                        "_Code": "FM", 
                        "_Name": "弗朗西斯科-莫拉桑"
                    }, 
                    {
                        "_Code": "GD", 
                        "_Name": "格拉西亚斯-阿迪奥斯"
                    }, 
                    {
                        "_Code": "IB", 
                        "_Name": "海湾群岛"
                    }, 
                    {
                        "_Code": "IN", 
                        "_Name": "因蒂布卡"
                    }, 
                    {
                        "_Code": "LE", 
                        "_Name": "伦皮拉"
                    }, 
                    {
                        "_Code": "OC", 
                        "_Name": "奥科特佩克"
                    }, 
                    {
                        "_Code": "OL", 
                        "_Name": "奥兰乔"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "埃尔帕拉伊索"
                    }, 
                    {
                        "_Code": "PZ", 
                        "_Name": "拉巴斯"
                    }, 
                    {
                        "_Code": "SB", 
                        "_Name": "圣巴巴拉"
                    }, 
                    {
                        "_Code": "VA", 
                        "_Name": "山谷"
                    }, 
                    {
                        "_Code": "YO", 
                        "_Name": "约罗"
                    }
                ]
            }
        }, 
        {
            "_Code": "HRV", 
            "_Name": "克罗地亚", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "萨格勒布"
                    }, 
                    {
                        "_Code": "10", 
                        "_Name": "维罗维蒂察-波德拉维纳"
                    }, 
                    {
                        "_Code": "11", 
                        "_Name": "波热加-斯拉沃尼亚"
                    }, 
                    {
                        "_Code": "12", 
                        "_Name": "布罗德-波萨维纳"
                    }, 
                    {
                        "_Code": "13", 
                        "_Name": "扎达尔"
                    }, 
                    {
                        "_Code": "14", 
                        "_Name": "奥西耶克-巴拉尼亚"
                    }, 
                    {
                        "_Code": "15", 
                        "_Name": "希贝尼克-克宁"
                    }, 
                    {
                        "_Code": "16", 
                        "_Name": "武科瓦尔-斯里耶姆"
                    }, 
                    {
                        "_Code": "17", 
                        "_Name": "斯普利特-达尔马提亚"
                    }, 
                    {
                        "_Code": "18", 
                        "_Name": "伊斯特拉"
                    }, 
                    {
                        "_Code": "19", 
                        "_Name": "杜布罗夫斯克-内雷特瓦"
                    }, 
                    {
                        "_Code": "2", 
                        "_Name": "克拉皮纳-扎戈列"
                    }, 
                    {
                        "_Code": "20", 
                        "_Name": "梅吉穆列"
                    }, 
                    {
                        "_Code": "21", 
                        "_Name": "萨格勒布市"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "锡萨克-莫斯拉维纳"
                    }, 
                    {
                        "_Code": "4", 
                        "_Name": "卡尔洛瓦茨"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "瓦拉日丁"
                    }, 
                    {
                        "_Code": "6", 
                        "_Name": "科普里夫尼察-克里热夫齐"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "别洛瓦尔-比洛戈拉"
                    }, 
                    {
                        "_Code": "8", 
                        "_Name": "滨海和山区"
                    }, 
                    {
                        "_Code": "9", 
                        "_Name": "利卡-塞尼"
                    }
                ]
            }
        }, 
        {
            "_Code": "HTI", 
            "_Name": "海地"
        }, 
        {
            "_Code": "HUN", 
            "_Name": "匈牙利", 
            "State": {
                "City": [
                    {
                        "_Code": "BA", 
                        "_Name": "巴兰尼亚"
                    }, 
                    {
                        "_Code": "BE", 
                        "_Name": "贝凯什"
                    }, 
                    {
                        "_Code": "BK", 
                        "_Name": "巴奇-基什孔"
                    }, 
                    {
                        "_Code": "BU", 
                        "_Name": "布达佩斯"
                    }, 
                    {
                        "_Code": "BZ", 
                        "_Name": "包尔绍德-奥包乌伊-曾普伦"
                    }, 
                    {
                        "_Code": "CS", 
                        "_Name": "琼格拉德"
                    }, 
                    {
                        "_Code": "FE", 
                        "_Name": "费耶尔"
                    }, 
                    {
                        "_Code": "GS", 
                        "_Name": "杰尔-莫松-肖普朗"
                    }, 
                    {
                        "_Code": "HB", 
                        "_Name": "豪伊杜-比豪尔"
                    }, 
                    {
                        "_Code": "HE", 
                        "_Name": "赫维什"
                    }, 
                    {
                        "_Code": "JN", 
                        "_Name": "加兹-纳杰孔-索尔诺克"
                    }, 
                    {
                        "_Code": "KE", 
                        "_Name": "科马罗姆"
                    }, 
                    {
                        "_Code": "NO", 
                        "_Name": "诺格拉德"
                    }, 
                    {
                        "_Code": "PE", 
                        "_Name": "佩斯"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "绍莫吉"
                    }, 
                    {
                        "_Code": "SZ", 
                        "_Name": "索博尔奇-索特马尔-贝拉格"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "托尔瑙"
                    }, 
                    {
                        "_Code": "VA", 
                        "_Name": "沃什"
                    }, 
                    {
                        "_Code": "VE", 
                        "_Name": "维斯普雷姆"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "佐洛"
                    }
                ]
            }
        }, 
        {
            "_Code": "IDN", 
            "_Name": "印度尼西亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AC", 
                        "_Name": "亚齐"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "巴厘"
                    }, 
                    {
                        "_Code": "BB", 
                        "_Name": "邦加－勿里洞群岛"
                    }, 
                    {
                        "_Code": "BE", 
                        "_Name": "明古鲁"
                    }, 
                    {
                        "_Code": "BT", 
                        "_Name": "万丹"
                    }, 
                    {
                        "_Code": "IJ", 
                        "_Name": "伊里安查亚"
                    }, 
                    {
                        "_Code": "JA", 
                        "_Name": "占碑"
                    }, 
                    {
                        "_Code": "JB", 
                        "_Name": "西爪哇"
                    }, 
                    {
                        "_Code": "JI", 
                        "_Name": "东爪哇"
                    }, 
                    {
                        "_Code": "JK", 
                        "_Name": "雅加达"
                    }, 
                    {
                        "_Code": "JT", 
                        "_Name": "中爪哇"
                    }, 
                    {
                        "_Code": "KB", 
                        "_Name": "大雅加达首都特区"
                    }, 
                    {
                        "_Code": "KI", 
                        "_Name": "东加里曼丹"
                    }, 
                    {
                        "_Code": "KS", 
                        "_Name": "南加里曼丹"
                    }, 
                    {
                        "_Code": "KT", 
                        "_Name": "中加里曼丹"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "楠榜"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马鲁古"
                    }, 
                    {
                        "_Code": "NB", 
                        "_Name": "西努沙登加拉"
                    }, 
                    {
                        "_Code": "NT", 
                        "_Name": "东努沙登加拉"
                    }, 
                    {
                        "_Code": "RI", 
                        "_Name": "廖内"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "北苏拉威西"
                    }, 
                    {
                        "_Code": "SG", 
                        "_Name": "东南苏拉威西"
                    }, 
                    {
                        "_Code": "SN", 
                        "_Name": "南苏拉威西"
                    }, 
                    {
                        "_Code": "SR", 
                        "_Name": "西苏门答腊"
                    }, 
                    {
                        "_Code": "SS", 
                        "_Name": "南苏门答腊"
                    }, 
                    {
                        "_Code": "ST", 
                        "_Name": "中苏拉威西"
                    }, 
                    {
                        "_Code": "SU", 
                        "_Name": "北苏门答腊"
                    }, 
                    {
                        "_Code": "YO", 
                        "_Name": "日惹特区"
                    }
                ]
            }
        }, 
        {
            "_Code": "IMN", 
            "_Name": "曼岛"
        }, 
        {
            "_Code": "IND", 
            "_Name": "印度", 
            "State": {
                "City": [
                    {
                        "_Code": "AJL", 
                        "_Name": "艾藻尔"
                    }, 
                    {
                        "_Code": "BBI", 
                        "_Name": "布巴内斯瓦尔"
                    }, 
                    {
                        "_Code": "BHO", 
                        "_Name": "博帕尔"
                    }, 
                    {
                        "_Code": "BLR", 
                        "_Name": "班加罗尔"
                    }, 
                    {
                        "_Code": "CCU", 
                        "_Name": "加尔各答"
                    }, 
                    {
                        "_Code": "CJB", 
                        "_Name": "哥印拜陀"
                    }, 
                    {
                        "_Code": "DAM", 
                        "_Name": "达曼"
                    }, 
                    {
                        "_Code": "DIU", 
                        "_Name": "第乌"
                    }, 
                    {
                        "_Code": "GTO", 
                        "_Name": "甘托克"
                    }, 
                    {
                        "_Code": "ICD", 
                        "_Name": "新德里"
                    }, 
                    {
                        "_Code": "IDR", 
                        "_Name": "印多尔"
                    }, 
                    {
                        "_Code": "IXC", 
                        "_Name": "昌迪加尔"
                    }, 
                    {
                        "_Code": "IXM", 
                        "_Name": "马杜赖"
                    }, 
                    {
                        "_Code": "IMF", 
                        "_Name": "因帕尔"
                    }, 
                    {
                        "_Code": "JAI", 
                        "_Name": "斋普尔"
                    }, 
                    {
                        "_Code": "JDH", 
                        "_Name": "焦特布尔"
                    }, 
                    {
                        "_Code": "JLR", 
                        "_Name": "贾巴尔普尔"
                    }, 
                    {
                        "_Code": "JUC", 
                        "_Name": "贾朗达尔"
                    }, 
                    {
                        "_Code": "KOM", 
                        "_Name": "科希马"
                    }, 
                    {
                        "_Code": "KRK", 
                        "_Name": "加里加尔"
                    }, 
                    {
                        "_Code": "KVA", 
                        "_Name": "卡瓦拉蒂"
                    }, 
                    {
                        "_Code": "MAA", 
                        "_Name": "金奈"
                    }, 
                    {
                        "_Code": "MAH", 
                        "_Name": "马埃"
                    }, 
                    {
                        "_Code": "PNY", 
                        "_Name": "本地治里"
                    }, 
                    {
                        "_Code": "SHL", 
                        "_Name": "西隆"
                    }, 
                    {
                        "_Code": "SIL", 
                        "_Name": "锡尔萨瓦"
                    }, 
                    {
                        "_Code": "SLR", 
                        "_Name": "森伯尔布尔"
                    }, 
                    {
                        "_Code": "SRV", 
                        "_Name": "亚南"
                    }, 
                    {
                        "_Code": "TRV", 
                        "_Name": "特里凡得琅"
                    }, 
                    {
                        "_Code": "UDR", 
                        "_Name": "乌代布尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "IOT", 
            "_Name": "英属印度洋领地"
        }, 
        {
            "_Code": "IRQ", 
            "_Name": "伊拉克"
        }, 
        {
            "_Code": "IRL", 
            "_Name": "爱尔兰", 
            "State": {
                "City": [
                    {
                        "_Code": "CK", 
                        "_Name": "科克"
                    }, 
                    {
                        "_Code": "CL", 
                        "_Name": "克莱尔"
                    }, 
                    {
                        "_Code": "CV", 
                        "_Name": "卡范"
                    }, 
                    {
                        "_Code": "CW", 
                        "_Name": "卡洛"
                    }, 
                    {
                        "_Code": "DB", 
                        "_Name": "都柏林"
                    }, 
                    {
                        "_Code": "DG", 
                        "_Name": "多内加尔"
                    }, 
                    {
                        "_Code": "GW", 
                        "_Name": "戈尔韦"
                    }, 
                    {
                        "_Code": "KD", 
                        "_Name": "基尔代尔"
                    }, 
                    {
                        "_Code": "KK", 
                        "_Name": "基尔肯尼"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "凯里"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "崂斯"
                    }, 
                    {
                        "_Code": "LF", 
                        "_Name": "朗福德"
                    }, 
                    {
                        "_Code": "LM", 
                        "_Name": "利默里克"
                    }, 
                    {
                        "_Code": "LR", 
                        "_Name": "利特里姆"
                    }, 
                    {
                        "_Code": "LT", 
                        "_Name": "劳斯"
                    }, 
                    {
                        "_Code": "MG", 
                        "_Name": "莫内根"
                    }, 
                    {
                        "_Code": "MT", 
                        "_Name": "米斯"
                    }, 
                    {
                        "_Code": "MY", 
                        "_Name": "梅奥"
                    }, 
                    {
                        "_Code": "OF", 
                        "_Name": "奥法利"
                    }, 
                    {
                        "_Code": "RC", 
                        "_Name": "罗斯康芒"
                    }, 
                    {
                        "_Code": "SL", 
                        "_Name": "斯莱戈"
                    }, 
                    {
                        "_Code": "TP", 
                        "_Name": "蒂珀雷里"
                    }, 
                    {
                        "_Code": "WF", 
                        "_Name": "沃特福德"
                    }, 
                    {
                        "_Code": "WX", 
                        "_Name": "韦克斯福德"
                    }, 
                    {
                        "_Code": "WK", 
                        "_Name": "威克洛"
                    }, 
                    {
                        "_Code": "WM", 
                        "_Name": "西米斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "IRN", 
            "_Name": "伊朗"
        }, 
        {
            "_Code": "ISL", 
            "_Name": "冰岛"
        }, 
        {
            "_Code": "ISR", 
            "_Name": "以色列", 
            "State": {
                "City": [
                    {
                        "_Code": "ASH", 
                        "_Name": "阿什杜德"
                    }, 
                    {
                        "_Code": "BAT", 
                        "_Name": "贝特雁"
                    }, 
                    {
                        "_Code": "BEV", 
                        "_Name": "贝尔谢巴"
                    }, 
                    {
                        "_Code": "HFA", 
                        "_Name": "海法"
                    }, 
                    {
                        "_Code": "HOL", 
                        "_Name": "霍隆"
                    }, 
                    {
                        "_Code": "J", 
                        "_Name": "耶路撒冷"
                    }, 
                    {
                        "_Code": "NAT", 
                        "_Name": "内坦亚"
                    }, 
                    {
                        "_Code": "TLV", 
                        "_Name": "特拉维夫"
                    }
                ]
            }
        }, 
        {
            "_Code": "ITA", 
            "_Name": "意大利", 
            "State": {
                "City": [
                    {
                        "_Code": "ALE", 
                        "_Name": "亚历山德里亚"
                    }, 
                    {
                        "_Code": "AOI", 
                        "_Name": "安科纳"
                    }, 
                    {
                        "_Code": "AOT", 
                        "_Name": "奥斯塔"
                    }, 
                    {
                        "_Code": "ASP", 
                        "_Name": "阿斯科利皮切诺"
                    }, 
                    {
                        "_Code": "AST", 
                        "_Name": "阿斯蒂"
                    }, 
                    {
                        "_Code": "BDS", 
                        "_Name": "布林迪西"
                    }, 
                    {
                        "_Code": "BEN", 
                        "_Name": "贝内文托"
                    }, 
                    {
                        "_Code": "BGO", 
                        "_Name": "贝加莫"
                    }, 
                    {
                        "_Code": "BIE", 
                        "_Name": "布拉"
                    }, 
                    {
                        "_Code": "BLQ", 
                        "_Name": "博洛尼亚"
                    }, 
                    {
                        "_Code": "BRC", 
                        "_Name": "布雷西亚"
                    }, 
                    {
                        "_Code": "BRI", 
                        "_Name": "巴里"
                    }, 
                    {
                        "_Code": "CAG", 
                        "_Name": "卡利亚里"
                    }, 
                    {
                        "_Code": "CAX", 
                        "_Name": "热那亚"
                    }, 
                    {
                        "_Code": "CIY", 
                        "_Name": "科摩"
                    }, 
                    {
                        "_Code": "COB", 
                        "_Name": "坎波巴索"
                    }, 
                    {
                        "_Code": "CRV", 
                        "_Name": "克罗托内"
                    }, 
                    {
                        "_Code": "CST", 
                        "_Name": "卡塞塔"
                    }, 
                    {
                        "_Code": "CTA", 
                        "_Name": "卡塔尼亚"
                    }, 
                    {
                        "_Code": "CUN", 
                        "_Name": "库内奥"
                    }, 
                    {
                        "_Code": "FLR", 
                        "_Name": "佛罗伦萨"
                    }, 
                    {
                        "_Code": "FOG", 
                        "_Name": "福贾"
                    }, 
                    {
                        "_Code": "FRR", 
                        "_Name": "费拉拉"
                    }, 
                    {
                        "_Code": "ISE", 
                        "_Name": "伊塞尔尼亚"
                    }, 
                    {
                        "_Code": "QCS", 
                        "_Name": "科森扎"
                    }, 
                    {
                        "_Code": "QCZ", 
                        "_Name": "卡坦扎罗"
                    }, 
                    {
                        "_Code": "QNU", 
                        "_Name": "努奥罗"
                    }, 
                    {
                        "_Code": "QOS", 
                        "_Name": "奥里斯塔诺"
                    }, 
                    {
                        "_Code": "QPO", 
                        "_Name": "波坦察"
                    }, 
                    {
                        "_Code": "QSS", 
                        "_Name": "萨萨里"
                    }, 
                    {
                        "_Code": "LAQ", 
                        "_Name": "拉奎拉"
                    }, 
                    {
                        "_Code": "LCC", 
                        "_Name": "莱切"
                    }, 
                    {
                        "_Code": "LCO", 
                        "_Name": "莱科"
                    }, 
                    {
                        "_Code": "LIV", 
                        "_Name": "里窝那"
                    }, 
                    {
                        "_Code": "MCR", 
                        "_Name": "马萨"
                    }, 
                    {
                        "_Code": "MIL", 
                        "_Name": "米兰"
                    }, 
                    {
                        "_Code": "MOD", 
                        "_Name": "摩德纳"
                    }, 
                    {
                        "_Code": "MSN", 
                        "_Name": "墨西拿"
                    }, 
                    {
                        "_Code": "MTR", 
                        "_Name": "马泰拉"
                    }, 
                    {
                        "_Code": "MZA", 
                        "_Name": "蒙扎"
                    }, 
                    {
                        "_Code": "NAP", 
                        "_Name": "那不勒斯"
                    }, 
                    {
                        "_Code": "NVR", 
                        "_Name": "诺瓦拉"
                    }, 
                    {
                        "_Code": "OLB", 
                        "_Name": "奥尔比亚"
                    }, 
                    {
                        "_Code": "PAV", 
                        "_Name": "帕维亚"
                    }, 
                    {
                        "_Code": "PEG", 
                        "_Name": "佩鲁贾"
                    }, 
                    {
                        "_Code": "PMF", 
                        "_Name": "帕尔马"
                    }, 
                    {
                        "_Code": "PMO", 
                        "_Name": "巴勒莫"
                    }, 
                    {
                        "_Code": "PRD", 
                        "_Name": "波代诺内"
                    }, 
                    {
                        "_Code": "PSA", 
                        "_Name": "比萨"
                    }, 
                    {
                        "_Code": "REG", 
                        "_Name": "雷焦卡拉布里亚"
                    }, 
                    {
                        "_Code": "RNE", 
                        "_Name": "雷焦艾米利亚"
                    }, 
                    {
                        "_Code": "ROM", 
                        "_Name": "罗马"
                    }, 
                    {
                        "_Code": "SAL", 
                        "_Name": "萨莱诺"
                    }, 
                    {
                        "_Code": "SNA", 
                        "_Name": "锡耶纳"
                    }, 
                    {
                        "_Code": "SPE", 
                        "_Name": "拉斯佩齐亚"
                    }, 
                    {
                        "_Code": "SVN", 
                        "_Name": "萨沃纳"
                    }, 
                    {
                        "_Code": "SYR", 
                        "_Name": "锡拉库扎"
                    }, 
                    {
                        "_Code": "TAR", 
                        "_Name": "塔兰托"
                    }, 
                    {
                        "_Code": "TPS", 
                        "_Name": "特拉帕尼"
                    }, 
                    {
                        "_Code": "TRN", 
                        "_Name": "都灵"
                    }, 
                    {
                        "_Code": "TRS", 
                        "_Name": "的里雅斯特"
                    }, 
                    {
                        "_Code": "TRT", 
                        "_Name": "特伦托"
                    }, 
                    {
                        "_Code": "UDN", 
                        "_Name": "乌迪内"
                    }, 
                    {
                        "_Code": "VCE", 
                        "_Name": "威尼斯"
                    }, 
                    {
                        "_Code": "VIT", 
                        "_Name": "维泰博"
                    }, 
                    {
                        "_Code": "VRL", 
                        "_Name": "韦尔切利"
                    }
                ]
            }
        }, 
        {
            "_Code": "JAM", 
            "_Name": "牙买加", 
            "State": {
                "City": [
                    {
                        "_Code": "AND", 
                        "_Name": "圣安德鲁斯"
                    }, 
                    {
                        "_Code": "ANN", 
                        "_Name": "圣安娜"
                    }, 
                    {
                        "_Code": "CAT", 
                        "_Name": "圣凯瑟琳"
                    }, 
                    {
                        "_Code": "CLA", 
                        "_Name": "克拉伦登"
                    }, 
                    {
                        "_Code": "ELI", 
                        "_Name": "圣伊丽莎白"
                    }, 
                    {
                        "_Code": "HAN", 
                        "_Name": "汉诺威"
                    }, 
                    {
                        "_Code": "JAM", 
                        "_Name": "圣詹姆斯"
                    }, 
                    {
                        "_Code": "KIN", 
                        "_Name": "金斯敦"
                    }, 
                    {
                        "_Code": "MAN", 
                        "_Name": "曼彻斯特"
                    }, 
                    {
                        "_Code": "MAR", 
                        "_Name": "圣玛丽"
                    }, 
                    {
                        "_Code": "POR", 
                        "_Name": "波特兰"
                    }, 
                    {
                        "_Code": "THO", 
                        "_Name": "圣托马斯"
                    }, 
                    {
                        "_Code": "TRL", 
                        "_Name": "特里洛尼"
                    }, 
                    {
                        "_Code": "WML", 
                        "_Name": "西摩兰"
                    }
                ]
            }
        }, 
        {
            "_Code": "JEY", 
            "_Name": "泽西岛"
        }, 
        {
            "_Code": "JOR", 
            "_Name": "约旦", 
            "State": {
                "City": [
                    {
                        "_Code": "AJ", 
                        "_Name": "阿吉隆"
                    }, 
                    {
                        "_Code": "AQ", 
                        "_Name": "亚喀巴"
                    }, 
                    {
                        "_Code": "AM", 
                        "_Name": "安曼"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "拜勒加"
                    }, 
                    {
                        "_Code": "IR", 
                        "_Name": "伊尔比德"
                    }, 
                    {
                        "_Code": "JA", 
                        "_Name": "杰拉什"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "卡拉克"
                    }, 
                    {
                        "_Code": "MD", 
                        "_Name": "马德巴"
                    }, 
                    {
                        "_Code": "MF", 
                        "_Name": "马夫拉克"
                    }, 
                    {
                        "_Code": "MN", 
                        "_Name": "马安"
                    }, 
                    {
                        "_Code": "RU", 
                        "_Name": "鲁赛法"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔菲拉"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "扎尔卡"
                    }
                ]
            }
        }, 
        {
            "_Code": "JPN", 
            "_Name": "日本", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "北海道"
                    }, 
                    {
                        "_Code": "10", 
                        "_Name": "群马"
                    }, 
                    {
                        "_Code": "11", 
                        "_Name": "埼玉"
                    }, 
                    {
                        "_Code": "12", 
                        "_Name": "千叶"
                    }, 
                    {
                        "_Code": "13", 
                        "_Name": "东京"
                    }, 
                    {
                        "_Code": "14", 
                        "_Name": "神奈川"
                    }, 
                    {
                        "_Code": "15", 
                        "_Name": "新潟"
                    }, 
                    {
                        "_Code": "16", 
                        "_Name": "富山"
                    }, 
                    {
                        "_Code": "17", 
                        "_Name": "石川"
                    }, 
                    {
                        "_Code": "18", 
                        "_Name": "福井"
                    }, 
                    {
                        "_Code": "19", 
                        "_Name": "山梨"
                    }, 
                    {
                        "_Code": "2", 
                        "_Name": "青森"
                    }, 
                    {
                        "_Code": "20", 
                        "_Name": "长野"
                    }, 
                    {
                        "_Code": "21", 
                        "_Name": "岐阜"
                    }, 
                    {
                        "_Code": "22", 
                        "_Name": "静冈"
                    }, 
                    {
                        "_Code": "23", 
                        "_Name": "爱知"
                    }, 
                    {
                        "_Code": "24", 
                        "_Name": "三重"
                    }, 
                    {
                        "_Code": "25", 
                        "_Name": "滋贺"
                    }, 
                    {
                        "_Code": "26", 
                        "_Name": "京都"
                    }, 
                    {
                        "_Code": "27", 
                        "_Name": "大阪"
                    }, 
                    {
                        "_Code": "28", 
                        "_Name": "兵库"
                    }, 
                    {
                        "_Code": "29", 
                        "_Name": "奈良"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "岩手"
                    }, 
                    {
                        "_Code": "30", 
                        "_Name": "和歌山"
                    }, 
                    {
                        "_Code": "31", 
                        "_Name": "鸟取"
                    }, 
                    {
                        "_Code": "32", 
                        "_Name": "岛根"
                    }, 
                    {
                        "_Code": "33", 
                        "_Name": "冈山"
                    }, 
                    {
                        "_Code": "34", 
                        "_Name": "广岛"
                    }, 
                    {
                        "_Code": "35", 
                        "_Name": "山口"
                    }, 
                    {
                        "_Code": "36", 
                        "_Name": "徳岛"
                    }, 
                    {
                        "_Code": "37", 
                        "_Name": "香川"
                    }, 
                    {
                        "_Code": "38", 
                        "_Name": "爱媛"
                    }, 
                    {
                        "_Code": "39", 
                        "_Name": "高知"
                    }, 
                    {
                        "_Code": "4", 
                        "_Name": "宮城"
                    }, 
                    {
                        "_Code": "40", 
                        "_Name": "福冈"
                    }, 
                    {
                        "_Code": "41", 
                        "_Name": "佐贺"
                    }, 
                    {
                        "_Code": "42", 
                        "_Name": "长崎"
                    }, 
                    {
                        "_Code": "43", 
                        "_Name": "熊本"
                    }, 
                    {
                        "_Code": "44", 
                        "_Name": "大分"
                    }, 
                    {
                        "_Code": "45", 
                        "_Name": "宫崎"
                    }, 
                    {
                        "_Code": "46", 
                        "_Name": "鹿儿岛"
                    }, 
                    {
                        "_Code": "47", 
                        "_Name": "冲绳"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "秋田"
                    }, 
                    {
                        "_Code": "6", 
                        "_Name": "山形"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "福岛"
                    }, 
                    {
                        "_Code": "8", 
                        "_Name": "茨城"
                    }, 
                    {
                        "_Code": "9", 
                        "_Name": "枥木"
                    }
                ]
            }
        }, 
        {
            "_Code": "KAZ", 
            "_Name": "哈萨克斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "AKM", 
                        "_Name": "阿克莫拉"
                    }, 
                    {
                        "_Code": "AKS", 
                        "_Name": "阿克苏"
                    }, 
                    {
                        "_Code": "AKT", 
                        "_Name": "阿克托别"
                    }, 
                    {
                        "_Code": "ALA", 
                        "_Name": "阿拉木图"
                    }, 
                    {
                        "_Code": "ARY", 
                        "_Name": "阿雷斯"
                    }, 
                    {
                        "_Code": "AST", 
                        "_Name": "阿斯塔纳市"
                    }, 
                    {
                        "_Code": "ATY", 
                        "_Name": "阿特劳"
                    }, 
                    {
                        "_Code": "AYK", 
                        "_Name": "阿尔卡累克"
                    }, 
                    {
                        "_Code": "BXH", 
                        "_Name": "巴尔喀什"
                    }, 
                    {
                        "_Code": "DMB", 
                        "_Name": "江布尔"
                    }, 
                    {
                        "_Code": "DZH", 
                        "_Name": "杰兹卡兹甘"
                    }, 
                    {
                        "_Code": "EKB", 
                        "_Name": "埃基巴斯图兹"
                    }, 
                    {
                        "_Code": "KAP", 
                        "_Name": "卡普恰盖"
                    }, 
                    {
                        "_Code": "KAR", 
                        "_Name": "卡拉干达"
                    }, 
                    {
                        "_Code": "KEN", 
                        "_Name": "肯套"
                    }, 
                    {
                        "_Code": "KGT", 
                        "_Name": "南哈萨克斯坦"
                    }, 
                    {
                        "_Code": "KST", 
                        "_Name": "科斯塔奈"
                    }, 
                    {
                        "_Code": "KUR", 
                        "_Name": "库尔恰托夫"
                    }, 
                    {
                        "_Code": "KZO", 
                        "_Name": "卡拉扎尔"
                    }, 
                    {
                        "_Code": "KZY", 
                        "_Name": "克孜勒奥尔达"
                    }, 
                    {
                        "_Code": "LEN", 
                        "_Name": "列宁诺戈尔斯克"
                    }, 
                    {
                        "_Code": "LKK", 
                        "_Name": "利萨科夫斯克"
                    }, 
                    {
                        "_Code": "MAN", 
                        "_Name": "曼格斯套"
                    }, 
                    {
                        "_Code": "PAV", 
                        "_Name": "巴甫洛达尔"
                    }, 
                    {
                        "_Code": "RUD", 
                        "_Name": "鲁德内"
                    }, 
                    {
                        "_Code": "SAK", 
                        "_Name": "沙赫京斯克"
                    }, 
                    {
                        "_Code": "SAR", 
                        "_Name": "萨兰"
                    }, 
                    {
                        "_Code": "SEM", 
                        "_Name": "塞梅伊"
                    }, 
                    {
                        "_Code": "SEV", 
                        "_Name": "北哈萨克斯坦"
                    }, 
                    {
                        "_Code": "STE", 
                        "_Name": "斯捷普诺戈尔斯克"
                    }, 
                    {
                        "_Code": "TEK", 
                        "_Name": "铁克利"
                    }, 
                    {
                        "_Code": "TEM", 
                        "_Name": "铁米尔套"
                    }, 
                    {
                        "_Code": "TUR", 
                        "_Name": "突厥斯坦"
                    }, 
                    {
                        "_Code": "VOS", 
                        "_Name": "东哈萨克斯坦"
                    }, 
                    {
                        "_Code": "ZAP", 
                        "_Name": "西哈萨克斯坦"
                    }, 
                    {
                        "_Code": "ZHA", 
                        "_Name": "扎纳奥津"
                    }, 
                    {
                        "_Code": "ZYR", 
                        "_Name": "济良诺夫斯克"
                    }
                ]
            }
        }, 
        {
            "_Code": "KEN", 
            "_Name": "肯尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BAR", 
                        "_Name": "巴林戈"
                    }, 
                    {
                        "_Code": "BOM", 
                        "_Name": "博美特"
                    }, 
                    {
                        "_Code": "BUN", 
                        "_Name": "邦戈马"
                    }, 
                    {
                        "_Code": "BUS", 
                        "_Name": "布希亚"
                    }, 
                    {
                        "_Code": "CE", 
                        "_Name": "中央"
                    }, 
                    {
                        "_Code": "EMA", 
                        "_Name": "埃尔格约-马拉奎特"
                    }, 
                    {
                        "_Code": "EMB", 
                        "_Name": "恩布"
                    }, 
                    {
                        "_Code": "GAS", 
                        "_Name": "加里萨"
                    }, 
                    {
                        "_Code": "HOB", 
                        "_Name": "霍马湾"
                    }, 
                    {
                        "_Code": "ISI", 
                        "_Name": "伊希约洛"
                    }, 
                    {
                        "_Code": "KAJ", 
                        "_Name": "卡耶亚多"
                    }, 
                    {
                        "_Code": "KAK", 
                        "_Name": "卡卡梅加"
                    }, 
                    {
                        "_Code": "KEY", 
                        "_Name": "凯里乔"
                    }, 
                    {
                        "_Code": "KIA", 
                        "_Name": "基安布"
                    }, 
                    {
                        "_Code": "KII", 
                        "_Name": "基西"
                    }, 
                    {
                        "_Code": "KIL", 
                        "_Name": "基里菲"
                    }, 
                    {
                        "_Code": "KIR", 
                        "_Name": "基里尼亚加"
                    }, 
                    {
                        "_Code": "KIS", 
                        "_Name": "基苏木"
                    }, 
                    {
                        "_Code": "KIT", 
                        "_Name": "基图伊"
                    }, 
                    {
                        "_Code": "KWA", 
                        "_Name": "夸勒"
                    }, 
                    {
                        "_Code": "LAI", 
                        "_Name": "莱基皮亚"
                    }, 
                    {
                        "_Code": "LAU", 
                        "_Name": "拉木"
                    }, 
                    {
                        "_Code": "MAC", 
                        "_Name": "马查科斯"
                    }, 
                    {
                        "_Code": "MAK", 
                        "_Name": "马瓜尼"
                    }, 
                    {
                        "_Code": "MAN", 
                        "_Name": "曼德拉"
                    }, 
                    {
                        "_Code": "MER", 
                        "_Name": "梅鲁"
                    }, 
                    {
                        "_Code": "MIG", 
                        "_Name": "米戈利"
                    }, 
                    {
                        "_Code": "MOM", 
                        "_Name": "蒙巴萨"
                    }, 
                    {
                        "_Code": "MUR", 
                        "_Name": "穆兰卡"
                    }, 
                    {
                        "_Code": "NA", 
                        "_Name": "内罗毕"
                    }, 
                    {
                        "_Code": "NAN", 
                        "_Name": "南迪"
                    }, 
                    {
                        "_Code": "NAR", 
                        "_Name": "纳罗克"
                    }, 
                    {
                        "_Code": "NIT", 
                        "_Name": "尼蒂"
                    }, 
                    {
                        "_Code": "NUU", 
                        "_Name": "纳库鲁"
                    }, 
                    {
                        "_Code": "NYE", 
                        "_Name": "涅里"
                    }, 
                    {
                        "_Code": "NYM", 
                        "_Name": "尼亚米拉"
                    }, 
                    {
                        "_Code": "NYN", 
                        "_Name": "年达鲁阿"
                    }, 
                    {
                        "_Code": "RBT", 
                        "_Name": "马萨布布"
                    }, 
                    {
                        "_Code": "SIA", 
                        "_Name": "夏亚"
                    }, 
                    {
                        "_Code": "TNZ", 
                        "_Name": "特兰斯-恩佐亚"
                    }, 
                    {
                        "_Code": "TRI", 
                        "_Name": "塔纳河"
                    }, 
                    {
                        "_Code": "TTA", 
                        "_Name": "泰塔塔维塔"
                    }, 
                    {
                        "_Code": "TUR", 
                        "_Name": "图尔卡纳"
                    }, 
                    {
                        "_Code": "UAS", 
                        "_Name": "桑布卢"
                    }, 
                    {
                        "_Code": "UGI", 
                        "_Name": "瓦辛基苏"
                    }, 
                    {
                        "_Code": "VIH", 
                        "_Name": "韦希加"
                    }, 
                    {
                        "_Code": "WJR", 
                        "_Name": "瓦吉尔"
                    }, 
                    {
                        "_Code": "WPO", 
                        "_Name": "西波克特"
                    }
                ]
            }
        }, 
        {
            "_Code": "KGZ", 
            "_Name": "吉尔吉斯斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "B", 
                        "_Name": "巴特肯"
                    }, 
                    {
                        "_Code": "C", 
                        "_Name": "楚河"
                    }, 
                    {
                        "_Code": "GB", 
                        "_Name": "比什凯克市"
                    }, 
                    {
                        "_Code": "J", 
                        "_Name": "贾拉拉巴德"
                    }, 
                    {
                        "_Code": "KAN", 
                        "_Name": "坎特"
                    }, 
                    {
                        "_Code": "KBA", 
                        "_Name": "卡拉巴尔塔"
                    }, 
                    {
                        "_Code": "KJ", 
                        "_Name": "科克扬加克"
                    }, 
                    {
                        "_Code": "KKO", 
                        "_Name": "卡拉库尔"
                    }, 
                    {
                        "_Code": "MS", 
                        "_Name": "迈利赛"
                    }, 
                    {
                        "_Code": "N", 
                        "_Name": "纳伦"
                    }, 
                    {
                        "_Code": "O", 
                        "_Name": "奥什"
                    }, 
                    {
                        "_Code": "SU", 
                        "_Name": "苏卢克图"
                    }, 
                    {
                        "_Code": "T", 
                        "_Name": "塔拉斯"
                    }, 
                    {
                        "_Code": "TK", 
                        "_Name": "塔什库梅尔"
                    }, 
                    {
                        "_Code": "UG", 
                        "_Name": "乌兹根"
                    }, 
                    {
                        "_Code": "Y", 
                        "_Name": "伊塞克湖"
                    }
                ]
            }
        }, 
        {
            "_Code": "KHM", 
            "_Name": "柬埔寨", 
            "State": {
                "City": [
                    {
                        "_Code": "BA", 
                        "_Name": "马德望"
                    }, 
                    {
                        "_Code": "BM", 
                        "_Name": "班迭棉吉"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "西哈努克市"
                    }, 
                    {
                        "_Code": "KB", 
                        "_Name": "白马市"
                    }, 
                    {
                        "_Code": "KH", 
                        "_Name": "桔井"
                    }, 
                    {
                        "_Code": "KKZ", 
                        "_Name": "戈公"
                    }, 
                    {
                        "_Code": "KL", 
                        "_Name": "干丹"
                    }, 
                    {
                        "_Code": "KM", 
                        "_Name": "磅湛"
                    }, 
                    {
                        "_Code": "KMT", 
                        "_Name": "贡布"
                    }, 
                    {
                        "_Code": "KO", 
                        "_Name": "磅士卑"
                    }, 
                    {
                        "_Code": "KZC", 
                        "_Name": "磅清扬"
                    }, 
                    {
                        "_Code": "KZK", 
                        "_Name": "磅同"
                    }, 
                    {
                        "_Code": "MWV", 
                        "_Name": "蒙多基里"
                    }, 
                    {
                        "_Code": "OC", 
                        "_Name": "奥多棉吉"
                    }, 
                    {
                        "_Code": "PG", 
                        "_Name": "波罗勉"
                    }, 
                    {
                        "_Code": "PL", 
                        "_Name": "拜林市"
                    }, 
                    {
                        "_Code": "PNH", 
                        "_Name": "金边市"
                    }, 
                    {
                        "_Code": "PO", 
                        "_Name": "菩萨"
                    }, 
                    {
                        "_Code": "PR", 
                        "_Name": "柏威夏"
                    }, 
                    {
                        "_Code": "RBE", 
                        "_Name": "腊塔纳基里"
                    }, 
                    {
                        "_Code": "REP", 
                        "_Name": "暹粒"
                    }, 
                    {
                        "_Code": "SVR", 
                        "_Name": "柴桢"
                    }, 
                    {
                        "_Code": "TK", 
                        "_Name": "茶胶"
                    }, 
                    {
                        "_Code": "TNX", 
                        "_Name": "上丁"
                    }
                ]
            }
        }, 
        {
            "_Code": "KIR", 
            "_Name": "基里巴斯", 
            "State": {
                "City": [
                    {
                        "_Code": "GIL", 
                        "_Name": "吉尔伯特群岛"
                    }, 
                    {
                        "_Code": "LIN", 
                        "_Name": "莱恩群岛"
                    }, 
                    {
                        "_Code": "PHO", 
                        "_Name": "菲尼克斯群岛"
                    }
                ]
            }
        }, 
        {
            "_Code": "KNA", 
            "_Name": "圣基茨和尼维斯"
        }, 
        {
            "_Code": "KOR", 
            "_Name": "韩国", 
            "State": [
                {
                    "_Code": "11", 
                    "_Name": "首尔"
                }, 
                {
                    "_Code": "26", 
                    "_Name": "釜山"
                }, 
                {
                    "City": [
                        {
                            "_Code": "DSG", 
                            "_Name": "达城郡"
                        }, 
                        {
                            "_Code": "SUS", 
                            "_Name": "寿城区"
                        }, 
                        {
                            "_Code": "TAE", 
                            "_Name": "大邱"
                        }
                    ], 
                    "_Code": "27", 
                    "_Name": "大邱"
                }, 
                {
                    "_Code": "28", 
                    "_Name": "仁川"
                }, 
                {
                    "_Code": "29", 
                    "_Name": "光州"
                }, 
                {
                    "_Code": "30", 
                    "_Name": "大田"
                }, 
                {
                    "_Code": "31", 
                    "_Name": "蔚山"
                }, 
                {
                    "_Code": "41", 
                    "_Name": "济州特别自治道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CHC", 
                            "_Name": "春川市"
                        }, 
                        {
                            "_Code": "CWN", 
                            "_Name": "铁原郡"
                        }, 
                        {
                            "_Code": "GSG", 
                            "_Name": "高城郡"
                        }, 
                        {
                            "_Code": "HCH", 
                            "_Name": "华川郡"
                        }, 
                        {
                            "_Code": "HCN", 
                            "_Name": "洪川郡"
                        }, 
                        {
                            "_Code": "HSG", 
                            "_Name": "横城郡"
                        }, 
                        {
                            "_Code": "IJE", 
                            "_Name": "麟蹄郡"
                        }, 
                        {
                            "_Code": "JSE", 
                            "_Name": "旌善郡"
                        }, 
                        {
                            "_Code": "KAG", 
                            "_Name": "江陵市"
                        }, 
                        {
                            "_Code": "POG", 
                            "_Name": "平昌郡"
                        }, 
                        {
                            "_Code": "SHO", 
                            "_Name": "束草市"
                        }, 
                        {
                            "_Code": "SUK", 
                            "_Name": "三陟市"
                        }, 
                        {
                            "_Code": "TBK", 
                            "_Name": "太白市"
                        }, 
                        {
                            "_Code": "TGH", 
                            "_Name": "东海市"
                        }, 
                        {
                            "_Code": "WJU", 
                            "_Name": "原州市"
                        }, 
                        {
                            "_Code": "YGU", 
                            "_Name": "杨口郡"
                        }, 
                        {
                            "_Code": "YNY", 
                            "_Name": "襄阳郡"
                        }, 
                        {
                            "_Code": "YWL", 
                            "_Name": "宁越郡"
                        }
                    ], 
                    "_Code": "42", 
                    "_Name": "江原道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ADG", 
                            "_Name": "安东市"
                        }, 
                        {
                            "_Code": "BHA", 
                            "_Name": "奉化郡"
                        }, 
                        {
                            "_Code": "CDO", 
                            "_Name": "淸道郡"
                        }, 
                        {
                            "_Code": "CGK", 
                            "_Name": "漆谷郡"
                        }, 
                        {
                            "_Code": "CSG", 
                            "_Name": "靑松郡"
                        }, 
                        {
                            "_Code": "GJU", 
                            "_Name": "庆州市"
                        }, 
                        {
                            "_Code": "GRG", 
                            "_Name": "高灵郡"
                        }, 
                        {
                            "_Code": "GWI", 
                            "_Name": "军威郡"
                        }, 
                        {
                            "_Code": "GYS", 
                            "_Name": "庆山市"
                        }, 
                        {
                            "_Code": "KMC", 
                            "_Name": "金泉市"
                        }, 
                        {
                            "_Code": "KPO", 
                            "_Name": "浦项市"
                        }, 
                        {
                            "_Code": "KUM", 
                            "_Name": "龟尾市"
                        }, 
                        {
                            "_Code": "MGG", 
                            "_Name": "闻庆市"
                        }, 
                        {
                            "_Code": "SEJ", 
                            "_Name": "星州郡"
                        }, 
                        {
                            "_Code": "SJU", 
                            "_Name": "尙州市"
                        }, 
                        {
                            "_Code": "UJN", 
                            "_Name": "蔚珍郡"
                        }, 
                        {
                            "_Code": "ULG", 
                            "_Name": "郁陵郡"
                        }, 
                        {
                            "_Code": "USG", 
                            "_Name": "义城郡"
                        }, 
                        {
                            "_Code": "YCH", 
                            "_Name": "永川市"
                        }, 
                        {
                            "_Code": "YDK", 
                            "_Name": "盈德郡"
                        }, 
                        {
                            "_Code": "YEC", 
                            "_Name": "醴泉郡"
                        }, 
                        {
                            "_Code": "YEJ", 
                            "_Name": "荣州市"
                        }, 
                        {
                            "_Code": "YYG", 
                            "_Name": "英阳郡"
                        }
                    ], 
                    "_Code": "43", 
                    "_Name": "庆尚北道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CHF", 
                            "_Name": "鎭海市"
                        }, 
                        {
                            "_Code": "CHW", 
                            "_Name": "昌原市"
                        }, 
                        {
                            "_Code": "CNG", 
                            "_Name": "昌宁郡"
                        }, 
                        {
                            "_Code": "GCH", 
                            "_Name": "居昌郡"
                        }, 
                        {
                            "_Code": "GSO", 
                            "_Name": "固城郡"
                        }, 
                        {
                            "_Code": "HAN", 
                            "_Name": "咸安郡"
                        }, 
                        {
                            "_Code": "HCE", 
                            "_Name": "陜川郡"
                        }, 
                        {
                            "_Code": "HDG", 
                            "_Name": "河东郡"
                        }, 
                        {
                            "_Code": "HIN", 
                            "_Name": "晋州市"
                        }, 
                        {
                            "_Code": "HYG", 
                            "_Name": "咸阳郡"
                        }, 
                        {
                            "_Code": "KJE", 
                            "_Name": "巨济市"
                        }, 
                        {
                            "_Code": "KMH", 
                            "_Name": "金海市"
                        }, 
                        {
                            "_Code": "MAS", 
                            "_Name": "马山市"
                        }, 
                        {
                            "_Code": "MIR", 
                            "_Name": "密阳市"
                        }, 
                        {
                            "_Code": "NHE", 
                            "_Name": "南海郡"
                        }, 
                        {
                            "_Code": "SAH", 
                            "_Name": "泗川市"
                        }, 
                        {
                            "_Code": "SCH", 
                            "_Name": "山淸郡"
                        }, 
                        {
                            "_Code": "TYG", 
                            "_Name": "统营市"
                        }, 
                        {
                            "_Code": "URG", 
                            "_Name": "宜宁郡"
                        }, 
                        {
                            "_Code": "YSN", 
                            "_Name": "梁山市"
                        }
                    ], 
                    "_Code": "44", 
                    "_Name": "庆尚南道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BEN", 
                            "_Name": "报恩郡"
                        }, 
                        {
                            "_Code": "CHU", 
                            "_Name": "忠州市"
                        }, 
                        {
                            "_Code": "CJJ", 
                            "_Name": "淸州市"
                        }, 
                        {
                            "_Code": "CWO", 
                            "_Name": "淸原郡"
                        }, 
                        {
                            "_Code": "DYG", 
                            "_Name": "丹阳郡"
                        }, 
                        {
                            "_Code": "ESG", 
                            "_Name": "阴城郡"
                        }, 
                        {
                            "_Code": "GSN", 
                            "_Name": "槐山郡"
                        }, 
                        {
                            "_Code": "JCH", 
                            "_Name": "堤川市"
                        }, 
                        {
                            "_Code": "JCN", 
                            "_Name": "鎭川郡"
                        }, 
                        {
                            "_Code": "JYG", 
                            "_Name": "曾坪郡"
                        }, 
                        {
                            "_Code": "OCN", 
                            "_Name": "沃川郡"
                        }, 
                        {
                            "_Code": "YDG", 
                            "_Name": "永同郡"
                        }
                    ], 
                    "_Code": "45", 
                    "_Name": "忠清北道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ASA", 
                            "_Name": "牙山市"
                        }, 
                        {
                            "_Code": "BOR", 
                            "_Name": "保宁市"
                        }, 
                        {
                            "_Code": "BYO", 
                            "_Name": "扶余郡"
                        }, 
                        {
                            "_Code": "CHO", 
                            "_Name": "天安市"
                        }, 
                        {
                            "_Code": "CYG", 
                            "_Name": "青阳郡"
                        }, 
                        {
                            "_Code": "GOJ", 
                            "_Name": "公州市"
                        }, 
                        {
                            "_Code": "GSA", 
                            "_Name": "锦山郡"
                        }, 
                        {
                            "_Code": "GYE", 
                            "_Name": "鸡龙市"
                        }, 
                        {
                            "_Code": "HSE", 
                            "_Name": "洪城郡"
                        }, 
                        {
                            "_Code": "NSN", 
                            "_Name": "论山市"
                        }, 
                        {
                            "_Code": "SCE", 
                            "_Name": "舒川郡"
                        }, 
                        {
                            "_Code": "SSA", 
                            "_Name": "瑞山市"
                        }, 
                        {
                            "_Code": "TAN", 
                            "_Name": "泰安郡"
                        }, 
                        {
                            "_Code": "TJI", 
                            "_Name": "唐津郡"
                        }, 
                        {
                            "_Code": "YGI", 
                            "_Name": "燕岐郡"
                        }, 
                        {
                            "_Code": "YOS", 
                            "_Name": "礼山郡"
                        }
                    ], 
                    "_Code": "46", 
                    "_Name": "忠清南道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "GCG", 
                            "_Name": "高敞郡"
                        }, 
                        {
                            "_Code": "GJE", 
                            "_Name": "金堤市"
                        }, 
                        {
                            "_Code": "IKS", 
                            "_Name": "益山市"
                        }, 
                        {
                            "_Code": "ISL", 
                            "_Name": "任实郡"
                        }, 
                        {
                            "_Code": "JAN", 
                            "_Name": "鎭安郡"
                        }, 
                        {
                            "_Code": "JEO", 
                            "_Name": "井邑市"
                        }, 
                        {
                            "_Code": "JNJ", 
                            "_Name": "全州市"
                        }, 
                        {
                            "_Code": "JSU", 
                            "_Name": "长水郡"
                        }, 
                        {
                            "_Code": "KUV", 
                            "_Name": "群山市"
                        }, 
                        {
                            "_Code": "MJU", 
                            "_Name": "茂朱郡"
                        }, 
                        {
                            "_Code": "NWN", 
                            "_Name": "南原市"
                        }, 
                        {
                            "_Code": "PUS", 
                            "_Name": "扶安郡"
                        }, 
                        {
                            "_Code": "SCG", 
                            "_Name": "淳昌郡"
                        }, 
                        {
                            "_Code": "WAJ", 
                            "_Name": "完州郡"
                        }
                    ], 
                    "_Code": "47", 
                    "_Name": "全罗北道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BSG", 
                            "_Name": "宝城郡"
                        }, 
                        {
                            "_Code": "DYA", 
                            "_Name": "潭阳郡"
                        }, 
                        {
                            "_Code": "GHG", 
                            "_Name": "高兴郡"
                        }, 
                        {
                            "_Code": "GJN", 
                            "_Name": "康津郡"
                        }, 
                        {
                            "_Code": "GRE", 
                            "_Name": "求礼郡"
                        }, 
                        {
                            "_Code": "GSE", 
                            "_Name": "谷城郡"
                        }, 
                        {
                            "_Code": "HAE", 
                            "_Name": "海南郡"
                        }, 
                        {
                            "_Code": "HPG", 
                            "_Name": "咸平郡"
                        }, 
                        {
                            "_Code": "HSN", 
                            "_Name": "和顺郡"
                        }, 
                        {
                            "_Code": "JDO", 
                            "_Name": "珍岛郡"
                        }, 
                        {
                            "_Code": "JHG", 
                            "_Name": "长兴郡"
                        }, 
                        {
                            "_Code": "JSN", 
                            "_Name": "长城郡"
                        }, 
                        {
                            "_Code": "KAN", 
                            "_Name": "光阳市"
                        }, 
                        {
                            "_Code": "MAN", 
                            "_Name": "务安郡"
                        }, 
                        {
                            "_Code": "MOK", 
                            "_Name": "木浦市"
                        }, 
                        {
                            "_Code": "NJU", 
                            "_Name": "罗州市"
                        }, 
                        {
                            "_Code": "SAN", 
                            "_Name": "新安郡"
                        }, 
                        {
                            "_Code": "SYS", 
                            "_Name": "顺天市"
                        }, 
                        {
                            "_Code": "WND", 
                            "_Name": "莞岛郡"
                        }, 
                        {
                            "_Code": "YAM", 
                            "_Name": "灵岩郡"
                        }, 
                        {
                            "_Code": "YGG", 
                            "_Name": "灵光郡"
                        }, 
                        {
                            "_Code": "YOS", 
                            "_Name": "丽水市"
                        }
                    ], 
                    "_Code": "48", 
                    "_Name": "全罗南道"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ANY", 
                            "_Name": "安养市"
                        }, 
                        {
                            "_Code": "ASG", 
                            "_Name": "安城市"
                        }, 
                        {
                            "_Code": "ASN", 
                            "_Name": "安山市"
                        }, 
                        {
                            "_Code": "BCN", 
                            "_Name": "富川市"
                        }, 
                        {
                            "_Code": "DDC", 
                            "_Name": "东豆川市"
                        }, 
                        {
                            "_Code": "GCN", 
                            "_Name": "果川市"
                        }, 
                        {
                            "_Code": "GMG", 
                            "_Name": "光明市"
                        }, 
                        {
                            "_Code": "GMP", 
                            "_Name": "金浦市"
                        }, 
                        {
                            "_Code": "GPG", 
                            "_Name": "加平郡"
                        }, 
                        {
                            "_Code": "GRI", 
                            "_Name": "九里市"
                        }, 
                        {
                            "_Code": "GUN", 
                            "_Name": "军浦市"
                        }, 
                        {
                            "_Code": "GYG", 
                            "_Name": "高阳市"
                        }, 
                        {
                            "_Code": "HCH", 
                            "_Name": "华城市"
                        }, 
                        {
                            "_Code": "HNM", 
                            "_Name": "河南市"
                        }, 
                        {
                            "_Code": "ICE", 
                            "_Name": "利川市"
                        }, 
                        {
                            "_Code": "KWU", 
                            "_Name": "广州市"
                        }, 
                        {
                            "_Code": "NYU", 
                            "_Name": "南杨州市"
                        }, 
                        {
                            "_Code": "OSN", 
                            "_Name": "乌山市"
                        }, 
                        {
                            "_Code": "PJU", 
                            "_Name": "坡州市"
                        }, 
                        {
                            "_Code": "POC", 
                            "_Name": "抱川市"
                        }, 
                        {
                            "_Code": "PTK", 
                            "_Name": "平泽市"
                        }, 
                        {
                            "_Code": "SEO", 
                            "_Name": "城南市"
                        }, 
                        {
                            "_Code": "SHE", 
                            "_Name": "始兴市"
                        }, 
                        {
                            "_Code": "SUO", 
                            "_Name": "水原市"
                        }, 
                        {
                            "_Code": "UIJ", 
                            "_Name": "议政府市"
                        }, 
                        {
                            "_Code": "UWN", 
                            "_Name": "仪旺市"
                        }, 
                        {
                            "_Code": "YCN", 
                            "_Name": "涟川郡"
                        }, 
                        {
                            "_Code": "YJU", 
                            "_Name": "骊州郡"
                        }, 
                        {
                            "_Code": "YNG", 
                            "_Name": "龙仁市"
                        }, 
                        {
                            "_Code": "YPG", 
                            "_Name": "扬平郡"
                        }, 
                        {
                            "_Code": "YYU", 
                            "_Name": "杨州市"
                        }
                    ], 
                    "_Code": "49", 
                    "_Name": "京畿道"
                }
            ]
        }, 
        {
            "_Code": "KWT", 
            "_Name": "科威特"
        }, 
        {
            "_Code": "QAT", 
            "_Name": "卡塔尔", 
            "State": {
                "City": [
                    {
                        "_Code": "DW", 
                        "_Name": "多哈"
                    }, 
                    {
                        "_Code": "GW", 
                        "_Name": "古韦里耶"
                    }, 
                    {
                        "_Code": "JB", 
                        "_Name": "杰里扬拜特奈"
                    }, 
                    {
                        "_Code": "JM", 
                        "_Name": "朱迈利耶"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "豪尔"
                    }, 
                    {
                        "_Code": "MS", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "RN", 
                        "_Name": "赖扬"
                    }, 
                    {
                        "_Code": "UL", 
                        "_Name": "乌姆锡拉勒"
                    }, 
                    {
                        "_Code": "WK", 
                        "_Name": "沃克拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "LAO", 
            "_Name": "老挝", 
            "State": {
                "City": [
                    {
                        "_Code": "AT", 
                        "_Name": "阿速坡"
                    }, 
                    {
                        "_Code": "BK", 
                        "_Name": "博乔"
                    }, 
                    {
                        "_Code": "BL", 
                        "_Name": "波里坎赛"
                    }, 
                    {
                        "_Code": "CH", 
                        "_Name": "占巴塞"
                    }, 
                    {
                        "_Code": "HO", 
                        "_Name": "华潘"
                    }, 
                    {
                        "_Code": "XA", 
                        "_Name": "沙耶武里"
                    }, 
                    {
                        "_Code": "XE", 
                        "_Name": "色贡"
                    }, 
                    {
                        "_Code": "XI", 
                        "_Name": "川圹"
                    }, 
                    {
                        "_Code": "XN", 
                        "_Name": "赛宋本行政特区"
                    }, 
                    {
                        "_Code": "KH", 
                        "_Name": "甘蒙"
                    }, 
                    {
                        "_Code": "LM", 
                        "_Name": "琅南塔"
                    }, 
                    {
                        "_Code": "LP", 
                        "_Name": "琅勃拉邦"
                    }, 
                    {
                        "_Code": "OU", 
                        "_Name": "乌多姆赛"
                    }, 
                    {
                        "_Code": "PH", 
                        "_Name": "丰沙里"
                    }, 
                    {
                        "_Code": "SL", 
                        "_Name": "沙拉湾"
                    }, 
                    {
                        "_Code": "SV", 
                        "_Name": "沙湾拿吉"
                    }, 
                    {
                        "_Code": "VI", 
                        "_Name": "万象"
                    }
                ]
            }
        }, 
        {
            "_Code": "LBN", 
            "_Name": "黎巴嫩", 
            "State": {
                "City": [
                    {
                        "_Code": "AS", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "贝鲁特"
                    }, 
                    {
                        "_Code": "BI", 
                        "_Name": "贝卡"
                    }, 
                    {
                        "_Code": "JA", 
                        "_Name": "南部"
                    }, 
                    {
                        "_Code": "JL", 
                        "_Name": "黎巴嫩山"
                    }, 
                    {
                        "_Code": "NA", 
                        "_Name": "奈拜提耶市"
                    }
                ]
            }
        }, 
        {
            "_Code": "LBR", 
            "_Name": "利比里亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BG", 
                        "_Name": "邦"
                    }, 
                    {
                        "_Code": "BM", 
                        "_Name": "博米"
                    }, 
                    {
                        "_Code": "BOP", 
                        "_Name": "博波卢"
                    }, 
                    {
                        "_Code": "CM", 
                        "_Name": "大角山"
                    }, 
                    {
                        "_Code": "FT", 
                        "_Name": "菲什敦"
                    }, 
                    {
                        "_Code": "GB", 
                        "_Name": "大巴萨"
                    }, 
                    {
                        "_Code": "GBA", 
                        "_Name": "巴波卢"
                    }, 
                    {
                        "_Code": "GG", 
                        "_Name": "大吉德"
                    }, 
                    {
                        "_Code": "GK", 
                        "_Name": "大克鲁"
                    }, 
                    {
                        "_Code": "LO", 
                        "_Name": "洛法"
                    }, 
                    {
                        "_Code": "MG", 
                        "_Name": "马吉比"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "蒙特塞拉多"
                    }, 
                    {
                        "_Code": "MY", 
                        "_Name": "马里兰"
                    }, 
                    {
                        "_Code": "NI", 
                        "_Name": "宁巴"
                    }, 
                    {
                        "_Code": "RG", 
                        "_Name": "吉河"
                    }, 
                    {
                        "_Code": "RI", 
                        "_Name": "里弗塞斯"
                    }, 
                    {
                        "_Code": "SI", 
                        "_Name": "锡诺"
                    }
                ]
            }
        }, 
        {
            "_Code": "LBY", 
            "_Name": "利比亚"
        }, 
        {
            "_Code": "LCA", 
            "_Name": "圣卢西亚"
        }, 
        {
            "_Code": "LIE", 
            "_Name": "列支敦士登"
        }, 
        {
            "_Code": "LKA", 
            "_Name": "斯里兰卡", 
            "State": {
                "City": [
                    {
                        "_Code": "ADP", 
                        "_Name": "阿努拉德普勒"
                    }, 
                    {
                        "_Code": "AMP", 
                        "_Name": "安帕赖"
                    }, 
                    {
                        "_Code": "BAD", 
                        "_Name": "巴杜勒"
                    }, 
                    {
                        "_Code": "BTC", 
                        "_Name": "拜蒂克洛"
                    }, 
                    {
                        "_Code": "CMB", 
                        "_Name": "科伦坡"
                    }, 
                    {
                        "_Code": "GAL", 
                        "_Name": "加勒"
                    }, 
                    {
                        "_Code": "GAM", 
                        "_Name": "加姆珀哈"
                    }, 
                    {
                        "_Code": "HBA", 
                        "_Name": "汉班托特"
                    }, 
                    {
                        "_Code": "JAF", 
                        "_Name": "贾夫纳"
                    }, 
                    {
                        "_Code": "KAN", 
                        "_Name": "康提"
                    }, 
                    {
                        "_Code": "KEG", 
                        "_Name": "凯格勒"
                    }, 
                    {
                        "_Code": "KIL", 
                        "_Name": "基里诺奇"
                    }, 
                    {
                        "_Code": "KLT", 
                        "_Name": "卡卢特勒"
                    }, 
                    {
                        "_Code": "KUR", 
                        "_Name": "库鲁内格勒"
                    }, 
                    {
                        "_Code": "MAA", 
                        "_Name": "马特勒"
                    }, 
                    {
                        "_Code": "MAN", 
                        "_Name": "马纳尔"
                    }, 
                    {
                        "_Code": "MAT", 
                        "_Name": "马特莱"
                    }, 
                    {
                        "_Code": "MON", 
                        "_Name": "莫讷勒格勒"
                    }, 
                    {
                        "_Code": "MUL", 
                        "_Name": "穆莱蒂武"
                    }, 
                    {
                        "_Code": "NUE", 
                        "_Name": "努沃勒埃利耶"
                    }, 
                    {
                        "_Code": "POL", 
                        "_Name": "波隆纳鲁沃"
                    }, 
                    {
                        "_Code": "PUT", 
                        "_Name": "普塔勒姆"
                    }, 
                    {
                        "_Code": "RAT", 
                        "_Name": "拉特纳普勒"
                    }, 
                    {
                        "_Code": "TRR", 
                        "_Name": "亭可马里"
                    }, 
                    {
                        "_Code": "VAV", 
                        "_Name": "瓦武尼亚"
                    }
                ]
            }
        }, 
        {
            "_Code": "LSO", 
            "_Name": "莱索托", 
            "State": {
                "City": [
                    {
                        "_Code": "A", 
                        "_Name": "马塞卢"
                    }, 
                    {
                        "_Code": "B", 
                        "_Name": "布塔布泰"
                    }, 
                    {
                        "_Code": "C", 
                        "_Name": "莱里贝"
                    }, 
                    {
                        "_Code": "D", 
                        "_Name": "伯里亚"
                    }, 
                    {
                        "_Code": "E", 
                        "_Name": "马费滕"
                    }, 
                    {
                        "_Code": "F", 
                        "_Name": "莫哈莱斯胡克"
                    }, 
                    {
                        "_Code": "G", 
                        "_Name": "古廷"
                    }, 
                    {
                        "_Code": "H", 
                        "_Name": "加查斯内克"
                    }, 
                    {
                        "_Code": "J", 
                        "_Name": "莫霍特隆"
                    }, 
                    {
                        "_Code": "K", 
                        "_Name": "塔巴采卡"
                    }
                ]
            }
        }, 
        {
            "_Code": "LTU", 
            "_Name": "立陶宛", 
            "State": {
                "City": [
                    {
                        "_Code": "AKM", 
                        "_Name": "亚克曼"
                    }, 
                    {
                        "_Code": "AL", 
                        "_Name": "阿利图斯"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "考纳斯"
                    }, 
                    {
                        "_Code": "KL", 
                        "_Name": "克莱佩达"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马里扬泊列"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "帕涅韦日斯"
                    }, 
                    {
                        "_Code": "SI", 
                        "_Name": "希奥利艾"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "陶拉格"
                    }, 
                    {
                        "_Code": "TE", 
                        "_Name": "特尔希艾"
                    }, 
                    {
                        "_Code": "UT", 
                        "_Name": "乌田纳"
                    }, 
                    {
                        "_Code": "VI", 
                        "_Name": "维尔纽斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "LUX", 
            "_Name": "卢森堡", 
            "State": {
                "City": [
                    {
                        "_Code": "DD", 
                        "_Name": "迪基希"
                    }, 
                    {
                        "_Code": "GG", 
                        "_Name": "格雷文马赫"
                    }, 
                    {
                        "_Code": "LL", 
                        "_Name": "卢森堡"
                    }
                ]
            }
        }, 
        {
            "_Code": "LVA", 
            "_Name": "拉脱维亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AIZ", 
                        "_Name": "爱兹克劳克雷"
                    }, 
                    {
                        "_Code": "ALU", 
                        "_Name": "阿卢克斯内"
                    }, 
                    {
                        "_Code": "BAL", 
                        "_Name": "巴尔维"
                    }, 
                    {
                        "_Code": "BAU", 
                        "_Name": "包斯卡"
                    }, 
                    {
                        "_Code": "CES", 
                        "_Name": "采西斯"
                    }, 
                    {
                        "_Code": "DGR", 
                        "_Name": "陶格夫皮尔斯"
                    }, 
                    {
                        "_Code": "DOB", 
                        "_Name": "多贝莱"
                    }, 
                    {
                        "_Code": "GUL", 
                        "_Name": "古尔贝内"
                    }, 
                    {
                        "_Code": "JEK", 
                        "_Name": "杰卡布皮尔斯"
                    }, 
                    {
                        "_Code": "JGR", 
                        "_Name": "叶尔加瓦"
                    }, 
                    {
                        "_Code": "KRA", 
                        "_Name": "克拉斯拉瓦"
                    }, 
                    {
                        "_Code": "KUL", 
                        "_Name": "库尔迪加"
                    }, 
                    {
                        "_Code": "LIM", 
                        "_Name": "林巴济"
                    }, 
                    {
                        "_Code": "LPK", 
                        "_Name": "利耶帕亚"
                    }, 
                    {
                        "_Code": "LUD", 
                        "_Name": "卢扎"
                    }, 
                    {
                        "_Code": "MAD", 
                        "_Name": "马多纳"
                    }, 
                    {
                        "_Code": "OGR", 
                        "_Name": "奥格雷"
                    }, 
                    {
                        "_Code": "PRE", 
                        "_Name": "普雷利"
                    }, 
                    {
                        "_Code": "RGA", 
                        "_Name": "里加"
                    }, 
                    {
                        "_Code": "RZR", 
                        "_Name": "雷泽克内"
                    }, 
                    {
                        "_Code": "SAL", 
                        "_Name": "萨尔杜斯"
                    }, 
                    {
                        "_Code": "TAL", 
                        "_Name": "塔尔西"
                    }, 
                    {
                        "_Code": "TUK", 
                        "_Name": "图库马"
                    }, 
                    {
                        "_Code": "VLK", 
                        "_Name": "瓦尔加"
                    }, 
                    {
                        "_Code": "VLM", 
                        "_Name": "瓦尔米耶拉"
                    }, 
                    {
                        "_Code": "VSL", 
                        "_Name": "文茨皮尔斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "MAR", 
            "_Name": "摩洛哥", 
            "State": {
                "City": [
                    {
                        "_Code": "CBL", 
                        "_Name": "卡萨布兰卡"
                    }, 
                    {
                        "_Code": "FES", 
                        "_Name": "非斯"
                    }, 
                    {
                        "_Code": "MKN", 
                        "_Name": "梅克内斯"
                    }, 
                    {
                        "_Code": "MRK", 
                        "_Name": "马拉喀什"
                    }, 
                    {
                        "_Code": "OUJ", 
                        "_Name": "乌季达"
                    }, 
                    {
                        "_Code": "RSA", 
                        "_Name": "拉巴特"
                    }, 
                    {
                        "_Code": "TET", 
                        "_Name": "得土安"
                    }, 
                    {
                        "_Code": "TGR", 
                        "_Name": "丹吉尔"
                    }, 
                    {
                        "_Code": "WSH", 
                        "_Name": "西撒哈拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "MCO", 
            "_Name": "摩纳哥"
        }, 
        {
            "_Code": "MDA", 
            "_Name": "摩尔多瓦"
        }, 
        {
            "_Code": "MDG", 
            "_Name": "马达加斯加", 
            "State": {
                "City": [
                    {
                        "_Code": "AN", 
                        "_Name": "塔那那利佛"
                    }, 
                    {
                        "_Code": "AS", 
                        "_Name": "安齐拉纳纳"
                    }, 
                    {
                        "_Code": "FN", 
                        "_Name": "菲亚纳兰楚阿"
                    }, 
                    {
                        "_Code": "MJ", 
                        "_Name": "马哈赞加"
                    }, 
                    {
                        "_Code": "TL", 
                        "_Name": "图利亚拉"
                    }, 
                    {
                        "_Code": "TM", 
                        "_Name": "图阿马西拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "MDV", 
            "_Name": "马尔代夫", 
            "State": {
                "City": [
                    {
                        "_Code": "AAD", 
                        "_Name": "北阿里"
                    }, 
                    {
                        "_Code": "AAU", 
                        "_Name": "南阿里"
                    }, 
                    {
                        "_Code": "ADD", 
                        "_Name": "阿杜"
                    }, 
                    {
                        "_Code": "FAA", 
                        "_Name": "法迪福卢"
                    }, 
                    {
                        "_Code": "FEA", 
                        "_Name": "费利杜"
                    }, 
                    {
                        "_Code": "FMU", 
                        "_Name": "福阿穆拉库"
                    }, 
                    {
                        "_Code": "HAD", 
                        "_Name": "北苏瓦迪瓦"
                    }, 
                    {
                        "_Code": "HAU", 
                        "_Name": "南苏瓦迪瓦"
                    }, 
                    {
                        "_Code": "HDH", 
                        "_Name": "哈杜马蒂"
                    }, 
                    {
                        "_Code": "KLH", 
                        "_Name": "科卢马杜卢"
                    }, 
                    {
                        "_Code": "MAA", 
                        "_Name": "马累岛"
                    }, 
                    {
                        "_Code": "MAD", 
                        "_Name": "北马洛斯马杜卢"
                    }, 
                    {
                        "_Code": "MAL", 
                        "_Name": "马累"
                    }, 
                    {
                        "_Code": "MAU", 
                        "_Name": "南马洛斯马杜卢"
                    }, 
                    {
                        "_Code": "MLD", 
                        "_Name": "北米拉杜马杜卢"
                    }, 
                    {
                        "_Code": "MLU", 
                        "_Name": "南米拉杜马杜卢"
                    }, 
                    {
                        "_Code": "MUA", 
                        "_Name": "穆拉库"
                    }, 
                    {
                        "_Code": "NAD", 
                        "_Name": "北尼兰杜"
                    }, 
                    {
                        "_Code": "NAU", 
                        "_Name": "南尼兰杜"
                    }, 
                    {
                        "_Code": "THD", 
                        "_Name": "北蒂拉杜马蒂"
                    }, 
                    {
                        "_Code": "THU", 
                        "_Name": "南蒂拉杜马蒂"
                    }
                ]
            }
        }, 
        {
            "_Code": "MEX", 
            "_Name": "墨西哥", 
            "State": {
                "City": [
                    {
                        "_Code": "ACA", 
                        "_Name": "阿卡普尔科"
                    }, 
                    {
                        "_Code": "AGU", 
                        "_Name": "阿瓜斯卡连斯特"
                    }, 
                    {
                        "_Code": "BJU", 
                        "_Name": "华雷斯港"
                    }, 
                    {
                        "_Code": "CAM", 
                        "_Name": "埃佩切"
                    }, 
                    {
                        "_Code": "CAR", 
                        "_Name": "卡门"
                    }, 
                    {
                        "_Code": "CHH", 
                        "_Name": "奇瓦瓦"
                    }, 
                    {
                        "_Code": "CHI", 
                        "_Name": "奇尔潘辛戈"
                    }, 
                    {
                        "_Code": "CLY", 
                        "_Name": "塞拉亚"
                    }, 
                    {
                        "_Code": "COA", 
                        "_Name": "夸察夸拉克斯"
                    }, 
                    {
                        "_Code": "COL", 
                        "_Name": "科利马"
                    }, 
                    {
                        "_Code": "CTM", 
                        "_Name": "切图马尔"
                    }, 
                    {
                        "_Code": "CUL", 
                        "_Name": "库利阿坎"
                    }, 
                    {
                        "_Code": "CVC", 
                        "_Name": "库埃纳瓦卡"
                    }, 
                    {
                        "_Code": "DUR", 
                        "_Name": "杜兰戈"
                    }, 
                    {
                        "_Code": "ESE", 
                        "_Name": "恩塞纳达"
                    }, 
                    {
                        "_Code": "GDL", 
                        "_Name": "瓜达拉哈拉"
                    }, 
                    {
                        "_Code": "GUA", 
                        "_Name": "瓜纳华托"
                    }, 
                    {
                        "_Code": "HMO", 
                        "_Name": "埃莫西约"
                    }, 
                    {
                        "_Code": "IRP", 
                        "_Name": "伊拉普阿托"
                    }, 
                    {
                        "_Code": "JAL", 
                        "_Name": "哈拉帕"
                    }, 
                    {
                        "_Code": "JUZ", 
                        "_Name": "华雷斯"
                    }, 
                    {
                        "_Code": "QUE", 
                        "_Name": "克雷塔罗"
                    }, 
                    {
                        "_Code": "LAP", 
                        "_Name": "拉巴斯"
                    }, 
                    {
                        "_Code": "LEN", 
                        "_Name": "莱昂"
                    }, 
                    {
                        "_Code": "LMM", 
                        "_Name": "洛斯莫奇斯"
                    }, 
                    {
                        "_Code": "LOV", 
                        "_Name": "蒙克洛瓦"
                    }, 
                    {
                        "_Code": "MAM", 
                        "_Name": "马塔莫罗斯"
                    }, 
                    {
                        "_Code": "MEX", 
                        "_Name": "墨西哥城"
                    }, 
                    {
                        "_Code": "MXL", 
                        "_Name": "墨西卡利"
                    }, 
                    {
                        "_Code": "MID", 
                        "_Name": "梅里达"
                    }, 
                    {
                        "_Code": "MLM", 
                        "_Name": "莫雷利亚"
                    }, 
                    {
                        "_Code": "MTY", 
                        "_Name": "蒙特雷"
                    }, 
                    {
                        "_Code": "MZT", 
                        "_Name": "马萨特兰"
                    }, 
                    {
                        "_Code": "NLE", 
                        "_Name": "新拉雷多"
                    }, 
                    {
                        "_Code": "NOG", 
                        "_Name": "诺加莱斯"
                    }, 
                    {
                        "_Code": "OAX", 
                        "_Name": "瓦哈卡"
                    }, 
                    {
                        "_Code": "OBR", 
                        "_Name": "奥夫雷贡城"
                    }, 
                    {
                        "_Code": "ORI", 
                        "_Name": "奥里萨巴"
                    }, 
                    {
                        "_Code": "PAC", 
                        "_Name": "帕丘卡"
                    }, 
                    {
                        "_Code": "PRH", 
                        "_Name": "波萨里卡"
                    }, 
                    {
                        "_Code": "PUE", 
                        "_Name": "普埃布拉"
                    }, 
                    {
                        "_Code": "PVR", 
                        "_Name": "巴亚尔塔港"
                    }, 
                    {
                        "_Code": "REX", 
                        "_Name": "雷诺萨"
                    }, 
                    {
                        "_Code": "SLP", 
                        "_Name": "圣路易斯波托亚"
                    }, 
                    {
                        "_Code": "SLW", 
                        "_Name": "萨尔蒂约"
                    }, 
                    {
                        "_Code": "TAM", 
                        "_Name": "坦皮科"
                    }, 
                    {
                        "_Code": "TAP", 
                        "_Name": "塔帕丘拉"
                    }, 
                    {
                        "_Code": "TCN", 
                        "_Name": "特瓦坎"
                    }, 
                    {
                        "_Code": "TGZ", 
                        "_Name": "图斯特拉-古铁雷斯"
                    }, 
                    {
                        "_Code": "TIJ", 
                        "_Name": "蒂华纳"
                    }, 
                    {
                        "_Code": "TLA", 
                        "_Name": "特拉斯卡拉"
                    }, 
                    {
                        "_Code": "TLC", 
                        "_Name": "托卢卡"
                    }, 
                    {
                        "_Code": "TPQ", 
                        "_Name": "特皮克"
                    }, 
                    {
                        "_Code": "TRC", 
                        "_Name": "托雷翁"
                    }, 
                    {
                        "_Code": "UPN", 
                        "_Name": "乌鲁阿潘"
                    }, 
                    {
                        "_Code": "VER", 
                        "_Name": "韦拉克鲁斯"
                    }, 
                    {
                        "_Code": "VHM", 
                        "_Name": "巴利城"
                    }, 
                    {
                        "_Code": "VIC", 
                        "_Name": "维多利亚城"
                    }, 
                    {
                        "_Code": "VSA", 
                        "_Name": "比利亚埃尔莫萨"
                    }, 
                    {
                        "_Code": "ZAC", 
                        "_Name": "萨卡特卡斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "MHL", 
            "_Name": "马绍尔群岛"
        }, 
        {
            "_Code": "MKD", 
            "_Name": "马其顿"
        }, 
        {
            "_Code": "MLI", 
            "_Name": "马里", 
            "State": {
                "City": [
                    {
                        "_Code": "CD", 
                        "_Name": "巴马科首都区"
                    }, 
                    {
                        "_Code": "GA", 
                        "_Name": "加奥"
                    }, 
                    {
                        "_Code": "KD", 
                        "_Name": "基达尔"
                    }, 
                    {
                        "_Code": "KL", 
                        "_Name": "库利科罗"
                    }, 
                    {
                        "_Code": "KY", 
                        "_Name": "卡伊"
                    }, 
                    {
                        "_Code": "MP", 
                        "_Name": "莫普提"
                    }, 
                    {
                        "_Code": "SG", 
                        "_Name": "塞古"
                    }, 
                    {
                        "_Code": "SK", 
                        "_Name": "锡卡索"
                    }, 
                    {
                        "_Code": "TB", 
                        "_Name": "通布图"
                    }
                ]
            }
        }, 
        {
            "_Code": "MLT", 
            "_Name": "马耳他"
        }, 
        {
            "_Code": "MMR", 
            "_Name": "缅甸", 
            "State": {
                "City": [
                    {
                        "_Code": "AY", 
                        "_Name": "伊洛瓦底省"
                    }, 
                    {
                        "_Code": "BG", 
                        "_Name": "勃固省"
                    }, 
                    {
                        "_Code": "CH", 
                        "_Name": "钦邦"
                    }, 
                    {
                        "_Code": "KC", 
                        "_Name": "克钦邦"
                    }, 
                    {
                        "_Code": "KH", 
                        "_Name": "克耶邦"
                    }, 
                    {
                        "_Code": "KN", 
                        "_Name": "克伦邦"
                    }, 
                    {
                        "_Code": "MD", 
                        "_Name": "曼德勒省"
                    }, 
                    {
                        "_Code": "MG", 
                        "_Name": "马圭省"
                    }, 
                    {
                        "_Code": "MN", 
                        "_Name": "孟邦"
                    }, 
                    {
                        "_Code": "RK", 
                        "_Name": "若开邦"
                    }, 
                    {
                        "_Code": "SG", 
                        "_Name": "实皆省"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "掸邦"
                    }, 
                    {
                        "_Code": "TN", 
                        "_Name": "德林达依省"
                    }, 
                    {
                        "_Code": "YG", 
                        "_Name": "仰光省"
                    }
                ]
            }
        }, 
        {
            "_Code": "MNG", 
            "_Name": "蒙古", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "乌兰巴托市"
                    }, 
                    {
                        "_Code": "35", 
                        "_Name": "鄂尔浑"
                    }, 
                    {
                        "_Code": "37", 
                        "_Name": "达尔汗乌勒"
                    }, 
                    {
                        "_Code": "39", 
                        "_Name": "肯特"
                    }, 
                    {
                        "_Code": "41", 
                        "_Name": "库苏古尔"
                    }, 
                    {
                        "_Code": "43", 
                        "_Name": "科布多"
                    }, 
                    {
                        "_Code": "46", 
                        "_Name": "乌布苏"
                    }, 
                    {
                        "_Code": "47", 
                        "_Name": "中央"
                    }, 
                    {
                        "_Code": "49", 
                        "_Name": "色楞格"
                    }, 
                    {
                        "_Code": "51", 
                        "_Name": "苏赫巴托尔"
                    }, 
                    {
                        "_Code": "57", 
                        "_Name": "扎布汗"
                    }, 
                    {
                        "_Code": "59", 
                        "_Name": "中戈壁"
                    }, 
                    {
                        "_Code": "61", 
                        "_Name": "东方"
                    }, 
                    {
                        "_Code": "63", 
                        "_Name": "东戈壁"
                    }, 
                    {
                        "_Code": "64", 
                        "_Name": "戈壁苏木贝尔"
                    }, 
                    {
                        "_Code": "65", 
                        "_Name": "戈壁阿尔泰"
                    }, 
                    {
                        "_Code": "67", 
                        "_Name": "布尔干"
                    }, 
                    {
                        "_Code": "69", 
                        "_Name": "巴彦洪格尔"
                    }, 
                    {
                        "_Code": "71", 
                        "_Name": "巴彦乌勒盖"
                    }, 
                    {
                        "_Code": "73", 
                        "_Name": "后杭爱"
                    }, 
                    {
                        "_Code": "UMN", 
                        "_Name": "南戈壁"
                    }, 
                    {
                        "_Code": "UVO", 
                        "_Name": "前杭爱"
                    }
                ]
            }
        }, 
        {
            "_Code": "MNP", 
            "_Name": "北马里亚纳群岛"
        }, 
        {
            "_Code": "MOZ", 
            "_Name": "莫桑比克"
        }, 
        {
            "_Code": "MRT", 
            "_Name": "毛里塔尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AD", 
                        "_Name": "阿德拉尔"
                    }, 
                    {
                        "_Code": "AS", 
                        "_Name": "阿萨巴"
                    }, 
                    {
                        "_Code": "BR", 
                        "_Name": "卜拉克纳"
                    }, 
                    {
                        "_Code": "DN", 
                        "_Name": "努瓦迪布湾"
                    }, 
                    {
                        "_Code": "GM", 
                        "_Name": "吉迪马卡"
                    }, 
                    {
                        "_Code": "GO", 
                        "_Name": "戈尔戈勒"
                    }, 
                    {
                        "_Code": "HC", 
                        "_Name": "西胡德"
                    }, 
                    {
                        "_Code": "HG", 
                        "_Name": "东胡德"
                    }, 
                    {
                        "_Code": "IN", 
                        "_Name": "因希里"
                    }, 
                    {
                        "_Code": "NO", 
                        "_Name": "努瓦克肖特特区"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔甘特"
                    }, 
                    {
                        "_Code": "TR", 
                        "_Name": "特拉扎"
                    }, 
                    {
                        "_Code": "TZ", 
                        "_Name": "提里斯-宰穆尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "MSR", 
            "_Name": "蒙特塞拉特"
        }, 
        {
            "_Code": "MTQ", 
            "_Name": "马提尼克"
        }, 
        {
            "_Code": "MUS", 
            "_Name": "毛里求斯"
        }, 
        {
            "_Code": "MWI", 
            "_Name": "马拉维", 
            "State": {
                "City": [
                    {
                        "_Code": "C", 
                        "_Name": "中央区"
                    }, 
                    {
                        "_Code": "N", 
                        "_Name": "北部区"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "南部区"
                    }
                ]
            }
        }, 
        {
            "_Code": "MYS", 
            "_Name": "马来西亚", 
            "State": [
                {
                    "City": [
                        {
                            "_Code": "BAT", 
                            "_Name": "峇株巴辖"
                        }, 
                        {
                            "_Code": "JHB", 
                            "_Name": "新山"
                        }, 
                        {
                            "_Code": "KLA", 
                            "_Name": "居銮"
                        }, 
                        {
                            "_Code": "KTI", 
                            "_Name": "哥打丁宜"
                        }, 
                        {
                            "_Code": "MEP", 
                            "_Name": "丰盛港"
                        }, 
                        {
                            "_Code": "MUA", 
                            "_Name": "麻坡"
                        }, 
                        {
                            "_Code": "POW", 
                            "_Name": "笨珍"
                        }, 
                        {
                            "_Code": "SGM", 
                            "_Name": "昔加末"
                        }
                    ], 
                    "_Code": "JH", 
                    "_Name": "柔佛"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BLZ", 
                            "_Name": "华玲"
                        }, 
                        {
                            "_Code": "BMA", 
                            "_Name": "万拉峇鲁"
                        }, 
                        {
                            "_Code": "KLM", 
                            "_Name": "居林"
                        }, 
                        {
                            "_Code": "KMU", 
                            "_Name": "瓜拉姆达"
                        }, 
                        {
                            "_Code": "KOR", 
                            "_Name": "哥打士打"
                        }, 
                        {
                            "_Code": "KPA", 
                            "_Name": "古邦巴素"
                        }, 
                        {
                            "_Code": "LGK", 
                            "_Name": "浮罗交怡"
                        }, 
                        {
                            "_Code": "PEN", 
                            "_Name": "笨筒"
                        }, 
                        {
                            "_Code": "PGT", 
                            "_Name": "巴东得腊"
                        }
                    ], 
                    "_Code": "KD", 
                    "_Name": "吉打"
                }, 
                {
                    "City": {
                        "_Code": "KUL", 
                        "_Name": "吉隆坡"
                    }, 
                    "_Code": "KL", 
                    "_Name": "吉隆坡"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BAC", 
                            "_Name": "登卓"
                        }, 
                        {
                            "_Code": "GMU", 
                            "_Name": "话望生"
                        }, 
                        {
                            "_Code": "JEL", 
                            "_Name": "日里"
                        }, 
                        {
                            "_Code": "KBR", 
                            "_Name": "哥打巴鲁"
                        }, 
                        {
                            "_Code": "KUG", 
                            "_Name": "瓜拉吉赖"
                        }, 
                        {
                            "_Code": "MAC", 
                            "_Name": "马樟"
                        }, 
                        {
                            "_Code": "PMA", 
                            "_Name": "巴西马"
                        }, 
                        {
                            "_Code": "PPU", 
                            "_Name": "巴西富地"
                        }, 
                        {
                            "_Code": "TMR", 
                            "_Name": "丹那美拉"
                        }, 
                        {
                            "_Code": "TUM", 
                            "_Name": "道北"
                        }
                    ], 
                    "_Code": "KN", 
                    "_Name": "吉兰丹"
                }, 
                {
                    "City": [
                        {
                            "_Code": "LBU", 
                            "_Name": "纳闽"
                        }, 
                        {
                            "_Code": "VIC", 
                            "_Name": "维多利亚"
                        }
                    ], 
                    "_Code": "LB", 
                    "_Name": "纳闽"
                }, 
                {
                    "City": [
                        {
                            "_Code": "AOG", 
                            "_Name": "亚罗牙也"
                        }, 
                        {
                            "_Code": "JAS", 
                            "_Name": "野新"
                        }, 
                        {
                            "_Code": "MEL", 
                            "_Name": "马六甲市"
                        }
                    ], 
                    "_Code": "ML", 
                    "_Name": "马六甲"
                }, 
                {
                    "City": [
                        {
                            "_Code": "JEL", 
                            "_Name": "日叻务"
                        }, 
                        {
                            "_Code": "JEP", 
                            "_Name": "仁保"
                        }, 
                        {
                            "_Code": "KPI", 
                            "_Name": "瓜拉庇劳"
                        }, 
                        {
                            "_Code": "PDI", 
                            "_Name": "波德申"
                        }, 
                        {
                            "_Code": "REM", 
                            "_Name": "林茂"
                        }, 
                        {
                            "_Code": "SRB", 
                            "_Name": "芙蓉"
                        }, 
                        {
                            "_Code": "TAI", 
                            "_Name": "淡边"
                        }
                    ], 
                    "_Code": "NS", 
                    "_Name": "森美兰"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BEN", 
                            "_Name": "文冬"
                        }, 
                        {
                            "_Code": "BER", 
                            "_Name": "百乐"
                        }, 
                        {
                            "_Code": "CAH", 
                            "_Name": "金马仑高原"
                        }, 
                        {
                            "_Code": "JER", 
                            "_Name": "而连突"
                        }, 
                        {
                            "_Code": "KUA", 
                            "_Name": "关丹"
                        }, 
                        {
                            "_Code": "KUL", 
                            "_Name": "立卑"
                        }, 
                        {
                            "_Code": "MAR", 
                            "_Name": "马兰"
                        }, 
                        {
                            "_Code": "PEK", 
                            "_Name": "北根"
                        }, 
                        {
                            "_Code": "RAU", 
                            "_Name": "劳勿"
                        }, 
                        {
                            "_Code": "TEM", 
                            "_Name": "淡马鲁"
                        }, 
                        {
                            "_Code": "TOM", 
                            "_Name": "云冰"
                        }
                    ], 
                    "_Code": "PG", 
                    "_Name": "彭亨"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BMJ", 
                            "_Name": "大山脚"
                        }, 
                        {
                            "_Code": "BWH", 
                            "_Name": "北海"
                        }, 
                        {
                            "_Code": "NTE", 
                            "_Name": "高渊"
                        }, 
                        {
                            "_Code": "PEN", 
                            "_Name": "槟城"
                        }
                    ], 
                    "_Code": "PH", 
                    "_Name": "槟榔屿"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BGA", 
                            "_Name": "华都牙也"
                        }, 
                        {
                            "_Code": "IPH", 
                            "_Name": "怡保"
                        }, 
                        {
                            "_Code": "KAR", 
                            "_Name": "江沙"
                        }, 
                        {
                            "_Code": "LUM", 
                            "_Name": "紅土坎"
                        }, 
                        {
                            "_Code": "SSP", 
                            "_Name": "和丰"
                        }, 
                        {
                            "_Code": "TAM", 
                            "_Name": "丹绒马"
                        }, 
                        {
                            "_Code": "TAS", 
                            "_Name": "安顺"
                        }, 
                        {
                            "_Code": "TPG", 
                            "_Name": "太平"
                        }
                    ], 
                    "_Code": "PK", 
                    "_Name": "霹雳"
                }, 
                {
                    "City": {
                        "_Code": "KGR", 
                        "_Name": "加央"
                    }, 
                    "_Code": "PS", 
                    "_Name": "玻璃市"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BEF", 
                            "_Name": "保佛"
                        }, 
                        {
                            "_Code": "BEL", 
                            "_Name": "比鲁兰"
                        }, 
                        {
                            "_Code": "BKI", 
                            "_Name": "哥打基纳巴鲁"
                        }, 
                        {
                            "_Code": "KBD", 
                            "_Name": "古打毛律"
                        }, 
                        {
                            "_Code": "KBT", 
                            "_Name": "京那巴登岸"
                        }, 
                        {
                            "_Code": "KEG", 
                            "_Name": "根地咬"
                        }, 
                        {
                            "_Code": "KMU", 
                            "_Name": "哥打马鲁都"
                        }, 
                        {
                            "_Code": "KPU", 
                            "_Name": "瓜拉班尤"
                        }, 
                        {
                            "_Code": "KUD", 
                            "_Name": "古达"
                        }, 
                        {
                            "_Code": "KUN", 
                            "_Name": "古纳"
                        }, 
                        {
                            "_Code": "LDU", 
                            "_Name": "拿笃"
                        }, 
                        {
                            "_Code": "NAB", 
                            "_Name": "纳巴湾"
                        }, 
                        {
                            "_Code": "PAP", 
                            "_Name": "吧巴"
                        }, 
                        {
                            "_Code": "PIT", 
                            "_Name": "必达士"
                        }, 
                        {
                            "_Code": "PMP", 
                            "_Name": "兵南邦"
                        }, 
                        {
                            "_Code": "RNU", 
                            "_Name": "兰脑"
                        }, 
                        {
                            "_Code": "SDK", 
                            "_Name": "山打根"
                        }, 
                        {
                            "_Code": "SMM", 
                            "_Name": "仙本那"
                        }, 
                        {
                            "_Code": "SPT", 
                            "_Name": "西比陶"
                        }, 
                        {
                            "_Code": "TAB", 
                            "_Name": "担布南"
                        }, 
                        {
                            "_Code": "TAW", 
                            "_Name": "斗湖"
                        }, 
                        {
                            "_Code": "TEN", 
                            "_Name": "丹南"
                        }, 
                        {
                            "_Code": "TUR", 
                            "_Name": "斗亚兰"
                        }
                    ], 
                    "_Code": "SB", 
                    "_Name": "沙巴"
                }, 
                {
                    "City": [
                        {
                            "_Code": "GOM", 
                            "_Name": "鹅麦"
                        }, 
                        {
                            "_Code": "HUL", 
                            "_Name": "乌鲁冷岳"
                        }, 
                        {
                            "_Code": "HUS", 
                            "_Name": "乌鲁雪兰莪"
                        }, 
                        {
                            "_Code": "KLG", 
                            "_Name": "瓜拉冷岳"
                        }, 
                        {
                            "_Code": "KSL", 
                            "_Name": "瓜拉雪兰莪"
                        }, 
                        {
                            "_Code": "PJA", 
                            "_Name": "八打灵"
                        }, 
                        {
                            "_Code": "SBM", 
                            "_Name": "沙白安南"
                        }, 
                        {
                            "_Code": "SEP", 
                            "_Name": "雪邦"
                        }
                    ], 
                    "_Code": "SL", 
                    "_Name": "雪兰莪"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BTG", 
                            "_Name": "木中"
                        }, 
                        {
                            "_Code": "BTU", 
                            "_Name": "民都鲁"
                        }, 
                        {
                            "_Code": "KCH", 
                            "_Name": "古晋"
                        }, 
                        {
                            "_Code": "KPI", 
                            "_Name": "加帛"
                        }, 
                        {
                            "_Code": "LMN", 
                            "_Name": "林梦"
                        }, 
                        {
                            "_Code": "MKM", 
                            "_Name": "木胶"
                        }, 
                        {
                            "_Code": "MYY", 
                            "_Name": "美里"
                        }, 
                        {
                            "_Code": "SAM", 
                            "_Name": "斯里阿曼"
                        }, 
                        {
                            "_Code": "SAR", 
                            "_Name": "泗里街"
                        }, 
                        {
                            "_Code": "SBW", 
                            "_Name": "泗务"
                        }, 
                        {
                            "_Code": "SMH", 
                            "_Name": "三马拉汉"
                        }
                    ], 
                    "_Code": "SR", 
                    "_Name": "沙捞越"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BES", 
                            "_Name": "勿述"
                        }, 
                        {
                            "_Code": "DGN", 
                            "_Name": "龙运"
                        }, 
                        {
                            "_Code": "HUL", 
                            "_Name": "乌鲁"
                        }, 
                        {
                            "_Code": "KEM", 
                            "_Name": "甘马挽"
                        }, 
                        {
                            "_Code": "MAR", 
                            "_Name": "马江"
                        }, 
                        {
                            "_Code": "SET", 
                            "_Name": "实兆"
                        }, 
                        {
                            "_Code": "TGG", 
                            "_Name": "瓜拉丁加奴"
                        }
                    ], 
                    "_Code": "TR", 
                    "_Name": "丁加奴"
                }
            ]
        }, 
        {
            "_Code": "MYT", 
            "_Name": "马约特岛"
        }, 
        {
            "_Code": "NAM", 
            "_Name": "纳米比亚", 
            "State": {
                "City": [
                    {
                        "_Code": "CA", 
                        "_Name": "卡普里维"
                    }, 
                    {
                        "_Code": "ER", 
                        "_Name": "埃龙戈"
                    }, 
                    {
                        "_Code": "HA", 
                        "_Name": "哈达普"
                    }, 
                    {
                        "_Code": "KH", 
                        "_Name": "霍马斯"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "卡拉斯"
                    }, 
                    {
                        "_Code": "KU", 
                        "_Name": "库内内"
                    }, 
                    {
                        "_Code": "KV", 
                        "_Name": "奥卡万戈"
                    }, 
                    {
                        "_Code": "OJ", 
                        "_Name": "奥乔宗蒂约巴"
                    }, 
                    {
                        "_Code": "OK", 
                        "_Name": "奥马赫科"
                    }, 
                    {
                        "_Code": "ON", 
                        "_Name": "奥沙纳"
                    }, 
                    {
                        "_Code": "OO", 
                        "_Name": "奥希科托"
                    }, 
                    {
                        "_Code": "OT", 
                        "_Name": "奥姆沙蒂"
                    }, 
                    {
                        "_Code": "OW", 
                        "_Name": "奥汉圭纳"
                    }
                ]
            }
        }, 
        {
            "_Code": "NCL", 
            "_Name": "新喀里多尼亚"
        }, 
        {
            "_Code": "NER", 
            "_Name": "尼日尔", 
            "State": {
                "City": [
                    {
                        "_Code": "AJY", 
                        "_Name": "阿加德兹"
                    }, 
                    {
                        "_Code": "DIF", 
                        "_Name": "迪法"
                    }, 
                    {
                        "_Code": "DSS", 
                        "_Name": "多索"
                    }, 
                    {
                        "_Code": "MFQ", 
                        "_Name": "马拉迪"
                    }, 
                    {
                        "_Code": "NIM", 
                        "_Name": "尼亚美市"
                    }, 
                    {
                        "_Code": "THZ", 
                        "_Name": "塔瓦"
                    }, 
                    {
                        "_Code": "TIL", 
                        "_Name": "蒂拉贝里"
                    }, 
                    {
                        "_Code": "ZND", 
                        "_Name": "津德尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "NFK", 
            "_Name": "诺福克"
        }, 
        {
            "_Code": "NGA", 
            "_Name": "尼日利亚", 
            "State": {
                "City": [
                    {
                        "_Code": "ABV", 
                        "_Name": "阿比亚"
                    }, 
                    {
                        "_Code": "IBA", 
                        "_Name": "伊巴丹"
                    }, 
                    {
                        "_Code": "KAN", 
                        "_Name": "卡诺"
                    }, 
                    {
                        "_Code": "LOS", 
                        "_Name": "拉各斯"
                    }, 
                    {
                        "_Code": "OGB", 
                        "_Name": "奥博莫绍"
                    }
                ]
            }
        }, 
        {
            "_Code": "NIC", 
            "_Name": "尼加拉瓜", 
            "State": {
                "City": [
                    {
                        "_Code": "AN", 
                        "_Name": "北大西洋"
                    }, 
                    {
                        "_Code": "AS", 
                        "_Name": "南大西洋"
                    }, 
                    {
                        "_Code": "BO", 
                        "_Name": "博阿科"
                    }, 
                    {
                        "_Code": "CA", 
                        "_Name": "卡拉索"
                    }, 
                    {
                        "_Code": "CD", 
                        "_Name": "奇南德加"
                    }, 
                    {
                        "_Code": "CT", 
                        "_Name": "琼塔莱斯"
                    }, 
                    {
                        "_Code": "ES", 
                        "_Name": "埃斯特利"
                    }, 
                    {
                        "_Code": "GR", 
                        "_Name": "格拉纳达"
                    }, 
                    {
                        "_Code": "JI", 
                        "_Name": "希诺特加"
                    }, 
                    {
                        "_Code": "LE", 
                        "_Name": "莱昂"
                    }, 
                    {
                        "_Code": "MD", 
                        "_Name": "马德里斯"
                    }, 
                    {
                        "_Code": "MN", 
                        "_Name": "马那瓜"
                    }, 
                    {
                        "_Code": "MS", 
                        "_Name": "马萨亚"
                    }, 
                    {
                        "_Code": "MT", 
                        "_Name": "马塔加尔帕"
                    }, 
                    {
                        "_Code": "NS", 
                        "_Name": "新塞哥维亚"
                    }, 
                    {
                        "_Code": "RV", 
                        "_Name": "里瓦斯"
                    }, 
                    {
                        "_Code": "SJ", 
                        "_Name": "圣胡安河"
                    }
                ]
            }
        }, 
        {
            "_Code": "NIU", 
            "_Name": "纽埃"
        }, 
        {
            "_Code": "NLD", 
            "_Name": "荷兰", 
            "State": {
                "City": [
                    {
                        "_Code": "AER", 
                        "_Name": "阿尔梅勒"
                    }, 
                    {
                        "_Code": "AME", 
                        "_Name": "阿默斯福特"
                    }, 
                    {
                        "_Code": "AMS", 
                        "_Name": "阿姆斯特丹"
                    }, 
                    {
                        "_Code": "APE", 
                        "_Name": "阿珀尔多伦"
                    }, 
                    {
                        "_Code": "ARN", 
                        "_Name": "阿纳姆"
                    }, 
                    {
                        "_Code": "ASS", 
                        "_Name": "阿森"
                    }, 
                    {
                        "_Code": "BRD", 
                        "_Name": "布雷达"
                    }, 
                    {
                        "_Code": "DOR", 
                        "_Name": "多德雷赫特"
                    }, 
                    {
                        "_Code": "EDE", 
                        "_Name": "埃德"
                    }, 
                    {
                        "_Code": "EIN", 
                        "_Name": "埃因霍芬"
                    }, 
                    {
                        "_Code": "EMM", 
                        "_Name": "埃门"
                    }, 
                    {
                        "_Code": "ENS", 
                        "_Name": "恩斯赫德"
                    }, 
                    {
                        "_Code": "GRQ", 
                        "_Name": "格罗宁根"
                    }, 
                    {
                        "_Code": "HAG", 
                        "_Name": "海牙"
                    }, 
                    {
                        "_Code": "HFD", 
                        "_Name": "霍夫多尔普"
                    }, 
                    {
                        "_Code": "HRA", 
                        "_Name": "哈勒姆"
                    }, 
                    {
                        "_Code": "HTB", 
                        "_Name": "斯海尔托亨博思"
                    }, 
                    {
                        "_Code": "LEY", 
                        "_Name": "莱利斯塔德"
                    }, 
                    {
                        "_Code": "LID", 
                        "_Name": "莱顿"
                    }, 
                    {
                        "_Code": "LWR", 
                        "_Name": "吕伐登"
                    }, 
                    {
                        "_Code": "MDL", 
                        "_Name": "米德尔堡"
                    }, 
                    {
                        "_Code": "MST", 
                        "_Name": "马斯特里赫特"
                    }, 
                    {
                        "_Code": "NIJ", 
                        "_Name": "奈梅亨"
                    }, 
                    {
                        "_Code": "RTM", 
                        "_Name": "鹿特丹"
                    }, 
                    {
                        "_Code": "TLB", 
                        "_Name": "蒂尔堡"
                    }, 
                    {
                        "_Code": "UTC", 
                        "_Name": "乌得勒支"
                    }, 
                    {
                        "_Code": "ZTM", 
                        "_Name": "佐特尔梅"
                    }, 
                    {
                        "_Code": "ZWO", 
                        "_Name": "兹沃勒"
                    }
                ]
            }
        }, 
        {
            "_Code": "NOR", 
            "_Name": "挪威", 
            "State": {
                "City": [
                    {
                        "_Code": "1", 
                        "_Name": "东福尔"
                    }, 
                    {
                        "_Code": "10", 
                        "_Name": "西阿格德尔"
                    }, 
                    {
                        "_Code": "11", 
                        "_Name": "罗加兰"
                    }, 
                    {
                        "_Code": "12", 
                        "_Name": "霍达兰"
                    }, 
                    {
                        "_Code": "14", 
                        "_Name": "松恩－菲尤拉讷"
                    }, 
                    {
                        "_Code": "15", 
                        "_Name": "默勒－鲁姆斯达尔"
                    }, 
                    {
                        "_Code": "16", 
                        "_Name": "南特伦德拉格"
                    }, 
                    {
                        "_Code": "17", 
                        "_Name": "北特伦德拉格"
                    }, 
                    {
                        "_Code": "18", 
                        "_Name": "诺尔兰"
                    }, 
                    {
                        "_Code": "19", 
                        "_Name": "特罗姆斯"
                    }, 
                    {
                        "_Code": "2", 
                        "_Name": "阿克什胡斯"
                    }, 
                    {
                        "_Code": "20", 
                        "_Name": "芬马克"
                    }, 
                    {
                        "_Code": "3", 
                        "_Name": "奥斯陆市"
                    }, 
                    {
                        "_Code": "4", 
                        "_Name": "海德马克"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "奥普兰"
                    }, 
                    {
                        "_Code": "6", 
                        "_Name": "布斯克吕"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "西福尔"
                    }, 
                    {
                        "_Code": "8", 
                        "_Name": "泰勒马克"
                    }, 
                    {
                        "_Code": "9", 
                        "_Name": "东阿格德尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "NPL", 
            "_Name": "尼泊尔", 
            "State": {
                "City": [
                    {
                        "_Code": "BA", 
                        "_Name": "巴格马蒂"
                    }, 
                    {
                        "_Code": "BH", 
                        "_Name": "佩里"
                    }, 
                    {
                        "_Code": "DH", 
                        "_Name": "道拉吉里"
                    }, 
                    {
                        "_Code": "GA", 
                        "_Name": "甘达基"
                    }, 
                    {
                        "_Code": "JA", 
                        "_Name": "贾纳克布尔"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "格尔纳利"
                    }, 
                    {
                        "_Code": "KO", 
                        "_Name": "戈西"
                    }, 
                    {
                        "_Code": "LU", 
                        "_Name": "蓝毗尼"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马哈卡利"
                    }, 
                    {
                        "_Code": "ME", 
                        "_Name": "梅吉"
                    }, 
                    {
                        "_Code": "NA", 
                        "_Name": "纳拉亚尼"
                    }, 
                    {
                        "_Code": "RA", 
                        "_Name": "拉布蒂"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "萨加玛塔"
                    }, 
                    {
                        "_Code": "SE", 
                        "_Name": "塞蒂"
                    }
                ]
            }
        }, 
        {
            "_Code": "NRU", 
            "_Name": "瑙鲁"
        }, 
        {
            "_Code": "NZL", 
            "_Name": "新西兰", 
            "State": {
                "City": [
                    {
                        "_Code": "AUK", 
                        "_Name": "奥克兰"
                    }, 
                    {
                        "_Code": "BHE", 
                        "_Name": "布莱尼姆"
                    }, 
                    {
                        "_Code": "CHC", 
                        "_Name": "克赖斯特彻奇"
                    }, 
                    {
                        "_Code": "DUD", 
                        "_Name": "达尼丁"
                    }, 
                    {
                        "_Code": "FNR", 
                        "_Name": "北远"
                    }, 
                    {
                        "_Code": "GIS", 
                        "_Name": "吉斯伯恩"
                    }, 
                    {
                        "_Code": "GMN", 
                        "_Name": "格雷茅斯"
                    }, 
                    {
                        "_Code": "HAS", 
                        "_Name": "黑斯廷斯"
                    }, 
                    {
                        "_Code": "HLZ", 
                        "_Name": "哈密尔顿"
                    }, 
                    {
                        "_Code": "IVC", 
                        "_Name": "因弗卡吉尔"
                    }, 
                    {
                        "_Code": "KAI", 
                        "_Name": "凯帕拉"
                    }, 
                    {
                        "_Code": "MNK", 
                        "_Name": "马努考"
                    }, 
                    {
                        "_Code": "NPE", 
                        "_Name": "内皮尔"
                    }, 
                    {
                        "_Code": "NPL", 
                        "_Name": "新普利茅斯"
                    }, 
                    {
                        "_Code": "NSH", 
                        "_Name": "北岸"
                    }, 
                    {
                        "_Code": "NSN", 
                        "_Name": "纳尔逊"
                    }, 
                    {
                        "_Code": "PMR", 
                        "_Name": "北帕默斯顿"
                    }, 
                    {
                        "_Code": "RMD", 
                        "_Name": "里士满"
                    }, 
                    {
                        "_Code": "STR", 
                        "_Name": "斯特拉特福德"
                    }, 
                    {
                        "_Code": "TAU", 
                        "_Name": "陶马鲁努伊"
                    }, 
                    {
                        "_Code": "WAE", 
                        "_Name": "怀塔科拉"
                    }, 
                    {
                        "_Code": "WAG", 
                        "_Name": "旺格努伊"
                    }, 
                    {
                        "_Code": "WHK", 
                        "_Name": "瓦卡塔尼"
                    }, 
                    {
                        "_Code": "WRE", 
                        "_Name": "旺阿雷"
                    }
                ]
            }
        }, 
        {
            "_Code": "OMN", 
            "_Name": "阿曼", 
            "State": {
                "City": [
                    {
                        "_Code": "BA", 
                        "_Name": "巴提奈地区"
                    }, 
                    {
                        "_Code": "DA", 
                        "_Name": "内地地区"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马斯喀特省"
                    }, 
                    {
                        "_Code": "MU", 
                        "_Name": "穆桑达姆省"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "东部地区"
                    }, 
                    {
                        "_Code": "WU", 
                        "_Name": "中部地区"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "达希莱地区"
                    }, 
                    {
                        "_Code": "ZU", 
                        "_Name": "佐法尔省"
                    }
                ]
            }
        }, 
        {
            "_Code": "PAK", 
            "_Name": "巴基斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "GUJ", 
                        "_Name": "故吉软瓦拉"
                    }, 
                    {
                        "_Code": "HDD", 
                        "_Name": "海德拉巴"
                    }, 
                    {
                        "_Code": "ISB", 
                        "_Name": "伊斯兰堡"
                    }, 
                    {
                        "_Code": "KCT", 
                        "_Name": "卡拉奇"
                    }, 
                    {
                        "_Code": "LHE", 
                        "_Name": "拉合尔"
                    }, 
                    {
                        "_Code": "LYP", 
                        "_Name": "费萨拉巴德"
                    }, 
                    {
                        "_Code": "MUX", 
                        "_Name": "木尔坦"
                    }, 
                    {
                        "_Code": "PEW", 
                        "_Name": "白沙瓦"
                    }, 
                    {
                        "_Code": "RWP", 
                        "_Name": "拉瓦尔品第"
                    }
                ]
            }
        }, 
        {
            "_Code": "PAN", 
            "_Name": "巴拿马"
        }, 
        {
            "_Code": "PCN", 
            "_Name": "皮特凯恩"
        }, 
        {
            "_Code": "PER", 
            "_Name": "秘鲁", 
            "State": {
                "City": [
                    {
                        "_Code": "AM", 
                        "_Name": "亚马孙"
                    }, 
                    {
                        "_Code": "AN", 
                        "_Name": "安卡什"
                    }, 
                    {
                        "_Code": "AP", 
                        "_Name": "阿普里马克"
                    }, 
                    {
                        "_Code": "AR", 
                        "_Name": "阿雷基帕"
                    }, 
                    {
                        "_Code": "AY", 
                        "_Name": "阿亚库乔"
                    }, 
                    {
                        "_Code": "CHI", 
                        "_Name": "钦查阿尔塔"
                    }, 
                    {
                        "_Code": "CHM", 
                        "_Name": "钦博特"
                    }, 
                    {
                        "_Code": "CJ", 
                        "_Name": "卡哈马卡"
                    }, 
                    {
                        "_Code": "CL", 
                        "_Name": "卡亚俄"
                    }, 
                    {
                        "_Code": "CU", 
                        "_Name": "库斯科"
                    }, 
                    {
                        "_Code": "HO", 
                        "_Name": "瓦努科"
                    }, 
                    {
                        "_Code": "HV", 
                        "_Name": "万卡维利卡"
                    }, 
                    {
                        "_Code": "IC", 
                        "_Name": "伊卡"
                    }, 
                    {
                        "_Code": "JU", 
                        "_Name": "胡宁"
                    }, 
                    {
                        "_Code": "JUL", 
                        "_Name": "胡利亚卡"
                    }, 
                    {
                        "_Code": "LD", 
                        "_Name": "拉利伯塔德"
                    }, 
                    {
                        "_Code": "LI", 
                        "_Name": "利马"
                    }, 
                    {
                        "_Code": "LO", 
                        "_Name": "洛雷托"
                    }, 
                    {
                        "_Code": "LY", 
                        "_Name": "兰巴耶克"
                    }, 
                    {
                        "_Code": "MD", 
                        "_Name": "马德雷德迪奥斯"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "莫克瓜"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "帕斯科"
                    }, 
                    {
                        "_Code": "PI", 
                        "_Name": "皮乌拉"
                    }, 
                    {
                        "_Code": "PU", 
                        "_Name": "普诺"
                    }, 
                    {
                        "_Code": "SM", 
                        "_Name": "圣马丁"
                    }, 
                    {
                        "_Code": "SUL", 
                        "_Name": "苏拉纳"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔克纳"
                    }, 
                    {
                        "_Code": "TU", 
                        "_Name": "通贝斯"
                    }, 
                    {
                        "_Code": "UC", 
                        "_Name": "乌卡亚利"
                    }
                ]
            }
        }, 
        {
            "_Code": "PHL", 
            "_Name": "菲律宾", 
            "State": {
                "City": [
                    {
                        "_Code": "CAO", 
                        "_Name": "卡卢坎"
                    }, 
                    {
                        "_Code": "CEB", 
                        "_Name": "宿务"
                    }, 
                    {
                        "_Code": "DOR", 
                        "_Name": "达沃"
                    }, 
                    {
                        "_Code": "MNL", 
                        "_Name": "马尼拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "PLW", 
            "_Name": "帕劳群岛"
        }, 
        {
            "_Code": "PNG", 
            "_Name": "巴布亚新几内亚", 
            "State": {
                "City": [
                    {
                        "_Code": "BV", 
                        "_Name": "布干维尔"
                    }, 
                    {
                        "_Code": "EB", 
                        "_Name": "东新不列颠"
                    }, 
                    {
                        "_Code": "EH", 
                        "_Name": "东部高地"
                    }, 
                    {
                        "_Code": "EN", 
                        "_Name": "恩加"
                    }, 
                    {
                        "_Code": "ES", 
                        "_Name": "东塞皮克"
                    }, 
                    {
                        "_Code": "GU", 
                        "_Name": "海湾"
                    }, 
                    {
                        "_Code": "MB", 
                        "_Name": "米尔恩湾"
                    }, 
                    {
                        "_Code": "MD", 
                        "_Name": "马当"
                    }, 
                    {
                        "_Code": "MN", 
                        "_Name": "马努斯"
                    }, 
                    {
                        "_Code": "MR", 
                        "_Name": "莫罗贝"
                    }, 
                    {
                        "_Code": "NC", 
                        "_Name": "莫尔兹比港"
                    }, 
                    {
                        "_Code": "NI", 
                        "_Name": "新爱尔兰"
                    }, 
                    {
                        "_Code": "NO", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "桑道恩"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "南部高地"
                    }, 
                    {
                        "_Code": "SI", 
                        "_Name": "钦布"
                    }, 
                    {
                        "_Code": "WB", 
                        "_Name": "西新不列颠"
                    }, 
                    {
                        "_Code": "WE", 
                        "_Name": "西部"
                    }, 
                    {
                        "_Code": "WH", 
                        "_Name": "西部高地"
                    }
                ]
            }
        }, 
        {
            "_Code": "POL", 
            "_Name": "波兰", 
            "State": {
                "City": [
                    {
                        "_Code": "BAP", 
                        "_Name": "比亚瓦波德拉斯卡"
                    }, 
                    {
                        "_Code": "BIA", 
                        "_Name": "比亚维斯托克"
                    }, 
                    {
                        "_Code": "BYT", 
                        "_Name": "比托姆"
                    }, 
                    {
                        "_Code": "BZG", 
                        "_Name": "比得哥什"
                    }, 
                    {
                        "_Code": "CHO", 
                        "_Name": "海乌姆"
                    }, 
                    {
                        "_Code": "CHZ", 
                        "_Name": "霍茹夫"
                    }, 
                    {
                        "_Code": "CIE", 
                        "_Name": "切哈努夫"
                    }, 
                    {
                        "_Code": "DAB", 
                        "_Name": "达布罗瓦戈尼察"
                    }, 
                    {
                        "_Code": "ELB", 
                        "_Name": "埃尔布隆格"
                    }, 
                    {
                        "_Code": "GDN", 
                        "_Name": "格但斯克"
                    }, 
                    {
                        "_Code": "GDY", 
                        "_Name": "格丁尼亚"
                    }, 
                    {
                        "_Code": "GOW", 
                        "_Name": "大波兰地区戈茹夫"
                    }, 
                    {
                        "_Code": "GRU", 
                        "_Name": "格鲁琼兹"
                    }, 
                    {
                        "_Code": "GWC", 
                        "_Name": "格利维采"
                    }, 
                    {
                        "_Code": "IEG", 
                        "_Name": "绿山城"
                    }, 
                    {
                        "_Code": "JAW", 
                        "_Name": "雅沃兹诺"
                    }, 
                    {
                        "_Code": "JEG", 
                        "_Name": "耶莱尼亚古拉"
                    }, 
                    {
                        "_Code": "KAL", 
                        "_Name": "卡利什"
                    }, 
                    {
                        "_Code": "KLC", 
                        "_Name": "凯尔采"
                    }, 
                    {
                        "_Code": "KON", 
                        "_Name": "科宁"
                    }, 
                    {
                        "_Code": "KRK", 
                        "_Name": "克拉科夫"
                    }, 
                    {
                        "_Code": "KRO", 
                        "_Name": "克罗斯诺"
                    }, 
                    {
                        "_Code": "KTW", 
                        "_Name": "卡托维兹"
                    }, 
                    {
                        "_Code": "QEP", 
                        "_Name": "塔尔诺布热格"
                    }, 
                    {
                        "_Code": "QOY", 
                        "_Name": "沃姆扎"
                    }, 
                    {
                        "_Code": "LEG", 
                        "_Name": "莱格尼察"
                    }, 
                    {
                        "_Code": "LEZ", 
                        "_Name": "莱什诺"
                    }, 
                    {
                        "_Code": "LOD", 
                        "_Name": "罗兹"
                    }, 
                    {
                        "_Code": "LUL", 
                        "_Name": "卢布林"
                    }, 
                    {
                        "_Code": "MYL", 
                        "_Name": "米什洛维采"
                    }, 
                    {
                        "_Code": "NOW", 
                        "_Name": "新松奇"
                    }, 
                    {
                        "_Code": "OLS", 
                        "_Name": "奥尔什丁"
                    }, 
                    {
                        "_Code": "OPO", 
                        "_Name": "波莱"
                    }, 
                    {
                        "_Code": "OSS", 
                        "_Name": "奥斯特罗文卡"
                    }, 
                    {
                        "_Code": "OSZ", 
                        "_Name": "科沙林"
                    }, 
                    {
                        "_Code": "PIL", 
                        "_Name": "皮瓦"
                    }, 
                    {
                        "_Code": "PIO", 
                        "_Name": "彼得库夫"
                    }, 
                    {
                        "_Code": "PLO", 
                        "_Name": "普沃茨克"
                    }, 
                    {
                        "_Code": "POZ", 
                        "_Name": "波兹南"
                    }, 
                    {
                        "_Code": "PRZ", 
                        "_Name": "普热梅希尔"
                    }, 
                    {
                        "_Code": "RDM", 
                        "_Name": "拉多姆"
                    }, 
                    {
                        "_Code": "RDS", 
                        "_Name": "鲁达"
                    }, 
                    {
                        "_Code": "RZE", 
                        "_Name": "热舒夫"
                    }, 
                    {
                        "_Code": "SDC", 
                        "_Name": "谢德尔采"
                    }, 
                    {
                        "_Code": "SIR", 
                        "_Name": "谢拉兹"
                    }, 
                    {
                        "_Code": "SKI", 
                        "_Name": "斯凯尔涅维采"
                    }, 
                    {
                        "_Code": "SLP", 
                        "_Name": "斯武普斯克"
                    }, 
                    {
                        "_Code": "SOP", 
                        "_Name": "索波特"
                    }, 
                    {
                        "_Code": "SOW", 
                        "_Name": "希米亚诺维采"
                    }, 
                    {
                        "_Code": "SWC", 
                        "_Name": "索斯诺维茨"
                    }, 
                    {
                        "_Code": "SWI", 
                        "_Name": "希维诺乌伊希切"
                    }, 
                    {
                        "_Code": "SWL", 
                        "_Name": "苏瓦乌基"
                    }, 
                    {
                        "_Code": "SWT", 
                        "_Name": "希维托赫洛维采"
                    }, 
                    {
                        "_Code": "SZZ", 
                        "_Name": "什切青"
                    }, 
                    {
                        "_Code": "TAR", 
                        "_Name": "塔尔努夫"
                    }, 
                    {
                        "_Code": "TOR", 
                        "_Name": "托伦"
                    }, 
                    {
                        "_Code": "TYY", 
                        "_Name": "特切"
                    }, 
                    {
                        "_Code": "WAW", 
                        "_Name": "华沙"
                    }, 
                    {
                        "_Code": "WLO", 
                        "_Name": "弗沃茨瓦韦克"
                    }, 
                    {
                        "_Code": "WRO", 
                        "_Name": "弗罗茨瓦夫"
                    }, 
                    {
                        "_Code": "WZH", 
                        "_Name": "瓦乌布日赫"
                    }, 
                    {
                        "_Code": "ZAB", 
                        "_Name": "扎布热"
                    }, 
                    {
                        "_Code": "ZAM", 
                        "_Name": "扎莫希奇"
                    }
                ]
            }
        }, 
        {
            "_Code": "PRI", 
            "_Name": "波多黎各"
        }, 
        {
            "_Code": "PRK", 
            "_Name": "朝鲜", 
            "State": {
                "City": [
                    {
                        "_Code": "CHO", 
                        "_Name": "清津"
                    }, 
                    {
                        "_Code": "FNJ", 
                        "_Name": "平壤"
                    }, 
                    {
                        "_Code": "HAE", 
                        "_Name": "海州"
                    }, 
                    {
                        "_Code": "HAM", 
                        "_Name": "咸兴"
                    }, 
                    {
                        "_Code": "HYE", 
                        "_Name": "惠山"
                    }, 
                    {
                        "_Code": "KAN", 
                        "_Name": "江界"
                    }, 
                    {
                        "_Code": "KSN", 
                        "_Name": "开城"
                    }, 
                    {
                        "_Code": "NAM", 
                        "_Name": "南浦"
                    }, 
                    {
                        "_Code": "NAS", 
                        "_Name": "罗先"
                    }, 
                    {
                        "_Code": "SAR", 
                        "_Name": "沙里院"
                    }, 
                    {
                        "_Code": "SII", 
                        "_Name": "新义州"
                    }, 
                    {
                        "_Code": "WON", 
                        "_Name": "元山"
                    }
                ]
            }
        }, 
        {
            "_Code": "PRT", 
            "_Name": "葡萄牙", 
            "State": {
                "City": [
                    {
                        "_Code": "AAT", 
                        "_Name": "上阿连特茹"
                    }, 
                    {
                        "_Code": "AES", 
                        "_Name": "万福"
                    }, 
                    {
                        "_Code": "ALC", 
                        "_Name": "中阿连特茹"
                    }, 
                    {
                        "_Code": "ALL", 
                        "_Name": "滨海阿连特茹"
                    }, 
                    {
                        "_Code": "ATM", 
                        "_Name": "上特拉斯山"
                    }, 
                    {
                        "_Code": "BAL", 
                        "_Name": "下阿连特茹"
                    }, 
                    {
                        "_Code": "BIN", 
                        "_Name": "内贝拉北"
                    }, 
                    {
                        "_Code": "BIS", 
                        "_Name": "内贝拉南"
                    }, 
                    {
                        "_Code": "BMO", 
                        "_Name": "下蒙德古"
                    }, 
                    {
                        "_Code": "BVO", 
                        "_Name": "下伏日"
                    }, 
                    {
                        "_Code": "CAV", 
                        "_Name": "卡瓦多"
                    }, 
                    {
                        "_Code": "CLB", 
                        "_Name": "科瓦贝拉"
                    }, 
                    {
                        "_Code": "EDV", 
                        "_Name": "恩特拉杜罗伏日"
                    }, 
                    {
                        "_Code": "FAO", 
                        "_Name": "法鲁"
                    }, 
                    {
                        "_Code": "FUN", 
                        "_Name": "丰沙尔"
                    }, 
                    {
                        "_Code": "LIS", 
                        "_Name": "里斯本"
                    }, 
                    {
                        "_Code": "LTE", 
                        "_Name": "利巴特茹"
                    }, 
                    {
                        "_Code": "MDR", 
                        "_Name": "杜罗"
                    }, 
                    {
                        "_Code": "MLI", 
                        "_Name": "米尼奥-利马"
                    }, 
                    {
                        "_Code": "MTE", 
                        "_Name": "梅地奥特茹"
                    }, 
                    {
                        "_Code": "OES", 
                        "_Name": "西部"
                    }, 
                    {
                        "_Code": "PDL", 
                        "_Name": "蓬塔德尔加达"
                    }, 
                    {
                        "_Code": "PIN", 
                        "_Name": "内皮尼亚尔北"
                    }, 
                    {
                        "_Code": "PIS", 
                        "_Name": "内皮尼亚尔南"
                    }, 
                    {
                        "_Code": "PLT", 
                        "_Name": "滨海皮尼亚尔"
                    }, 
                    {
                        "_Code": "PSE", 
                        "_Name": "塞图巴尔半岛"
                    }, 
                    {
                        "_Code": "SES", 
                        "_Name": "山后"
                    }, 
                    {
                        "_Code": "TAM", 
                        "_Name": "塔梅加"
                    }, 
                    {
                        "_Code": "VDP", 
                        "_Name": "波尔图"
                    }
                ]
            }
        }, 
        {
            "_Code": "PRY", 
            "_Name": "巴拉圭", 
            "State": {
                "City": [
                    {
                        "_Code": "AG", 
                        "_Name": "上巴拉圭"
                    }, 
                    {
                        "_Code": "AM", 
                        "_Name": "阿曼拜"
                    }, 
                    {
                        "_Code": "AN", 
                        "_Name": "上巴拉那"
                    }, 
                    {
                        "_Code": "AS", 
                        "_Name": "亚松森特别区"
                    }, 
                    {
                        "_Code": "BO", 
                        "_Name": "博克龙"
                    }, 
                    {
                        "_Code": "CC", 
                        "_Name": "康塞普西翁"
                    }, 
                    {
                        "_Code": "CD", 
                        "_Name": "科迪勒拉"
                    }, 
                    {
                        "_Code": "CE", 
                        "_Name": "中央"
                    }, 
                    {
                        "_Code": "CG", 
                        "_Name": "卡瓜苏"
                    }, 
                    {
                        "_Code": "CN", 
                        "_Name": "卡嫩迪尤"
                    }, 
                    {
                        "_Code": "CZ", 
                        "_Name": "卡萨帕"
                    }, 
                    {
                        "_Code": "GU", 
                        "_Name": "瓜伊拉"
                    }, 
                    {
                        "_Code": "IT", 
                        "_Name": "伊塔普亚"
                    }, 
                    {
                        "_Code": "MI", 
                        "_Name": "米西奥内斯"
                    }, 
                    {
                        "_Code": "NE", 
                        "_Name": "涅恩布库"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "巴拉瓜里"
                    }, 
                    {
                        "_Code": "PH", 
                        "_Name": "阿耶斯总统省"
                    }, 
                    {
                        "_Code": "SP", 
                        "_Name": "圣佩德罗"
                    }
                ]
            }
        }, 
        {
            "_Code": "PSE", 
            "_Name": "巴勒斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "GZ", 
                        "_Name": "加沙地带"
                    }, 
                    {
                        "_Code": "WE", 
                        "_Name": "西岸"
                    }
                ]
            }
        }, 
        {
            "_Code": "PYF", 
            "_Name": "法属波利尼西亚"
        }, 
        {
            "_Code": "REU", 
            "_Name": "留尼旺岛"
        }, 
        {
            "_Code": "ROU", 
            "_Name": "罗马尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AD", 
                        "_Name": "亚厉山德里亚"
                    }, 
                    {
                        "_Code": "AL", 
                        "_Name": "阿尔巴尤利亚"
                    }, 
                    {
                        "_Code": "AR", 
                        "_Name": "阿拉德"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "巴克乌"
                    }, 
                    {
                        "_Code": "BC", 
                        "_Name": "布加勒斯特"
                    }, 
                    {
                        "_Code": "BL", 
                        "_Name": "布勒伊拉"
                    }, 
                    {
                        "_Code": "BM", 
                        "_Name": "巴亚马雷"
                    }, 
                    {
                        "_Code": "BN", 
                        "_Name": "比斯特里察"
                    }, 
                    {
                        "_Code": "BO", 
                        "_Name": "博托沙尼"
                    }, 
                    {
                        "_Code": "BS", 
                        "_Name": "布拉索夫"
                    }, 
                    {
                        "_Code": "BZ", 
                        "_Name": "布泽乌"
                    }, 
                    {
                        "_Code": "CN", 
                        "_Name": "克卢日纳波卡"
                    }, 
                    {
                        "_Code": "CR", 
                        "_Name": "克勒拉希"
                    }, 
                    {
                        "_Code": "CT", 
                        "_Name": "康斯坦察"
                    }, 
                    {
                        "_Code": "DE", 
                        "_Name": "德瓦"
                    }, 
                    {
                        "_Code": "DO", 
                        "_Name": "克拉约瓦"
                    }, 
                    {
                        "_Code": "DT", 
                        "_Name": "德罗贝塔-塞维林堡"
                    }, 
                    {
                        "_Code": "FO", 
                        "_Name": "福克沙尼"
                    }, 
                    {
                        "_Code": "GG", 
                        "_Name": "久尔久"
                    }, 
                    {
                        "_Code": "GL", 
                        "_Name": "加拉茨"
                    }, 
                    {
                        "_Code": "IS", 
                        "_Name": "雅西"
                    }, 
                    {
                        "_Code": "MC", 
                        "_Name": "梅尔库里亚丘克"
                    }, 
                    {
                        "_Code": "OR", 
                        "_Name": "奥拉迪亚"
                    }, 
                    {
                        "_Code": "PI", 
                        "_Name": "皮特什蒂"
                    }, 
                    {
                        "_Code": "PL", 
                        "_Name": "普洛耶什蒂"
                    }, 
                    {
                        "_Code": "PN", 
                        "_Name": "皮亚特拉尼亚姆茨"
                    }, 
                    {
                        "_Code": "RE", 
                        "_Name": "雷希察"
                    }, 
                    {
                        "_Code": "SB", 
                        "_Name": "斯洛博齐亚"
                    }, 
                    {
                        "_Code": "SG", 
                        "_Name": "圣格奥尔基"
                    }, 
                    {
                        "_Code": "SM", 
                        "_Name": "萨图·马雷"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "锡比乌"
                    }, 
                    {
                        "_Code": "ST", 
                        "_Name": "斯拉蒂纳"
                    }, 
                    {
                        "_Code": "SU", 
                        "_Name": "苏恰瓦"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "特尔戈维什泰"
                    }, 
                    {
                        "_Code": "TI", 
                        "_Name": "蒂米什瓦拉"
                    }, 
                    {
                        "_Code": "TJ", 
                        "_Name": "特尔古日乌"
                    }, 
                    {
                        "_Code": "TM", 
                        "_Name": "特尔古穆列什"
                    }, 
                    {
                        "_Code": "TU", 
                        "_Name": "图尔恰"
                    }, 
                    {
                        "_Code": "VA", 
                        "_Name": "瓦斯卢伊"
                    }, 
                    {
                        "_Code": "VI", 
                        "_Name": "勒姆尼库沃尔恰"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "扎勒乌"
                    }
                ]
            }
        }, 
        {
            "_Code": "RUS", 
            "_Name": "俄罗斯", 
            "State": {
                "City": [
                    {
                        "_Code": "ABA", 
                        "_Name": "阿巴坎"
                    }, 
                    {
                        "_Code": "AGI", 
                        "_Name": "阿金斯科耶"
                    }, 
                    {
                        "_Code": "ARK", 
                        "_Name": "阿尔汉格尔斯克"
                    }, 
                    {
                        "_Code": "AST", 
                        "_Name": "阿斯特拉罕"
                    }, 
                    {
                        "_Code": "BAX", 
                        "_Name": "巴尔瑙尔"
                    }, 
                    {
                        "_Code": "BBZ", 
                        "_Name": "比罗比詹"
                    }, 
                    {
                        "_Code": "BEL", 
                        "_Name": "别尔哥罗德"
                    }, 
                    {
                        "_Code": "BQS", 
                        "_Name": "布拉戈维申斯克"
                    }, 
                    {
                        "_Code": "BRY", 
                        "_Name": "布良斯克"
                    }, 
                    {
                        "_Code": "CHE", 
                        "_Name": "车里雅宾斯克"
                    }, 
                    {
                        "_Code": "CHI", 
                        "_Name": "赤塔"
                    }, 
                    {
                        "_Code": "CKS", 
                        "_Name": "切尔克斯克"
                    }, 
                    {
                        "_Code": "COK", 
                        "_Name": "伯力"
                    }, 
                    {
                        "_Code": "CSY", 
                        "_Name": "切博克萨雷"
                    }, 
                    {
                        "_Code": "DYR", 
                        "_Name": "阿纳德尔"
                    }, 
                    {
                        "_Code": "ESL", 
                        "_Name": "埃利斯塔"
                    }, 
                    {
                        "_Code": "GOA", 
                        "_Name": "戈尔诺－阿尔泰斯克"
                    }, 
                    {
                        "_Code": "GOJ", 
                        "_Name": "下诺夫哥罗德"
                    }, 
                    {
                        "_Code": "GRV", 
                        "_Name": "格罗兹尼"
                    }, 
                    {
                        "_Code": "IJK", 
                        "_Name": "伊热夫斯克"
                    }, 
                    {
                        "_Code": "IKT", 
                        "_Name": "伊尔库茨克"
                    }, 
                    {
                        "_Code": "IN", 
                        "_Name": "马加斯"
                    }, 
                    {
                        "_Code": "IVO", 
                        "_Name": "伊万诺沃"
                    }, 
                    {
                        "_Code": "JAK", 
                        "_Name": "雅库茨克"
                    }, 
                    {
                        "_Code": "JAR", 
                        "_Name": "雅罗斯拉夫尔"
                    }, 
                    {
                        "_Code": "JEK", 
                        "_Name": "叶卡捷林堡"
                    }, 
                    {
                        "_Code": "JSA", 
                        "_Name": "南萨哈林斯克"
                    }, 
                    {
                        "_Code": "KEM", 
                        "_Name": "克麦罗沃"
                    }, 
                    {
                        "_Code": "KGD", 
                        "_Name": "加里宁格勒"
                    }, 
                    {
                        "_Code": "KHM", 
                        "_Name": "汉特－曼西斯克"
                    }, 
                    {
                        "_Code": "KIR", 
                        "_Name": "基洛夫"
                    }, 
                    {
                        "_Code": "KLF", 
                        "_Name": "卡卢加"
                    }, 
                    {
                        "_Code": "KOS", 
                        "_Name": "科斯特罗马"
                    }, 
                    {
                        "_Code": "KRO", 
                        "_Name": "库尔干"
                    }, 
                    {
                        "_Code": "KRR", 
                        "_Name": "克拉斯诺达尔"
                    }, 
                    {
                        "_Code": "KUD", 
                        "_Name": "库德姆卡尔"
                    }, 
                    {
                        "_Code": "KYA", 
                        "_Name": "克拉斯诺亚尔斯克"
                    }, 
                    {
                        "_Code": "KYZ", 
                        "_Name": "克孜勒"
                    }, 
                    {
                        "_Code": "KZN", 
                        "_Name": "喀山"
                    }, 
                    {
                        "_Code": "LIP", 
                        "_Name": "利佩茨克"
                    }, 
                    {
                        "_Code": "LNX", 
                        "_Name": "斯摩棱斯克"
                    }, 
                    {
                        "_Code": "MAG", 
                        "_Name": "马加丹"
                    }, 
                    {
                        "_Code": "MAY", 
                        "_Name": "迈科普"
                    }, 
                    {
                        "_Code": "MCX", 
                        "_Name": "马哈奇卡拉"
                    }, 
                    {
                        "_Code": "MMK", 
                        "_Name": "摩尔曼斯克"
                    }, 
                    {
                        "_Code": "MOW", 
                        "_Name": "莫斯科"
                    }, 
                    {
                        "_Code": "NAL", 
                        "_Name": "纳尔奇克"
                    }, 
                    {
                        "_Code": "NNM", 
                        "_Name": "纳里扬马尔"
                    }, 
                    {
                        "_Code": "NVS", 
                        "_Name": "新西伯利亚"
                    }, 
                    {
                        "_Code": "OMS", 
                        "_Name": "鄂木斯克"
                    }, 
                    {
                        "_Code": "ORE", 
                        "_Name": "奥伦堡"
                    }, 
                    {
                        "_Code": "ORL", 
                        "_Name": "奥廖尔"
                    }, 
                    {
                        "_Code": "PAL", 
                        "_Name": "帕拉纳"
                    }, 
                    {
                        "_Code": "PER", 
                        "_Name": "彼尔姆"
                    }, 
                    {
                        "_Code": "PES", 
                        "_Name": "彼得罗扎沃茨克"
                    }, 
                    {
                        "_Code": "PKC", 
                        "_Name": "彼得罗巴甫洛夫斯克"
                    }, 
                    {
                        "_Code": "PNZ", 
                        "_Name": "奔萨"
                    }, 
                    {
                        "_Code": "PSK", 
                        "_Name": "普斯科夫"
                    }, 
                    {
                        "_Code": "ROS", 
                        "_Name": "顿河畔罗斯托夫"
                    }, 
                    {
                        "_Code": "RYA", 
                        "_Name": "梁赞"
                    }, 
                    {
                        "_Code": "SAM", 
                        "_Name": "萨马拉"
                    }, 
                    {
                        "_Code": "SAR", 
                        "_Name": "萨拉托夫"
                    }, 
                    {
                        "_Code": "SCW", 
                        "_Name": "瑟克特夫卡尔"
                    }, 
                    {
                        "_Code": "SKX", 
                        "_Name": "萨兰斯克"
                    }, 
                    {
                        "_Code": "SLY", 
                        "_Name": "萨列哈尔德"
                    }, 
                    {
                        "_Code": "SPE", 
                        "_Name": "圣彼得堡"
                    }, 
                    {
                        "_Code": "STA", 
                        "_Name": "斯塔夫罗波尔"
                    }, 
                    {
                        "_Code": "TAM", 
                        "_Name": "坦波夫"
                    }, 
                    {
                        "_Code": "TOM", 
                        "_Name": "托木斯克"
                    }, 
                    {
                        "_Code": "TUL", 
                        "_Name": "图拉"
                    }, 
                    {
                        "_Code": "TVE", 
                        "_Name": "特维尔"
                    }, 
                    {
                        "_Code": "TYU", 
                        "_Name": "秋明"
                    }, 
                    {
                        "_Code": "UFA", 
                        "_Name": "乌法"
                    }, 
                    {
                        "_Code": "ULY", 
                        "_Name": "乌里扬诺夫斯克"
                    }, 
                    {
                        "_Code": "UOB", 
                        "_Name": "乌斯季奥尔登斯基"
                    }, 
                    {
                        "_Code": "URS", 
                        "_Name": "库尔斯克"
                    }, 
                    {
                        "_Code": "UUD", 
                        "_Name": "乌兰乌德"
                    }, 
                    {
                        "_Code": "VLA", 
                        "_Name": "弗拉季高加索"
                    }, 
                    {
                        "_Code": "VLG", 
                        "_Name": "沃洛格达"
                    }, 
                    {
                        "_Code": "VMR", 
                        "_Name": "弗拉基米尔"
                    }, 
                    {
                        "_Code": "VOG", 
                        "_Name": "伏尔加格勒"
                    }, 
                    {
                        "_Code": "VOR", 
                        "_Name": "沃罗涅什"
                    }, 
                    {
                        "_Code": "VUS", 
                        "_Name": "诺夫哥罗德"
                    }, 
                    {
                        "_Code": "VVO", 
                        "_Name": "海参崴"
                    }, 
                    {
                        "_Code": "YOL", 
                        "_Name": "约什卡尔奥拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "RWA", 
            "_Name": "卢旺达", 
            "State": {
                "City": [
                    {
                        "_Code": "BU", 
                        "_Name": "布塔雷"
                    }, 
                    {
                        "_Code": "BY", 
                        "_Name": "比温巴"
                    }, 
                    {
                        "_Code": "CY", 
                        "_Name": "尚古古"
                    }, 
                    {
                        "_Code": "GK", 
                        "_Name": "吉孔戈罗"
                    }, 
                    {
                        "_Code": "GS", 
                        "_Name": "吉塞尼"
                    }, 
                    {
                        "_Code": "GT", 
                        "_Name": "吉塔拉马"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "卡布加"
                    }, 
                    {
                        "_Code": "KG", 
                        "_Name": "基本古"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "基加利-恩加利"
                    }, 
                    {
                        "_Code": "KV", 
                        "_Name": "基加利市"
                    }, 
                    {
                        "_Code": "KY", 
                        "_Name": "基布耶"
                    }, 
                    {
                        "_Code": "NY", 
                        "_Name": "恩延扎"
                    }, 
                    {
                        "_Code": "RH", 
                        "_Name": "鲁汉戈"
                    }, 
                    {
                        "_Code": "RU", 
                        "_Name": "鲁亨盖里"
                    }, 
                    {
                        "_Code": "RW", 
                        "_Name": "卢瓦马加纳"
                    }, 
                    {
                        "_Code": "UM", 
                        "_Name": "乌姆塔拉"
                    }
                ]
            }
        }, 
        {
            "_Code": "SAU", 
            "_Name": "沙特阿拉伯", 
            "State": {
                "City": [
                    {
                        "_Code": "AHB", 
                        "_Name": "艾卜哈"
                    }, 
                    {
                        "_Code": "AKH", 
                        "_Name": "海耶"
                    }, 
                    {
                        "_Code": "ARA", 
                        "_Name": "阿尔阿尔"
                    }, 
                    {
                        "_Code": "BH", 
                        "_Name": "巴哈"
                    }, 
                    {
                        "_Code": "BUR", 
                        "_Name": "布赖代"
                    }, 
                    {
                        "_Code": "DAM", 
                        "_Name": "达曼"
                    }, 
                    {
                        "_Code": "HBT", 
                        "_Name": "哈费尔巴廷"
                    }, 
                    {
                        "_Code": "HFF", 
                        "_Name": "胡富夫"
                    }, 
                    {
                        "_Code": "HL", 
                        "_Name": "哈伊勒"
                    }, 
                    {
                        "_Code": "JBI", 
                        "_Name": "朱拜勒"
                    }, 
                    {
                        "_Code": "JED", 
                        "_Name": "吉达"
                    }, 
                    {
                        "_Code": "JZ", 
                        "_Name": "吉赞"
                    }, 
                    {
                        "_Code": "KMX", 
                        "_Name": "海米斯穆谢特"
                    }, 
                    {
                        "_Code": "MBR", 
                        "_Name": "姆巴拉兹"
                    }, 
                    {
                        "_Code": "MED", 
                        "_Name": "麦地那"
                    }, 
                    {
                        "_Code": "ML", 
                        "_Name": "麦加"
                    }, 
                    {
                        "_Code": "NR", 
                        "_Name": "纳季兰"
                    }, 
                    {
                        "_Code": "RD", 
                        "_Name": "利雅得"
                    }, 
                    {
                        "_Code": "SAK", 
                        "_Name": "塞卡卡"
                    }, 
                    {
                        "_Code": "TAR", 
                        "_Name": "塔伊夫"
                    }, 
                    {
                        "_Code": "TB", 
                        "_Name": "塔布克"
                    }, 
                    {
                        "_Code": "YNB", 
                        "_Name": "延布"
                    }
                ]
            }
        }, 
        {
            "_Code": "SCG", 
            "_Name": "塞尔维亚,黑山", 
            "State": {
                "City": [
                    {
                        "_Code": "BEG", 
                        "_Name": "贝尔格莱德"
                    }, 
                    {
                        "_Code": "INI", 
                        "_Name": "尼什"
                    }, 
                    {
                        "_Code": "KGV", 
                        "_Name": "克拉古涅瓦茨"
                    }, 
                    {
                        "_Code": "NVS", 
                        "_Name": "诺维萨德"
                    }, 
                    {
                        "_Code": "POD", 
                        "_Name": "波德戈里察"
                    }, 
                    {
                        "_Code": "PRN", 
                        "_Name": "普里什蒂纳"
                    }, 
                    {
                        "_Code": "SUB", 
                        "_Name": "苏博蒂察"
                    }, 
                    {
                        "_Code": "ZEM", 
                        "_Name": "泽蒙"
                    }
                ]
            }
        }, 
        {
            "_Code": "SDN", 
            "_Name": "苏丹", 
            "State": {
                "City": [
                    {
                        "_Code": "ANB", 
                        "_Name": "上尼罗"
                    }, 
                    {
                        "_Code": "ASH", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "GKU", 
                        "_Name": "科尔多凡"
                    }, 
                    {
                        "_Code": "KRT", 
                        "_Name": "喀土穆"
                    }, 
                    {
                        "_Code": "SBG", 
                        "_Name": "加扎勒河"
                    }, 
                    {
                        "_Code": "SDA", 
                        "_Name": "达尔富尔"
                    }, 
                    {
                        "_Code": "SHA", 
                        "_Name": "东部"
                    }, 
                    {
                        "_Code": "SIS", 
                        "_Name": "赤道"
                    }, 
                    {
                        "_Code": "WDH", 
                        "_Name": "中部"
                    }
                ]
            }
        }, 
        {
            "_Code": "SEN", 
            "_Name": "塞内加尔", 
            "State": {
                "City": [
                    {
                        "_Code": "DA", 
                        "_Name": "达喀尔"
                    }, 
                    {
                        "_Code": "DI", 
                        "_Name": "久尔贝勒"
                    }, 
                    {
                        "_Code": "FA", 
                        "_Name": "法蒂克"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "考拉克"
                    }, 
                    {
                        "_Code": "KO", 
                        "_Name": "科尔达"
                    }, 
                    {
                        "_Code": "LO", 
                        "_Name": "卢加"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马塔姆"
                    }, 
                    {
                        "_Code": "SL", 
                        "_Name": "圣路易"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "坦巴昆达"
                    }, 
                    {
                        "_Code": "TH", 
                        "_Name": "捷斯"
                    }, 
                    {
                        "_Code": "ZI", 
                        "_Name": "济金绍尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "SGP", 
            "_Name": "新加坡"
        }, 
        {
            "_Code": "SGS", 
            "_Name": "南乔治亚和南桑德威奇群岛"
        }, 
        {
            "_Code": "SHN", 
            "_Name": "圣赫勒拿"
        }, 
        {
            "_Code": "SJM", 
            "_Name": "斯瓦尔巴和扬马廷"
        }, 
        {
            "_Code": "SLB", 
            "_Name": "所罗门群岛", 
            "State": {
                "City": [
                    {
                        "_Code": "CE", 
                        "_Name": "中部群岛"
                    }, 
                    {
                        "_Code": "CH", 
                        "_Name": "乔伊索"
                    }, 
                    {
                        "_Code": "GC", 
                        "_Name": "瓜达尔卡纳尔"
                    }, 
                    {
                        "_Code": "HO", 
                        "_Name": "霍尼亚拉"
                    }, 
                    {
                        "_Code": "IS", 
                        "_Name": "伊萨贝尔"
                    }, 
                    {
                        "_Code": "MK", 
                        "_Name": "马基拉"
                    }, 
                    {
                        "_Code": "ML", 
                        "_Name": "马莱塔"
                    }, 
                    {
                        "_Code": "RB", 
                        "_Name": "拉纳尔和贝罗纳"
                    }, 
                    {
                        "_Code": "TM", 
                        "_Name": "泰莫图"
                    }, 
                    {
                        "_Code": "WE", 
                        "_Name": "西部"
                    }
                ]
            }
        }, 
        {
            "_Code": "SLE", 
            "_Name": "塞拉利昂", 
            "State": {
                "City": [
                    {
                        "_Code": "E", 
                        "_Name": "东部"
                    }, 
                    {
                        "_Code": "N", 
                        "_Name": "北部"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "南部"
                    }, 
                    {
                        "_Code": "W", 
                        "_Name": "西部区"
                    }
                ]
            }
        }, 
        {
            "_Code": "SLV", 
            "_Name": "萨尔瓦多", 
            "State": {
                "City": [
                    {
                        "_Code": "AH", 
                        "_Name": "阿瓦查潘"
                    }, 
                    {
                        "_Code": "APO", 
                        "_Name": "阿波帕"
                    }, 
                    {
                        "_Code": "CA", 
                        "_Name": "卡瓦尼亚斯"
                    }, 
                    {
                        "_Code": "CH", 
                        "_Name": "查拉特南戈"
                    }, 
                    {
                        "_Code": "CS", 
                        "_Name": "中南"
                    }, 
                    {
                        "_Code": "CU", 
                        "_Name": "库斯卡特兰"
                    }, 
                    {
                        "_Code": "DE", 
                        "_Name": "德尔加多"
                    }, 
                    {
                        "_Code": "IL", 
                        "_Name": "伊洛潘戈"
                    }, 
                    {
                        "_Code": "KN", 
                        "_Name": "基埃-恩特姆"
                    }, 
                    {
                        "_Code": "LB", 
                        "_Name": "拉利伯塔德"
                    }, 
                    {
                        "_Code": "LI", 
                        "_Name": "滨海"
                    }, 
                    {
                        "_Code": "MEJ", 
                        "_Name": "梅基卡诺斯"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "莫拉桑"
                    }, 
                    {
                        "_Code": "PZ", 
                        "_Name": "拉巴斯"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "圣安娜"
                    }, 
                    {
                        "_Code": "SM", 
                        "_Name": "圣米格尔"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "松索纳特"
                    }, 
                    {
                        "_Code": "SOY", 
                        "_Name": "索亚潘戈"
                    }, 
                    {
                        "_Code": "SS", 
                        "_Name": "圣萨尔瓦多"
                    }, 
                    {
                        "_Code": "SV", 
                        "_Name": "圣维森特"
                    }, 
                    {
                        "_Code": "UN", 
                        "_Name": "拉乌尼翁"
                    }, 
                    {
                        "_Code": "US", 
                        "_Name": "乌苏卢坦"
                    }, 
                    {
                        "_Code": "WN", 
                        "_Name": "韦莱-恩萨斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "SMR", 
            "_Name": "圣马力诺"
        }, 
        {
            "_Code": "SOM", 
            "_Name": "索马里"
        }, 
        {
            "_Code": "SPM", 
            "_Name": "圣皮埃尔和米克隆群岛"
        }, 
        {
            "_Code": "STP", 
            "_Name": "圣多美和普林西比"
        }, 
        {
            "_Code": "SUR", 
            "_Name": "苏里南", 
            "State": {
                "City": [
                    {
                        "_Code": "BR", 
                        "_Name": "布罗科蓬多"
                    }, 
                    {
                        "_Code": "CM", 
                        "_Name": "科默韦讷"
                    }, 
                    {
                        "_Code": "CR", 
                        "_Name": "科罗尼"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马罗韦讷"
                    }, 
                    {
                        "_Code": "NI", 
                        "_Name": "尼克里"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "帕拉"
                    }, 
                    {
                        "_Code": "PM", 
                        "_Name": "帕拉马里博"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "萨拉马卡"
                    }, 
                    {
                        "_Code": "SI", 
                        "_Name": "西帕里韦尼"
                    }, 
                    {
                        "_Code": "WA", 
                        "_Name": "瓦尼卡"
                    }
                ]
            }
        }, 
        {
            "_Code": "SVK", 
            "_Name": "斯洛伐克", 
            "State": {
                "City": [
                    {
                        "_Code": "BBY", 
                        "_Name": "班斯卡-比斯特里察"
                    }, 
                    {
                        "_Code": "BTS", 
                        "_Name": "布拉迪斯拉发"
                    }, 
                    {
                        "_Code": "KOR", 
                        "_Name": "科希策"
                    }, 
                    {
                        "_Code": "NRA", 
                        "_Name": "尼特拉"
                    }, 
                    {
                        "_Code": "POV", 
                        "_Name": "普雷绍夫"
                    }, 
                    {
                        "_Code": "RIL", 
                        "_Name": "日利纳"
                    }, 
                    {
                        "_Code": "TNA", 
                        "_Name": "特尔纳瓦"
                    }, 
                    {
                        "_Code": "TRE", 
                        "_Name": "特伦钦"
                    }
                ]
            }
        }, 
        {
            "_Code": "SVN", 
            "_Name": "斯洛文尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "DLJ", 
                        "_Name": "多雷尼"
                    }, 
                    {
                        "_Code": "GSK", 
                        "_Name": "戈里"
                    }, 
                    {
                        "_Code": "GSZ", 
                        "_Name": "戈雷尼"
                    }, 
                    {
                        "_Code": "KOR", 
                        "_Name": "科洛"
                    }, 
                    {
                        "_Code": "NKR", 
                        "_Name": "诺特拉尼"
                    }, 
                    {
                        "_Code": "OKR", 
                        "_Name": "奥巴尔诺-克拉"
                    }, 
                    {
                        "_Code": "OSR", 
                        "_Name": "奥斯雷德涅斯洛文"
                    }, 
                    {
                        "_Code": "POD", 
                        "_Name": "波德拉夫"
                    }, 
                    {
                        "_Code": "POM", 
                        "_Name": "波穆尔"
                    }, 
                    {
                        "_Code": "SAV", 
                        "_Name": "萨维尼"
                    }, 
                    {
                        "_Code": "SPO", 
                        "_Name": "斯波德涅波萨夫"
                    }, 
                    {
                        "_Code": "ZAS", 
                        "_Name": "扎萨夫"
                    }
                ]
            }
        }, 
        {
            "_Code": "SWE", 
            "_Name": "瑞典", 
            "State": {
                "City": [
                    {
                        "_Code": "AB", 
                        "_Name": "斯德哥尔摩"
                    }, 
                    {
                        "_Code": "AC", 
                        "_Name": "西博滕"
                    }, 
                    {
                        "_Code": "BD", 
                        "_Name": "北博滕"
                    }, 
                    {
                        "_Code": "C", 
                        "_Name": "乌普萨拉"
                    }, 
                    {
                        "_Code": "D", 
                        "_Name": "南曼兰"
                    }, 
                    {
                        "_Code": "DLN", 
                        "_Name": "达拉纳"
                    }, 
                    {
                        "_Code": "F", 
                        "_Name": "延雪平"
                    }, 
                    {
                        "_Code": "G", 
                        "_Name": "克鲁努贝里"
                    }, 
                    {
                        "_Code": "H", 
                        "_Name": "卡尔马"
                    }, 
                    {
                        "_Code": "X", 
                        "_Name": "耶夫勒堡"
                    }, 
                    {
                        "_Code": "I", 
                        "_Name": "哥得兰"
                    }, 
                    {
                        "_Code": "K", 
                        "_Name": "布莱金厄"
                    }, 
                    {
                        "_Code": "M", 
                        "_Name": "斯科耐"
                    }, 
                    {
                        "_Code": "N", 
                        "_Name": "哈兰"
                    }, 
                    {
                        "_Code": "O", 
                        "_Name": "西约特兰"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "韦姆兰"
                    }, 
                    {
                        "_Code": "T", 
                        "_Name": "厄勒布鲁"
                    }, 
                    {
                        "_Code": "U", 
                        "_Name": "西曼兰"
                    }, 
                    {
                        "_Code": "UGL", 
                        "_Name": "东约特兰"
                    }, 
                    {
                        "_Code": "Y", 
                        "_Name": "西诺尔兰"
                    }, 
                    {
                        "_Code": "Z", 
                        "_Name": "耶姆特兰"
                    }
                ]
            }
        }, 
        {
            "_Code": "SWZ", 
            "_Name": "斯威士兰"
        }, 
        {
            "_Code": "SYC", 
            "_Name": "塞舌尔"
        }, 
        {
            "_Code": "SYR", 
            "_Name": "叙利亚", 
            "State": {
                "City": [
                    {
                        "_Code": "DA", 
                        "_Name": "德拉"
                    }, 
                    {
                        "_Code": "DI", 
                        "_Name": "大马士革市"
                    }, 
                    {
                        "_Code": "DZ", 
                        "_Name": "代尔祖尔"
                    }, 
                    {
                        "_Code": "GH", 
                        "_Name": "加布"
                    }, 
                    {
                        "_Code": "HA", 
                        "_Name": "哈塞克"
                    }, 
                    {
                        "_Code": "HI", 
                        "_Name": "霍姆斯"
                    }, 
                    {
                        "_Code": "HL", 
                        "_Name": "阿勒颇"
                    }, 
                    {
                        "_Code": "HM", 
                        "_Name": "哈马"
                    }, 
                    {
                        "_Code": "ID", 
                        "_Name": "伊德利卜"
                    }, 
                    {
                        "_Code": "QA", 
                        "_Name": "卡米什利"
                    }, 
                    {
                        "_Code": "QU", 
                        "_Name": "库奈特拉"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "拉塔基亚"
                    }, 
                    {
                        "_Code": "RD", 
                        "_Name": "大马士革"
                    }, 
                    {
                        "_Code": "RQ", 
                        "_Name": "拉卡"
                    }, 
                    {
                        "_Code": "SU", 
                        "_Name": "苏韦达"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔尔图斯"
                    }
                ]
            }
        }, 
        {
            "_Code": "TAA", 
            "_Name": "特里斯坦达昆哈"
        }, 
        {
            "_Code": "TCA", 
            "_Name": "特克斯和凯克特斯群岛"
        }, 
        {
            "_Code": "TCD", 
            "_Name": "乍得"
        }, 
        {
            "_Code": "TGO", 
            "_Name": "多哥", 
            "State": {
                "City": [
                    {
                        "_Code": "C", 
                        "_Name": "中部区"
                    }, 
                    {
                        "_Code": "K", 
                        "_Name": "卡拉区"
                    }, 
                    {
                        "_Code": "M", 
                        "_Name": "滨海区"
                    }, 
                    {
                        "_Code": "P", 
                        "_Name": "高原区"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "草原区"
                    }
                ]
            }
        }, 
        {
            "_Code": "THA", 
            "_Name": "泰国", 
            "State": {
                "City": [
                    {
                        "_Code": "10", 
                        "_Name": "曼谷"
                    }, 
                    {
                        "_Code": "11", 
                        "_Name": "北揽"
                    }, 
                    {
                        "_Code": "12", 
                        "_Name": "暖武里"
                    }, 
                    {
                        "_Code": "13", 
                        "_Name": "巴吞他尼"
                    }, 
                    {
                        "_Code": "14", 
                        "_Name": "大城"
                    }, 
                    {
                        "_Code": "15", 
                        "_Name": "红统"
                    }, 
                    {
                        "_Code": "16", 
                        "_Name": "华富里"
                    }, 
                    {
                        "_Code": "17", 
                        "_Name": "信武里"
                    }, 
                    {
                        "_Code": "18", 
                        "_Name": "猜那"
                    }, 
                    {
                        "_Code": "19", 
                        "_Name": "北标"
                    }, 
                    {
                        "_Code": "20", 
                        "_Name": "春武里"
                    }, 
                    {
                        "_Code": "21", 
                        "_Name": "拉农"
                    }, 
                    {
                        "_Code": "22", 
                        "_Name": "尖竹汶"
                    }, 
                    {
                        "_Code": "23", 
                        "_Name": "达叻"
                    }, 
                    {
                        "_Code": "24", 
                        "_Name": "北柳"
                    }, 
                    {
                        "_Code": "25", 
                        "_Name": "巴真"
                    }, 
                    {
                        "_Code": "26", 
                        "_Name": "那空那育"
                    }, 
                    {
                        "_Code": "27", 
                        "_Name": "沙缴"
                    }, 
                    {
                        "_Code": "31", 
                        "_Name": "武里南"
                    }, 
                    {
                        "_Code": "32", 
                        "_Name": "素林"
                    }, 
                    {
                        "_Code": "33", 
                        "_Name": "四色菊"
                    }, 
                    {
                        "_Code": "34", 
                        "_Name": "乌汶"
                    }, 
                    {
                        "_Code": "35", 
                        "_Name": "耶梭通"
                    }, 
                    {
                        "_Code": "36", 
                        "_Name": "猜也奔"
                    }, 
                    {
                        "_Code": "37", 
                        "_Name": "安纳乍能"
                    }, 
                    {
                        "_Code": "39", 
                        "_Name": "廊莫那浦"
                    }, 
                    {
                        "_Code": "40", 
                        "_Name": "孔敬"
                    }, 
                    {
                        "_Code": "41", 
                        "_Name": "乌隆"
                    }, 
                    {
                        "_Code": "42", 
                        "_Name": "黎"
                    }, 
                    {
                        "_Code": "43", 
                        "_Name": "廊开"
                    }, 
                    {
                        "_Code": "44", 
                        "_Name": "玛哈沙拉堪"
                    }, 
                    {
                        "_Code": "45", 
                        "_Name": "黎逸"
                    }, 
                    {
                        "_Code": "46", 
                        "_Name": "加拉信"
                    }, 
                    {
                        "_Code": "47", 
                        "_Name": "色军"
                    }, 
                    {
                        "_Code": "48", 
                        "_Name": "那空帕农"
                    }, 
                    {
                        "_Code": "49", 
                        "_Name": "莫达汉"
                    }, 
                    {
                        "_Code": "50", 
                        "_Name": "清迈"
                    }, 
                    {
                        "_Code": "51", 
                        "_Name": "南奔"
                    }, 
                    {
                        "_Code": "53", 
                        "_Name": "程逸"
                    }, 
                    {
                        "_Code": "54", 
                        "_Name": "帕"
                    }, 
                    {
                        "_Code": "55", 
                        "_Name": "难"
                    }, 
                    {
                        "_Code": "56", 
                        "_Name": "帕尧"
                    }, 
                    {
                        "_Code": "57", 
                        "_Name": "清莱"
                    }, 
                    {
                        "_Code": "58", 
                        "_Name": "夜丰颂"
                    }, 
                    {
                        "_Code": "60", 
                        "_Name": "北榄坡"
                    }, 
                    {
                        "_Code": "61", 
                        "_Name": "乌泰他尼"
                    }, 
                    {
                        "_Code": "62", 
                        "_Name": "甘烹碧"
                    }, 
                    {
                        "_Code": "63", 
                        "_Name": "达"
                    }, 
                    {
                        "_Code": "64", 
                        "_Name": "素可泰"
                    }, 
                    {
                        "_Code": "65", 
                        "_Name": "彭世洛"
                    }, 
                    {
                        "_Code": "66", 
                        "_Name": "披集"
                    }, 
                    {
                        "_Code": "70", 
                        "_Name": "叻丕"
                    }, 
                    {
                        "_Code": "71", 
                        "_Name": "北碧"
                    }, 
                    {
                        "_Code": "72", 
                        "_Name": "素攀武里"
                    }, 
                    {
                        "_Code": "73", 
                        "_Name": "佛统"
                    }, 
                    {
                        "_Code": "74", 
                        "_Name": "龙仔厝"
                    }, 
                    {
                        "_Code": "75", 
                        "_Name": "夜功"
                    }, 
                    {
                        "_Code": "76", 
                        "_Name": "碧差汶"
                    }, 
                    {
                        "_Code": "77", 
                        "_Name": "巴蜀"
                    }, 
                    {
                        "_Code": "78", 
                        "_Name": "佛丕"
                    }, 
                    {
                        "_Code": "80", 
                        "_Name": "洛坤"
                    }, 
                    {
                        "_Code": "81", 
                        "_Name": "甲米"
                    }, 
                    {
                        "_Code": "82", 
                        "_Name": "攀牙"
                    }, 
                    {
                        "_Code": "83", 
                        "_Name": "普吉"
                    }, 
                    {
                        "_Code": "84", 
                        "_Name": "素叻"
                    }, 
                    {
                        "_Code": "85", 
                        "_Name": "罗勇"
                    }, 
                    {
                        "_Code": "86", 
                        "_Name": "春蓬"
                    }, 
                    {
                        "_Code": "90", 
                        "_Name": "宋卡"
                    }, 
                    {
                        "_Code": "91", 
                        "_Name": "沙敦"
                    }, 
                    {
                        "_Code": "92", 
                        "_Name": "董里"
                    }, 
                    {
                        "_Code": "93", 
                        "_Name": "博达伦"
                    }, 
                    {
                        "_Code": "94", 
                        "_Name": "北大年"
                    }, 
                    {
                        "_Code": "95", 
                        "_Name": "也拉"
                    }, 
                    {
                        "_Code": "96", 
                        "_Name": "陶公"
                    }
                ]
            }
        }, 
        {
            "_Code": "TJK", 
            "_Name": "塔吉克斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "DYU", 
                        "_Name": "杜尚别"
                    }, 
                    {
                        "_Code": "ISF", 
                        "_Name": "伊斯法拉"
                    }, 
                    {
                        "_Code": "KAN", 
                        "_Name": "卡尼巴达姆"
                    }, 
                    {
                        "_Code": "KHO", 
                        "_Name": "霍罗格"
                    }, 
                    {
                        "_Code": "KHU", 
                        "_Name": "苦盏"
                    }, 
                    {
                        "_Code": "KLB", 
                        "_Name": "库洛布"
                    }, 
                    {
                        "_Code": "KOF", 
                        "_Name": "科法尔尼洪"
                    }, 
                    {
                        "_Code": "KTJ", 
                        "_Name": "库尔干-秋别"
                    }, 
                    {
                        "_Code": "NUR", 
                        "_Name": "努雷克"
                    }, 
                    {
                        "_Code": "PJK", 
                        "_Name": "彭吉肯特"
                    }, 
                    {
                        "_Code": "RGU", 
                        "_Name": "洛贡"
                    }, 
                    {
                        "_Code": "SBA", 
                        "_Name": "萨班特"
                    }, 
                    {
                        "_Code": "TBS", 
                        "_Name": "塔博沙尔"
                    }, 
                    {
                        "_Code": "TSZ", 
                        "_Name": "图尔孙扎德"
                    }, 
                    {
                        "_Code": "UTJ", 
                        "_Name": "乌拉秋别"
                    }
                ]
            }
        }, 
        {
            "_Code": "TKL", 
            "_Name": "托克劳"
        }, 
        {
            "_Code": "TKM", 
            "_Name": "土库曼斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "A", 
                        "_Name": "阿哈尔"
                    }, 
                    {
                        "_Code": "ASB", 
                        "_Name": "阿什哈巴德市"
                    }, 
                    {
                        "_Code": "B", 
                        "_Name": "巴尔坎"
                    }, 
                    {
                        "_Code": "D", 
                        "_Name": "达沙古兹"
                    }, 
                    {
                        "_Code": "L", 
                        "_Name": "列巴普"
                    }, 
                    {
                        "_Code": "M", 
                        "_Name": "马雷"
                    }, 
                    {
                        "_Code": "NEB", 
                        "_Name": "涅比特达格"
                    }
                ]
            }
        }, 
        {
            "_Code": "TLS", 
            "_Name": "东帝汶", 
            "State": {
                "City": [
                    {
                        "_Code": "AL", 
                        "_Name": "阿伊莱乌"
                    }, 
                    {
                        "_Code": "AM", 
                        "_Name": "安贝诺"
                    }, 
                    {
                        "_Code": "AN", 
                        "_Name": "阿伊纳罗"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "包考"
                    }, 
                    {
                        "_Code": "BO", 
                        "_Name": "博博纳罗"
                    }, 
                    {
                        "_Code": "DI", 
                        "_Name": "帝力"
                    }, 
                    {
                        "_Code": "ER", 
                        "_Name": "埃尔梅拉"
                    }, 
                    {
                        "_Code": "KO", 
                        "_Name": "科瓦利马"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "劳滕"
                    }, 
                    {
                        "_Code": "LI", 
                        "_Name": "利基卡"
                    }, 
                    {
                        "_Code": "MF", 
                        "_Name": "马努法伊"
                    }, 
                    {
                        "_Code": "MT", 
                        "_Name": "马纳图托"
                    }, 
                    {
                        "_Code": "VI", 
                        "_Name": "维克克"
                    }
                ]
            }
        }, 
        {
            "_Code": "TON", 
            "_Name": "汤加", 
            "State": {
                "City": [
                    {
                        "_Code": "E", 
                        "_Name": "埃瓦"
                    }, 
                    {
                        "_Code": "H", 
                        "_Name": "哈派"
                    }, 
                    {
                        "_Code": "N", 
                        "_Name": "纽阿斯"
                    }, 
                    {
                        "_Code": "T", 
                        "_Name": "汤加塔布"
                    }, 
                    {
                        "_Code": "V", 
                        "_Name": "瓦瓦乌"
                    }
                ]
            }
        }, 
        {
            "_Code": "TTO", 
            "_Name": "特立尼达和多巴哥"
        }, 
        {
            "_Code": "TUN", 
            "_Name": "突尼斯", 
            "State": {
                "City": [
                    {
                        "_Code": "AR", 
                        "_Name": "艾尔亚奈"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "本阿鲁斯"
                    }, 
                    {
                        "_Code": "BI", 
                        "_Name": "比塞大"
                    }, 
                    {
                        "_Code": "BJ", 
                        "_Name": "巴杰"
                    }, 
                    {
                        "_Code": "GB", 
                        "_Name": "加贝斯"
                    }, 
                    {
                        "_Code": "GF", 
                        "_Name": "加夫萨"
                    }, 
                    {
                        "_Code": "JE", 
                        "_Name": "坚杜拜"
                    }, 
                    {
                        "_Code": "KB", 
                        "_Name": "吉比利"
                    }, 
                    {
                        "_Code": "KR", 
                        "_Name": "凯鲁万"
                    }, 
                    {
                        "_Code": "KS", 
                        "_Name": "卡塞林"
                    }, 
                    {
                        "_Code": "LK", 
                        "_Name": "卡夫"
                    }, 
                    {
                        "_Code": "ME", 
                        "_Name": "梅德宁"
                    }, 
                    {
                        "_Code": "MH", 
                        "_Name": "马赫迪耶"
                    }, 
                    {
                        "_Code": "MN", 
                        "_Name": "马努巴"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "莫纳斯提尔"
                    }, 
                    {
                        "_Code": "NA", 
                        "_Name": "纳布勒"
                    }, 
                    {
                        "_Code": "SD", 
                        "_Name": "西迪布济德"
                    }, 
                    {
                        "_Code": "SF", 
                        "_Name": "斯法克斯"
                    }, 
                    {
                        "_Code": "SL", 
                        "_Name": "锡勒亚奈"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "苏塞"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "泰塔温"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "托泽尔"
                    }, 
                    {
                        "_Code": "TU", 
                        "_Name": "突尼斯"
                    }, 
                    {
                        "_Code": "ZA", 
                        "_Name": "宰格万"
                    }
                ]
            }
        }, 
        {
            "_Code": "TUR", 
            "_Name": "土耳其", 
            "State": {
                "City": [
                    {
                        "_Code": "ADA", 
                        "_Name": "阿达纳"
                    }, 
                    {
                        "_Code": "ADI", 
                        "_Name": "阿德亚曼"
                    }, 
                    {
                        "_Code": "AFY", 
                        "_Name": "阿菲永"
                    }, 
                    {
                        "_Code": "AGR", 
                        "_Name": "阿勒"
                    }, 
                    {
                        "_Code": "AKS", 
                        "_Name": "阿克萨赖"
                    }, 
                    {
                        "_Code": "AMA", 
                        "_Name": "阿马西亚"
                    }, 
                    {
                        "_Code": "ANK", 
                        "_Name": "安卡拉"
                    }, 
                    {
                        "_Code": "ANT", 
                        "_Name": "安塔利亚"
                    }, 
                    {
                        "_Code": "ARD", 
                        "_Name": "阿尔达罕"
                    }, 
                    {
                        "_Code": "ART", 
                        "_Name": "阿尔特温"
                    }, 
                    {
                        "_Code": "AYI", 
                        "_Name": "艾登"
                    }, 
                    {
                        "_Code": "BAL", 
                        "_Name": "巴勒克埃西尔"
                    }, 
                    {
                        "_Code": "BAR", 
                        "_Name": "巴尔腾"
                    }, 
                    {
                        "_Code": "BAT", 
                        "_Name": "巴特曼"
                    }, 
                    {
                        "_Code": "BAY", 
                        "_Name": "巴伊布尔特"
                    }, 
                    {
                        "_Code": "BIL", 
                        "_Name": "比莱吉克"
                    }, 
                    {
                        "_Code": "BIN", 
                        "_Name": "宾格尔"
                    }, 
                    {
                        "_Code": "BIT", 
                        "_Name": "比特利斯"
                    }, 
                    {
                        "_Code": "BOL", 
                        "_Name": "博卢"
                    }, 
                    {
                        "_Code": "BRD", 
                        "_Name": "布尔杜尔"
                    }, 
                    {
                        "_Code": "BRS", 
                        "_Name": "布尔萨"
                    }, 
                    {
                        "_Code": "CKL", 
                        "_Name": "恰纳卡莱"
                    }, 
                    {
                        "_Code": "CKR", 
                        "_Name": "昌克勒"
                    }, 
                    {
                        "_Code": "COR", 
                        "_Name": "乔鲁姆"
                    }, 
                    {
                        "_Code": "DEN", 
                        "_Name": "代尼兹利"
                    }, 
                    {
                        "_Code": "DIY", 
                        "_Name": "迪亚巴克尔"
                    }, 
                    {
                        "_Code": "EDI", 
                        "_Name": "埃迪尔内"
                    }, 
                    {
                        "_Code": "ELA", 
                        "_Name": "埃拉泽"
                    }, 
                    {
                        "_Code": "ESK", 
                        "_Name": "埃斯基谢希尔"
                    }, 
                    {
                        "_Code": "EZC", 
                        "_Name": "埃尔津詹"
                    }, 
                    {
                        "_Code": "EZR", 
                        "_Name": "埃尔祖鲁姆"
                    }, 
                    {
                        "_Code": "GAZ", 
                        "_Name": "加济安泰普"
                    }, 
                    {
                        "_Code": "GIR", 
                        "_Name": "吉雷松"
                    }, 
                    {
                        "_Code": "GMS", 
                        "_Name": "居米什哈内"
                    }, 
                    {
                        "_Code": "HKR", 
                        "_Name": "哈卡里"
                    }, 
                    {
                        "_Code": "HTY", 
                        "_Name": "哈塔伊"
                    }, 
                    {
                        "_Code": "ICE", 
                        "_Name": "伊切尔"
                    }, 
                    {
                        "_Code": "IGD", 
                        "_Name": "伊迪尔"
                    }, 
                    {
                        "_Code": "ISP", 
                        "_Name": "伊斯帕尔塔"
                    }, 
                    {
                        "_Code": "IST", 
                        "_Name": "伊斯坦布尔"
                    }, 
                    {
                        "_Code": "IZM", 
                        "_Name": "伊兹密尔"
                    }, 
                    {
                        "_Code": "KAH", 
                        "_Name": "卡赫拉曼马拉什"
                    }, 
                    {
                        "_Code": "KAS", 
                        "_Name": "卡斯塔莫努"
                    }, 
                    {
                        "_Code": "KAY", 
                        "_Name": "开塞利"
                    }, 
                    {
                        "_Code": "KLR", 
                        "_Name": "柯克拉雷利"
                    }, 
                    {
                        "_Code": "KLS", 
                        "_Name": "基利斯"
                    }, 
                    {
                        "_Code": "KOC", 
                        "_Name": "科贾埃利"
                    }, 
                    {
                        "_Code": "KON", 
                        "_Name": "科尼亚"
                    }, 
                    {
                        "_Code": "KRB", 
                        "_Name": "卡拉比克"
                    }, 
                    {
                        "_Code": "KRH", 
                        "_Name": "克尔谢希尔"
                    }, 
                    {
                        "_Code": "KRK", 
                        "_Name": "克勒克卡莱"
                    }, 
                    {
                        "_Code": "KRM", 
                        "_Name": "卡拉曼"
                    }, 
                    {
                        "_Code": "KRS", 
                        "_Name": "卡尔斯"
                    }, 
                    {
                        "_Code": "KUT", 
                        "_Name": "屈塔希亚"
                    }, 
                    {
                        "_Code": "MAL", 
                        "_Name": "马拉蒂亚"
                    }, 
                    {
                        "_Code": "MAN", 
                        "_Name": "马尼萨"
                    }, 
                    {
                        "_Code": "MAR", 
                        "_Name": "马尔丁"
                    }, 
                    {
                        "_Code": "MUG", 
                        "_Name": "穆拉"
                    }, 
                    {
                        "_Code": "MUS", 
                        "_Name": "穆什"
                    }, 
                    {
                        "_Code": "NEV", 
                        "_Name": "内夫谢希尔"
                    }, 
                    {
                        "_Code": "NIG", 
                        "_Name": "尼代"
                    }, 
                    {
                        "_Code": "ORD", 
                        "_Name": "奥尔杜"
                    }, 
                    {
                        "_Code": "RIZ", 
                        "_Name": "里泽"
                    }, 
                    {
                        "_Code": "SAK", 
                        "_Name": "萨卡里亚"
                    }, 
                    {
                        "_Code": "SAM", 
                        "_Name": "萨姆松"
                    }, 
                    {
                        "_Code": "SII", 
                        "_Name": "锡尔特"
                    }, 
                    {
                        "_Code": "SIN", 
                        "_Name": "锡诺普"
                    }, 
                    {
                        "_Code": "SIR", 
                        "_Name": "锡尔纳克"
                    }, 
                    {
                        "_Code": "SIV", 
                        "_Name": "锡瓦斯"
                    }, 
                    {
                        "_Code": "TEL", 
                        "_Name": "泰基尔达"
                    }, 
                    {
                        "_Code": "TOK", 
                        "_Name": "托卡特"
                    }, 
                    {
                        "_Code": "TRA", 
                        "_Name": "特拉布宗"
                    }, 
                    {
                        "_Code": "TUN", 
                        "_Name": "通杰利"
                    }, 
                    {
                        "_Code": "URF", 
                        "_Name": "拉飞"
                    }, 
                    {
                        "_Code": "USK", 
                        "_Name": "乌萨克"
                    }, 
                    {
                        "_Code": "VAN", 
                        "_Name": "凡"
                    }, 
                    {
                        "_Code": "YOZ", 
                        "_Name": "约兹加特"
                    }, 
                    {
                        "_Code": "ZON", 
                        "_Name": "宗古尔达克"
                    }
                ]
            }
        }, 
        {
            "_Code": "TUV", 
            "_Name": "图瓦卢"
        }, 
        {
            "_Code": "TZA", 
            "_Name": "坦桑尼亚", 
            "State": {
                "City": [
                    {
                        "_Code": "AR", 
                        "_Name": "阿鲁沙"
                    }, 
                    {
                        "_Code": "DO", 
                        "_Name": "多多马"
                    }, 
                    {
                        "_Code": "DS", 
                        "_Name": "达累斯萨拉姆"
                    }, 
                    {
                        "_Code": "IR", 
                        "_Name": "伊林加"
                    }, 
                    {
                        "_Code": "KA", 
                        "_Name": "卡盖拉"
                    }, 
                    {
                        "_Code": "KI", 
                        "_Name": "基戈马"
                    }, 
                    {
                        "_Code": "KJ", 
                        "_Name": "乞力马扎罗"
                    }, 
                    {
                        "_Code": "LN", 
                        "_Name": "林迪"
                    }, 
                    {
                        "_Code": "MB", 
                        "_Name": "姆贝亚"
                    }, 
                    {
                        "_Code": "MM", 
                        "_Name": "桑给巴尔市和西"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "莫洛戈罗"
                    }, 
                    {
                        "_Code": "MR", 
                        "_Name": "马腊"
                    }, 
                    {
                        "_Code": "MT", 
                        "_Name": "姆特瓦拉"
                    }, 
                    {
                        "_Code": "MW", 
                        "_Name": "姆万扎"
                    }, 
                    {
                        "_Code": "MY", 
                        "_Name": "曼亚拉"
                    }, 
                    {
                        "_Code": "PN", 
                        "_Name": "奔巴北"
                    }, 
                    {
                        "_Code": "PS", 
                        "_Name": "奔巴南"
                    }, 
                    {
                        "_Code": "PW", 
                        "_Name": "滨海"
                    }, 
                    {
                        "_Code": "RK", 
                        "_Name": "鲁夸"
                    }, 
                    {
                        "_Code": "RV", 
                        "_Name": "鲁伍马"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "欣延加"
                    }, 
                    {
                        "_Code": "SI", 
                        "_Name": "辛吉达"
                    }, 
                    {
                        "_Code": "TB", 
                        "_Name": "塔波拉"
                    }, 
                    {
                        "_Code": "TN", 
                        "_Name": "坦噶"
                    }, 
                    {
                        "_Code": "UN", 
                        "_Name": "桑给巴尔北"
                    }, 
                    {
                        "_Code": "US", 
                        "_Name": "桑给巴尔南"
                    }, 
                    {
                        "_Code": "ZN", 
                        "_Name": "桑给巴尔"
                    }
                ]
            }
        }, 
        {
            "_Code": "UGA", 
            "_Name": "乌干达", 
            "State": {
                "City": [
                    {
                        "_Code": "ADJ", 
                        "_Name": "阿朱马尼"
                    }, 
                    {
                        "_Code": "APC", 
                        "_Name": "阿帕克"
                    }, 
                    {
                        "_Code": "ARU", 
                        "_Name": "阿鲁阿"
                    }, 
                    {
                        "_Code": "BSH", 
                        "_Name": "布谢尼"
                    }, 
                    {
                        "_Code": "BUG", 
                        "_Name": "布吉里"
                    }, 
                    {
                        "_Code": "BUN", 
                        "_Name": "本迪布焦"
                    }, 
                    {
                        "_Code": "BUS", 
                        "_Name": "布西亚"
                    }, 
                    {
                        "_Code": "GUL", 
                        "_Name": "古卢"
                    }, 
                    {
                        "_Code": "HOI", 
                        "_Name": "霍伊马"
                    }, 
                    {
                        "_Code": "IGA", 
                        "_Name": "伊甘加"
                    }, 
                    {
                        "_Code": "JIN", 
                        "_Name": "金贾"
                    }, 
                    {
                        "_Code": "KAB", 
                        "_Name": "卡贝拉马伊多"
                    }, 
                    {
                        "_Code": "KAL", 
                        "_Name": "卡兰加拉"
                    }, 
                    {
                        "_Code": "KAM", 
                        "_Name": "卡姆文盖"
                    }, 
                    {
                        "_Code": "KAN", 
                        "_Name": "卡农古"
                    }, 
                    {
                        "_Code": "KAR", 
                        "_Name": "卡巴罗莱"
                    }, 
                    {
                        "_Code": "KAS", 
                        "_Name": "卡塞塞"
                    }, 
                    {
                        "_Code": "KAY", 
                        "_Name": "卡永加"
                    }, 
                    {
                        "_Code": "KBA", 
                        "_Name": "基巴莱"
                    }, 
                    {
                        "_Code": "KBL", 
                        "_Name": "卡巴莱"
                    }, 
                    {
                        "_Code": "KIB", 
                        "_Name": "基博加"
                    }, 
                    {
                        "_Code": "KIS", 
                        "_Name": "基索罗"
                    }, 
                    {
                        "_Code": "KIT", 
                        "_Name": "基特古姆"
                    }, 
                    {
                        "_Code": "KML", 
                        "_Name": "卡穆利"
                    }, 
                    {
                        "_Code": "KMP", 
                        "_Name": "坎帕拉"
                    }, 
                    {
                        "_Code": "KOT", 
                        "_Name": "科蒂多"
                    }, 
                    {
                        "_Code": "KPC", 
                        "_Name": "卡普乔鲁瓦"
                    }, 
                    {
                        "_Code": "KTK", 
                        "_Name": "卡塔奎"
                    }, 
                    {
                        "_Code": "KUM", 
                        "_Name": "库米"
                    }, 
                    {
                        "_Code": "KYE", 
                        "_Name": "基恩乔乔"
                    }, 
                    {
                        "_Code": "LIR", 
                        "_Name": "利拉"
                    }, 
                    {
                        "_Code": "LUW", 
                        "_Name": "卢韦罗"
                    }, 
                    {
                        "_Code": "MAS", 
                        "_Name": "马萨卡"
                    }, 
                    {
                        "_Code": "MAY", 
                        "_Name": "马尤盖"
                    }, 
                    {
                        "_Code": "MBA", 
                        "_Name": "姆巴莱"
                    }, 
                    {
                        "_Code": "MBR", 
                        "_Name": "姆巴拉拉"
                    }, 
                    {
                        "_Code": "MOY", 
                        "_Name": "莫约"
                    }, 
                    {
                        "_Code": "MPI", 
                        "_Name": "姆皮吉"
                    }, 
                    {
                        "_Code": "MRT", 
                        "_Name": "莫罗托"
                    }, 
                    {
                        "_Code": "MSN", 
                        "_Name": "马辛迪"
                    }, 
                    {
                        "_Code": "MUB", 
                        "_Name": "穆本德"
                    }, 
                    {
                        "_Code": "MUK", 
                        "_Name": "穆科诺"
                    }, 
                    {
                        "_Code": "NAK", 
                        "_Name": "纳卡皮里皮里特"
                    }, 
                    {
                        "_Code": "NEB", 
                        "_Name": "内比"
                    }, 
                    {
                        "_Code": "NKS", 
                        "_Name": "纳卡松戈拉"
                    }, 
                    {
                        "_Code": "NTU", 
                        "_Name": "恩通加莫"
                    }, 
                    {
                        "_Code": "PAD", 
                        "_Name": "帕德尔"
                    }, 
                    {
                        "_Code": "PAL", 
                        "_Name": "帕利萨"
                    }, 
                    {
                        "_Code": "RAK", 
                        "_Name": "拉卡伊"
                    }, 
                    {
                        "_Code": "RUK", 
                        "_Name": "鲁昆吉里"
                    }, 
                    {
                        "_Code": "SEM", 
                        "_Name": "森巴布莱"
                    }, 
                    {
                        "_Code": "SIR", 
                        "_Name": "锡龙科"
                    }, 
                    {
                        "_Code": "SOR", 
                        "_Name": "索罗提"
                    }, 
                    {
                        "_Code": "TOR", 
                        "_Name": "托罗罗"
                    }, 
                    {
                        "_Code": "WAK", 
                        "_Name": "瓦基索"
                    }, 
                    {
                        "_Code": "YUM", 
                        "_Name": "永贝"
                    }
                ]
            }
        }, 
        {
            "_Code": "UKR", 
            "_Name": "乌克兰", 
            "State": {
                "City": [
                    {
                        "_Code": "12", 
                        "_Name": "第聂伯罗波得罗夫斯克"
                    }, 
                    {
                        "_Code": "14", 
                        "_Name": "顿涅茨克"
                    }, 
                    {
                        "_Code": "18", 
                        "_Name": "日托米尔"
                    }, 
                    {
                        "_Code": "21", 
                        "_Name": "外喀尔巴阡"
                    }, 
                    {
                        "_Code": "23", 
                        "_Name": "扎波罗热"
                    }, 
                    {
                        "_Code": "26", 
                        "_Name": "伊万－弗兰科夫州"
                    }, 
                    {
                        "_Code": "30", 
                        "_Name": "基辅"
                    }, 
                    {
                        "_Code": "35", 
                        "_Name": "基洛夫格勒"
                    }, 
                    {
                        "_Code": "43", 
                        "_Name": "克里米亚自治共和国"
                    }, 
                    {
                        "_Code": "46", 
                        "_Name": "利沃夫"
                    }, 
                    {
                        "_Code": "48", 
                        "_Name": "尼古拉耶夫"
                    }, 
                    {
                        "_Code": "5", 
                        "_Name": "文尼察"
                    }, 
                    {
                        "_Code": "51", 
                        "_Name": "敖德萨"
                    }, 
                    {
                        "_Code": "53", 
                        "_Name": "波尔塔瓦"
                    }, 
                    {
                        "_Code": "56", 
                        "_Name": "罗夫诺"
                    }, 
                    {
                        "_Code": "59", 
                        "_Name": "苏梅"
                    }, 
                    {
                        "_Code": "61", 
                        "_Name": "捷尔诺波尔"
                    }, 
                    {
                        "_Code": "63", 
                        "_Name": "哈尔科夫"
                    }, 
                    {
                        "_Code": "65", 
                        "_Name": "赫尔松州"
                    }, 
                    {
                        "_Code": "68", 
                        "_Name": "赫梅利尼茨基"
                    }, 
                    {
                        "_Code": "7", 
                        "_Name": "沃伦"
                    }, 
                    {
                        "_Code": "71", 
                        "_Name": "切尔卡瑟"
                    }, 
                    {
                        "_Code": "74", 
                        "_Name": "切尔尼戈夫"
                    }, 
                    {
                        "_Code": "77", 
                        "_Name": "切尔诺夫策"
                    }, 
                    {
                        "_Code": "9", 
                        "_Name": "卢甘斯克"
                    }
                ]
            }
        }, 
        {
            "_Code": "UMI", 
            "_Name": "美属外岛"
        }, 
        {
            "_Code": "URY", 
            "_Name": "乌拉圭", 
            "State": {
                "City": [
                    {
                        "_Code": "AR", 
                        "_Name": "阿蒂加斯"
                    }, 
                    {
                        "_Code": "CA", 
                        "_Name": "卡内洛内斯"
                    }, 
                    {
                        "_Code": "CL", 
                        "_Name": "塞罗拉尔戈"
                    }, 
                    {
                        "_Code": "CO", 
                        "_Name": "科洛尼亚"
                    }, 
                    {
                        "_Code": "DU", 
                        "_Name": "杜拉斯诺"
                    }, 
                    {
                        "_Code": "FA", 
                        "_Name": "佛罗里达"
                    }, 
                    {
                        "_Code": "FS", 
                        "_Name": "弗洛雷斯"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "拉瓦耶哈"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马尔多纳多"
                    }, 
                    {
                        "_Code": "MO", 
                        "_Name": "蒙得维的亚"
                    }, 
                    {
                        "_Code": "PA", 
                        "_Name": "派桑杜"
                    }, 
                    {
                        "_Code": "RN", 
                        "_Name": "内格罗河"
                    }, 
                    {
                        "_Code": "RO", 
                        "_Name": "罗恰"
                    }, 
                    {
                        "_Code": "RV", 
                        "_Name": "里韦拉"
                    }, 
                    {
                        "_Code": "SJ", 
                        "_Name": "圣何塞"
                    }, 
                    {
                        "_Code": "SL", 
                        "_Name": "萨尔托"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "索里亚诺"
                    }, 
                    {
                        "_Code": "TAW", 
                        "_Name": "塔夸伦博"
                    }, 
                    {
                        "_Code": "TT", 
                        "_Name": "三十三人"
                    }
                ]
            }
        }, 
        {
            "_Code": "USA", 
            "_Name": "美国", 
            "State": [
                {
                    "City": [
                        {
                            "_Code": "ANC", 
                            "_Name": "安克雷奇"
                        }, 
                        {
                            "_Code": "FAI", 
                            "_Name": "费尔班克斯"
                        }, 
                        {
                            "_Code": "JNU", 
                            "_Name": "朱诺"
                        }
                    ], 
                    "_Code": "AK", 
                    "_Name": "阿拉斯加"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BHM", 
                            "_Name": "伯明罕"
                        }, 
                        {
                            "_Code": "MGM", 
                            "_Name": "蒙哥马利"
                        }, 
                        {
                            "_Code": "MOB", 
                            "_Name": "莫比尔"
                        }
                    ], 
                    "_Code": "AL", 
                    "_Name": "阿拉巴马"
                }, 
                {
                    "City": [
                        {
                            "_Code": "FSM", 
                            "_Name": "史密斯堡"
                        }, 
                        {
                            "_Code": "FYV", 
                            "_Name": "费耶特维尔"
                        }, 
                        {
                            "_Code": "LIT", 
                            "_Name": "小石城"
                        }
                    ], 
                    "_Code": "AR", 
                    "_Name": "阿肯色"
                }, 
                {
                    "City": [
                        {
                            "_Code": "GDA", 
                            "_Name": "格兰代尔"
                        }, 
                        {
                            "_Code": "MQA", 
                            "_Name": "梅萨"
                        }, 
                        {
                            "_Code": "PHX", 
                            "_Name": "凤凰城"
                        }, 
                        {
                            "_Code": "STZ", 
                            "_Name": "史卡兹代尔"
                        }, 
                        {
                            "_Code": "TPE", 
                            "_Name": "坦普"
                        }, 
                        {
                            "_Code": "TUC", 
                            "_Name": "图森"
                        }, 
                        {
                            "_Code": "YUM", 
                            "_Name": "优玛"
                        }
                    ], 
                    "_Code": "AZ", 
                    "_Name": "亚利桑那"
                }, 
                {
                    "City": [
                        {
                            "_Code": "LAX", 
                            "_Name": "洛杉矶"
                        }, 
                        {
                            "_Code": "SAN", 
                            "_Name": "圣迭戈"
                        }, 
                        {
                            "_Code": "SFO", 
                            "_Name": "旧金山"
                        }, 
                        {
                            "_Code": "SJC", 
                            "_Name": "圣何塞"
                        }
                    ], 
                    "_Code": "CA", 
                    "_Name": "加利福尼亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ASE", 
                            "_Name": "阿斯彭"
                        }, 
                        {
                            "_Code": "AUX", 
                            "_Name": "奥罗拉"
                        }, 
                        {
                            "_Code": "COS", 
                            "_Name": "科罗拉多斯普林斯"
                        }, 
                        {
                            "_Code": "DEN", 
                            "_Name": "丹佛"
                        }, 
                        {
                            "_Code": "FNL", 
                            "_Name": "柯林斯堡"
                        }, 
                        {
                            "_Code": "GJT", 
                            "_Name": "大章克申"
                        }, 
                        {
                            "_Code": "VAC", 
                            "_Name": "韦尔"
                        }, 
                        {
                            "_Code": "WBU", 
                            "_Name": "博尔德"
                        }
                    ], 
                    "_Code": "CO", 
                    "_Name": "科罗拉多"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BDR", 
                            "_Name": "布里奇波特"
                        }, 
                        {
                            "_Code": "DAQ", 
                            "_Name": "达里恩"
                        }, 
                        {
                            "_Code": "GRH", 
                            "_Name": "格林尼治"
                        }, 
                        {
                            "_Code": "HFD", 
                            "_Name": "哈特福德"
                        }, 
                        {
                            "_Code": "HVN", 
                            "_Name": "纽黑文"
                        }, 
                        {
                            "_Code": "XIN", 
                            "_Name": "米德尔顿"
                        }, 
                        {
                            "_Code": "NWT", 
                            "_Name": "新不列颠"
                        }, 
                        {
                            "_Code": "WAT", 
                            "_Name": "沃特伯里"
                        }, 
                        {
                            "_Code": "WPT", 
                            "_Name": "韦斯特波特"
                        }
                    ], 
                    "_Code": "CT", 
                    "_Name": "康涅狄格"
                }, 
                {
                    "City": {
                        "_Code": "WAS", 
                        "_Name": "华盛顿哥伦比亚特区"
                    }, 
                    "_Code": "DC", 
                    "_Name": "哥伦比亚特区"
                }, 
                {
                    "City": [
                        {
                            "_Code": "DOR", 
                            "_Name": "多佛"
                        }, 
                        {
                            "_Code": "ILG", 
                            "_Name": "威明顿"
                        }, 
                        {
                            "_Code": "NWK", 
                            "_Name": "纽瓦克"
                        }
                    ], 
                    "_Code": "DE", 
                    "_Name": "特拉华"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CPV", 
                            "_Name": "卡纳维尔角"
                        }, 
                        {
                            "_Code": "EYW", 
                            "_Name": "基韦斯特"
                        }, 
                        {
                            "_Code": "FLL", 
                            "_Name": "罗德岱堡"
                        }, 
                        {
                            "_Code": "JAX", 
                            "_Name": "杰克逊维尔"
                        }, 
                        {
                            "_Code": "MIA", 
                            "_Name": "迈阿密"
                        }, 
                        {
                            "_Code": "ORL", 
                            "_Name": "奥兰多"
                        }, 
                        {
                            "_Code": "PIE", 
                            "_Name": "圣彼德斯堡市"
                        }, 
                        {
                            "_Code": "TLH", 
                            "_Name": "塔拉哈西"
                        }, 
                        {
                            "_Code": "TPA", 
                            "_Name": "坦帕"
                        }
                    ], 
                    "_Code": "FL", 
                    "_Name": "佛罗里达"
                }, 
                {
                    "City": [
                        {
                            "_Code": "AUT", 
                            "_Name": "奥古斯塔"
                        }, 
                        {
                            "_Code": "CZX", 
                            "_Name": "哥伦布"
                        }, 
                        {
                            "_Code": "MCN", 
                            "_Name": "梅肯"
                        }, 
                        {
                            "_Code": "SAV", 
                            "_Name": "沙瓦纳"
                        }, 
                        {
                            "_Code": "TAT", 
                            "_Name": "亚特兰大"
                        }
                    ], 
                    "_Code": "GA", 
                    "_Name": "佐治亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "HNL", 
                            "_Name": "檀香山"
                        }, 
                        {
                            "_Code": "ITO", 
                            "_Name": "希洛"
                        }, 
                        {
                            "_Code": "KHH", 
                            "_Name": "凯卢阿"
                        }
                    ], 
                    "_Code": "HI", 
                    "_Name": "夏威夷"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CID", 
                            "_Name": "锡达拉皮兹"
                        }, 
                        {
                            "_Code": "DSM", 
                            "_Name": "得梅因"
                        }, 
                        {
                            "_Code": "DVN", 
                            "_Name": "达文波特"
                        }
                    ], 
                    "_Code": "IA", 
                    "_Name": "爱荷华"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BLK", 
                            "_Name": "布莱克富特"
                        }, 
                        {
                            "_Code": "BOI", 
                            "_Name": "博伊西"
                        }, 
                        {
                            "_Code": "COE", 
                            "_Name": "科达伦"
                        }, 
                        {
                            "_Code": "IDA", 
                            "_Name": "爱达荷福尔斯"
                        }, 
                        {
                            "_Code": "QKM", 
                            "_Name": "岂彻姆"
                        }, 
                        {
                            "_Code": "LWS", 
                            "_Name": "刘易斯顿"
                        }, 
                        {
                            "_Code": "MJL", 
                            "_Name": "莫斯科"
                        }, 
                        {
                            "_Code": "NPA", 
                            "_Name": "楠帕"
                        }, 
                        {
                            "_Code": "PIH", 
                            "_Name": "波卡特洛"
                        }, 
                        {
                            "_Code": "SVY", 
                            "_Name": "森瓦利"
                        }, 
                        {
                            "_Code": "YAF", 
                            "_Name": "亚美利加瀑布城"
                        }, 
                        {
                            "_Code": "ZMU", 
                            "_Name": "墨菲"
                        }
                    ], 
                    "_Code": "ID", 
                    "_Name": "爱达荷"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ALN", 
                            "_Name": "奥尔顿"
                        }, 
                        {
                            "_Code": "AUZ", 
                            "_Name": "奥罗拉"
                        }, 
                        {
                            "_Code": "BLO", 
                            "_Name": "布卢明顿"
                        }, 
                        {
                            "_Code": "CHI", 
                            "_Name": "芝加哥"
                        }, 
                        {
                            "_Code": "CMI", 
                            "_Name": "厄巴纳-香槟"
                        }, 
                        {
                            "_Code": "CRA", 
                            "_Name": "森特勒利亚"
                        }, 
                        {
                            "_Code": "DEC", 
                            "_Name": "迪凯持"
                        }, 
                        {
                            "_Code": "DEK", 
                            "_Name": "迪卡尔布"
                        }, 
                        {
                            "_Code": "DVI", 
                            "_Name": "丹维尓"
                        }, 
                        {
                            "_Code": "ESL", 
                            "_Name": "东圣路易斯"
                        }, 
                        {
                            "_Code": "GSU", 
                            "_Name": "盖尔斯堡"
                        }, 
                        {
                            "_Code": "MDH", 
                            "_Name": "卡本代尔"
                        }, 
                        {
                            "_Code": "NOM", 
                            "_Name": "诺黙尔"
                        }, 
                        {
                            "_Code": "PLA", 
                            "_Name": "皮奥里亚"
                        }, 
                        {
                            "_Code": "RFD", 
                            "_Name": "罗克福德"
                        }, 
                        {
                            "_Code": "RKI", 
                            "_Name": "罗克艾兰"
                        }, 
                        {
                            "_Code": "SPI", 
                            "_Name": "斯普林菲尔德"
                        }, 
                        {
                            "_Code": "UGN", 
                            "_Name": "沃其根"
                        }
                    ], 
                    "_Code": "IL", 
                    "_Name": "伊利诺斯"
                }, 
                {
                    "City": [
                        {
                            "_Code": "EVV", 
                            "_Name": "埃文斯维尔"
                        }, 
                        {
                            "_Code": "FWA", 
                            "_Name": "韦恩堡"
                        }, 
                        {
                            "_Code": "IND", 
                            "_Name": "印第安纳波利斯"
                        }
                    ], 
                    "_Code": "IN", 
                    "_Name": "印第安那"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ABZ", 
                            "_Name": "阿比林"
                        }, 
                        {
                            "_Code": "HCH", 
                            "_Name": "哈钦森"
                        }, 
                        {
                            "_Code": "XIA", 
                            "_Name": "莱文沃思"
                        }, 
                        {
                            "_Code": "ICT", 
                            "_Name": "威奇托"
                        }, 
                        {
                            "_Code": "KCK", 
                            "_Name": "堪萨斯城"
                        }, 
                        {
                            "_Code": "LWC", 
                            "_Name": "劳伦斯"
                        }, 
                        {
                            "_Code": "MHK", 
                            "_Name": "曼哈顿"
                        }, 
                        {
                            "_Code": "OVL", 
                            "_Name": "奥弗兰公园"
                        }, 
                        {
                            "_Code": "TOP", 
                            "_Name": "托皮卡"
                        }
                    ], 
                    "_Code": "KS", 
                    "_Name": "堪萨斯"
                }, 
                {
                    "City": [
                        {
                            "_Code": "LEX", 
                            "_Name": "列克星敦"
                        }, 
                        {
                            "_Code": "LUI", 
                            "_Name": "路易斯维尔"
                        }, 
                        {
                            "_Code": "OWB", 
                            "_Name": "欧文斯伯勒"
                        }
                    ], 
                    "_Code": "KY", 
                    "_Name": "肯塔基"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BTR", 
                            "_Name": "巴吞鲁日"
                        }, 
                        {
                            "_Code": "MSY", 
                            "_Name": "新奥尔良"
                        }, 
                        {
                            "_Code": "SHV", 
                            "_Name": "什里夫波特"
                        }
                    ], 
                    "_Code": "LA", 
                    "_Name": "路易斯安那"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BZD", 
                            "_Name": "波士顿"
                        }, 
                        {
                            "_Code": "ORH", 
                            "_Name": "伍斯特"
                        }, 
                        {
                            "_Code": "SFY", 
                            "_Name": "斯普林菲尔德"
                        }
                    ], 
                    "_Code": "MA", 
                    "_Name": "马萨诸塞"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BAL", 
                            "_Name": "巴尔的摩"
                        }, 
                        {
                            "_Code": "GAI", 
                            "_Name": "盖瑟斯堡"
                        }, 
                        {
                            "_Code": "RKV", 
                            "_Name": "罗克维尔"
                        }
                    ], 
                    "_Code": "MD", 
                    "_Name": "马里兰"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BNQ", 
                            "_Name": "班戈"
                        }, 
                        {
                            "_Code": "QLW", 
                            "_Name": "刘易斯顿"
                        }, 
                        {
                            "_Code": "POL", 
                            "_Name": "波特兰"
                        }
                    ], 
                    "_Code": "ME", 
                    "_Name": "缅因"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ARB", 
                            "_Name": "安娜堡"
                        }, 
                        {
                            "_Code": "AZO", 
                            "_Name": "卡拉马袓"
                        }, 
                        {
                            "_Code": "BCY", 
                            "_Name": "贝城"
                        }, 
                        {
                            "_Code": "BTL", 
                            "_Name": "巴特尔克里克"
                        }, 
                        {
                            "_Code": "DEO", 
                            "_Name": "迪尔伯恩"
                        }, 
                        {
                            "_Code": "DET", 
                            "_Name": "底特律"
                        }, 
                        {
                            "_Code": "FNT", 
                            "_Name": "弗林特"
                        }, 
                        {
                            "_Code": "GRR", 
                            "_Name": "大急流城"
                        }, 
                        {
                            "_Code": "LAN", 
                            "_Name": "兰辛"
                        }, 
                        {
                            "_Code": "MKG", 
                            "_Name": "马斯基根"
                        }, 
                        {
                            "_Code": "PHN", 
                            "_Name": "休伦港"
                        }, 
                        {
                            "_Code": "PTK", 
                            "_Name": "庞菷亚克"
                        }, 
                        {
                            "_Code": "SGM", 
                            "_Name": "萨吉诺"
                        }, 
                        {
                            "_Code": "SSM", 
                            "_Name": "苏圣玛丽"
                        }, 
                        {
                            "_Code": "WAM", 
                            "_Name": "沃伦"
                        }, 
                        {
                            "_Code": "WYD", 
                            "_Name": "怀恩多特"
                        }
                    ], 
                    "_Code": "MI", 
                    "_Name": "密歇根"
                }, 
                {
                    "City": [
                        {
                            "_Code": "MES", 
                            "_Name": "明尼阿波利斯"
                        }, 
                        {
                            "_Code": "RST", 
                            "_Name": "罗切斯特"
                        }, 
                        {
                            "_Code": "STP", 
                            "_Name": "圣保罗"
                        }
                    ], 
                    "_Code": "MN", 
                    "_Name": "明尼苏达"
                }, 
                {
                    "City": [
                        {
                            "_Code": "COV", 
                            "_Name": "哥伦比亚"
                        }, 
                        {
                            "_Code": "JEF", 
                            "_Name": "杰佛逊市"
                        }, 
                        {
                            "_Code": "MKC", 
                            "_Name": "堪萨斯城"
                        }, 
                        {
                            "_Code": "SGF", 
                            "_Name": "斯普林菲尔德"
                        }, 
                        {
                            "_Code": "STL", 
                            "_Name": "圣路易斯"
                        }
                    ], 
                    "_Code": "MO", 
                    "_Name": "密苏里"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BIX", 
                            "_Name": "比洛克西"
                        }, 
                        {
                            "_Code": "GLH", 
                            "_Name": "格林维尔"
                        }, 
                        {
                            "_Code": "GPT", 
                            "_Name": "格尔夫波特"
                        }, 
                        {
                            "_Code": "HBG", 
                            "_Name": "哈蒂斯堡"
                        }, 
                        {
                            "_Code": "JAN", 
                            "_Name": "杰克逊"
                        }, 
                        {
                            "_Code": "MEI", 
                            "_Name": "默里迪恩"
                        }, 
                        {
                            "_Code": "VKS", 
                            "_Name": "维克斯堡"
                        }
                    ], 
                    "_Code": "MS", 
                    "_Name": "密西西比"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BGS", 
                            "_Name": "比灵斯"
                        }, 
                        {
                            "_Code": "GTF", 
                            "_Name": "大瀑布村"
                        }, 
                        {
                            "_Code": "MSO", 
                            "_Name": "米苏拉"
                        }
                    ], 
                    "_Code": "MT", 
                    "_Name": "蒙大拿"
                }, 
                {
                    "City": [
                        {
                            "_Code": "AEV", 
                            "_Name": "艾许维尔"
                        }, 
                        {
                            "_Code": "CHE", 
                            "_Name": "教堂山"
                        }, 
                        {
                            "_Code": "CRQ", 
                            "_Name": "夏洛特"
                        }, 
                        {
                            "_Code": "DHH", 
                            "_Name": "杜罕"
                        }, 
                        {
                            "_Code": "GBO", 
                            "_Name": "格林斯伯勒"
                        }, 
                        {
                            "_Code": "RAG", 
                            "_Name": "罗利"
                        }, 
                        {
                            "_Code": "RDU", 
                            "_Name": "洛利杜罕都会区"
                        }
                    ], 
                    "_Code": "NC", 
                    "_Name": "北卡罗来纳"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BIS", 
                            "_Name": "俾斯麦"
                        }, 
                        {
                            "_Code": "FAR", 
                            "_Name": "法戈"
                        }, 
                        {
                            "_Code": "GFK", 
                            "_Name": "大福克斯"
                        }, 
                        {
                            "_Code": "MOT", 
                            "_Name": "迈诺特"
                        }
                    ], 
                    "_Code": "ND", 
                    "_Name": "北达科他"
                }, 
                {
                    "City": [
                        {
                            "_Code": "XDE", 
                            "_Name": "贝尔维尤"
                        }, 
                        {
                            "_Code": "LNK", 
                            "_Name": "林肯"
                        }, 
                        {
                            "_Code": "OMA", 
                            "_Name": "奥马哈"
                        }
                    ], 
                    "_Code": "NE", 
                    "_Name": "内布拉斯加"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ASH", 
                            "_Name": "纳舒厄"
                        }, 
                        {
                            "_Code": "CON", 
                            "_Name": "康科德"
                        }, 
                        {
                            "_Code": "MHT", 
                            "_Name": "曼彻斯特"
                        }
                    ], 
                    "_Code": "NH", 
                    "_Name": "新罕布什尔"
                }, 
                {
                    "City": [
                        {
                            "_Code": "JEC", 
                            "_Name": "泽西城"
                        }, 
                        {
                            "_Code": "NRK", 
                            "_Name": "纽瓦克"
                        }, 
                        {
                            "_Code": "PAT", 
                            "_Name": "帕特森"
                        }
                    ], 
                    "_Code": "NJ", 
                    "_Name": "新泽西"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ABQ", 
                            "_Name": "阿尔伯克基"
                        }, 
                        {
                            "_Code": "LRU", 
                            "_Name": "拉斯克鲁塞斯"
                        }, 
                        {
                            "_Code": "ROW", 
                            "_Name": "罗斯韦尔"
                        }, 
                        {
                            "_Code": "SAF", 
                            "_Name": "圣菲"
                        }
                    ], 
                    "_Code": "NM", 
                    "_Name": "新墨西哥"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CSN", 
                            "_Name": "卡森城"
                        }, 
                        {
                            "_Code": "EKO", 
                            "_Name": "埃尔科"
                        }, 
                        {
                            "_Code": "HNZ", 
                            "_Name": "亨德森"
                        }, 
                        {
                            "_Code": "LAS", 
                            "_Name": "拉斯维加斯"
                        }, 
                        {
                            "_Code": "NVS", 
                            "_Name": "北拉斯维加斯"
                        }, 
                        {
                            "_Code": "RNO", 
                            "_Name": "里诺"
                        }, 
                        {
                            "_Code": "SPK", 
                            "_Name": "斯帕克斯"
                        }, 
                        {
                            "_Code": "VGI", 
                            "_Name": "弗吉尼亚城"
                        }
                    ], 
                    "_Code": "NV", 
                    "_Name": "内华达"
                }, 
                {
                    "City": [
                        {
                            "_Code": "FFO", 
                            "_Name": "布法罗"
                        }, 
                        {
                            "_Code": "QEE", 
                            "_Name": "纽约市"
                        }, 
                        {
                            "_Code": "ROC", 
                            "_Name": "罗切斯特"
                        }
                    ], 
                    "_Code": "NY", 
                    "_Name": "纽约"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CLE", 
                            "_Name": "克利夫兰"
                        }, 
                        {
                            "_Code": "CVG", 
                            "_Name": "辛辛那提"
                        }, 
                        {
                            "_Code": "CZX", 
                            "_Name": "哥伦布"
                        }, 
                        {
                            "_Code": "DYT", 
                            "_Name": "代顿"
                        }, 
                        {
                            "_Code": "TOL", 
                            "_Name": "托莱多"
                        }
                    ], 
                    "_Code": "OH", 
                    "_Name": "俄亥俄"
                }, 
                {
                    "City": [
                        {
                            "_Code": "OKC", 
                            "_Name": "俄克拉荷马城"
                        }, 
                        {
                            "_Code": "OUN", 
                            "_Name": "诺曼"
                        }, 
                        {
                            "_Code": "TUL", 
                            "_Name": "塔尔萨"
                        }
                    ], 
                    "_Code": "OK", 
                    "_Name": "俄克拉荷马"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BZO", 
                            "_Name": "本德"
                        }, 
                        {
                            "_Code": "COB", 
                            "_Name": "库斯贝"
                        }, 
                        {
                            "_Code": "CTR", 
                            "_Name": "火山口湖"
                        }, 
                        {
                            "_Code": "DAC", 
                            "_Name": "达拉斯"
                        }, 
                        {
                            "_Code": "DLS", 
                            "_Name": "达尔斯"
                        }, 
                        {
                            "_Code": "EUG", 
                            "_Name": "尤金"
                        }, 
                        {
                            "_Code": "HDX", 
                            "_Name": "胡德里弗"
                        }, 
                        {
                            "_Code": "XFX", 
                            "_Name": "格兰茨帕斯"
                        }, 
                        {
                            "_Code": "MFR", 
                            "_Name": "梅德福"
                        }, 
                        {
                            "_Code": "PDX", 
                            "_Name": "波特兰"
                        }, 
                        {
                            "_Code": "SLE", 
                            "_Name": "塞勒姆"
                        }, 
                        {
                            "_Code": "SPY", 
                            "_Name": "斯普林菲尔德"
                        }, 
                        {
                            "_Code": "STH", 
                            "_Name": "圣海伦斯"
                        }, 
                        {
                            "_Code": "TLM", 
                            "_Name": "蒂拉穆克"
                        }, 
                        {
                            "_Code": "YCV", 
                            "_Name": "科瓦利斯"
                        }
                    ], 
                    "_Code": "OR", 
                    "_Name": "俄勒冈"
                }, 
                {
                    "City": [
                        {
                            "_Code": "AEW", 
                            "_Name": "阿伦敦"
                        }, 
                        {
                            "_Code": "PHL", 
                            "_Name": "费城"
                        }, 
                        {
                            "_Code": "PIT", 
                            "_Name": "匹兹堡"
                        }
                    ], 
                    "_Code": "PA", 
                    "_Name": "宾夕法尼亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CQH", 
                            "_Name": "克兰斯顿"
                        }, 
                        {
                            "_Code": "NPO", 
                            "_Name": "纽波特"
                        }, 
                        {
                            "_Code": "PAW", 
                            "_Name": "波塔基特"
                        }, 
                        {
                            "_Code": "PVD", 
                            "_Name": "普罗维登斯"
                        }, 
                        {
                            "_Code": "SFN", 
                            "_Name": "文索基特"
                        }, 
                        {
                            "_Code": "UZO", 
                            "_Name": "沃威克"
                        }, 
                        {
                            "_Code": "WST", 
                            "_Name": "韦斯特利"
                        }
                    ], 
                    "_Code": "RI", 
                    "_Name": "罗德岛"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CHS", 
                            "_Name": "查尔斯顿"
                        }, 
                        {
                            "_Code": "COV", 
                            "_Name": "哥伦比亚"
                        }, 
                        {
                            "_Code": "NTS", 
                            "_Name": "北查尔斯顿"
                        }
                    ], 
                    "_Code": "SC", 
                    "_Name": "南卡罗来纳"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ABK", 
                            "_Name": "阿伯丁"
                        }, 
                        {
                            "_Code": "FSD", 
                            "_Name": "苏福尔斯"
                        }, 
                        {
                            "_Code": "RAP", 
                            "_Name": "拉皮德城"
                        }
                    ], 
                    "_Code": "SD", 
                    "_Name": "南达科他"
                }, 
                {
                    "City": [
                        {
                            "_Code": "AUS", 
                            "_Name": "奥斯汀"
                        }, 
                        {
                            "_Code": "CRP", 
                            "_Name": "哥帕斯基斯蒂"
                        }, 
                        {
                            "_Code": "DAL", 
                            "_Name": "达拉斯"
                        }, 
                        {
                            "_Code": "ELP", 
                            "_Name": "埃尔帕索"
                        }, 
                        {
                            "_Code": "GLS", 
                            "_Name": "交维斯顿"
                        }, 
                        {
                            "_Code": "HOU", 
                            "_Name": "休斯敦"
                        }, 
                        {
                            "_Code": "LRD", 
                            "_Name": "拉雷多"
                        }, 
                        {
                            "_Code": "SAT", 
                            "_Name": "圣安东尼奥"
                        }, 
                        {
                            "_Code": "TXC", 
                            "_Name": "麦亚伦"
                        }
                    ], 
                    "_Code": "TX", 
                    "_Name": "德克萨斯"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BNA", 
                            "_Name": "纳什维尔"
                        }, 
                        {
                            "_Code": "BSJ", 
                            "_Name": "布利斯托"
                        }, 
                        {
                            "_Code": "CHA", 
                            "_Name": "查塔努加"
                        }, 
                        {
                            "_Code": "JCY", 
                            "_Name": "约翰逊城"
                        }, 
                        {
                            "_Code": "MEM", 
                            "_Name": "孟菲斯"
                        }, 
                        {
                            "_Code": "MQY", 
                            "_Name": "士麦那"
                        }, 
                        {
                            "_Code": "RGI", 
                            "_Name": "斯普林希尔"
                        }, 
                        {
                            "_Code": "TRI", 
                            "_Name": "金斯波特"
                        }, 
                        {
                            "_Code": "TYS", 
                            "_Name": "诺克斯维尔"
                        }, 
                        {
                            "_Code": "YTC", 
                            "_Name": "三城区"
                        }
                    ], 
                    "_Code": "TN", 
                    "_Name": "田纳西"
                }, 
                {
                    "City": [
                        {
                            "_Code": "LTJ", 
                            "_Name": "雷登"
                        }, 
                        {
                            "_Code": "OEU", 
                            "_Name": "欧仁"
                        }, 
                        {
                            "_Code": "OGD", 
                            "_Name": "奥格登"
                        }, 
                        {
                            "_Code": "PAC", 
                            "_Name": "帕克城"
                        }, 
                        {
                            "_Code": "PVU", 
                            "_Name": "普罗沃"
                        }, 
                        {
                            "_Code": "SGU", 
                            "_Name": "圣乔治"
                        }, 
                        {
                            "_Code": "SLC", 
                            "_Name": "盐湖城"
                        }, 
                        {
                            "_Code": "WVC", 
                            "_Name": "西瓦利城"
                        }
                    ], 
                    "_Code": "UT", 
                    "_Name": "犹他"
                }, 
                {
                    "City": [
                        {
                            "_Code": "HTW", 
                            "_Name": "切萨皮克"
                        }, 
                        {
                            "_Code": "ORF", 
                            "_Name": "诺福克"
                        }, 
                        {
                            "_Code": "VAB", 
                            "_Name": "弗吉尼亚比奇"
                        }
                    ], 
                    "_Code": "VA", 
                    "_Name": "维吉尼亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "BTV", 
                            "_Name": "伯灵顿"
                        }, 
                        {
                            "_Code": "RUT", 
                            "_Name": "拉特兰"
                        }, 
                        {
                            "_Code": "ZBR", 
                            "_Name": "南伯灵顿"
                        }
                    ], 
                    "_Code": "VT", 
                    "_Name": "佛蒙特"
                }, 
                {
                    "City": [
                        {
                            "_Code": "GEG", 
                            "_Name": "斯波坎"
                        }, 
                        {
                            "_Code": "SEA", 
                            "_Name": "西雅图"
                        }, 
                        {
                            "_Code": "TTW", 
                            "_Name": "塔科马"
                        }
                    ], 
                    "_Code": "WA", 
                    "_Name": "华盛顿"
                }, 
                {
                    "City": [
                        {
                            "_Code": "ATW", 
                            "_Name": "阿普尓顿"
                        }, 
                        {
                            "_Code": "AUW", 
                            "_Name": "沃索"
                        }, 
                        {
                            "_Code": "EAU", 
                            "_Name": "欧克莱尓"
                        }, 
                        {
                            "_Code": "ENW", 
                            "_Name": "基诺沙"
                        }, 
                        {
                            "_Code": "GBK", 
                            "_Name": "格林贝"
                        }, 
                        {
                            "_Code": "QMD", 
                            "_Name": "迈迪逊"
                        }, 
                        {
                            "_Code": "LSE", 
                            "_Name": "拉克罗斯"
                        }, 
                        {
                            "_Code": "MKE", 
                            "_Name": "密尔沃基"
                        }, 
                        {
                            "_Code": "MTW", 
                            "_Name": "马尼托沃克"
                        }, 
                        {
                            "_Code": "OSH", 
                            "_Name": "奥什科什"
                        }, 
                        {
                            "_Code": "RAC", 
                            "_Name": "拉辛"
                        }, 
                        {
                            "_Code": "SBM", 
                            "_Name": "希博伊根"
                        }
                    ], 
                    "_Code": "WI", 
                    "_Name": "威斯康星"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CRW", 
                            "_Name": "查尔斯顿"
                        }, 
                        {
                            "_Code": "HNU", 
                            "_Name": "亨廷顿"
                        }, 
                        {
                            "_Code": "PKB", 
                            "_Name": "帕克斯堡"
                        }
                    ], 
                    "_Code": "WV", 
                    "_Name": "西佛吉尼亚"
                }, 
                {
                    "City": [
                        {
                            "_Code": "CPR", 
                            "_Name": "卡斯珀"
                        }, 
                        {
                            "_Code": "CYS", 
                            "_Name": "夏延"
                        }, 
                        {
                            "_Code": "EVD", 
                            "_Name": "埃文斯顿"
                        }, 
                        {
                            "_Code": "LAR", 
                            "_Name": "拉勒米"
                        }, 
                        {
                            "_Code": "RKS", 
                            "_Name": "罗克斯普林斯"
                        }, 
                        {
                            "_Code": "SHR", 
                            "_Name": "谢里登"
                        }
                    ], 
                    "_Code": "WY", 
                    "_Name": "怀俄明"
                }
            ]
        }, 
        {
            "_Code": "UZB", 
            "_Name": "乌兹别克斯坦", 
            "State": {
                "City": [
                    {
                        "_Code": "AN", 
                        "_Name": "安集延"
                    }, 
                    {
                        "_Code": "BU", 
                        "_Name": "布哈拉"
                    }, 
                    {
                        "_Code": "FA", 
                        "_Name": "费尔干纳"
                    }, 
                    {
                        "_Code": "XO", 
                        "_Name": "花拉子模"
                    }, 
                    {
                        "_Code": "JI", 
                        "_Name": "吉扎克"
                    }, 
                    {
                        "_Code": "QA", 
                        "_Name": "卡什卡达里亚"
                    }, 
                    {
                        "_Code": "QR", 
                        "_Name": "卡拉卡尔帕克斯坦共和国"
                    }, 
                    {
                        "_Code": "NG", 
                        "_Name": "纳曼干"
                    }, 
                    {
                        "_Code": "NW", 
                        "_Name": "纳沃伊"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "撒马尔罕"
                    }, 
                    {
                        "_Code": "SI", 
                        "_Name": "锡尔河"
                    }, 
                    {
                        "_Code": "SU", 
                        "_Name": "苏尔汉河"
                    }, 
                    {
                        "_Code": "TK", 
                        "_Name": "塔什干"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "塔什干市"
                    }
                ]
            }
        }, 
        {
            "_Code": "VAT", 
            "_Name": "梵蒂冈"
        }, 
        {
            "_Code": "VCT", 
            "_Name": "圣文森特和格林纳丁斯"
        }, 
        {
            "_Code": "VEN", 
            "_Name": "委内瑞拉", 
            "State": {
                "City": [
                    {
                        "_Code": "A", 
                        "_Name": "加拉加斯"
                    }, 
                    {
                        "_Code": "B", 
                        "_Name": "安索阿特吉"
                    }, 
                    {
                        "_Code": "C", 
                        "_Name": "阿普雷"
                    }, 
                    {
                        "_Code": "D", 
                        "_Name": "阿拉瓜"
                    }, 
                    {
                        "_Code": "E", 
                        "_Name": "巴里纳斯"
                    }, 
                    {
                        "_Code": "F", 
                        "_Name": "玻利瓦尔"
                    }, 
                    {
                        "_Code": "G", 
                        "_Name": "卡拉沃沃"
                    }, 
                    {
                        "_Code": "H", 
                        "_Name": "科赫德斯"
                    }, 
                    {
                        "_Code": "I", 
                        "_Name": "法尔孔"
                    }, 
                    {
                        "_Code": "J", 
                        "_Name": "瓜里科"
                    }, 
                    {
                        "_Code": "K", 
                        "_Name": "拉腊"
                    }, 
                    {
                        "_Code": "L", 
                        "_Name": "梅里达"
                    }, 
                    {
                        "_Code": "M", 
                        "_Name": "米兰达"
                    }, 
                    {
                        "_Code": "N", 
                        "_Name": "莫纳加斯"
                    }, 
                    {
                        "_Code": "O", 
                        "_Name": "新埃斯帕塔"
                    }, 
                    {
                        "_Code": "P", 
                        "_Name": "波图格萨"
                    }, 
                    {
                        "_Code": "R", 
                        "_Name": "苏克雷"
                    }, 
                    {
                        "_Code": "S", 
                        "_Name": "塔奇拉"
                    }, 
                    {
                        "_Code": "T", 
                        "_Name": "特鲁希略"
                    }, 
                    {
                        "_Code": "U", 
                        "_Name": "亚拉奎"
                    }, 
                    {
                        "_Code": "V", 
                        "_Name": "苏利亚"
                    }, 
                    {
                        "_Code": "W", 
                        "_Name": "联邦属地"
                    }, 
                    {
                        "_Code": "Y", 
                        "_Name": "阿马库罗三角洲"
                    }, 
                    {
                        "_Code": "Z", 
                        "_Name": "亚马孙"
                    }
                ]
            }
        }, 
        {
            "_Code": "VGB", 
            "_Name": "维尔京群岛，英属"
        }, 
        {
            "_Code": "VIR", 
            "_Name": "维尔京群岛，美属"
        }, 
        {
            "_Code": "VNM", 
            "_Name": "越南", 
            "State": {
                "City": [
                    {
                        "_Code": "HC", 
                        "_Name": "胡志明市"
                    }, 
                    {
                        "_Code": "HI", 
                        "_Name": "河内"
                    }, 
                    {
                        "_Code": "HP", 
                        "_Name": "海防"
                    }
                ]
            }
        }, 
        {
            "_Code": "VUT", 
            "_Name": "瓦努阿图", 
            "State": {
                "City": [
                    {
                        "_Code": "MA", 
                        "_Name": "马朗帕"
                    }, 
                    {
                        "_Code": "PE", 
                        "_Name": "彭纳马"
                    }, 
                    {
                        "_Code": "SA", 
                        "_Name": "桑马"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "谢法"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔菲阿"
                    }, 
                    {
                        "_Code": "TO", 
                        "_Name": "托尔巴"
                    }
                ]
            }
        }, 
        {
            "_Code": "WLF", 
            "_Name": "瓦利斯和福图纳"
        }, 
        {
            "_Code": "WSM", 
            "_Name": "萨摩亚"
        }, 
        {
            "_Code": "YEM", 
            "_Name": "也门", 
            "State": {
                "City": [
                    {
                        "_Code": "AB", 
                        "_Name": "阿比扬"
                    }, 
                    {
                        "_Code": "AD", 
                        "_Name": "亚丁"
                    }, 
                    {
                        "_Code": "AM", 
                        "_Name": "阿姆兰"
                    }, 
                    {
                        "_Code": "ASR", 
                        "_Name": "希赫尔"
                    }, 
                    {
                        "_Code": "BA", 
                        "_Name": "贝达"
                    }, 
                    {
                        "_Code": "DA", 
                        "_Name": "达利"
                    }, 
                    {
                        "_Code": "DH", 
                        "_Name": "扎玛尔"
                    }, 
                    {
                        "_Code": "GXF", 
                        "_Name": "赛文"
                    }, 
                    {
                        "_Code": "HD", 
                        "_Name": "哈德拉毛"
                    }, 
                    {
                        "_Code": "HJ", 
                        "_Name": "哈杰"
                    }, 
                    {
                        "_Code": "HU", 
                        "_Name": "荷台达"
                    }, 
                    {
                        "_Code": "IB", 
                        "_Name": "伊卜"
                    }, 
                    {
                        "_Code": "JA", 
                        "_Name": "焦夫"
                    }, 
                    {
                        "_Code": "LA", 
                        "_Name": "拉赫季"
                    }, 
                    {
                        "_Code": "MA", 
                        "_Name": "马里卜"
                    }, 
                    {
                        "_Code": "MR", 
                        "_Name": "迈赫拉"
                    }, 
                    {
                        "_Code": "MW", 
                        "_Name": "迈赫维特"
                    }, 
                    {
                        "_Code": "SD", 
                        "_Name": "萨达"
                    }, 
                    {
                        "_Code": "SH", 
                        "_Name": "舍卜沃"
                    }, 
                    {
                        "_Code": "SN", 
                        "_Name": "萨那"
                    }, 
                    {
                        "_Code": "TA", 
                        "_Name": "塔伊兹"
                    }
                ]
            }
        }, 
        {
            "_Code": "ZAF", 
            "_Name": "南非", 
            "State": {
                "City": [
                    {
                        "_Code": "BAE", 
                        "_Name": "东巴克利"
                    }, 
                    {
                        "_Code": "BDD", 
                        "_Name": "布雷达斯多普"
                    }, 
                    {
                        "_Code": "BEW", 
                        "_Name": "西博福特"
                    }, 
                    {
                        "_Code": "BFN", 
                        "_Name": "布隆方丹"
                    }, 
                    {
                        "_Code": "BHT", 
                        "_Name": "布隆克斯特斯普利特"
                    }, 
                    {
                        "_Code": "BIY", 
                        "_Name": "比索"
                    }, 
                    {
                        "_Code": "CPT", 
                        "_Name": "开普敦"
                    }, 
                    {
                        "_Code": "DAA", 
                        "_Name": "德阿尔"
                    }, 
                    {
                        "_Code": "DUN", 
                        "_Name": "邓迪"
                    }, 
                    {
                        "_Code": "DUR", 
                        "_Name": "德班"
                    }, 
                    {
                        "_Code": "ELS", 
                        "_Name": "东伦敦"
                    }, 
                    {
                        "_Code": "GBD", 
                        "_Name": "格罗布莱斯达尔"
                    }, 
                    {
                        "_Code": "GIY", 
                        "_Name": "基雅尼"
                    }, 
                    {
                        "_Code": "GRJ", 
                        "_Name": "乔治"
                    }, 
                    {
                        "_Code": "IXO", 
                        "_Name": "特克索波"
                    }, 
                    {
                        "_Code": "JNB", 
                        "_Name": "约翰内斯堡"
                    }, 
                    {
                        "_Code": "KXE", 
                        "_Name": "克莱克斯多普"
                    }, 
                    {
                        "_Code": "KIM", 
                        "_Name": "金伯利"
                    }, 
                    {
                        "_Code": "KMH", 
                        "_Name": "库鲁曼"
                    }, 
                    {
                        "_Code": "LAY", 
                        "_Name": "莱迪史密斯"
                    }, 
                    {
                        "_Code": "MAY", 
                        "_Name": "艾利弗山"
                    }, 
                    {
                        "_Code": "MDB", 
                        "_Name": "米德尔堡"
                    }, 
                    {
                        "_Code": "MOO", 
                        "_Name": "穆里斯堡"
                    }, 
                    {
                        "_Code": "MZQ", 
                        "_Name": "姆库泽"
                    }, 
                    {
                        "_Code": "NCS", 
                        "_Name": "纽卡斯尔"
                    }, 
                    {
                        "_Code": "NLP", 
                        "_Name": "内尔斯普雷特"
                    }, 
                    {
                        "_Code": "NYL", 
                        "_Name": "尼尔斯特隆"
                    }, 
                    {
                        "_Code": "PLZ", 
                        "_Name": "伊丽莎白港"
                    }, 
                    {
                        "_Code": "PRY", 
                        "_Name": "比勒陀利亚"
                    }, 
                    {
                        "_Code": "PSS", 
                        "_Name": "谢普斯通港"
                    }, 
                    {
                        "_Code": "PTG", 
                        "_Name": "彼德斯堡"
                    }, 
                    {
                        "_Code": "PZB", 
                        "_Name": "彼德马里茨堡"
                    }, 
                    {
                        "_Code": "RCB", 
                        "_Name": "理查兹湾"
                    }, 
                    {
                        "_Code": "RFT", 
                        "_Name": "兰德方丹"
                    }, 
                    {
                        "_Code": "RSB", 
                        "_Name": "利斯滕堡"
                    }, 
                    {
                        "_Code": "SAS", 
                        "_Name": "萨索尔堡"
                    }, 
                    {
                        "_Code": "SBU", 
                        "_Name": "跳羚"
                    }, 
                    {
                        "_Code": "THY", 
                        "_Name": "托霍延杜"
                    }, 
                    {
                        "_Code": "TLH", 
                        "_Name": "图拉马哈谢"
                    }, 
                    {
                        "_Code": "TPB", 
                        "_Name": "特隆普斯堡"
                    }, 
                    {
                        "_Code": "ULD", 
                        "_Name": "乌伦迪"
                    }, 
                    {
                        "_Code": "UTN", 
                        "_Name": "阿平顿"
                    }, 
                    {
                        "_Code": "UTT", 
                        "_Name": "乌姆塔塔"
                    }, 
                    {
                        "_Code": "UTW", 
                        "_Name": "昆士敦"
                    }, 
                    {
                        "_Code": "VGG", 
                        "_Name": "弗里尼欣"
                    }, 
                    {
                        "_Code": "VRU", 
                        "_Name": "弗雷堡"
                    }, 
                    {
                        "_Code": "WEL", 
                        "_Name": "韦尔科姆"
                    }, 
                    {
                        "_Code": "WOR", 
                        "_Name": "伍斯特"
                    }, 
                    {
                        "_Code": "WSH", 
                        "_Name": "韦茨肖克"
                    }, 
                    {
                        "_Code": "ZEC", 
                        "_Name": "瑟孔达"
                    }
                ]
            }
        }, 
        {
            "_Code": "ZMB", 
            "_Name": "赞比亚", 
            "State": {
                "City": [
                    {
                        "_Code": "CB", 
                        "_Name": "铜带"
                    }, 
                    {
                        "_Code": "CE", 
                        "_Name": "中央"
                    }, 
                    {
                        "_Code": "EA", 
                        "_Name": "东方"
                    }, 
                    {
                        "_Code": "LK", 
                        "_Name": "卢萨卡"
                    }, 
                    {
                        "_Code": "LP", 
                        "_Name": "卢阿普拉"
                    }, 
                    {
                        "_Code": "NO", 
                        "_Name": "北方"
                    }, 
                    {
                        "_Code": "NW", 
                        "_Name": "西北"
                    }, 
                    {
                        "_Code": "SO", 
                        "_Name": "南方"
                    }, 
                    {
                        "_Code": "WE", 
                        "_Name": "西方"
                    }
                ]
            }
        }, 
        {
            "_Code": "ZWE", 
            "_Name": "津巴布韦", 
            "State": {
                "City": [
                    {
                        "_Code": "BU", 
                        "_Name": "布拉瓦约"
                    }, 
                    {
                        "_Code": "HA", 
                        "_Name": "哈拉雷"
                    }, 
                    {
                        "_Code": "MC", 
                        "_Name": "中马绍纳兰"
                    }, 
                    {
                        "_Code": "MD", 
                        "_Name": "中部"
                    }, 
                    {
                        "_Code": "ME", 
                        "_Name": "东马绍纳兰"
                    }, 
                    {
                        "_Code": "ML", 
                        "_Name": "马尼卡兰"
                    }, 
                    {
                        "_Code": "MN", 
                        "_Name": "北马塔贝莱兰"
                    }, 
                    {
                        "_Code": "MS", 
                        "_Name": "南马塔贝莱兰"
                    }, 
                    {
                        "_Code": "MV", 
                        "_Name": "马斯温戈"
                    }, 
                    {
                        "_Code": "MW", 
                        "_Name": "西马绍纳兰"
                    }
                ]
            }
        }
    ]
}



var CountryRegion=Location.CountryRegion;
//init
var country = document.getElementById("country");
country.add(new Option("-","0"));
var state = document.getElementById("state");
state.add(new Option("-","0"));
var city = document.getElementById("city");
city.add(new Option("-","0"));

for(i=0;i<CountryRegion.length;i++){
	country.add(new Option(CountryRegion[i]._Name,CountryRegion[i]._Name));
}

function getState(){
	var country = document.getElementById("country");
	var state = document.getElementById("state");
	var city = document.getElementById("city");
		city.options.length = 1; 
		state.options.length = 1; 
	

	for(i=0;i<CountryRegion.length;i++){
		
		if(CountryRegion[i]._Name==country.value&&CountryRegion[i].State!='undefined'){
			var State=CountryRegion[i].State;
	
			for(j=0;j<State.length;j++){
		   	  state.add(new Option(State[j]._Name,State[j]._Name));
			}
			break;
		}
	}
	
	
}

function getCity(){
	var country = document.getElementById("country");
	var state = document.getElementById("state");
	var city = document.getElementById("city");
	
		city.options.length = 1; 
	

	for(i=0;i<CountryRegion.length;i++){
		
		if(CountryRegion[i]._Name==country.value&&CountryRegion[i].State!='undefined'){
			var State=CountryRegion[i].State;
		
			for(j=0;j<State.length;j++){
				if(State[j]._Name==state.value&&State[j].City!='undefined'){
					
					 var City=State[j].City;
					 	
			   		for(k=0;k<City.length;k++){
					   	  city.add(new Option(City[k]._Name,City[k]._Name));
						}
			   		break;	
				}

		}
		
		}
	}
	
	
}

//}});