!function ($) {

  "use strict"; // jshint ;_;


 /* selectdown CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle=selectdown]'
    , selectdown = function (element) {
        var $el = $(element).on('click.selectdown.data-api', this.toggle)
        onDropMenu($el);
        $('html').on('click.selectdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  selectdown.prototype = {

    constructor: selectdown

  ,toggle: function (e) {
      var $this = $(this)
        , $parent
        , isActive

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)
      isActive = $parent.hasClass('open')
      clearMenus()

      if (!isActive) {
        if ('ontouchstart' in document.documentElement) {
          // if mobile we we use a backdrop because click events don't delegate
          $('<div class="selectdown-backdrop"/>').insertBefore($(this)).on('click', clearMenus)
        }
        $parent.toggleClass('open')
      }

      $this.focus()

      return false
    }
/*,
   keydown: function (e) { 
      var $this
        , $items
        , $active
        , $parent
        , isActive
        , index

      if (!/(38|40|27)/.test(e.keyCode)) return

      $this = $(this)

      e.preventDefault()
      e.stopPropagation()

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      if (!isActive || (isActive && e.keyCode == 27)) {
        if (e.which == 27) $parent.find(toggle).focus()
        return $this.click()
      }

      $items = $('[role=menu] li:not(.divider):visible a', $parent)

      if (!$items.length) return

      index = $items.index($items.filter(':focus'))

      if (e.keyCode == 38 && index > 0) index--                                        // up
      if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
      if (!~index) index = 0

      $items
        .eq(index)
        .focus()
    }*/

  }

  function clearMenus() {
    /**$('.selectdown-backdrop').remove()**/
    $(toggle).each(function () {  // 去除所有[data-toggle=selectdown] 的open class
      getParent($(this)).removeClass('open')
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = selector && $(selector)

    if (!$parent || !$parent.length) $parent = $this.parent()

    return $parent
  }
  
  function onDropMenu($el){
  	$el.next(".dropdown-menu").find("li").click(function(){
  		var val = $(this).find("a").html();
  		$el.html(val+"&nbsp;<span class='caret'></span>");
  	})
  }


  /* selectdown PLUGIN DEFINITION
   * ========================== */

  var old = $.fn.selectdown

  $.fn.selectdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('selectdown')
      if (!data) {
      	$this.data('selectdown', (data = new selectdown(this)))
      }
      if (typeof option == 'string') {
      	data[option].call($this)
      }
    })
  }

  $.fn.selectdown.Constructor = selectdown


 /* selectdown NO CONFLICT
  * ==================== */

  $.fn.selectdown.noConflict = function () {
    $.fn.selectdown = old
    return this
  }


  /* APPLY TO STANDARD selectdown ELEMENTS
   * =================================== */
/*
  $(document)
    .on('click.selectdown.data-api', clearMenus)
    .on('click.selectdown.data-api', '.selectdown form', function (e) { e.stopPropagation() })
    .on('click.selectdown.data-api'  , toggle, selectdown.prototype.toggle)
    .on('keydown.selectdown.data-api', toggle + ', [role=menu]' , selectdown.prototype.keydown)*/

}(window.jQuery);