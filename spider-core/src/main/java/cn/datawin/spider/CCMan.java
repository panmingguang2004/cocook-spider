package cn.datawin.spider;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.datawin.spider.page.Page;
import cn.datawin.spider.scheduler.PageQueue;
import cn.datawin.spider.scheduler.QueueThreadPool;

public class CCMan implements Runnable {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private PageQueue pageQueue ;

	protected QueueThreadPool queueThreadPool  ;

	private boolean stop = false;
	
	public CCMan thread(int initThread, int execThread, int queueThread) {
		queueThreadPool = new QueueThreadPool(initThread, execThread, queueThread);
		return this;
	}
	
	public QueueThreadPool getQueueThreadPool() {
		return queueThreadPool;
	}


	public void setQueueThreadPool(QueueThreadPool queueThreadPool) {
		this.queueThreadPool = queueThreadPool;
	}


	@Override
	public void run() {
		while(!stop){
			if(pageQueue.size() == 0){
				try{
					Thread.sleep(5000);
					continue;
				}catch(Exception e){}
			}
			// 控制抓取的频率
			try {Thread.sleep(2000);} catch (InterruptedException e1) {	}
			if(stop) return;
			
			final Page page =  pageQueue.poll();
			
			queueThreadPool.execute(new Runnable() {
				
				@Override
				public void run() {
					try{
						page.exec();
					}catch(Exception e){
						logger.warn("page process error ["+page.getHttpRequest().getUrl()+ "]", e);
					}finally{
						
					}
				}
			});
		}
	}

	public void start() {
		queueThreadPool.model();
		Thread thread = new Thread(this);
		thread.setDaemon(false); // 非守护进程, 立即结束
		thread.start();
	}

	public PageQueue getPageQueue() {
		return pageQueue;
	}

	public void setPageQueue(PageQueue pageQueue) {
		this.pageQueue = pageQueue;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}
	
	

}
