package cn.datawin.spider.httputil;


import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class HttpRequest {

	/* URL */
	private String url;

	/* Method */
	private Method method = Method.get;

	public static enum Method {
		get, post
	}

	/* 请求的头文件 */
	private Map<String, String> headers = new HashMap<String, String>();
	
	/* 请求的cookie */
	private Map<String, String> cookies = new HashMap<String, String>();

	/* 请求的参数, 包括get,post */
	private Map<String, String> postParams = new HashMap<String, String>();
	
	/* setContent 流*/
	private InputStream instream = null ;

	/* 请求的post文件 */
	private Map<String, File> fileMap = new HashMap<String, File>();

	/* 参数编码 */
	private String charset = "UTF-8";

	/* 连接超时 */
	private int connectTimeout = 50000;

	/* 读取超时 */
	private int readTimeout = 120000;

	/**
	 * 默认构造 get方式
	 * @param url
	 */
	public HttpRequest(String url) {
		this.url = url;
	}
	public HttpRequest() {
	}

	public HttpRequest(String url, Method method) {
		this.url = url;
		this.method = method;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public void addHeader(String key, String value) {
		this.headers.put(key, value);
	}

	public void addHeader(Map<String, String> headers) {
		this.headers= headers;
	}
	
	public void setRequestBody(InputStream instream){
		this.instream = instream;
	}
	public InputStream getRequestBody(){
		return instream;
	}

	public Map<String, String> getHeaders() {
		headers.put("Accept-Encoding", "gzip,deflate,sdch");
		headers.put("Accept", "*/*");
		headers.put("Accept-Charset", "GBK,utf-8;q=0.7,*;q=0.3");
		headers.put("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6");
		headers.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36");
		return headers;
	}

	public void setHeader(String key, String value) {
		this.headers.put(key,value);
	}
	
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	
	public void setCookies(Map<String, String> cookies) {
		this.cookies = cookies;
	}
	
	public void setCookie(String key, String value) {
		this.cookies.put(key, value);
	}

	public Map<String, String> getCookies() {
		return cookies;
	}

	public Map<String, String> getPostParams() {
		return postParams;
	}
	
	public void setPostParam(String key, String value){
		this.postParams.put(key, value);
	}

	public void setPostParams(Map<String, String> params) {
		this.postParams = params;
	}
	
	public String postParamsToString(){
		return toString(this.postParams);
	}
	
	public String postParamsToJson(){
		return toJson(this.postParams);
	}
	
	public void setFile(String key, File f){
		fileMap.put(key, f);
	}

	public Map<String, File> getFileMap() {
		return fileMap;
	}

	public void setFileMap(Map<String, File> fileMap) {
		this.fileMap = fileMap;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	public String getUrl() {
		return url;
	}

	public Method getMethod() {
		return method;
	}

	public String toString(Map<String, String> params){
		StringBuffer buffer = new StringBuffer();
		for(String key: params.keySet()){
			if(buffer.length()>0){
				buffer.append("&");
			}
			buffer.append(key).append("=").append(encode(params.get(key)));
		}
		return buffer.toString();
	}
	
	public String toJson(Map<String, String> params){
		return JSONArray.toJSONString(params, SerializerFeature.DisableCircularReferenceDetect, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue,SerializerFeature.WriteDateUseDateFormat);
	}
	
	private String encode(String url){
		try {
			return URLEncoder.encode(url,getCharset());
		} catch (Exception e) {
			return "";
		}
	}

}
