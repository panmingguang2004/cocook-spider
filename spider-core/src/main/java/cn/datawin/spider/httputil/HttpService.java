package cn.datawin.spider.httputil;


import org.apache.http.client.methods.HttpRequestBase;

public interface HttpService {

	public abstract HttpResponse execute(HttpRequest req) throws Exception;
	
	public abstract void bindInterceptor();
	
	public HttpRequestBase getHttpRequestBase(HttpRequest req);
	
	public void parseHeader(HttpRequestBase requestBase, HttpRequest req);
	
	public void parseCookies(HttpRequestBase requestBase, HttpRequest req);
	
	public void parsePostParams(HttpRequestBase requestBase, HttpRequest req);

	public void parseMartipart(HttpRequestBase requestBase,HttpRequest req);
	
	public void shutdown();
	
	/**
	 * 是否被停用,false-未停,true-已停
	 * @return
	 */
	public boolean isShutdown();
}
