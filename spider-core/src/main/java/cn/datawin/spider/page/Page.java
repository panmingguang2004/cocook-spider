package cn.datawin.spider.page;

import cn.datawin.spider.httputil.HttpRequest;
import cn.datawin.spider.httputil.HttpResponse;
import cn.datawin.spider.httputil.HttpService;
import cn.datawin.spider.pipeline.DefaultPipeLine;
import cn.datawin.spider.pipeline.PipeLine;
import cn.datawin.spider.processor.DefaultProcessor;
import cn.datawin.spider.processor.Processor;
import cn.datawin.spider.seletor.Html;
import cn.datawin.spider.util.UrlUtil;

public class Page {
	
	private HttpService httpService ;
	
	private HttpRequest httpRequest;
	
	private HttpResponse httpResponse;
	
	private Html html;
	
	/* 多解析器 */
	private Processor processor = new DefaultProcessor();

	/* 或多存储器 */
	private PipeLine pipeLine = new DefaultPipeLine();
	
	public void exec() throws Exception{
		httpResponse = httpService.execute(httpRequest);
		html  = new Html(httpResponse.getResponseString());
		processor.process(this);
		pipeLine.pipe(processor);
	}
	
	public HttpService getHttpService() {
		return httpService;
	}

	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}

	public HttpRequest getHttpRequest() {
		return httpRequest;
	}

	public void setHttpRequest(HttpRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	public HttpResponse getHttpResponse() {
		return httpResponse;
	}

	public void setHttpResponse(HttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}

	public Html getHtml() {
		try{
			return (Html)html.clone();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public void setHtml(Html html) {
		this.html = html;
	}

	public Processor getProcessor() {
		return processor;
	}

	public void setProcessor(Processor processor) {
		this.processor = processor;
	}

	public PipeLine getPipeLine() {
		return pipeLine;
	}

	public void setPipeLine(PipeLine pipeLine) {
		this.pipeLine = pipeLine;
	}
	
	public String getHost(){
		return UrlUtil.getHost(httpRequest.getUrl());
	}
	
	public String getUri(){
		return UrlUtil.getUri(httpRequest.getUrl());
	}
	
	public String getDoman(){
		return UrlUtil.getDomain(httpRequest.getUrl());
	}

}
