package cn.datawin.spider.pipeline;

import cn.datawin.spider.processor.Processor;

/**
 * data pipe
 * @author panmg
 *
 */
public interface PipeLine {
	
	void pipe(Processor process) throws Exception;

}
