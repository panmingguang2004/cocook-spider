package cn.datawin.spider.processor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import cn.datawin.spider.page.Page;

public class DefaultProcessor implements Processor{

	@Override
	public void process(Page page) {
		System.out.println(page.getHtml().$("div.landlord").text());
		System.out.println(page.getHtml().$("ul.attr_info.fang").text());
	}

	@Override
	public Map<String, String> getParams() {
		return Collections.emptyMap();
	}

	@Override
	public List<Object> getList() {
		return Collections.emptyList();
	}
	
	@Override
	public Page getPage() {
		return null;
	}

	@Override
	public void setTask(Serializable obj) {
		
	}

	@Override
	public Serializable getTask() {
		return null;
	}

}
