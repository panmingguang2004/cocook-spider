package cn.datawin.spider.processor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import cn.datawin.spider.page.Page;

/**
 * page processor
 * @author panmg
 *
 */
public interface Processor {
	
	void process(Page page);
	
	Map<String, String> getParams();
	
	List<Object> getList(); 
	
	Page getPage();
	
	void setTask(Serializable obj);
	
	Serializable getTask();
	

}
