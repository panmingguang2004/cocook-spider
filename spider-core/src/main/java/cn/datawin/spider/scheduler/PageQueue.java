package cn.datawin.spider.scheduler;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import cn.datawin.spider.page.Page;
import cn.datawin.spider.seletor.Html;

public class PageQueue {
	
	private Queue<Page> queue = new LinkedList<Page>();
	
	public Page poll(){
		return queue.poll();
	}
	
	public void offer(Page p){
		queue.offer(p);
	}
	
	public int size(){
		return queue.size();
	}
	
	public void clear(){
		queue.clear();
	}
	
	public List<Page> getQueue(){
		return (List<Page>) queue;
	}
	
	public static void main(String[] args) {
		PageQueue queue = new PageQueue();
		
		for(int i=0;i<30;i++){
			Page p = new Page();
			p.setHtml(new Html(i+""));
			queue.offer(p);
		}
		while(true){
			
			Page p = queue.poll();

			System.out.println(p.getHtml().html());
			
		}
		
	}
	

}
