package cn.datawin.spider.seletor;

import java.util.List;

import org.jsoup.nodes.Element;

/**
 * Selectable text.<br>
 *
 * @author code4crafter@gmail.com <br>
 * @since 0.1.0
 */
public interface Selectable {

    /**
     * select list with xpath
     *
     * @param xpath
     * @return new Selectable after extract
     */
    public Selectable xpath(String xpath);

    /**
     * select list with css selector
     *
     * @param selector css selector expression
     * @return new Selectable after extract
     */
    public Selectable $(String selector);
    
	public Selectable first();
	
	public Selectable last();

    /**
     * select list with css selector
     *
     * @param selector css selector expression
     * @return new Selectable after extract
     */
    public Selectable css(String selector);

    /**
     * select smart content with ReadAbility algorithm
     *
     * @return content
     */
    public Selectable smartContent();

    /**
     * select all links
     *
     * @return all links
     */
    public Selectable links();

    /**
     * select list with regex, default group is group 1
     *
     * @param regex
     * @return new Selectable after extract
     */
    public String regex(String regex);
    public String regexAll(String regex);

    /**
     * select list with regex
     *
     * @param regex
     * @param group
     * @return new Selectable after extract
     */
    public String regex(String regex, int group);
    
    

    /**
     * single string result
     *
     * @return single string result
     */
    public String toString();

    /**
     * single string result
     *
     * @return single string result
     */
    public String get();
    
    public Element get(int num);

    /**
     * if result exist for select
     *
     * @return true if result exist
     */
    public boolean match();

    /**
     * multi string result
     *
     * @return multi string result
     */
    public List<String> all();
    
    public List<Element> allele();

    /**
     * 取 html 内容
     * @return
     */
    public String html();

    /**
     * 取 text 内容
     * @return
     */
    public String text();

    /**
     * 取 属性的值
     * @param attr
     * @return
     */
    public String attr(String attr);
	
    
    public String jsonPath(String path);
    
    public List<String> jsonPathList(String path);
    
    public List<String> jsonPathList(String text, String path);


}
