package cn.datawin.spider.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class IOUtil {
	

	public static void copy(InputStream _in, OutputStream _out) {
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new BufferedInputStream(_in);
			out = new BufferedOutputStream(_out);
			byte[] b = new byte[1024];
			int len = 0;
			while ((len = in.read(b, 0, b.length)) != -1) {
				out.write(b, 0, len);
			}
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(in,out);
		}
	}
	
	public static void close(InputStream in, OutputStream out){
		if(in!=null){
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(out!=null){
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
