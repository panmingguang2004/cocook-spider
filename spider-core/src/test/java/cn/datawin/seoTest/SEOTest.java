package cn.datawin.seoTest;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import cn.datawin.spider.util.HttpUtil;

public class SEOTest {
	
//	@Test
	public void test() throws Exception{
		long a = System.currentTimeMillis();
		String url = "http://www.oschina.net/";
		String resStr = HttpUtil.getStr("http://www.oschina.net/", "utf8");
		InsideOptimizeAnalyze analyze = new InsideOptimizeAnalyze(resStr, url);

		Method [] me =  InsideOptimizeAnalyze.class.getDeclaredMethods();
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		int maps =0;
		for(Method m : me){
			if(m.getReturnType() == Map.class){
				maps++;
			 	Map<String, Object> map = (Map<String, Object>) m.invoke(analyze);
			 	list.add(map);
			}
		}
		
		System.out.println("methods === "+maps);
		int i=0;
		for(Map map: list){
			System.out.println(map);
			System.out.println(i);
			i+= (Integer)map.get("score");
		}
		
		System.out.println("score======"+ i);
		System.out.println(System.currentTimeMillis()-a);
		
	}
	
	
	/**
	 * @author panmg
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void test1() throws Exception{
		String url = "http://www.oschina.net/";
		String resStr = HttpUtil.getStr("http://www.oschina.net/", "utf8");
		InsideOptimizeAnalyze analyze = new InsideOptimizeAnalyze(resStr, url);
		long a = System.currentTimeMillis();
		System.out.println(analyze.flinkAnalyze());;
		System.out.println(analyze.mirrorAnalyze());;
		System.out.println(System.currentTimeMillis()-a);
	}
	

}
