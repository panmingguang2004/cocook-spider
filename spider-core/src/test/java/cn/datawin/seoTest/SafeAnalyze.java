package cn.datawin.seoTest;

import java.util.HashMap;
import java.util.Map;

/**
 * 网站安全检测
 * 
 * @author sdf
 */
public class SafeAnalyze {

	/**
	 * 360安全检测
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> safe360Analyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * QQ管家网站安全检测
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> safeQQAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 百度网页安全检测
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> safeBAIDUAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

}
