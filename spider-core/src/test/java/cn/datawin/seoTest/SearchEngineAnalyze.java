package cn.datawin.seoTest;

import java.util.HashMap;
import java.util.Map;

/**
 * 搜索引擎表现检测
 * 
 * @author sdf
 * 
 */
public class SearchEngineAnalyze {

	/**
	 * SITE是否首页在第一位
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> siteAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * DOMAIN首页是否在第一位
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> domainAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 标题排名是否在第一位
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> tOrderAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 外链是否存在未被收录站点
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> linkEmbodyAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 外链中是否存在质量过低的站
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> linkQualityAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 百度快照时间是否正常
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> photoTimeAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 百度收录是否过少
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> embodyNumAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 百度权重及流量预估
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> predictionAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 是否可能被百度降权
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> fallPowerAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * PR值是否过低
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> rpAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 360搜索引擎收录是否正常
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> embody36mapAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

	/**
	 * 搜狗搜索收录是否正常
	 * 
	 * @param html
	 * @return
	 */
	public Map<String, Object> embodySOUGOUAnalyze(String html) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("item", "");
		map.put("msg", "");
		map.put("score", 0);

		return map;
	}

}
