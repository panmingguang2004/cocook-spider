package cn.datawin.spider;

import org.junit.Test;

import cn.datawin.spider.httputil.HttpClientService;
import cn.datawin.spider.httputil.HttpRequest;
import cn.datawin.spider.page.Page;
import cn.datawin.spider.scheduler.PageQueue;

public class CCManTest {
	
	@Test
	public void test(){
		CCMan cc = new CCMan();
		PageQueue pageQueue = new PageQueue();
		Page p = new Page();
		p.setHttpService(new HttpClientService());
		HttpRequest req = new HttpRequest("http://www.baidu.com");
		req.setCharset("UTF-8");
		p.setHttpRequest(req);
		pageQueue.offer(p);
		
		/**
		 * 添加Queue
		 */
		 //addQueue(pageQueue);
		
		cc.setPageQueue(pageQueue);
		cc.thread(1, 1, 1).start();
	}
	
	public static void main(String[] args) {
	
		CCManTest ccManTest = new CCManTest();
		ccManTest.test();
		
	}

}
