package cn.datawin.spider.scheduler;

import org.junit.Test;

public class ThreadPoolTest {
	
	@Test
	public void test(){
		CountableThreadPool pool = new CountableThreadPool(3);
		pool.execute(new Runnable() {
			int i=0;
			@Override
			public void run() {
				System.out.println(i++);
				
			}
		});
		
		pool.execute(new Runnable() {
			int i=1;
			@Override
			public void run() {
				System.out.println(i++);
				
			}
		});
	} 

}
