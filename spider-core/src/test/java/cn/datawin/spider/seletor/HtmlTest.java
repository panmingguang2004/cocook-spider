package cn.datawin.spider.seletor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.junit.Test;

public class HtmlTest {
	
	@Test
	public void test(){
		String text = "<html><div><a href='https://github.com'>github.com</a><a href='https://oschina.com'>github.com</a></div></html>";
		Html html = new Html(text);
		//System.out.println(html.toString());
		System.out.println(html.$("div a").first());
		System.out.println(html.regex("href=\"(.*?)\""));    
//		System.out.println("-------");
//		text = "<div><a href='http://whatever.com/aaa'>panmg</a></div><div><a href='http://whatever.com/bbb'>wangzeng</a></div>";
//		html = new Html(text);
//		System.out.println(html.toString());
//		System.out.println(html.links().text());
//		System.out.println("-------");
//		System.out.println(html.$("a[href]"));
//		System.out.println("-------");
//		System.out.println(html.$("a[href=http://whatever.com/aaa]"));
		
	}
	
	static String str = "\"codeString\" : \"captchaservice383931644c54427468425874696f6e687467534d745947695745744876655639707338696f6853714945637a30754f327772546c2b334d66666d67536d64474c542b4f5661786f5342476b6f51343439444c6b43615a4d426346504a54452b5132774c375a50666e617a42464c31396f2f2b71644a66384e6b3135777146315a466b745842616e50496370632f6d666d4c5a636b646a374f7044722b59596f6f6572426a677671332b4b4c7853504347444c4537655673727775542f45507a657a6c4a4f34544e4f362f6276777645346e394e565a783759526778614a7a2b622b70524d726f4d5a4154796179456355716f6d333933666763346a62334f737359494c34392b63652b585549683451596c51474336794f496d37452f55476959317a544c7a36664a5661655572656944664c48635075353573474e586875745545516f344e326c6a6d67\",";
	public static String regex(String regexStr, int group) {
		Pattern regex = Pattern.compile(regexStr, Pattern.DOTALL
				| Pattern.CASE_INSENSITIVE);
		Matcher matcher = regex.matcher(str);
		if (matcher.find()) {
			String[] groups = new String[matcher.groupCount() + 1];
			for (int i = 0; i < groups.length; i++) {
				groups[i] = matcher.group(i);
			}
			return groups[group];
		}
		return null;
	}
	
	{
		System.out.println(regex("\"codeString\" : \"(.+?)\"", 1));
		String ss = "<div>\"codeString</div>";
		ss = Jsoup.parse(ss).select("div").text();

		System.out.println(ss);
	}
	
	public static void main(String[] args) {
		String a ="2000-3000元/月^|^|申请职位^|^|地点：^|- ^|朝阳^|太阳宫^|^|^|^|^|^|职位：^|^|客房服务员^|^|要求：^|招3人| 学历不限| 1-2年^|";
		String b  = "面议^|^|申请职位^|^|地点：^|- ^|朝阳^|国贸^|^|^|^|^|^|职位：^|^|客房服务员^|^|要求：^|招3人| 高中| 1-2年^|^|包吃^|^|包住^|^|交通补助^|^|年假^|^|提供被褥^|^|奖金^|";
		
		String aa[] = a.split("\\^\\|");
		System.out.println(aa.length);
		String bb[] = b.split("\\^\\|");
		System.out.println(bb.length);
	}
	

}
