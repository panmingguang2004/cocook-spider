package cn.datawin.spider.seletor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONPath;

public class JsonTest{
	
//	@Test
	public void parse(){
		System.out.println("=== deserialize ===");
		/*自动转换的, 但是要自己判断是 jsonObject 还是 jsonArray*/
		Map<String, Integer> map =  (Map<String, Integer>) JSON.parse("{text:1}");
		System.out.println(map.get("text"));
		
		List<Map<String, Integer>> list =  (List<Map<String, Integer>>) JSON.parse("[{'text':1},{text:2}]");
		for(Map<String, Integer> map1 : list){
			System.out.println(map1.get("text"));
		}
	
	}
	
//	@Test
	public void serialize(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("test", 1);
		map.put("tees", 2);
		System.out.println("=== serialize ===");
		System.out.println("map=="+map);
		System.out.println("json=="+JSON.toJSONString(map));
		
	}
	
	@Test
	public void jsonpath(){
//		String text = "{text:1,text2:2}";
		String text = "[{'text':1},{text:2,text2:'tes,t2'}]";
		Html html = new Html(text);
		System.out.println( "==json=="+html.jsonPathList("$.text"));
		System.out.println( "==json=="+html.jsonPathList("$.text2"));
		System.out.println( "==json=="+html.jsonPathList("$.text"));
		
	}
	

}
