package cn.datawin.spider.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class URltest {
	
	@Test
	public void test(){
		long a = System.currentTimeMillis();
		System.out.println(UrlUtil.getDomain("http://wap.ganji.com/bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		System.out.println(UrlUtil.getHost("http://wap.ganji.com/bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		System.out.println(UrlUtil.getUri("http://wap.ganji.com/bj/fang5/./?domain=bj&url=fang5&page=2&ifid=gjv2_list_next_fang"));
		System.out.println(UrlUtil.getUri("http://weixin.sogou.com/weixin?query=%E9%A4%90%E9%A5%AE&_asf=www.sogou.com&_ast=1410838854&w=01019900&p=40040100&ie=utf8&type=2&sut=4701&sst0=1410838854424&lkt=1%2C1410838849723%2C1410838849723"));
		String url = "http://wap.ganji.com/bj/fang5/?domain=bj&url=fang5&page=10&page=20";
//		
//		System.out.println(UrlUtil.replace(url, "page=\\d+", "page=22")); 
//		
//		url = "http://m.58.com/bj/zufang/pn2/?from=home_house_3";
//		
//		System.out.println(url.replaceFirst("pn\\d+", "pn40"));
//		
//		System.out.println(UrlUtil.isUrl("http://wap.ganji.com/bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
//		System.out.println(UrlUtil.isAbUrl("/bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
//		System.out.println(UrlUtil.isNabUrl("./bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
//		System.out.println(UrlUtil.isNabUrl("../bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
//		System.out.println(UrlUtil.isNabUrl("bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		
		
		url = "http://m.58.com/bj/zufang/pn2/?from=home_house_3";
		
		System.out.println(url.replaceFirst("pn\\d+", "pn40"));
		
		System.out.println(UrlUtil.isUrl("http://wap.ganji.com/bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		System.out.println(UrlUtil.isAbUrl("/bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		System.out.println(UrlUtil.isNabUrl("./bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		System.out.println(UrlUtil.isNabUrl("../bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		System.out.println(UrlUtil.isNabUrl("bj/fang5/?domain=bj&url=fang5&page=10&page=20"));
		
	}
	
	public static void main(String[] args) {
		new URltest().test();
	}

    @Test
    public void testFixRelativeUrl() {
        String absoluteUrl = UrlUtil.canonicalizeUrl("aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/ss/aa");

        absoluteUrl = UrlUtil.canonicalizeUrl("../aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/aa");

        absoluteUrl = UrlUtil.canonicalizeUrl("..aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/ss/..aa");

        absoluteUrl = UrlUtil.canonicalizeUrl("../../aa", "http://www.dianping.com/sh/ss/com/");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/aa");

        absoluteUrl = UrlUtil.canonicalizeUrl("../../aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/aa");
        
        absoluteUrl = UrlUtil.canonicalizeUrl("aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/ss/aa");
        
        absoluteUrl = UrlUtil.canonicalizeUrl("/aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/aa");
        
        absoluteUrl = UrlUtil.canonicalizeUrl("./aa", "http://www.dianping.com/sh/ss/com");
        System.out.println(absoluteUrl);
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/ss/aa");
        
        absoluteUrl = UrlUtil.canonicalizeUrl("../aa", "http://www.dianping.com/sh/ss/com");
        assertThat(absoluteUrl).isEqualTo("http://www.dianping.com/sh/aa");
        
        absoluteUrl = UrlUtil.canonicalizeUrl("./?domain=bj&url=jzxiaoshigong&page=2&ifid=gjv2_list_next_jianzhi", "http://wap.ganji.com/bj/jzxiaoshigong/");
        assertThat(absoluteUrl).isEqualTo("http://wap.ganji.com/bj/jzxiaoshigong/?domain=bj&url=jzxiaoshigong&page=2&ifid=gjv2_list_next_jianzhi");
        
        absoluteUrl = UrlUtil.canonicalizeUrl("/gongsi/26503249/?domain=bj", "http://wap.ganji.com/bj/jzxiaoshigong/?domain=bj&url=jzxiaoshigong&page=2&ifid=gjv2_list_next_jianzhi");
        assertThat(absoluteUrl).isEqualTo("http://wap.ganji.com/gongsi/26503249/?domain=bj");
        System.out.println(absoluteUrl);
    }
	

}
