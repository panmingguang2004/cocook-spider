package cn.datawin.adsl;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * 09 IP工具类 10
 *
 * @author geeksun 11 2011-9-23 12
 */
public class IpUtil {

    /**
     * 34
     *
     * @return 本机IP 35
     * @throws SocketException 36
     */
    public static String getRealIp() throws SocketException {

        String localip = null;// 本地IP，如果没有配置外网IP则返回它

        String netip = null;// 外网IP

        Enumeration<NetworkInterface> netInterfaces = NetworkInterface
                .getNetworkInterfaces();
        InetAddress ip = null;
        boolean finded = false;// 是否找到外网IP
        while (netInterfaces.hasMoreElements() && !finded) {
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> address = ni.getInetAddresses();
            while (address.hasMoreElements()) {
                ip = address.nextElement();
                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                        && ip.getHostAddress().indexOf(":") == -1) {// 外网IP

                    netip = ip.getHostAddress();

                    finded = true;

                    break;

                } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                        && ip.getHostAddress().indexOf(":") == -1) {// 内网IP
                    localip = ip.getHostAddress();

                }
            }
        }
        if (netip != null && !"".equals(netip)) {
            return netip;
        } else {
            return localip;
        }
    }

    public static void main(String[] args) throws SocketException {
        System.out.println(getRealIp());
    }
}
