package cn.datawin.adsl;

/* 输出

 Original:       黄   彪彪

 to unicode:     \u9EC4\ \t\u5F6A\u5F6A

 from unicode:   黄   彪彪

 使用命令转换: native2ascii -encoding utf-8 a.txt a.txt

 Java的properties属性文件会把字符先转换成unicode的形式存储.

 */

import java.io.UnsupportedEncodingException;

public class UnicodeConverter {

	public static void main(String[] args) throws UnsupportedEncodingException {
		
		System.out.println("\u4e0d\u60e7\u98ce\u66b4");

		String s = "[[\"2013-01-02T00\u003a45\u003a15.8730000-08\u003a00\",[2013,1,2,,45,15,873],\"W5.M2\",[[\"c674fabc-0000-0000-0000-000000000000\",[[\"ca469311-0000-0000-0000-000000000000\",4,8,[[\"1\u003aada_xy1982\u0040hotmail.com\"]],\"WL\",\"-8658800139171640074\",\"ada_xy1982\u0040hotmail.com\"]],\"\u6768\",\"\u859b\",,,\"-8658800139171640074\",\"87D5C32653924CF6\",,\"2\",,,,,,\"ada_xy1982\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u859b\u6768\",\"8\"],[\"9b4fe3b4-0000-0000-0000-000000000000\",[[\"ffd8183b-0000-0000-0000-000000000000\",3,12,[],\"WL\",\"-428399627537915770\"]],\"\u738b\",\"\u738b\",,,\"-428399627537915770\",\"FA0E04CEFAB16086\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mRp0tLxoTEes3LMR_GnN33KS0LPeu2tF9w3sDsICVhyHqxolcW4OoydGlhWjp2_OHYwHD6diHDm7MMErWaODRhQ\",,,,,,,,,,,,,,,,,,,\"\u738b\u738b\",\"8\"],[\"6504850a-0000-0000-0000-000000000000\",[[\"ee802d44-0000-0000-0000-000000000000\",6,,[[\"1\u003aShirleenMahomesaqito\u0040hotmail.com\"]],\"WL\",\"5289057319550018465\"]],,,,,\"5289057319550018465\",\"4966819C087563A1\",,\"3\",,,,,,\"ShirleenMahomesaqito\u0040hotmail.com\"],[\"44659f30-0000-0000-0000-000000000000\",[[\"0c37825e-0000-0000-0000-000000000000\",5,8,[],\"ABCH\",\"2\u003a0c37825e-0000-0000-0000-000000000000\"],[\"693c73fd-fe41-4744-9120-a011752a84ab\",6,524324,[[\"1\u003akent5424\u0040yahoo.com.tw\"]],\"WL\",\"1146984681240427756\",,\"\u6b63\u5c71\",\"\u6e38\",[,,],,,,[0,0,0],[\"0\",,],,,,,,,,,,,,,,,,,\"\u6e38\u6b63\u5c71\",\"8\"]],\"\u6b63\u5c71\",\"\u6e38\",[\"kent5424\u0040yahoo.com.tw\",,],,\"1146984681240427756\",\"FEAE87D98267CEC\",,\"3\",,\"https\u003a\u002f\u002flivefiles18.vo.msecnd.net\u002fy1mSuFmuMS1K19n-5y0zf7mlHTboiWQx7MsD2KeCGMFkc5pVvNpn0cyHqSFzqlfmAOVWsm3ka38bGo6y_XbFUdKwUp3K_gVhRe9\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"kent5424\u0040yahoo.com.tw\",[1965,2,4],\"kent\",,,,,,,,,,,,,\"\u6e38\u6b63\u5c71\",\"8\"],[\"cc3e08f2-0000-0000-0000-000000000000\",[[\"a25c706a-0000-0000-0000-000000000000\",6,4,[[\"1\u003ajipiao3710\u0040live.cn\"]],\"WL\",\"-715147416872559956\"]],,,,,\"-715147416872559956\",\"F61349369F45DAAC\",,\"3\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mh3MDVp94ntWClR593LbvfxVr0yf7yM69FNum9RJWcDBemQ1BS8xbePN8pmVZLrky4j0joKUQSfn5wczZhFQBEA\",,,,\"jipiao3710\u0040live.cn\"],[\"0587350d-0000-0000-0000-000000000000\",[[\"59e20970-0000-0000-0000-000000000000\",6,,[[\"1\u003aArnitaMcinturffnoiak\u0040hotmail.com\"]],\"WL\",\"5378128034575418844\"]],,,,,\"5378128034575418844\",\"4AA2F2F3F17005DC\",,\"3\",,,,,,\"ArnitaMcinturffnoiak\u0040hotmail.com\"],[\"5b1b7f10-0000-0000-0000-000000000000\",[[\"d1d3aca5-0000-0000-0000-000000000000\",5,56,[],\"ABCH\",\"2\u003ad1d3aca5-0000-0000-0000-000000000000\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"wei\",\"haha\",[\"zhnacywang\u0040hotmail.com\",,],,,,,,\"com\",,,,,,,,,,,,,,,,,,,,\"wei haha\",\"0\"],[\"c92fbd94-0000-0000-0000-000000000000\",[[\"dbba0daa-0000-0000-0000-000000000000\",5,8,[],\"ABCH\",\"2\u003adbba0daa-0000-0000-0000-000000000000\"],[\"ff22747a-4d10-41f4-b58b-6464bf574f2a\",6,4,[[\"1\u003ahaitaowa\u0040hotmail.com\"]],\"WL\",\"61543845474338783\",,\"Haitao\",,,,,,,,,,,,,,,,,,,,,,,,\"Haitao\",\"0\"]],\"Haitao\",,,,\"61543845474338783\",\"DAA5CD279C07DF\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f18\",[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\"],,,\"haitaowa\u0040hotmail.com\",,\"\u738b\u6d77\u6d9b\",,,,,,,,,,,,,\"Haitao\",\"0\"],[\"bb86afe1-0000-0000-0000-000000000000\",[[\"3fc300b4-0000-0000-0000-000000000000\",5,8,[],\"ABCH\",\"2\u003a3fc300b4-0000-0000-0000-000000000000\"],[\"45eba195-a64d-4b8f-8a8f-30fc0ee7ad99\",6,12,[[\"1\u003astream1975\u0040hotmail.com\"]],\"WL\",\"-8306393228412295394\"]],\"stream\",\"ma\",,,\"-8306393228412295394\",\"8CB9C35E2FC6671E\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mEyC8OTgzBE-m2ibRaiu3PQy8YpZylExtIyKoGiJzgbh3gjLIERmGpb8uG8pp1WY_GCQa5jGS5LFNhTNzjtVSjQ\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"stream1975\u0040hotmail.com\",,,,,,,,,,,,,,,\"stream ma\",\"0\"],[\"1f3d5b64-0000-0000-0000-000000000000\",[[\"04d137e5-0000-0000-0000-000000000000\",5,8,[],\"ABCH\",\"2\u003a04d137e5-0000-0000-0000-000000000000\"],[\"f039b143-7f3e-45a4-b2b9-85870e29228b\",6,12,[[\"1\u003alouying198243\u0040hotmail.com\"]],\"WL\",\"-6289311969783945146\"]],\"\u83b9\",\"\u5a04\",,,\"-6289311969783945146\",\"A8B7E015EF6A6846\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mqRm_me_kKp_Dx7UTUHaQPMJ_a1K0GSbC594TjJj4Duh-ocQa5xRzsfz-PnxIyDVVhwU6Ga2mUD2xu24iz3siVA\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"louying198243\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5a04\u83b9\",\"8\"],[\"734b6971-0000-0000-0000-000000000000\",[[\"866bb8fd-0000-0000-0000-000000000000\",7,,[],\"WL\",\"-3890343260131768040\",\"lorebaldreey858\u0040hotmail.com\"]],,,,,\"-3890343260131768040\",\"CA02B96AB582C918\",,\"3\",,,[\"8f2c0aac-44ce-4c10-bea1-8483586f01d3\"],1,,\"lorebaldreey858\u0040hotmail.com\"],[\"8152030d-0000-0000-0000-000000000000\",[[\"a9ba7b1d-fe31-4182-8543-0b5eecd7d67b\",5,32,[],\"ABCH\",\"2\u003aa9ba7b1d-fe31-4182-8543-0b5eecd7d67b\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"heaven.xin\u0040126.com\",,]],[\"49f42db4-0000-0000-0000-000000000000\",[[\"95910cbe-44d4-4ba8-8ba7-0c5df95d0919\",6,12,[[\"1\u003ajiangwei1\u0040hotmail.com\"]],\"WL\",\"7379197912888915376\"]],\"Lily\",\"jiang\",,,\"7379197912888915376\",\"66682D67B50C35B0\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mpLoASWouGKViQmgkH0WB4yF6w_pTmI2azfn_58ywc64Cl0cfXt8XvuaAnN_abwBv08lBktvVJgup1hW744mzhA\",,,,\"jiangwei1\u0040hotmail.com\",,,,,,,,,,,,,,,\"Lily jiang\",\"0\"],[\"5e5b8ff1-0000-0000-0000-000000000000\",[[\"87cc0b83-49ff-474c-987b-0d49cd1082cc\",6,12,[[\"1\u003aljxtju\u0040hotmail.com\"]],\"WL\",\"-8063034632858833549\"]],\"\u5bb6\u7965\",\"\u674e\",,,\"-8063034632858833549\",\"901A58B8A0C6ED73\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f12\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"ljxtju\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u674e\u5bb6\u7965\",\"8\"],[\"6f793305-0000-0000-0000-000000000000\",[[\"03a88558-6944-4620-98be-0e8e31e2c1c6\",5,40,[],\"ABCH\",\"2\u003a03a88558-6944-4620-98be-0e8e31e2c1c6\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u5fae\u8f6fMSN\",,[\"communications_msn_cs_zhcn\u0040Microsoft.msn.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u5fae\u8f6fMSN\",\"8\"],[\"025e7598-0000-0000-0000-000000000000\",[[\"af41417a-35b5-47b2-99bb-0edc75e39a6e\",6,12,[[\"1\u003asally_huang\u0040msn.com\"]],\"WL\",\"-6001495510730266673\"]],\"huang\",\"sally\",,,\"-6001495510730266673\",\"ACB667A159D227CF\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mqupegXJzB5Zpp1IACAIqhedDeGVwOvKQjdRUT-av-uCbyml5rOu01UUJ4GPqArUcplOzrCoDJiKShi1O48Rq4w\",[\"f43df50e-9939-4c23-8abb-ec4189e7e3fe\"],,,\"sally_huang\u0040msn.com\",,,,,,,,,,,,,,,\"huang sally\",\"0\"],[\"d7d9617e-0000-0000-0000-000000000000\",[[\"ea070d97-edd2-4285-bcf7-0f109effcf44\",5,32,[],\"ABCH\",\"2\u003aea070d97-edd2-4285-bcf7-0f109effcf44\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"freshgloria\u0040hotmail.com\",,]],[\"052300b6-0000-0000-0000-000000000000\",[[\"885d15b5-64fa-445e-8748-12442e7e9492\",6,12,[[\"1\u003alotrcz\u0040hotmail.com\"]],\"WL\",\"-7306231359468239480\"]],\"Peng\",\"Zhao\",,,\"-7306231359468239480\",\"9A9B0D49E4138588\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mvBJI1-3_evoTnQbfBils-9MdQ1M6nlBT367tpHVBknH2Pxq8uZ_YFGOUuWP3hylCrpVPATsL9DfxHUHs9Xbsj8V-0JXg_-HE\",,,,\"lotrcz\u0040hotmail.com\",,,,,,,,,,,,,,,\"Peng Zhao\",\"0\"],[\"8b1f0643-0000-0000-0000-000000000000\",[[\"408e7470-1348-4b29-aca2-146267df3a77\",6,12,[[\"1\u003anwpu_heqiao\u0040msn.com\"]],\"WL\",\"-363000675130349932\"]],\"\u6865\",\"\u4f55\",,,\"-363000675130349932\",\"FAF65CCF29A0E694\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1m31iAMsna4NQxaqSNUBC6_oT0lzpRH9GnhUOa-oJd3TV-wTvnxHhFByVlgYV8ZEow5yGZob0EIizPTUDUaojrcA\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"nwpu_heqiao\u0040msn.com\",,,,,,,,,,,,,,,\"\u4f55\u6865\",\"8\"],[\"6dfdc16e-0000-0000-0000-000000000000\",[[\"1ef6df28-13da-411c-813d-14aee13c60ab\",6,12,[[\"1\u003acskingslayer\u0040hotmail.com\"]],\"WL\",\"5155067683304073976\"]],\"Shan\",\"Cao\",,,\"5155067683304073976\",\"478A7ABEC86F6AF8\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f40\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"cskingslayer\u0040hotmail.com\",,,,,,,,,,,,,,,\"Shan Cao\",\"0\"],[\"a3297266-0000-0000-0000-000000000000\",[[\"d366c3fe-5a90-4ffa-998b-19e946ea5935\",6,524556,[[\"1\u003axiachang31\u0040hotmail.com\"]],\"WL\",\"-3283523900107376245\",,,,,[,,,,,,,,,,]]],\"\u7d2b\u7535\",\"xia\",,[\"13476198651\",,,,,,,,,,],\"-3283523900107376245\",\"D26E9468E298398B\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f52\",,,,\"xiachang31\u0040hotmail.com\",[1986,11,5],,,,,,,,,,,,,,\"xia\u7d2b\u7535\",\"8\"],[\"bb833fd3-0000-0000-0000-000000000000\",[[\"cb3e3199-db0d-4085-b20b-1ca2a6a5d76b\",6,12,[[\"1\u003azuoyu515\u0040hotmail.com\"]],\"WL\",\"-8460029011854193190\"]],\"\u8273\",\"\u5360\",,,\"-8460029011854193190\",\"8A97F06FD6EE8DDA\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1m-8lG_ThSbhL_XJgqR7uWWCw7emu8jjuzuQs3Le-zqAusDbMPIC7_EMip9M51rmr_8TF4sBHtAfzW9jWdVsDfYg\",[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\"],,,\"zuoyu515\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5360\u8273\",\"8\"],[\"92043d2d-0000-0000-0000-000000000000\",[[\"11c06bf6-9252-43f2-a571-1f988d0d9611\",5,32,[],\"ABCH\",\"2\u003a11c06bf6-9252-43f2-a571-1f988d0d9611\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"taok\u0040wholewise.com\",,]],[\"a24000b8-0000-0000-0000-000000000000\",[[\"e5f18c34-5a08-476a-ae19-2000777f31b4\",6,12,[[\"1\u003ababyrabbit1215\u0040hotmail.com\"]],\"WL\",\"-4027328184755659546\"]],\"\u6f9c\",\"\u6885\",,,\"-4027328184755659546\",\"C81C0E5A89B85CE6\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mvBJI1-3_evpbddOZZgevGhKa_n0q8LZD2ugeR9_W43UNblVVo8mqXHJLzu-Y9i5UmFMIuXEXfRNjjrjim3NYNgCEFmu4OY-y\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"babyrabbit1215\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u6885\u6f9c\",\"8\"],[\"fdaf6f49-0000-0000-0000-000000000000\",[[\"0a96cc09-9d95-4443-9029-215d9877e156\",5,40,[],\"ABCH\",\"2\u003a0a96cc09-9d95-4443-9029-215d9877e156\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"Hang\",\"Chengfang\",[\"chang\u0040ss.pku.edu.cn\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"Hang Chengfang\",\"0\"],[\"4ad3c194-0000-0000-0000-000000000000\",[[\"884c7bac-678a-4a99-b74d-231dfc7ad5d5\",6,,[],\"WL\",\"1057116643853560831\"]],,,,,\"1057116643853560831\",\"EABA1FC9B0B6BFF\",,\"3\",,,[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"pkuss.sh\u0040hotmail.com\"],[\"0822c759-0000-0000-0000-000000000000\",[[\"abddf27b-0dcf-457f-8500-23654b03fe8e\",5,32,[],\"ABCH\",\"2\u003aabddf27b-0dcf-457f-8500-23654b03fe8e\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"wangl\u0040ss.pku.edu.cn\",,]],[\"5c1ebe46-0000-0000-0000-000000000000\",[[\"89494624-5e46-47b2-a37b-272f630e455b\",6,12,[[\"1\u003asongshang951\u0040163.com\"]],\"WL\",\"349482517895816538\"]],\"\u9ed8\u5c14\u6839\",\"bai\",,,\"349482517895816538\",\"4D99C7F9663A55A\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f18\",,,,\"songshang951\u0040163.com\",,,,,,,,,,,,,,,\"bai\u9ed8\u5c14\u6839\",\"8\"],[\"c5bd42a3-f949-46c3-ba24-295318cfb798\",[[,1,13,[],\"WL\",\"-6404091471428156281\",\"foo_zhu\u0040hotmail.com\"]],\"\u6768\",\"\u859b\",,,\"-6404091471428156281\",\"A72018BCB06A8487\",,\"0\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mUwvaG6LCHzhQ7xA8fButv8Os60tes8lAnl9vYeXl5DY_g-YqFNKCnkFCScB3qe7Yeqdiwx0qpfSyq9mzPDSi4A\",,,,\"foo_zhu\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u859b\u6768\",\"8\"],[\"f461a5b9-0000-0000-0000-000000000000\",[[\"3c1a82f0-d871-4a0b-b216-2b1fc18a14f2\",6,12,[[\"1\u003axianachen\u0040hotmail.com\"]],\"WL\",\"-1391259473451566644\"]],\"xiana\",\"Chen\",,,\"-1391259473451566644\",\"ECB140E14E85B9CC\",,\"3\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mcN-QmxmvAfxlp6p9p7x_erIWjoeZSVh8lrnQi_Kmp0LXbGgHYPmt4-NsWt86i8eLV-oSj4skqepN3tlyCo2www\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"xianachen\u0040hotmail.com\",,,,,,,,,,,,,,,\"xiana Chen\",\"0\"],[\"daf7a78c-0000-0000-0000-000000000000\",[[\"dbd26805-52af-481d-8212-2b20aaa3c8a5\",6,12,[[\"1\u003aelily_cn\u0040hotmail.com\"]],\"WL\",\"-3238100205355711201\"]],\"\u67ab\",\"\u674e\",,,\"-3238100205355711201\",\"D30FF504F8A7451F\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f19\",,,,\"elily_cn\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u674e\u67ab\",\"8\"],[\"e300ce7a-0000-0000-0000-000000000000\",[[\"b24eb232-17e1-4f34-964d-2b994305988f\",6,525116,[],\"WL\",\"6418469548435606250\",,,,[,,],[,,,,,,,,,,],,,[0,0,0],[\"0\",,]]],\"lemonbo\",\"george\",[\"lemonbo\u0040hotmail.com\",,],[\"13588855834\",\"0571\uff0d81950181\",,,,,,,,,],\"6418469548435606250\",\"5912FC0C3C2A8AEA\",,\"2\",\"\u6d59\u6c5f\u7701\u7ecf\u6d4e\u4fe1\u606f\u4e2d\u5fc3\",\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mp3o4EEQbTE52-zjdSVqCRJjdf0QihxV7rinTNDQHtcgOfzdqtxVOrW9wxLt4lWaiBpwSwqHSgkqHhSvxtg07fA\",[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\",\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\",\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"lemonbo\u0040hotmail.com\",[1983,1,23],,,,,,,,,,,,,,\"lemonbo george\",\"0\"],[\"b85350e7-0000-0000-0000-000000000000\",[[\"fd62622c-8be2-447d-b422-2db5ff3bf870\",5,40,[],\"ABCH\",\"2\u003afd62622c-8be2-447d-b422-2db5ff3bf870\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,\"hunan\",[\"flowerboat\u0040eyou.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"hunan\",\"0\"],[\"57bf0449-0000-0000-0000-000000000000\",[[\"5a26cb84-ea03-4597-a0a0-31014994841f\",6,12,[[\"1\u003aivan442889405\u0040hotmail.com\"]],\"WL\",\"-1923609108480781354\"]],\"ivan\",\"shan\",,,\"-1923609108480781354\",\"E54DF7B5224C13D6\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f12\",[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\"],,,\"ivan442889405\u0040hotmail.com\",,,,,,,,,,,,,,,\"ivan shan\",\"0\"],[\"84db149a-0000-0000-0000-000000000000\",[[\"0221bc39-5532-4830-b144-3352b147ba8b\",6,14,[[\"1\u003ajones_shanghai\u0040hotmail.com\"]],\"WL\",\"7670800789200356076\"]],\"jones\",\"deng\",,,\"7670800789200356076\",\"6A7428AD1712C2EC\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mTesQPO_7xnhb2K2mrbkl81qCLl6oUOELHuVek4eklntDeTjvKprU6f_bY_cRmRVxGRnrOLMB-M4A0Kld2yekww\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,1000,\"jones_shanghai\u0040hotmail.com\",,,,,,,,,,,,,,,\"jones deng\",\"0\"],[\"5a563043-0000-0000-0000-000000000000\",[[\"717c6359-de4b-42e1-8e17-365ff82b12ed\",6,12,[[\"1\u003ayashir0_x\u0040hotmail.com\"]],\"WL\",\"-2196255696901918359\"]],\"\u4e91\u9f99\",\"\u8bb8\",,,\"-2196255696901918359\",\"E1855514763BB169\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mvBJI1-3_evoX0wEoqgletiN_Jem-nqdKhLXBUy46OSUOhdxQl50TYnfXo_iDpT6WCalHskOOV1jpR7wQ5olWrd_EttOzotDs\",,,,\"yashir0_x\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u8bb8\u4e91\u9f99\",\"8\"],[\"3e0ee9b0-0000-0000-0000-000000000000\",[[\"ec4518ae-f9a0-4474-a47a-366067876f3e\",6,12,[[\"1\u003alinggchin\u0040hotmail.com\"]],\"WL\",\"-5747470876341983004\"]],\"\u6a2a\",\"\u4e91\",,,\"-5747470876341983004\",\"B03CE1B07ABBA8E4\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f32\",[\"2a66fa24-6f80-4df4-8419-68200267941b\"],,,\"linggchin\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u4e91\u6a2a\",\"8\"],[\"899bb966-0000-0000-0000-000000000000\",[[\"4c15381a-55e3-4b77-b642-4111dfd63a51\",6,44,[[\"1\u003awd2_zl\u0040hotmail.com\"]],\"WL\",\"1554621274874584667\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u4e1c\",\"\u5434\",[\"wd2_zl\u0040yahoo.com.cn\",,],,\"1554621274874584667\",\"15931FD0112A1E5B\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mUORxDbg7L7c0YH7jT9543mCoesZFFUkc68MITALWTFzZPRbxf9vDNgspW01gMfrR4EjUZo0IGOdVw5EsL61mig\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"wd2_zl\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5434\u4e1c\",\"8\"],[\"62ad93a4-0000-0000-0000-000000000000\",[[\"6fb8243a-d4fe-4727-bb25-41f6e3552ca0\",6,12,[[\"1\u003ayangli_rr831\u0040hotmail.com\"]],\"WL\",\"-5201898092572520241\"]],\"Alejandro\",\"\u61ff\",,,\"-5201898092572520241\",\"B7CF253EE26ED0CF\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mzL40Ka7fzv6YcuqNeaFSiM8qU-JLtNSaywuLUwuIh5FGlCW4XMMUOpIqxL3_7nbS4z3VUuJhYOereqlSUwbqvg\",,,,\"yangli_rr831\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u61ffAlejandro\",\"8\"],[\"51e4c735-0000-0000-0000-000000000000\",[[\"1deae513-c9d2-4d8e-96be-43e4ec98e3dc\",6,12,[[\"1\u003apeiqihere\u0040hotmail.com\"]],\"WL\",\"124313712763929297\"]],\"qi\",\"pei\",,,\"124313712763929297\",\"1B9A6A9C20102D1\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mDok9wsMW9yXYNoe_DwB92lRGgjqZUHhZspBRs_E0Pf-dVP2Awh2oTOY5TtHf8LOhXjauyT0N4f_yg1L70wRsFg\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"peiqihere\u0040hotmail.com\",,,,,,,,,,,,,,,\"qi pei\",\"0\"],[\"b609e801-0000-0000-0000-000000000000\",[[\"6b4ee446-c9ba-49f8-9f51-44eef75aa724\",5,32,[],\"ABCH\",\"2\u003a6b4ee446-c9ba-49f8-9f51-44eef75aa724\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"moononthesea\u0040eyou.com\",,]],[\"ffdde51f-0000-0000-0000-000000000000\",[[\"dacd7777-23d9-434b-8f4e-49caf3ed8831\",5,40,[],\"ABCH\",\"2\u003adacd7777-23d9-434b-8f4e-49caf3ed8831\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,\"\u6b66\u6c49\u4e49\u5de5\u8054\",[\"whyigong\u0040163.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u6b66\u6c49\u4e49\u5de5\u8054\",\"8\"],[\"ad50a4b3-0000-0000-0000-000000000000\",[[\"d41c3663-a069-47d7-b425-4b2b5977e194\",6,8,[[\"1\u003ajenny20093.chen\u0040hotmail.com\"]],\"WL\",\"6294845457322926495\"]],\"\u9648\u8273\",,,,\"6294845457322926495\",\"575BC89787B6E19F\",,\"2\",,,,,,\"jenny20093.chen\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u9648\u8273\",\"8\"],[\"773c1a50-0000-0000-0000-000000000000\",[[\"b6ce0a59-eb3c-459e-b91b-4b622800109f\",5,40,[],\"ABCH\",\"2\u003ab6ce0a59-eb3c-459e-b91b-4b622800109f\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u730e\u5934\u7f51\",,[\"service-784C\u0040lietoumail.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u730e\u5934\u7f51\",\"8\"],[\"c56cafbb-0000-0000-0000-000000000000\",[[\"600f80b3-41fa-4efa-bb7b-50cc281eac7e\",6,12,[[\"1\u003ataranchen\u0040live.com\"]],\"WL\",\"4433935645952201387\"]],\"Taran\",\"Chen\",,,\"4433935645952201387\",\"3D8880FA5DEC52AB\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f17\",[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\"],,,\"taranchen\u0040live.com\",,,,,,,,,,,,,,,\"Taran Chen\",\"0\"],[\"9e7b31ae-0000-0000-0000-000000000000\",[[\"af95df78-f414-4e66-a8bc-55523fa03aea\",7,12,[],\"WL\",\"1038942303214580268\",\"rkxu_7c1w\u0040msn.cn\"]],\"\u7389\u6885\",\"\u6768\",,,\"1038942303214580268\",\"E6B10852D3B322C\",,\"3\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mActbFxDqRLUQGgyTpF44Ra0XjeEJYWAzXRZqy5HlYY8sBmXbme_4ftnhJCpIeJsvCyNAuXoUPS22mhZTr9p7eg\",[\"8f2c0aac-44ce-4c10-bea1-8483586f01d3\"],1,,\"rkxu_7c1w\u0040msn.cn\",,,,,,,,,,,,,,,\"\u6768\u7389\u6885\",\"8\"],[\"6340b25b-0000-0000-0000-000000000000\",[[\"a8c13846-e262-4126-979b-58abe3743459\",5,32,[],\"ABCH\",\"2\u003aa8c13846-e262-4126-979b-58abe3743459\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"pengz\u0040innov8tion.com\",,]],[\"06cd9e34-0000-0000-0000-000000000000\",[[\"2069f83c-31f4-42ed-b3eb-6158f2d6f77f\",6,12,[[\"1\u003arenxn\u0040hotmail.com\"]],\"WL\",\"-8805826437840024847\"]],\"Ren\",\"ren\",,,\"-8805826437840024847\",\"85CB6B8292CD4AF1\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mbOxo6DHfK31aGL9p9F7Ol94cZyMV1Nvu8P0Uxe9A_YwrwOutDmntcCHhlat2A9u1LxZgtOVnTw6FSElA2ZIVnw\",[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\"],,,\"renxn\u0040hotmail.com\",,,,,,,,,,,,,,,\"Ren ren\",\"0\"],[\"e3cc3a8a-0000-0000-0000-000000000000\",[[\"8cdb2308-ca9e-46e2-946b-66adff718e6a\",6,12,[[\"1\u003alianlian2211\u0040hotmail.com\"]],\"WL\",\"1199384150809256649\"]],\"\u8fde\",\"\u8fde\",,,\"1199384150809256649\",\"10A51187D23822C9\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f50\",,,,\"lianlian2211\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u8fde\u8fde\",\"8\"],[\"0b5be0fc-0000-0000-0000-000000000000\",[[\"6e7d2b64-f686-4338-99be-68b3d14a2659\",6,524300,[[\"1\u003awilliamwulm\u0040hotmail.com\"]],\"WL\",\"1908233322808490638\"]],\"\u7acb\u660e\",\"\u5434\",,,\"1908233322808490638\",\"1A7B6818CA903A8E\",,\"3\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f29\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"williamwulm\u0040hotmail.com\",[1982,5,23],,,,,,,,,,,,,,\"\u5434\u7acb\u660e\",\"8\"],[\"6aff1223-0000-0000-0000-000000000000\",[[\"f69d7ec4-3926-43e5-acd5-769b5a88f061\",7,524300,[],\"WL\",\"-7306753860918778206\",\"linda77918\u0040hotmail.com\"]],\"linda\",\"linda\",,,\"-7306753860918778206\",\"9A9932138AB8E6A2\",,\"3\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1me5gxiBvRzKNondsGJv7thYPTrzVM9CF1sOom4EP6fGSBk6-dazJLl_WvFY2PJ60eNMvBTcRcj93HCr7LK3U2kw\",[\"8f2c0aac-44ce-4c10-bea1-8483586f01d3\"],1,,\"linda77918\u0040hotmail.com\",[,9,18],,,,,,,,,,,,,,\"linda linda\",\"0\"],[\"ae723e03-0000-0000-0000-000000000000\",[[\"3362473b-d94d-46b8-9e17-7b656805db32\",5,40,[],\"ABCH\",\"2\u003a3362473b-d94d-46b8-9e17-7b656805db32\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u80e1\u660e\u534e\",,[\"qiqihhh.527\u0040163.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u80e1\u660e\u534e\",\"8\"],[\"ca0fbfb3-0000-0000-0000-000000000000\",[[\"80473bae-0d6c-492d-b263-7b970f218cf6\",6,,[[\"1\u003aSandaPyer0268\u0040hotmail.com\"]],\"WL\",\"-3384879031895618797\"]],,,,,\"-3384879031895618797\",\"D1067E7450318B13\",,\"3\",,,,,,\"SandaPyer0268\u0040hotmail.com\"],[\"e2910454-0000-0000-0000-000000000000\",[[\"7ec9b469-aeaf-4f95-988e-7ca330f46af5\",6,,[],\"WL\",\"5172360302899265065\"]],,,,,\"5172360302899265065\",\"47C7EA4A88A47E29\",,\"0\",,,,,,\"129t\u0040live.cn\"],[\"babb8427-0000-0000-0000-000000000000\",[[\"fc7f0fee-0f76-4900-bc01-7dd464c301ec\",5,40,[],\"ABCH\",\"2\u003afc7f0fee-0f76-4900-bc01-7dd464c301ec\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u6d2a\u60a6\",\"\u5b59\",[\"hongyuesun\u0040yahoo.com.cn\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u5b59\u6d2a\u60a6\",\"8\"],[\"1b8b424d-0000-0000-0000-000000000000\",[[\"41e3bdd7-5524-4b83-b69e-7e77e7068a9a\",5,32,[],\"ABCH\",\"2\u003a41e3bdd7-5524-4b83-b69e-7e77e7068a9a\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"admin\u0040hh6y.com\",,]],[\"dd514e40-0000-0000-0000-000000000000\",[[\"d987879f-a041-4fc9-b089-7e9ba6b03a05\",6,12,[[\"1\u003apaypie\u0040gmail.com\"]],\"WL\",\"262711742675716037\"]],\"\u58ee\u575a\",\"chen\",,,\"262711742675716037\",\"3A556F0248287C5\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mvBJI1-3_evq2IqOZFDn3QrO8MQm869aIqoTh5wv9n7TooZBQcp9qCZFn1EAJ8Rr6ry-rBwCozfL1fL2HA41kmuMKfajlwfm7\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"paypie\u0040gmail.com\",,,,,,,,,,,,,,,\"chen\u58ee\u575a\",\"8\"],[\"c006078c-0000-0000-0000-000000000000\",[[\"8bc8dca8-bcf5-4c67-92e7-7f236369a499\",6,12,[[\"1\u003alrh815\u0040hotmail.com\"]],\"WL\",\"9087955614298409298\"]],\"\u8363\u534e\",\"\u5218\",,,\"9087955614298409298\",\"7E1EE77B2CC60952\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mMQXiXW5cHhjSg8NsWaIUW1a_NT3Z-zDlUBctqCc3bIFSNudqBMZK_lu8nlmDbgS2McKk3U9C12YyVquGuxH8AQ\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"lrh815\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5218\u8363\u534e\",\"8\"],[\"bb0a722a-0000-0000-0000-000000000000\",[[\"1e82feeb-f0bd-4a83-b82e-7f38c5a037f5\",5,40,[],\"ABCH\",\"2\u003a1e82feeb-f0bd-4a83-b82e-7f38c5a037f5\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u4e07\u5cf0\",\"\u6731\",[\"zwf0704\u0040126.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u6731\u4e07\u5cf0\",\"8\"],[\"1e1cd5c1-0000-0000-0000-000000000000\",[[\"77a1ec43-d497-41ef-b6e2-8167ea382fef\",6,12,[[\"1\u003axiaofen127\u0040hotmail.com\"]],\"WL\",\"-5415904515078049070\"]],\"\u5b81\",,,,\"-5415904515078049070\",\"B4D6D789C956B2D2\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mvBJI1-3_evpSIxCXNW_XWrm52SLHj7S-H3IjysLgIBopsOcDCPNnBtaR3DtU3t6npkrnoVwW4jalKOxe5cgqqTSmsVUaSjrn\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"xiaofen127\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5b81\",\"8\"],[\"b02a062c-0000-0000-0000-000000000000\",[[\"56c3161a-43fc-45c8-9580-8c07fd63b7a6\",6,12,[[\"1\u003ajiang2000wei\u0040passport.com\"]],\"WL\",\"-6564110322538148839\"]],\"wei\",\"jiang\",,,\"-6564110322538148839\",\"A4E7987096A38C19\",,\"2\",,\"https\u003a\u002f\u002fsn2files.storage.msn.com\u002fy1m3fLg2C-3ExVPsJqq9TVwNCkMWdortojmnz2_y9s_bmqGIoiGb51-WqueXfpxjtPHcvn3p9K6778MXFlJQ4S1Ow\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"jiang2000wei\u0040passport.com\",,,,,,,,,,,,,,,\"wei jiang\",\"0\"],[\"9962b2b3-0000-0000-0000-000000000000\",[[\"2cb017ad-52eb-45ee-84a5-8df3eb8081f3\",6,12,[[\"1\u003aqicaiweiwei\u0040hotmail.com\"]],\"WL\",\"-6493533186218415604\"]],\"\u5408\",\"\u97e6\",,,\"-6493533186218415604\",\"A5E255F8AA3DAA0C\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f12\",,,,\"qicaiweiwei\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u97e6\u5408\",\"8\"],[\"f87ef54c-0000-0000-0000-000000000000\",[[\"9a4ceb75-3bb9-4fac-9688-9284f65d9e7d\",6,12,[[\"1\u003anowo\u0040sohu.com\"]],\"WL\",\"-1182227775975206993\"]],\"Lionel\",\"Li\",,,\"-1182227775975206993\",\"EF97E219F965CFAF\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mEL4ehCN0z6uCaNbLBfa62zBjVpwJnpsRhO5TZytHuqO_tu29usgqU9UMZBzO1rvyG0BMskmD4mGwRmgkpzxvfg\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"nowo\u0040sohu.com\",,,,,,,,,,,,,,,\"Lionel Li\",\"0\"],[\"edd44ce5-0000-0000-0000-000000000000\",[[\"1c19d138-e6c5-4302-a210-9295312bc1a8\",6,12,[[\"1\u003ahaman910\u0040hotmail.com\"]],\"WL\",\"6977870102474390951\"]],\"\u5f69\u534e\",,,,\"6977870102474390951\",\"60D65FE34ACDFDA7\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f20\",,,,\"haman910\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5f69\u534e\",\"8\"],[\"6741a740-0000-0000-0000-000000000000\",[[\"6b6f2cbc-3300-4d65-8110-9727460e6c3b\",6,12,[[\"1\u003ajiemin_fon543\u0040hotmail.com\"]],\"WL\",\"-5814358484566095752\"]],\"\u5cb8\u51b0\",\"\u5b59\",,,\"-5814358484566095752\",\"AF4F3FC39AA8C878\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mbWL7SDUPmr-PTrqO_KbxxYXYxIYjbzKUWKNSzPP8hk2IyO-rI6e9g_Kj3EkcEqycM7MdJcAXJKcxl8MM0JTCfw\",,,,\"jiemin_fon543\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5b59\u5cb8\u51b0\",\"8\"],[\"c61d96b6-0000-0000-0000-000000000000\",[[\"25a807b8-b981-4d3e-8690-977293dd8d1d\",6,12,[],\"WL\",\"-1431882152034112144\"]],\"\u5a67\u5a67\",\"\u9f9a\",,,\"-1431882152034112144\",\"EC20EEC4E7C8ED70\",,\"0\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mUL1DEoHnmPjVv0TvhX2BfoJDx51nU2SLZ9liTDXR8UZoXROyg08HEBw2J25qRONSxvhoVoNwfOTB3JQ3ZA0_Bg\",,,,\"snv_dsailn_hs33908106\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u9f9a\u5a67\u5a67\",\"8\"],[\"4dd195de-0000-0000-0000-000000000000\",[[\"491cc47f-b385-46a2-83ef-97d76c781c51\",5,32,[],\"ABCH\",\"2\u003a491cc47f-b385-46a2-83ef-97d76c781c51\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"yanzhi910\u0040sina.com\",,],,,,,,,,[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\",\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"]],[\"3ae68385-0000-0000-0000-000000000000\",[[\"7d2ce48f-0e23-4daa-92ff-9f5d4f26d57e\",6,,[],\"WL\",\"387944514367665553\"]],,,,,\"387944514367665553\",\"562417AFB723591\",,\"0\",,,,,,\"group80804\u0040msnzone.cn\"],[\"81b52979-0000-0000-0000-000000000000\",[[\"4c706044-284d-4d64-87e2-a3358d839cbd\",6,12,[[\"1\u003alinjingnan\u0040hotmail.com\"]],\"WL\",\"1964323716296292033\"]],\"Davis\",\"lin\",,,\"1964323716296292033\",\"1B42AE0335B736C1\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mpWXErGvZqEDxcNH8OJkSDjNGt1oLAiWOb-zYugP3uCG4YUaa3C7nRjb3l_HAHcssQoKBOHPM6m57plVX0Ce6Wg\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"linjingnan\u0040hotmail.com\",,,,,,,,,,,,,,,\"Davis lin\",\"0\"],[\"fca337fc-0000-0000-0000-000000000000\",[[\"6892a52f-3d7e-4aae-a58c-a46658ddf7a0\",6,4,[[\"1\u003aIrisOstorgaceyad\u0040hotmail.com\"]],\"WL\",\"4672894743164765605\"]],,,,,\"4672894743164765605\",\"40D97502C72045A5\",,\"3\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mr5GJ3gscOVFHBexnopElnuT9GBMfHvc2GGFHi4rIki3gfXTkuRHtfLYsWJ2n9x-cfkfDtn02HPh3p5blHxSujA\",,,,\"IrisOstorgaceyad\u0040hotmail.com\"],[\"e20b8869-0000-0000-0000-000000000000\",[[\"e4945052-f637-47af-9bbb-a8a0205848e8\",6,12,[[\"1\u003aliulitao_2005\u0040hotmail.com\"]],\"WL\",\"3348658396509933718\"]],\"\u529b\u6dd8\",\"\u5218\",,,\"3348658396509933718\",\"2E78D311D61A9096\",,\"0\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fstatic\u002f22\",,,,\"liulitao_2005\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5218\u529b\u6dd8\",\"8\"],[\"3f672694-0000-0000-0000-000000000000\",[[\"1e0ef049-8936-41bb-83bf-a967cf61617d\",5,40,[],\"ABCH\",\"2\u003a1e0ef049-8936-41bb-83bf-a967cf61617d\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"\u4ed8\u5efa\u59b9\",,[\"loveautumntrees\u0040yahoo.com\",,],,,,,,,,,,,,,,,,,,,,,,,,,,\"\u4ed8\u5efa\u59b9\",\"8\"],[\"e94bac80-0000-0000-0000-000000000000\",[[\"28d8a885-6097-4697-82b0-afde69679329\",6,12,[[\"1\u003afreshgloria\u0040hotmail.com\"]],\"WL\",\"-9076910334260965236\"]],\"jieru\",\"wang\",,,\"-9076910334260965236\",\"820856247C4D908C\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mMhsSs2QLb7o5YtW8_kAEvr86J6atQHk9v30qEn0d65g-kbnMabPh59Mr55D33rVYZwzUC9MwcRlBVl2GfJAxxg\",[\"2a66fa24-6f80-4df4-8419-68200267941b\"],,,\"freshgloria\u0040hotmail.com\",,,,,,,,,,,,,,,\"jieru wang\",\"0\"],[\"f50e37c1-0000-0000-0000-000000000000\",[[\"9f5974a3-b638-489a-b1a4-b0ee74f9c236\",6,12,[],\"WL\",\"7831371618936645110\"]],\"\u5fb7\u519b\",\"\u674e\",,,\"7831371618936645110\",\"6CAE9EFEBACB65F6\",,\"2\",,\"https\u003a\u002f\u002fsn2files.storage.msn.com\u002fy1mvBJI1-3_evq_TYdNCrFg7OxcB4BklVD93j3Qoyf5jlbazGNQ6W3Neb-axdw4MoUE1gnod30oPdY3HLkPjWhzMFf4G10RFtpq\",,,,\"akhanxun\u0040msn.cn\",,,,,,,,,,,,,,,\"\u674e\u5fb7\u519b\",\"8\"],[\"26c0d958-0000-0000-0000-000000000000\",[[\"3f4e3d6e-0934-4a02-8714-b2bdb747000c\",5,32,[],\"ABCH\",\"2\u003a3f4e3d6e-0934-4a02-8714-b2bdb747000c\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"sshw\u0040ebusiness.pku.edu.cn\",,]],[\"cc0fb24b-0000-0000-0000-000000000000\",[[\"79078728-2332-454c-ae78-b5c315e2213a\",6,,[],\"WL\",\"3952462178009739945\"]],,,,,\"3952462178009739945\",\"36D9F768EC6FF2A9\",,\"0\",,,,,,\"aixinqm1197\u0040hotmail.com\"],[\"12d01486-0000-0000-0000-000000000000\",[[\"95cc4e6c-88b7-436a-9638-c27e6d2189c0\",6,12,[[\"1\u003achizhun_6160\u0040hotmail.com\"]],\"WL\",\"-7228842393994010889\"]],\"\u6d77\u98ce\",\"\u738b\",,,\"-7228842393994010889\",\"9BADFE2488B3DEF7\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mjXuG2FpA-vDqgSur2QthI-1Z7Y-rXSQ1VVV2J1uCUsEL74CYTqFokRCw9rZuHh6LYJT3Dwv8-ZQK1fKIMT5cHg\",,,,\"chizhun_6160\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u738b\u6d77\u98ce\",\"8\"],[\"36f851d0-0000-0000-0000-000000000000\",[[\"3f19b81c-76ae-4310-979f-c9408cf39c13\",6,12,[[\"1\u003aliuxb76\u0040sina.com\"]],\"WL\",\"-3006515352779977910\"]],\"Bean\",\"Liu\",,,\"-3006515352779977910\",\"D646B63741CA8B4A\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mtkBshSRGO6-_4p-vGFXryKk-t9kV8OK3srrf8sq2g99PRtVjB9VKMpBrGPwMDszytIYkR3e3z8scn6H_zoYbDg\",[\"2a66fa24-6f80-4df4-8419-68200267941b\"],,,\"liuxb76\u0040sina.com\",,,,,,,,,,,,,,,\"Bean Liu\",\"0\"],[\"d19d9556-0000-0000-0000-000000000000\",[[\"9c68864b-e929-492f-b59e-c950defcae70\",7,527228,[],\"WL\",\"8724036686077023073\",\"ptchm\u0040hotmail.com\",,,[,,],[,,,,,,,,,,],,,[0,0,0],[\"0\",\"0\",]]],\"\u6c49\u6c11\",\"\u66f9\",[\"caohanmin\u0040gmail.com\",\"ericcao\u0040haojiaolian.com\",],[\"13817994388\",\"021-34504711\",,\"021-34504711\",,,,,,,],\"8724036686077023073\",\"79120124E118D361\",,\"3\",\"\u597d\u6559\u7ec3\u7f51\",\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mtOiG8lo6y-qVGz44ILezwsB-IWJdtbpHP2shxwOCYI4rWqFjaQ8vVqbT_qp0HtrfoV6bE0I2xwTek_uhr-N2gQ\",[\"8f2c0aac-44ce-4c10-bea1-8483586f01d3\"],1,,\"ptchm\u0040hotmail.com\",[1977,3,3],,,,,,,,,,,,,,\"\u66f9\u6c49\u6c11\",\"8\"],[\"cce1140a-0000-0000-0000-000000000000\",[[\"b6d12e8d-06f9-4771-97a8-d012fa654f48\",6,,[],\"WL\",\"4484485686597521949\"]],,,,,\"4484485686597521949\",\"3E3C17F8DDF8121D\",,\"0\",,,,,,\"mgroup107129\u0040hotmail.com\"],[\"637a20b7-0000-0000-0000-000000000000\",[[\"1d07963d-815b-478e-8ca2-d0e90009c7e2\",6,12,[[\"1\u003akanjia99\u0040hotmail.com\"]],\"WL\",\"-7522939348799570843\"]],\"\u599e\u599e\",\"\u674e\",,,\"-7522939348799570843\",\"979926853FFE3C65\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1msRs92n1Znuvojujyg-XGHClSzGeOw-uaKg84PICPBgpFOJOdVwNeSu_AMvg5gVKcfj__XwPGl0VjyZm05zPKXQ\",,,,\"kanjia99\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u674e\u599e\u599e\",\"8\"],[\"0e7819ee-0000-0000-0000-000000000000\",[[\"9afbf065-95e9-47b9-b8d3-d41eb01afa2a\",6,524572,[[\"1\u003atankangxian\u0040hotmail.com\"]],\"WL\",\"-3892751113517524509\",,,,,[,,,,,,,,,,]]],\"\u5eb7\u8d24\",\"\u8c2d\",,[\"15920382580\",,,,,,,,,,],\"-3892751113517524509\",\"C9FA2B7CA7C819E3\",,\"2\",\"\u7f51\u805a\u65e0\u9650\u901a\u4fe1\u6280\u672f\u6709\u9650\u516c\u53f8\",\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f52\",,,,\"tankangxian\u0040hotmail.com\",[1983,12,10],,,,,,,,,,,,,,\"\u8c2d\u5eb7\u8d24\",\"8\"],[\"c7f2ab71-0000-0000-0000-000000000000\",[[\"9dd85ea2-f871-4c28-8461-d5e38336e84d\",6,12,[[\"1\u003ajiangwei\u0040hotmail.com\"]],\"WL\",\"-1901377574618817850\"]],\"Wei\",\"Jiang\",,,\"-1901377574618817850\",\"E59CF32B78F39AC6\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mns4u_Z8_f9gpAbkVmHdHmfioHjetNZfWGfcj1HOXJc7eBioQFAXioXMNNvTb8wMTu2wGSbnfAmCCNAaQ4YujXA\",,,,\"jiangwei\u0040hotmail.com\",,,,,,,,,,,,,,,\"Wei Jiang\",\"0\"],[\"fccfa1ce-0000-0000-0000-000000000000\",[[\"0a0ea6a8-2a83-4175-ade5-d7fae1732595\",5,32,[],\"ABCH\",\"2\u003a0a0ea6a8-2a83-4175-ade5-d7fae1732595\",,,,[,,],,,,[0,0,0],[\"0\",,]]],,,[\"lynn.liu\u0040atosorigin.com\",,]],[\"67464770-0000-0000-0000-000000000000\",[[\"ea023ce9-d338-4bd9-87ee-e43b9683d3b4\",6,524300,[[\"1\u003aliuxin7777\u0040hotmail.com\"]],\"WL\",\"1555320089590732465\"]],\"\u5c0f\u6615\",\"\u5218\",,,\"1555320089590732465\",\"15959B618C1596B1\",,\"2\",,\"https\u003a\u002f\u002fsn2files.storage.msn.com\u002fy1mYXtv_t0cuuTWLCFc3t85I14D-eHvsfR-f-denggQUOrIJ60XXY22y5peDhxLg1LVRH7ycWe2EhYG_el-k1wZBQ\",[\"2a66fa24-6f80-4df4-8419-68200267941b\"],,,\"liuxin7777\u0040hotmail.com\",[,8,25],,,,,,,,,,,,,,\"\u5218\u5c0f\u6615\",\"8\"],[\"81884066-0000-0000-0000-000000000000\",[[\"b80cf698-4c4f-487f-81d7-ebedf86bcc04\",6,12,[[\"1\u003aabby_xi\u0040live.cn\"]],\"WL\",\"83061601078356499\"]],\"Xi\",\"Abby\",,,\"83061601078356499\",\"12718160AC02A13\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1mOBEh06fAUFxdGhwkpQvyeHv76zlihv7OD43AmwqfKVgGR1MrvQfPLsgmZMS8h5vu7GS-y5UHetyDkgPR8hfP_Q\",[\"2a66fa24-6f80-4df4-8419-68200267941b\"],,,\"abby_xi\u0040live.cn\",,,,,,,,,,,,,,,\"Xi Abby\",\"0\"],[\"211266ca-0000-0000-0000-000000000000\",[[\"a51682f0-71ea-4609-9e4a-f07c12aaed3a\",6,524588,[[\"1\u003ateam\u0040live.com\"]],\"WL\",\"372927556099006289\",,,,[,,],[,,,,,,,,,,],,,[0,0,0],[\"0\",,]]],\"Kevin\",\"Lee\",[\"team\u0040live.com\",,],[\"15542648247\",,,,,,,,,,],\"372927556099006289\",\"52CE7A2EC3C5F51\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mvBJI1-3_evrfc1c9IKosI7-3Oie_sWsfmy-jUTTfWN_7HH31pxbYBHPR49JL614da6D36CNwXvWFh5uMQw0MioDuve2i9rQZ\",,,,\"team\u0040live.com\",[,1,19],,,,,,,,,,,,,,\"Kevin Lee\",\"0\"],[\"171e9069-0000-0000-0000-000000000000\",[[\"99ac6ff7-1917-4be2-a50a-f5144bc79ffe\",6,,[[\"1\u003ame_ku22\u0040hotmail.com\"]],\"WL\",\"-4839042567557862226\"]],,,,,\"-4839042567557862226\",\"BCD8446C53B82CAE\",,\"0\",,,,,,\"me_ku22\u0040hotmail.com\"],[\"31674c57-0000-0000-0000-000000000000\",[[\"c95809a3-bdab-47e9-83fe-f892a6047b41\",6,60,[[\"1\u003asoway\u0040hotmail.com\"]],\"WL\",\"5938533260007235233\",,,,[,,],,,,[0,0,0],[\"0\",,]]],\"Soway\",\"Wu\",[\"soway\u0040hotmail.com\",,],,\"5938533260007235233\",\"5269E8894992DEA1\",,\"2\",\"Tindeo Inc.\",\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fy1m6BgHJiBelwXV6mT2GBcelamV3sQgIfZDZHK0UIMa7nR1jmvALVfvdT92XiudUoFEBhkrtNJiYKlem6s1ewzVWQ\",[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"soway\u0040hotmail.com\",,,,,,,,,,,,,,,\"Soway Wu\",\"0\"],[\"e9267580-0000-0000-0000-000000000000\",[[\"9f1fa123-356b-4507-b8ae-fab52cc10917\",6,12,[[\"1\u003asonichappy2003\u0040hotmail.com\"]],\"WL\",\"6477246887968273128\"]],\"zhe\",\"wang\",,,\"6477246887968273128\",\"59E3CDBA07DA72E8\",,\"2\",,\"https\u003a\u002f\u002fbyfiles.storage.msn.com\u002fy1mQj3sWmCuMsC_lSL62mGZWFnmwj66Qnk6WqYp1iaDkB6JROZxgtdr2bt-UKZVZkMEKlskq0h82UgtQ4ccmnE1Bw\",[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\"],,,\"sonichappy2003\u0040hotmail.com\",,,,,,,,,,,,,,,\"zhe wang\",\"0\"],[\"34a898f0-0000-0000-0000-000000000000\",[[\"d42b2336-8373-4260-a570-fdf57ecfad91\",6,12,[[\"1\u003aliuhy0704\u0040hotmail.com\"]],\"WL\",\"-5958464394041390715\"]],\"\u6d77\u52c7\",\"\u5218\",,,\"-5958464394041390715\",\"AD4F4833F2598985\",,\"2\",,\"https\u003a\u002f\u002fblufiles.storage.msn.com\u002fstatic\u002f18\",[\"2a66fa24-6f80-4df4-8419-68200267941b\",\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\"],,,\"liuhy0704\u0040hotmail.com\",,,,,,,,,,,,,,,\"\u5218\u6d77\u52c7\",\"8\"]],[[\"d0f50f87-4ac9-41e0-a669-10d38a4b0dc7\",\"\u8d2d\u9f99\"],[\"2a66fa24-6f80-4df4-8419-68200267941b\",\"\u540c\u4e8b\"],[\"8f2c0aac-44ce-4c10-bea1-8483586f01d3\",\"\u5e38\u7528\u8054\u7cfb\u4eba\",1],[\"cf4126aa-e0dd-4cb8-9891-93222cc2113b\",\"\u5317\u5927\"],[\"e88ddaa9-7569-4866-ae18-bc6f2c5fad49\",\"\u670b\u53cb\"],[\"f43df50e-9939-4c23-8abb-ec4189e7e3fe\",\"\u5bb6\u4eba\"]],[],[\"2\",,\"0\u007c0\u007c\u007c1\"],[[\"WL\",\"Messenger\",,,,\"http\u003a\u002f\u002fsecure.wlxrs.com\u002f\u0024live.controls.images\u002fsn\u002fPsaSmall\u002fWLActive5.png\",\"http\u003a\u002f\u002fsecure.wlxrs.com\u002f\u0024live.controls.images\u002fsn\u002fPsaMedium\u002fWL5.png\",\"http\u003a\u002f\u002fsecure.wlxrs.com\u002f\u0024live.controls.images\u002fsn\u002fPsaLarge\u002fWL5.png\",,,,,3]],1]]";

		s = fromEncodedUnicode(s.toCharArray(), 0, s.length());

		System.out.println("from unicode:\t" + s);

	}

	private static final char[] hexDigit = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A',

			'B', 'C', 'D', 'E', 'F' };

	private static char toHex(int nibble) {

		return hexDigit[(nibble & 0xF)];

	}

	/**
	 * 
	 * 将字符串编码成 Unicode 形式的字符串. 如 \"黄\" to \"\u9EC4\"
	 * 
	 * Converts unicodes to encoded \\uxxxx and escapes
	 * 
	 * special characters with a preceding slash
	 * 
	 * 
	 * 
	 * @param theString
	 * 
	 *            待转换成Unicode编码的字符串。
	 * 
	 * @param escapeSpace
	 * 
	 *            是否忽略空格，为true时在空格后面是否加个反斜杠。
	 * 
	 * @return 返回转换后Unicode编码的字符串。
	 */

	public static String toEncodedUnicode(String theString, boolean escapeSpace) {

		int len = theString.length();

		int bufLen = len * 2;

		if (bufLen < 0) {

			bufLen = Integer.MAX_VALUE;

		}

		StringBuffer outBuffer = new StringBuffer(bufLen);

		for (int x = 0; x < len; x++) {

			char aChar = theString.charAt(x);

			// Handle common case first, selecting largest block that

			// avoids the specials below

			if ((aChar > 61) && (aChar < 127)) {

				if (aChar == '\\') {

					outBuffer.append('\\');

					outBuffer.append('\\');

					continue;

				}

				outBuffer.append(aChar);

				continue;

			}

			switch (aChar) {

			case ' ':

				if (x == 0 || escapeSpace)
					outBuffer.append('\\');

				outBuffer.append(' ');

				break;

			case '\t':

				outBuffer.append('\\');

				outBuffer.append('t');

				break;

			case '\n':

				outBuffer.append('\\');

				outBuffer.append('n');

				break;

			case '\r':

				outBuffer.append('\\');

				outBuffer.append('r');

				break;

			case '\f':

				outBuffer.append('\\');

				outBuffer.append('f');

				break;

			case '=': // Fall through

			case ':': // Fall through

			case '#': // Fall through

			case '!':

				outBuffer.append('\\');

				outBuffer.append(aChar);

				break;

			default:

				if ((aChar < 0x0020) || (aChar > 0x007e)) {

					// 每个unicode有16位，每四位对应的16进制从高位保存到低位

					outBuffer.append('\\');

					outBuffer.append('u');

					outBuffer.append(toHex((aChar >> 12) & 0xF));

					outBuffer.append(toHex((aChar >> 8) & 0xF));

					outBuffer.append(toHex((aChar >> 4) & 0xF));

					outBuffer.append(toHex(aChar & 0xF));

				} else {

					outBuffer.append(aChar);

				}

			}

		}

		return outBuffer.toString();

	}

	/**
	 * 
	 * 从 Unicode 形式的字符串转换成对应的编码的特殊字符串。 如 \"\u9EC4\" to \"黄\".
	 * 
	 * Converts encoded \\uxxxx to unicode chars
	 * 
	 * and changes special saved chars to their original forms
	 * 
	 * 
	 * 
	 * @param in
	 * 
	 *            Unicode编码的字符数组。
	 * 
	 * @param off
	 * 
	 *            转换的起始偏移量。
	 * 
	 * @param len
	 * 
	 *            转换的字符长度。
	 * 
	 * @param convtBuf
	 * 
	 *            转换的缓存字符数组。
	 * 
	 * @return 完成转换，返回编码前的特殊字符串。
	 */

	public static String fromEncodedUnicode(char[] in, int off, int len) {

		char aChar;

		char[] out = new char[len]; // 只短不长

		int outLen = 0;

		int end = off + len;

		while (off < end) {

			aChar = in[off++];

			if (aChar == '\\') {

				aChar = in[off++];

				if (aChar == 'u') {

					// Read the xxxx

					int value = 0;

					for (int i = 0; i < 4; i++) {

						aChar = in[off++];

						switch (aChar) {

						case '0':

						case '1':

						case '2':

						case '3':

						case '4':

						case '5':

						case '6':

						case '7':

						case '8':

						case '9':

							value = (value << 4) + aChar - '0';

							break;

						case 'a':

						case 'b':

						case 'c':

						case 'd':

						case 'e':

						case 'f':

							value = (value << 4) + 10 + aChar - 'a';

							break;

						case 'A':

						case 'B':

						case 'C':

						case 'D':

						case 'E':

						case 'F':

							value = (value << 4) + 10 + aChar - 'A';

							break;

						default:

							throw new IllegalArgumentException(
									"Malformed \\uxxxx encoding.");

						}

					}

					out[outLen++] = (char) value;

				} else {

					if (aChar == 't') {

						aChar = '\t';

					} else if (aChar == 'r') {

						aChar = '\r';

					} else if (aChar == 'n') {

						aChar = '\n';

					} else if (aChar == 'f') {

						aChar = '\f';

					}

					out[outLen++] = aChar;

				}

			} else {

				out[outLen++] = (char) aChar;

			}

		}

		return new String(out, 0, outLen);

	}

}