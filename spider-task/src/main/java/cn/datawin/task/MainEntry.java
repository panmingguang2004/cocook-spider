package cn.datawin.task;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.commons.logging.LogFactory;

import cn.datawin.spider.CCMan;
import cn.datawin.task.dao.DbUtil;
import cn.datawin.task.dao.Resource;
import cn.datawin.task.util.PropertiesUtil;

public class MainEntry {
	
//	{
//		AtomicInteger num = new AtomicInteger();
//		num.getAndAdd(arg0);
//	}
	
	public static void init() throws IOException{
		LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log","org.apache.commons.logging.impl.NoOpLog");
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		
		Resource resource = new Resource("/client.properties");
		Properties props = PropertiesUtil.loadProperties(resource);
		Config.apps.putAll(PropertiesUtil.toMap(props));
	}
	
	public static void main(String[] args) throws Exception {
		init();
		Statistic.getInstance().start();
	}
}

