package cn.datawin.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

public class Rule extends HashMap<String, Object> {
	/**
	 *  Rule  extends Map 作为规则的类
	 */
	private static final long serialVersionUID = 1L;
	
	public Rule() {
		super();
	}
	
	public Rule(Map<String, Object> map){
		super.putAll(map);
	}
	
	String id;
	
	/* 任务id*/
	String taskid;
	
	/* 保存的子节点 */
	String cid;
	
	/* 保存的父节点 */
	String pid;
	
	/* 保存的name key */
	String name;
	
	/* 保存的规则解析类型*/
	String type;
	
	/*Method*/
	String method;

	/*post 数据*/
	String data;
	
	/*编码*/
	String charset;
	
	/* 表达式 选择器的类型 selector xpath regex  attr jsonpath test html */
	/*{type: expression, type: expression}, 多个表达式构成
	 */
	List<Map<String, String>> expression;
	
	/*typeURL 生成规则方式*/
	String urlRule;
	
	/* 分页规则 begin */
	String pageRule;
	/*取分页数量  取出pageSize 个 URL*/
	int pageSize;
	 /*分页递增个数  p=0 p=10*/
	int pageNum;
	/* 分页规则 end */

	public String getName() {
		return (String)super.get("name");
	}

	public void setName(String name) {
		super.put("name", name);
	}

	public List<Map<String, String>> getExpression() {
		String 	expression = (String)super.get("expression");
		List<LinkedHashMap> list = (List<LinkedHashMap>) JSONArray.parseArray(expression, LinkedHashMap.class);
		if(list == null) return Collections.EMPTY_LIST;
		List<Map<String, String>> _list = new ArrayList<Map<String,String>>();
		for(LinkedHashMap map: list){
			_list.add(map);
		}
		return _list;
	}

	public String getPageRule() {
		return (String)super.get("pageRule");
	}

	public void setPageRule(String pageRule) {
		super.put("pageRule", pageRule);
	}

	public int getPageSize() {
		return (Integer)super.get("pageSize");
	}

	public void setPageSize(int pageSize) {
		super.put("pageSize", pageSize);
	}

	
	public int getPageNum() {
		return (Integer)super.get("pageNum");
	}

	public void setPageNum(int pageNum) {
		super.put("pageNum", pageNum);
	}

	public String getId() {
		return (String)super.get("id");
	}

	public String getTaskid() {
		return (String) super.get("taskid");
	}

	public String getCid() {
		return (String) super.get("cid");
	}

	public String getPid() {
		return (String) super.get("pid");
	}
	public String getType() {
		return (String) super.get("type");
	}
	
	public String getMethod(){
		return (String) super.get("method1");
	}
	
	public String getCharset(){
		return (String) super.get("charset");
	}
	
	public String getUrlRule(){
		return (String) super.get("urlRule");
	}
	
	public Map<String, String> getData(){
		String data = (String) super.get("data");
		if(data == null) return Collections.EMPTY_MAP;
		return (Map<String, String>) JSON.parse(data);
	}
}
