package cn.datawin.task;

import java.util.Calendar;
import java.util.Date;
import cn.datawin.adsl.Adsl;
import cn.datawin.spider.CCMan;

public class Statistic {
	private int count=0;
	private CCMan ccMan;
	private PrepareTask prepareTask;
	private int c=Integer.parseInt(Config.apps.get("count"));//每抓取多少条切换ip
	private  static Statistic statistic = null;
	private int beginTime=Integer.parseInt(Config.apps.get("beginTime"));
	private int endTime=Integer.parseInt(Config.apps.get("endTime"));
	private boolean flag=true;
	
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public static Statistic getInstance() {  
	       if (statistic == null) {  
	    	   statistic = new Statistic();  
	       }  
	       return statistic;  
	    }  
	
	public void addCount(Task task) throws Exception {
		count++;
		concut();
	}
	
	public CCMan getCcMan() {
		return ccMan;
	}
	public void setCcMan(CCMan ccMan) {
		this.ccMan = ccMan;
	}
	public PrepareTask getPrepareTask() {
		return prepareTask;
	}
	public void setPrepareTask(PrepareTask prepareTask) {
		this.prepareTask = prepareTask;
	}
	
	public void concut() throws Exception{
		if(count!=0 && count%c==0){
			this.prepareTask.setStop(true);
			this.prepareTask = null;
			this.ccMan.setStop(true);
			this.ccMan.getQueueThreadPool().shutdown();
			this.ccMan = null;
			
			Adsl.cutAdsl(Config.apps.get("adsltitle"));
			Thread.sleep(2000);
			Adsl.connAdsl(Config.apps.get("adsltitle"), Config.apps.get("adslname"), Config.apps.get("adslpass"));
			
			PrepareTask pTask = new PrepareTask(Config.apps.get("workbeach"),"0");
    		pTask.start();
    		statistic.setPrepareTask(pTask);
    		CCMan cc = new CCMan();
    		cc.setPageQueue(PrepareTask.pageQueue);
    		cc.thread(20, 20, 100).start();
    		statistic.setCcMan(cc);
		}
	}

	public void start() {
		while (true) {
			try {
				_start();
				Thread.sleep(60 *1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String[] args) {
		 Calendar cal = Calendar.getInstance();
		  cal.setTime(new Date());
		  System.out.println(cal.get(Calendar.DAY_OF_WEEK));;
	}
		
	
	void _start (){
		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		cal.setTime(date);
		int hours= cal.get(Calendar.HOUR_OF_DAY);
		/*
		 * 星期六 星期天 抓取
		 * 工作日按照 配置抓取
		 */
		if(hours>=beginTime || hours<endTime || cal.get(Calendar.DAY_OF_WEEK)==1 ||cal.get(Calendar.DAY_OF_WEEK)==7){
			//启动抓取
			System.out.println(date+"-------------抓取时间段---------------");
			if(this.flag){
				System.out.println(date+"-------------开始抓取---------------");
	        	PrepareTask pTask = new PrepareTask(Config.apps.get("workbeach"),"0");
	    		pTask.start();
	    		statistic.setPrepareTask(pTask);
	    		CCMan cc = new CCMan();
	    		cc.setPageQueue(PrepareTask.pageQueue);
	    		cc.thread(20, 20, 100).start();
	    		statistic.setCcMan(cc);
	    		this.flag = false;
	    	 }
	     }else{
	    	 System.out.println(date+"-------------暂停时间段---------------");
	    	 if(!this.flag){
	    		 	System.out.println(date+"-------------暂停抓取---------------");
	    		    this.prepareTask.setStop(true);
					this.prepareTask=null;
					this.ccMan.setStop(true);
					this.ccMan.getQueueThreadPool().shutdown();
					this.ccMan=null;
					this.flag=true;
	    	 }
	     }
	
	}
}
