package cn.datawin.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class Task implements Serializable , Cloneable{
	
	private String id;

	private String workbeach;

	private String state = "0";

	private String url;
	
	private String taskid;
	
	private boolean root;


	private Map<String, String> header = Collections.emptyMap();
	private Map<String, String> extedata = Collections.emptyMap();

	private Map<String, String> cookie = Collections.emptyMap();
	/**
	 * post 数据
	 * { params: {} }
	 */

	private List<Map<String, Object>> rules = new ArrayList<Map<String, Object>>();
	
	private Rule currentRule = null;
	
	/**
	 * 数据存储 方式
	 * URL 为新生成
	 * DATA 为数据存储
	 * 为 pipeLine 所用
	 * */
	private String dataTarget = "URL";
	
	private String processor = "DataProcessor";

	/* 分配的 VPS*/
	private String[] client;
	
	/*登录的httpclient 实例 id*/
	private String httpClientid;

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}

	public String[] getClient() {
		return client;
	}

	public void setClient(String[] clients) {
		this.client = clients;
	}

	public void setDataTarget(String dataTarget) {
		this.dataTarget = dataTarget;
	}

	public String getWorkbeach() {
		return workbeach;
	}

	public void setWorkbeach(String workbeach) {
		this.workbeach = workbeach;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, String> getHeader() {
		return header;
	}

	public void setHeader(Map<String, String> header) {
		this.header = header;
	}

	public Map<String, String> getCookie() {
		return cookie;
	}

	public void setCookie(Map<String, String> cookie) {
		this.cookie = cookie;
	}

	public List<Map<String, Object>> getRules() {
		return rules;
	}

	public void setRules(List<Map<String, Object>> rules) {
		this.rules = rules;
	}


	public String getDataTarget() {
		return dataTarget;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getProcessor() {
		return processor;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public boolean isRoot() {
		return root;
	}

	public void setRoot(boolean root) {
		this.root = root;
	}
	
	
	/**
	 * 将List<Rule> --> List<List<Rule>> 安装 pid 分成一个List<Rule> , 再整合为一个 list
	 * 每次remove(0) 作为当前task的Rule
	 * @param rules
	 * @return
	 
	public List<List<Rule>> toRules(){
		if(_rules.size()!=0){
			return _rules;
		}
		int pid = 0;
		for(Rule rule : rules){
			
			if(rule.getPid().equals(pid+"")){
				List<Rule> list = null;
				if(_rules.size() == 0) {
					list = new ArrayList<Rule>();
					_rules.add(list);
				}else{
					list = _rules.get(pid);
				}
				list.add(rule);
			}else{
				pid++;
				List<Rule> list = new ArrayList<Rule>();
				list.add(rule);
				_rules.add(list);
			}
		}
		return _rules;
	}
	*/
	
	
	/**
	 * 每个task 调一次
	 * @return
	 */
	public void next(){
		if(!isBranch() && rules.size() >0){
			Map<String, Object> _rules =  (Map<String, Object>) rules.get(0);
			rules = _rules.get("children") != null ? (List<Map<String, Object>>) _rules.remove("children") : Collections.EMPTY_LIST ;
			this.currentRule = new Rule(_rules);
			return;
		}
		this.currentRule = new Rule();
	}
	
	public Rule currentRule(){
		return currentRule;
	}
	
	public boolean isBranch(){
		return rules.size() > 1;
	}
	
	public String nextProcessor(){ //!isBranch() && 
		if(rules.size()>0){
			return (String) rules.get(0).get("type");
		}
		return "DataProcessor";
	}
	
	public Map<String, Object>  toMap(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("workbeach", workbeach);
		map.put("state", state);
		map.put("url", url);
		map.put("root", root);
		map.put("header", header);
		map.put("cookie", cookie);
		map.put("rules", rules);
		map.put("processor", processor);
		map.put("client", client);
		map.put("extedata", extedata);
		return map;
	}
	
	public static void main(String[] args) {
		Task task = new Task();
		List<Rule> list = new ArrayList<Rule>();
		Rule  rule = new Rule();
		rule.put("pid", "0");
		rule.put("cid", "1");
		rule.setName("aaaaa");
		list.add(rule);
		
		Rule  rule2 = new Rule();
		rule2.put("pid", "0");
		rule2.put("cid", "2");
		rule2.setName("bbbbbbbbb");
		list.add(rule2);
		
		Rule  rule3 = new Rule();
		rule3.put("pid", "1");
		rule3.put("cid", "3");
		rule3.setName("ccccccccccc");
		list.add(rule3);
		
		Rule  rule4 = new Rule();
		rule4.put("pid", "1");
		rule4.put("cid", "4");
		rule4.setName("dddddddddd");
		list.add(rule4);
		
		Rule  rule5 = new Rule();
		rule5.put("pid", "2");
		rule5.put("cid", "5");
		rule5.setName("eeeeeeeeeeee");
		list.add(rule5);
		
		
		System.out.println(JSON.toJSONString(task, SerializerFeature.DisableCircularReferenceDetect));
		
//		System.out.println(task.currentRule());
//		System.out.println(task.getRules());
//		System.out.println(task.currentRule());
//		System.out.println(task.getRules());
		
		
		
		
		
		
	}

	public Map<String, String> getExtedata() {
		return extedata;
	}

	public void setExtedata(Map<String, String> extedata) {
		this.extedata = extedata;
	}

	public void setHttpClientid(String httpClientid) {
		this.httpClientid = httpClientid;
	}

	public String getHttpClientid() {
		return httpClientid;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}



}
