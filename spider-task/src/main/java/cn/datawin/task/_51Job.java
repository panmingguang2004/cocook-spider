package cn.datawin.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.bson.types.ObjectId;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import cn.datawin.task.dao.DbUtil;
import cn.datawin.task.dao.Resource;
import cn.datawin.task.util.PropertiesUtil;

public class _51Job {
	public static void main(String[] args) throws IOException, InterruptedException {
		Resource resource = new Resource("/client.properties");
		Properties props = PropertiesUtil.loadProperties(resource);
		Config.apps.putAll(PropertiesUtil.toMap(props));
		DbUtil.connect();
		DBCollection dbc = DbUtil.getcoll("task");
		DBObject dbo =  dbc.findOne(new BasicDBObject("_id", new ObjectId("54fd4ecee4b01b94fa8773b2")));
		int a = Integer.parseInt(Config.apps.get("tasknum")); // 初始值
		dbo.removeField("_id");
		dbo.put("state", "0");
		dbo.put("root", false);
		long s=System.currentTimeMillis();
		for(int j=1; j<=10000; j++){
			List<DBObject> list = new ArrayList<DBObject>();
			for(int i=1;i<=100;i++){
				dbo.put("url", "http://search.51job.com/list/co,c,"+a+",000000,50,1.html");
				list.add(new BasicDBObject(dbo.toMap()));
				a++;
			}
			System.out.println(a);
			dbc.insert(list);
		}
		System.out.println(System.currentTimeMillis()-s);
	}

}
