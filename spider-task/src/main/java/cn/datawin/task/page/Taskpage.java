package cn.datawin.task.page;

import cn.datawin.spider.page.Page;
import cn.datawin.task.Config;
import cn.datawin.task.Statistic;
import cn.datawin.task.Task;
import cn.datawin.task.util.TaskUtil;

public class Taskpage extends Page{
	
	
	@Override
	public void exec() throws Exception {
		try{
			super.exec();
		}finally{
			finish();
		}
	}
	
	
	public void finish(){
		Task task = (Task) super.getProcessor().getTask();
		try {
			Statistic.getInstance().addCount(task);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String flag=Config.apps.get("isReport");
		if("true".equals(flag)){
			//更改对应task状态为2
			comptask(task.getId());
		}
	}
	
	public void comptask(String taskid){
		try {
			TaskUtil.comptask(taskid);
		} catch (Exception e) {
		}
	}
}
