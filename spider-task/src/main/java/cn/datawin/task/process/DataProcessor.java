package cn.datawin.task.process;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.datawin.spider.page.Page;
import cn.datawin.task.Rule;
import cn.datawin.task.Task;

public class DataProcessor extends AbstractProcessor{
	
	Page page;

	List<Object> list;
	
	private Task task;

	@Override
	public void process(Page page) {
		this.page = page;
		//获取所需的目标区块数据
		Rule rules = new Rule(task.currentRule());
		super.params = new HashMap<String, String>();
		super.parseRule(rules, page);
	}

	@Override
	public Map<String, String> getParams() {

		return params;
	}

	@Override
	public List<Object> getList() {

		return list;
	}

	@Override
	public Page getPage() {

		return page;
	}

	public void setTask(Serializable task) {
		this.task = (Task)task;
	}

	@Override
	public Serializable getTask() {
		
		return task;
	}

}
