package cn.datawin.task.process;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import cn.datawin.spider.page.Page;
import cn.datawin.task.Rule;
import cn.datawin.task.Task;

public class TypeProcessor extends AbstractProcessor {

	Page page;

	List<Object> list;

	Task task;

	@Override
	public void process(Page page) {
		this.page = page;
		Rule rule = new Rule(task.currentRule());
		super.params = new HashMap<String, String>();
		super.parseRule(rule, page);
		parseTypeUrl(rule);
	}

	public void parseTypeUrl(Rule rule) {
		list = new ArrayList<Object>();
		int i =0;
		for (Map<String, String> map : rule.getExpression()) {
			String name = map.get("name");
			String typeUrls = (String) params.remove(name);
			if (StringUtils.isEmpty(typeUrls))	return;
			List<String> _typeUrls = quis(Arrays.asList(typeUrls.split("\\^\\|"))) ;  // 去重
			List<Object> _tmp = null;
			if(i>0){  // Typeprocessor 两个 rule 的话 默认触发此中模式 重复使用list的值作为初始值替换多次
				_tmp = new ArrayList<Object>();
				_tmp.addAll(list);
				list.clear();
			}
			for (String typeUrl : _typeUrls) {
				if(i>0){
					for(Object str: _tmp){
						String typeUrl_rule = parseUrlRule(str.toString(),typeUrl);
						list.add(typeUrl_rule);
					}
					continue;
				}
				if(!StringUtils.isEmpty(rule.getUrlRule())){
					typeUrl = parseUrlRule(rule.getUrlRule(), typeUrl);
				}
				list.add(typeUrl);
			} 
			i++;
		}
	}
	
	public List<String> quis(List<String> list){
		return new ArrayList<String>(new HashSet<String>(list));
	}
	
	public String parseUrlRule(String urlRule,String typeUrl){
		return urlRule.replaceFirst("%s", typeUrl);
	}

	@Override
	public Map<String, String> getParams() {
		return super.params;
	}

	@Override
	public List<Object> getList() {
		return list;
	}

	public Page getPage() {
		return page;
	}

	@Override
	public void setTask(Serializable task) {
		this.task = (Task) task;
	}

	@Override
	public Serializable getTask() {
		return task;
	}
	
}
