package cn.datawin.task.util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import cn.datawin.spider.seletor.Html;

public class HtmlImg extends Html{
	
	private String url;
	
	public HtmlImg() {
	}
	
	public HtmlImg(String text) {
		super(text);
	}
	
	@Override
	public void init(String text) {
		super.init(text);
	}
	
	public void execjs(List<org.apache.http.cookie.Cookie> list){
//		init(WebDriver.get(url, list));
	}
	
//	public byte[] cut(){
//		return WebpageScreenshot.getInstance().cut(getUrl());
//	}
	
	public void wb_load(){
		WebpageScreenshot.getInstance().wb_load(getUrl());
	}
	
	public void wb_click(String xpath){
		WebpageScreenshot.getInstance().wb_click(xpath);
	}
	
	public String wb_$(String xpath){
		return WebpageScreenshot.getInstance().wb_$(xpath);
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public byte[] img() throws Exception{
		return HttpUtil.getImg(getUrl());
	}
	
	public String purl(){
		return getUrl();
	}
}
