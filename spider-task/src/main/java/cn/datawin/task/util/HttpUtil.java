package cn.datawin.task.util;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mongodb.DBObject;

import cn.datawin.spider.httputil.HttpClientService;
import cn.datawin.spider.httputil.HttpClientService_2_1;
import cn.datawin.spider.httputil.HttpRequest;
import cn.datawin.spider.httputil.HttpRequest.Method;
import cn.datawin.spider.httputil.HttpResponse;
import cn.datawin.spider.httputil.HttpService;

public class HttpUtil {
	private static ArrayList<HttpService> services =  new ArrayList<HttpService>();
	static Random random = new Random();
	static{
		services.add(new HttpClientService_2_1(new ThreadSafeClientConnManager()));
		services.add(new HttpClientService_2_1(new ThreadSafeClientConnManager()));
		services.add(new HttpClientService_2_1(new ThreadSafeClientConnManager()));
	}
	
	public static HttpService getHttpClient(){
		return services.get(random.nextInt(services.size()));
	}
	
	public static List<Map<String, Object>> get(String url, Map<String, String> params) throws Exception{
		return get(url + toString(params));
	}
	
	public static List<Map<String, Object>> get(String url) throws Exception{
		return get(url, "UTF-8");
	}
	
	public static List<Map<String, Object>> get(String url, String charset, Map<String, String> params) throws Exception{
		return get(url+ toString(params), charset);
	}
	
	public static List<Map<String, Object>> get(String url, String charset) throws Exception{
//		System.out.println("获取的task规则："+getStr(url, charset));
		return (List<Map<String, Object>>) deserialize(getStr(url, charset));
	}
	
	public static String getStr(String url, String charset) throws Exception{
		HttpRequest request = new HttpRequest(url, Method.get);
		HttpResponse res =  getHttpClient().execute(request);
		return res.getResponseString(charset);
	}
	
	public static byte[] getImg(String url) throws Exception{
		HttpRequest request = new HttpRequest(url, Method.get);
		HttpResponse res =  getHttpClient().execute(request);
		return res.getResponseData();
	}
	
	public static String post(String url, Map<String, String> params, Map<String, File>... fileMap ) throws Exception{
		return postStr(url, "UTF-8", params, fileMap);
	}
	
	public static String postStr(String url,String charset, Map<String, String> params, Map<String, File>... fileMap ) throws Exception{
		HttpRequest request = new HttpRequest(url, Method.post);
		request.setPostParams(params);
		for(Map<String, File> map: fileMap){
			request.setFileMap(map);
		}
		HttpResponse res =  getHttpClient().execute(request);
		return res.getResponseString(charset);
	}
	
	public static Object deserialize(String str){
		Object obj =  com.mongodb.util.JSON.parse(str);
		return obj;
	}
	
	public static String serialize(Object obj){
		try{
			return JSON.toJSONString(obj, SerializerFeature.DisableCircularReferenceDetect, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String encode(String url) {
		try {
			return URLEncoder.encode(url, "utf8");
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String toString(Map<String, String> params){
		StringBuffer buffer = new StringBuffer();
		for(String key: params.keySet()){
			if(buffer.length()>0){
				buffer.append("&");
			}
			buffer.append(key).append("=").append(encode(params.get(key)));
		}
		return buffer.toString();
	}
	
	
	public static void main(String[] args) throws Exception {
		Map<String, File> map = new HashMap<String, File>();
		map.put("file", new File("D:/wqe/psb.jpg"));
		System.out.println(post("http://127.0.0.1/task/inserttaskF", Collections.EMPTY_MAP, map));
	}
	

}
