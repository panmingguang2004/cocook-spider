package cn.datawin.task.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import cn.datawin.task.Rule;
import cn.datawin.task.Task;

public class PaserUtil {

	public static List<Rule> dboToRule(List<Map<String, Object>> list){
		List<Rule>  rules = new ArrayList<Rule>();
		for(Map<String, Object> map: list){
			rules.add(new Rule(map));
		}
		return rules;
	}
	

	public static List<List<Rule>> _dboToRule(List<List<Map<String, Object>>> list){
		List<List<Rule>> rules = new ArrayList<List<Rule>>();
		if(list==null || list.size()==0) return rules;
		for(List<Map<String, Object>> _list: list){
			List<Rule>  rule = new ArrayList<Rule>();
			for(Map<String, Object> map: _list){
				rule.add(new Rule(map));
			}
			rules.add(rule);
		}
		return rules;
	}
	
	
	public static List<Task> dboToTask(List<Map<String, Object>> list){
		List<Task> tasklist = new ArrayList<Task>();
		if (list!=null &&list.size() >0) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = list.get(i);
				Task task = new Task();
				List<Map<String, Object>> rules = (List<Map<String, Object>>)map.get("rules");
				if(rules == null || rules.size()==0) continue ;
				task.setRules(rules);
				task.setCookie((Map<String, String>) map.get("cookie"));
				task.setHeader((Map<String, String>) map.get("header"));
				task.setId(map.get("_id").toString());
				task.setTaskid(map.get("taskid") ==null ?map.get("_id").toString():map.get("taskid").toString());
				task.setHttpClientid((String)map.get("httpClientid"));
				
				task.setState(map.get("state").toString());
				task.setUrl(map.get("url").toString());
				task.setExtedata((Map<String, String>) map.get("extedata"));
				task.setWorkbeach((String)map.get("workbeach"));
				BasicDBList dblist = (BasicDBList) map.get("client");
				task.setClient(dblist.toArray(new String[]{}));
				tasklist.add(task);
			}
		}
		return tasklist;
	}
	
}
