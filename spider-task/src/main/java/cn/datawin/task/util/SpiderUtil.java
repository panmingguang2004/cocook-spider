package cn.datawin.task.util;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import cn.datawin.spider.pipeline.PipeLine;
import cn.datawin.spider.processor.Processor;
import cn.datawin.task.pipe.DataPipeLine;
import cn.datawin.task.pipe.PagePipeLine;
import cn.datawin.task.process.DataProcessor;
import cn.datawin.task.process.PageProcessor;
import cn.datawin.task.process.TypeProcessor;

public class SpiderUtil {
	@SuppressWarnings("serial")
	static Map<String, Class<? extends Processor>> processor = new HashMap<String, Class<? extends Processor>>(){
		{
			put("DataProcessor", DataProcessor.class);
			put("PageProcessor", PageProcessor.class);
			put("TypeProcessor", TypeProcessor.class);
			/**
			 * 扩展
			 */
		}
	};
	
	static Map<String, Class<? extends PipeLine>> pipe = new HashMap<String, Class<? extends PipeLine>>();
	static{
		pipe.put("DataProcessor", DataPipeLine.class);
		pipe.put("PageProcessor", PagePipeLine.class);
		pipe.put("TypeProcessor", PagePipeLine.class);
		/**
		 * 扩展
		 */
	}
	
	public static PipeLine getPipeLine(String process){
		try {
			return pipe.get(process).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	public static Processor getProcessor(String process){
		try {
			return processor.get(process).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	static Map<String, Method> htmlMethod = new HashMap<String, Method>();
	static{
		//selector xpath regex
		try{
			Class<?> _class = String.class;
			htmlMethod.put("selector", HtmlImg.class.getMethod("$", _class));
			htmlMethod.put("$", HtmlImg.class.getMethod("$", _class));
			htmlMethod.put("find", HtmlImg.class.getMethod("$", _class));
			htmlMethod.put("xpath", HtmlImg.class.getMethod("xpath", _class));
			htmlMethod.put("regex", HtmlImg.class.getMethod("regex", _class));
			htmlMethod.put("regexAll", HtmlImg.class.getMethod("regexAll", _class));
			htmlMethod.put("attr", HtmlImg.class.getMethod("attr", _class));
			htmlMethod.put("jsonpath", HtmlImg.class.getMethod("jsonPath", _class));  // jsonpath 表达式
			htmlMethod.put("html", HtmlImg.class.getMethod("html"));
			htmlMethod.put("first", HtmlImg.class.getMethod("first"));
			htmlMethod.put("last", HtmlImg.class.getMethod("last"));
			htmlMethod.put("text", HtmlImg.class.getMethod("text"));
			htmlMethod.put("img", HtmlImg.class.getMethod("img"));  
			htmlMethod.put("purl", HtmlImg.class.getMethod("purl"));  // 直接取当前URL 作为分页URL
			htmlMethod.put("wb_load", HtmlImg.class.getMethod("wb_load"));  // 使用webdriver 加载页面
			htmlMethod.put("wb_click", HtmlImg.class.getMethod("wb_click",_class));  // xpath 点击事件
			htmlMethod.put("wb_$", HtmlImg.class.getMethod("wb_$", _class));  // 寻xpath选择器
			
			/*[{
				"name":"data",
				"wb_load":"", 
				"wb_click":"//*[@id=\"fui-tab1\"]/div[2]",
				"wb_$":"//*[@id=\"archive-tab-content\"]/div[2]/div/div"
			}]
			*/
//			htmlMethod.put("cutImg", HtmlImg.class.getMethod("cut"));  // 注释掉, 使用了 selenium 包超级多
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static Method getHtmlMethod(String type){
		return htmlMethod.get(type);
	}
	
	public static Object invokeMethod(Method method, Object obj, Object... params){
		try {
			return method.invoke(obj, params);
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	
	

}
