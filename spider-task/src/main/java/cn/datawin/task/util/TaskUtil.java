package cn.datawin.task.util;

import java.util.List;
import java.util.Map;
import cn.datawin.task.Config;


public class TaskUtil {

	public static String addtask(Map<String, String> map) throws Exception{
		return HttpUtil.post(Config.apps.get("url")+"/task/inserttask",map);
	}
	
	public static String insertData(Map<String, String> map) throws Exception{
		return HttpUtil.post(Config.apps.get("url")+"/task/insertdata",map);
	}
	
	public static String updateclient(String workbeach,String count) throws Exception{
		return HttpUtil.getStr(Config.apps.get("url")+"/client/update?workbeach="+workbeach+"&count="+count,"UTF-8");
	}
	
	public static String comptask(String taskid) throws Exception{
		return HttpUtil.getStr(Config.apps.get("url")+"/task/comptask?taskid="+taskid,"UTF-8");
	}

	public static List<Map<String, Object>> gettask(String workbeach, String state, String num) throws Exception{
		return HttpUtil.get(Config.apps.get("url")+"/task/gettask?workbeach="+workbeach+"&state="+state+"&num="+num);
	}
	
	public static List<Map<String, Object>> deltask(String id) throws Exception{
		return HttpUtil.get(Config.apps.get("url")+"/task/deletetask?id="+id);
	}
}
