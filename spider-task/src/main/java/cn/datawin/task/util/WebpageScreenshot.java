package cn.datawin.task.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cn.datawin.task.Config;
import cn.datawin.task.MainEntry;

public class WebpageScreenshot {

	private static WebpageScreenshot screenshot = new WebpageScreenshot();
	private WebDriver driver;
	private byte[] bytes;

	public static WebpageScreenshot getInstance() {
		return screenshot;
	}

	public WebpageScreenshot() {
		this(Config.apps.get("webdriver.firefox.bin"));
	}

	public WebpageScreenshot(String execPath) {
		init(execPath);
	}

	public void init(String execPath) {
		System.setProperty("webdriver.firefox.bin", execPath);
		driver = new FirefoxDriver();
		driver.manage().timeouts()
		.implicitlyWait(2500, TimeUnit.MILLISECONDS)
		.pageLoadTimeout(2500, TimeUnit.MILLISECONDS)
		.setScriptTimeout(2500, TimeUnit.MILLISECONDS);
	}
	
	public void wb_click(String xpath){
		synchronized (driver) {
			driver.findElement(By.xpath(xpath)).click();
		}
	}
	
	public String wb_$(String xpath){
		synchronized (driver) {
			List<WebElement> elements=driver.findElements(By.xpath(xpath));
			StringBuilder sb = new StringBuilder();
			for (WebElement e : elements) {
				// e.getAttribute("innerHTML")  // 获取html
				sb.append(e.getText());
			}
			return sb.toString();
		}
	}
	
	public void wb_load(String url){
		synchronized (driver) {
			try {
				driver.get(url);
			} catch (TimeoutException timeout) {
			} catch (Exception e) {
				init(Config.apps.get("webdriver.firefox.bin"));
				driver.get(url);
			}
		}
	}
	

	public byte[] cut(String url) {
		synchronized (driver) {
			try {
				driver.get(url);
			} catch (TimeoutException timeout) {
			} catch (Exception e) {
				init(Config.apps.get("webdriver.firefox.bin"));
				driver.get(url);
			}
			bytes = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.BYTES);
			return bytes;
		}
	}

	public InputStream getImg() {
		return new ByteArrayInputStream(bytes);
	}

	public File toImgFile(String path) throws IOException {
		File file = new File(path);
		FileUtils.copyInputStreamToFile(getImg(), file);
		return file;
	}

	
	public static void main(String[] args) throws IOException {
		MainEntry.init();
		WebpageScreenshot screenshot = WebpageScreenshot.getInstance();
//		screenshot.cut("http://www.taobao.com/");
//		screenshot.toImgFile("d://taobao.jpg");
//		screenshot.cut("http://www.baidu.com/");
//		screenshot.toImgFile("d://baidu.jpg");
//		screenshot.cut("http://www.58.com/");
//		screenshot.toImgFile("d://58.jpg");
//		screenshot.cut("http://www.ganji.com/");
//		screenshot.toImgFile("d://ganji.jpg");

		screenshot.wb_load("http://m.1688.com/winport/company/jk2sh.html?spm=0.0.0.0.iWzzW4");
		
		System.out.println(screenshot.wb_$("//*[@id=\"archive-tab-content\"]/div[1]/ul"));
		System.out.println("-------------------");
		screenshot.wb_click("//*[@id=\"fui-tab1\"]/div[2]");
		System.out.println(screenshot.wb_$("//*[@id=\"archive-tab-content\"]/div[2]/div/div"));
		System.out.println("-------------------");
		screenshot.wb_click("//*[@id=\"fui-tab1\"]/div[3]");
		System.out.println(screenshot.wb_$("//*[@id=\"archive-tab-content\"]/div[3]/div/div[1]"));
		
	}

}
