package SeoTest;

import java.util.ArrayList;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class HttpClientUtil {
	private static ArrayList<DefaultHttpClient> services =  new ArrayList<DefaultHttpClient>();
	static Random random = new Random();
	static{
		services.add(new DefaultHttpClient(new ThreadSafeClientConnManager()));
		services.add(new DefaultHttpClient(new ThreadSafeClientConnManager()));
		services.add(new DefaultHttpClient(new ThreadSafeClientConnManager()));
	}
	
	public static DefaultHttpClient getHttpClient(){
		return services.get(random.nextInt(services.size()));
	}
	
	
	public static String getStr(String url, String charset){
		try {
			HttpGet get=new HttpGet(url);
			HttpClient client=getHttpClient();
			client.getParams().setParameter("http.socket.timeout",3000);
			client.getParams().setParameter("http.connection.timeout",3000);
//			client.getParams().setParameter("http.connection-manager.timeout",60*60L);
//			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(3000).setConnectTimeout(3000).build();//设置请求和传输超时时间
//			get.setConfig(requestConfig);
			HttpResponse httpResponse= client.execute(get);
			return EntityUtils.toString(httpResponse.getEntity(),charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
