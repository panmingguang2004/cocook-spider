package SeoTest;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class TestSeo {
	private String resp;
	private String res;
	private String charset;
	private String url="http://seo.chinaz.com/?host=";
	private String url1="http://alexa.chinaz.com/?domain=";
	private Map<String, Object> result=new HashMap<String, Object>();

	public TestSeo(String host,String charset){
		  LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log","org.apache.commons.logging.impl.NoOpLog");
		  java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		  this.charset=charset;
//		  this.resp=HUjsDriver.get(url+host);
		  this.res=HttpClientUtil.getStr(url1+host, this.charset);
	}
	
	public Map<String, Object> getWorldTop(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String worldtop=document.select("span#worldtop").text();
		if(null!=worldtop && !worldtop.equals("") && !worldtop.equals("--")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", worldtop);
			result.put("worldtop", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("worldtop", map);
		return map;
	}
	
	public Map<String, Object> getPV(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String pv=document.select("span#pvavg").text();
		if(null!=pv && !pv.equals("") && !pv.equals("--")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", pv);
			result.put("pv", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("pv", map);
		return map;
	}
	
	public Map<String, Object> getUV(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String uv=document.select("span#ipavg").text();
		if(null!=uv && !uv.equals("") && !uv.equals("--")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", uv);
			result.put("uv", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
	    result.put("uv", map);
		return map;
	}
	
	public Map<String, Object> getPR(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String pr=document.select("span#pr img").attr("src");
		if(null!=pr && !pr.equals("")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", "http://seo.chinaz.com"+pr);
			result.put("pr", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("pr", map);
		return map;
	}
	
	public Map<String, Object> getBaiduPages(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String seo_BaiduPages=document.select("span#seo_BaiduPages").text();
		if(null!=seo_BaiduPages && !seo_BaiduPages.equals("")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", seo_BaiduPages);
			result.put("seo_BaiduPages", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("seo_BaiduPages", map);
		return map;
	}
	
	public Map<String, Object> getGooglePages(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String seo_GooglePages=document.select("span#seo_GooglePages").text();
		if(null!=seo_GooglePages && !seo_GooglePages.equals("")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", seo_GooglePages);
			result.put("seo_GooglePages", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("seo_GooglePages", map);
		return map;
	}
	
	public Map<String, Object> getBaiduLink(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String seo_BaiduLink=document.select("span#seo_BaiduLink").text();
		if(null!=seo_BaiduLink && !seo_BaiduLink.equals("")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", seo_BaiduLink);
			result.put("seo_GooglePages", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("seo_GooglePages", map);
		return map;
	}
	
	public Map<String, Object> getGoogleLink(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		String seo_GoogleLink=document.select("span#seo_GoogleLink").text();
		if(null!=seo_GoogleLink && !seo_GoogleLink.equals("")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", seo_GoogleLink);
			result.put("seo_GoogleLink", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("seo_GoogleLink", map);
		return map;
	}
	
	public Map<String, Object> getBaiduZhiShu(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(resp);
		Elements es=document.select("table.seo");
		if(es.size()>4){
			String baiduZhiShu=es.get(4).html();
			if(null!=baiduZhiShu && !baiduZhiShu.equals("")){
				map.put("code", "1");
				map.put("msg", "success");
				map.put("value", baiduZhiShu);
				result.put("baiduZhiShu", map);
				return map;
			}
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("baiduZhiShu", map);
		return map;
	}
	
	public void innit(){
		  System.out.println("世界排名："+"\t"+getWorldTop());
		  System.out.println("PV值："+"\t"+getPV());
		  System.out.println("UV值："+"\t"+getUV());
		  System.out.println("PR值："+"\t"+getPR());
		  System.out.println("搜索引擎百度收录："+"\t"+getBaiduPages());
		  System.out.println("搜索引擎谷歌收录："+"\t"+getGooglePages());
		  System.out.println("反链百度："+"\t"+getBaiduLink());
		  System.out.println("反链谷歌："+"\t"+getGoogleLink());
		  System.out.println("百度指数："+"\t"+getBaiduZhiShu());
		  System.out.println("Alexa 排名："+"\t"+getAlexa());
	}
	
	public Map<String, Object> getAlexa(){
		Map<String, Object> map=new HashMap<String, Object>();
		Document document=Jsoup.parse(res);
		String alex=document.select("div.title:contains(全球综合排名)").text();
		if(null!=alex && !alex.equals("")){
			map.put("code", "1");
			map.put("msg", "success");
			map.put("value", alex);
			result.put("Alexa", map);
			return map;
		}
		map.put("code", "0");
		map.put("msg", "failure");
		map.put("value", "");
		result.put("Alexa", map);
		return map;
	}
	
	public static void main(String[] args) {
		//需要执行js获取response
		  TestSeo seo=new TestSeo("www.taobao.com", "UTF-8");
		  seo.innit();
		  System.out.println("-----------seo综合查询---------");
		  System.out.println(seo.result);
		  
	}
	
}
