//package cn.data.test;
//
//
//import java.io.File;
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxBinary;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.support.ui.ExpectedCondition;
//import org.openqa.selenium.support.ui.WebDriverWait;
//
//public class ExampleForFireFox  {
//    void aa() {
//    	// 如果你的 FireFox 没有安装在默认目录，那么必须在程序中设置
//    	System.setProperty("webdriver.firefox.bin", "E:\\Program Files\\Mozilla Firefox\\firefox.exe");
//    	// 创建一个 FireFox 的浏览器实例
//        WebDriver driver = new FirefoxDriver();
//
//        // 让浏览器访问 Baidu
//        driver.get("http://www.baidu.com");
//        // 用下面代码也可以实现
//        // driver.navigate().to("http://www.baidu.com");
//
//        // 获取 网页的 title
//        System.out.println("1 Page title is: " + driver.getTitle());
//
//        // 通过 id 找到 input 的 DOM
//        WebElement element = driver.findElement(By.id("kw"));
//
//        // 输入关键字
//        element.sendKeys("zTree");
//
//        // 提交 input 所在的  form
//        element.submit();
//        
//        // 通过判断 title 内容等待搜索页面加载完毕，间隔10秒
//        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
//            public Boolean apply(WebDriver d) {
//                return d.getTitle().toLowerCase().endsWith("ztree");
//            }
//        });
//
//        // 显示搜索结果页面的 title
//        System.out.println("2 Page title is: " + driver.getTitle());
//        
//        //关闭浏览器
//        driver.quit();
//    }
//    
//    
//    static int i = 0;
//    static void img(String url) {
//    	i++;
//        driver.get(url); 
//        try { 
//        	Thread.sleep(2000);
////        	driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);  
//            File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
//            //FileUtils.copyFile (srcFile,new File("d:\\"+i+".png")); 
//        } catch (Exception e) { 
//            e.printStackTrace(); 
//        }  
//        System.out.println("title:"+driver.getTitle());
//        //System.out.println(driver.getPageSource());
//        //driver.close(); 
//
//    }
//    
//    static WebDriver driver;
//    static void init(){
//    	File pathToFirefoxBinary = new File("E:\\Program Files\\Mozilla Firefox\\firefox.exe");    
//        FirefoxBinary firefoxbin = new FirefoxBinary(pathToFirefoxBinary);   
//        FirefoxProfile firefoxProfile=new FirefoxProfile();
//       // firefoxProfile.setPreference("general.useragent.override","Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16");
//        driver= new FirefoxDriver(firefoxbin,firefoxProfile); 
//    }
//    
//    
//   
//   public static void main(String[] args) {
//	   init();
//	   img("http://58.com/");
//	   img("http://www.taobao.com/");
//}
//}