package cn.data.test;

import java.io.File;
import java.io.FileReader;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class JsExc {

	public static Object mdP(String pt_uin, String pwd,String code){  
		System.out.println("--------------");
		System.out.println("l:"+pt_uin);
		System.out.println("c:"+pwd);
		System.out.println("rsa:"+code);

		Object t = null ;
        try {  
            ScriptEngineManager m = new ScriptEngineManager();  
            ScriptEngine se = m.getEngineByName("javascript");  
            se.eval(new FileReader(new File("D://base.js")));
            
            Invocable jsInvoke = (Invocable) se;
            QQMd5I qQMd5I = jsInvoke.getInterface(QQMd5I.class);
            t = qQMd5I.RSAKeyPair(pt_uin, pwd, code);
            return t;  
        }catch (Exception e) {  
            e.printStackTrace();  
        }   
        
        return t.toString();  
    }  
	
	public static String mdP1(Object l, String pwd){  
		System.out.println("--------------");
		System.out.println("l:"+l);
		System.out.println("c:"+pwd);

		String t = null ;
        try {  
            ScriptEngineManager m = new ScriptEngineManager();  
            ScriptEngine se = m.getEngineByName("javascript");  
            se.eval(new FileReader(new File("D://base.js")));
            
            Invocable jsInvoke = (Invocable) se;
            QQMd5I qQMd5I = jsInvoke.getInterface(QQMd5I.class);
            t = qQMd5I.encryptedString(l, pwd);
            System.out.println("加密后的值:"+t);
            return t.toString();  
        }catch (Exception e) {  
            e.printStackTrace();  
        }   
        
        return t.toString();  
    }  
	
	public static void main(String[] args){
	Object o=mdP("1001", "", "B3C61EBBA4659C4CE3639287EE871F1F48F7930EA977991C7AFE3CC442FEA49643212E7D570C853F368065CC57A2014666DA8AE7D493FD47D171C0D894EEE3ED7F99F6798B7FFD7B5873227038AD23E3197631A8CB642213B9F27D4901AB0D92BFA27542AE890855396ED92775255C977F5C302F1E7ED4B1E369C12CB6B1822F");
//	String h=mdP1(o, "WANGZENG5566");
//	
//	System.out.println(h);	
	}

}

interface QQMd5I
{
	String encryptedString(Object  l, String pwd);
	Object RSAKeyPair(String  b, String c,String a);
}
