package cn.data.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cn.datawin.spider.CCMan;
import cn.datawin.spider.httputil.HttpClientService;
import cn.datawin.spider.httputil.HttpRequest;
import cn.datawin.spider.page.Page;
import cn.datawin.spider.processor.Processor;
import cn.datawin.spider.scheduler.PageQueue;
import cn.datawin.task.Rule;
import cn.datawin.task.Task;
import cn.datawin.task.dao.DbUtil;
import cn.datawin.task.pipe.DataPipeLine;
import cn.datawin.task.pipe.PagePipeLine;

public class PageMan {

	static PageQueue pageQueue = new PageQueue();

	public static List<Task> list = new ArrayList<Task>();

	static void aa() {

		CCMan cc = new CCMan();
		
		getTask();
		addQueue();
		cc.setPageQueue(pageQueue);
		try {
			DbUtil.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		cc.thread(1, 1, 1).start();

	}

	static void getTask() {
		/**
		 * 数据库取出
		 */
		Task task = new Task();
		task.setProcessor("PageProcessor");
		task.setUrl("http://m.58.com/bj/zufang/?from=index_house_1");
		Map<String, String> header = new HashMap<String, String>();
		header.put("User-Agent", "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16");
		task.setHeader(header);

		setRule(task);

		list.add(task);
	}


	public static void setRule(Task task) {
		/**
		 * 数据库取出
		 */
		Map<String, Object> rr = new HashMap<String, Object>();
		rr.put("name", "result");
		HashMap<String, String> exp = new LinkedHashMap<String, String>();
		exp.put("selector", "a.pagenext");
		exp.put("attr", "href");
		rr.put("expression", exp);
		Rule rule = new Rule(rr);
		rule.setPageRule("pn\\d");
		rule.setPageSize(20);
		
		/*****
		Map<String, Object> rr2 = new HashMap<String, Object>();
		rr2.put("name", "result2");
		HashMap<String, String> exp2= new LinkedHashMap<String, String>();
		exp2.put("xpath", "//dl/dt/strong");
		rr2.put("expression", exp2);
		Rule rule2 = new Rule(rr2);
		
		***/
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(rule);
//		rules.add(rule2);
//		task.setRules(rules);
	}

	static Page taskToPage(Task task) {
		Page p = new Page();
		Processor processor = null;
		try {
			processor = (Processor) Thread.currentThread().getContextClassLoader()
				.loadClass("cn.datawin.task.process." + task.getProcessor()).newInstance();
			processor.setTask(task);
		} catch (Exception e) {}
		
		p.setProcessor(processor);
		if(task.getProcessor().equals("PageProcessor")){
			p.setPipeLine(new PagePipeLine());
		}else{
			p.setPipeLine(new DataPipeLine());
		}

		p.setHttpService(new HttpClientService());
		HttpRequest req = new HttpRequest(task.getUrl());
		req.setHeaders(task.getHeader());
		p.setHttpRequest(req);
		return p;
	}

	public static void addQueue() {
		for (Task task : list) {
			pageQueue.offer(taskToPage(task));
		}
		list = new ArrayList<Task>(); 
	}
	

	public static void main(String[] args) {
		aa();
	}

}
